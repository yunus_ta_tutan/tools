
package com.fit.earsiv.rapor.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for durum complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="durum">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="durumAciklama" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="durumKodu" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="paketId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TCKimlikNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vergiKimlikNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "durum", propOrder = {
    "durumAciklama",
    "durumKodu",
    "paketId",
    "tcKimlikNo",
    "vergiKimlikNo"
})
public class Durum {

    protected String durumAciklama;
    protected int durumKodu;
    @XmlElement(required = true)
    protected String paketId;
    @XmlElement(name = "TCKimlikNo")
    protected String tcKimlikNo;
    protected String vergiKimlikNo;

    /**
     * Gets the value of the durumAciklama property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDurumAciklama() {
        return durumAciklama;
    }

    /**
     * Sets the value of the durumAciklama property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDurumAciklama(String value) {
        this.durumAciklama = value;
    }

    /**
     * Gets the value of the durumKodu property.
     * 
     */
    public int getDurumKodu() {
        return durumKodu;
    }

    /**
     * Sets the value of the durumKodu property.
     * 
     */
    public void setDurumKodu(int value) {
        this.durumKodu = value;
    }

    /**
     * Gets the value of the paketId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaketId() {
        return paketId;
    }

    /**
     * Sets the value of the paketId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaketId(String value) {
        this.paketId = value;
    }

    /**
     * Gets the value of the tcKimlikNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTCKimlikNo() {
        return tcKimlikNo;
    }

    /**
     * Sets the value of the tcKimlikNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTCKimlikNo(String value) {
        this.tcKimlikNo = value;
    }

    /**
     * Gets the value of the vergiKimlikNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVergiKimlikNo() {
        return vergiKimlikNo;
    }

    /**
     * Sets the value of the vergiKimlikNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVergiKimlikNo(String value) {
        this.vergiKimlikNo = value;
    }

}
