
package com.fit.earsiv.rapor.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.fit.earsiv.rapor.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetBatchStatus_QNAME = new QName("http://earsiv.vedop3.ggm.gov.org/", "getBatchStatus");
    private final static QName _GetBatchStatusResponse_QNAME = new QName("http://earsiv.vedop3.ggm.gov.org/", "getBatchStatusResponse");
    private final static QName _SendDocumentFileResponse_QNAME = new QName("http://earsiv.vedop3.ggm.gov.org/", "sendDocumentFileResponse");
    private final static QName _SendDocumentFile_QNAME = new QName("http://earsiv.vedop3.ggm.gov.org/", "sendDocumentFile");
    private final static QName _EArsivFault_QNAME = new QName("http://earsiv.vedop3.ggm.gov.org/", "EArsivFault");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.fit.earsiv.rapor.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EArsivFault }
     * 
     */
    public EArsivFault createEArsivFault() {
        return new EArsivFault();
    }

    /**
     * Create an instance of {@link SendDocumentFile }
     * 
     */
    public SendDocumentFile createSendDocumentFile() {
        return new SendDocumentFile();
    }

    /**
     * Create an instance of {@link SendDocumentFileResponse }
     * 
     */
    public SendDocumentFileResponse createSendDocumentFileResponse() {
        return new SendDocumentFileResponse();
    }

    /**
     * Create an instance of {@link GetBatchStatusResponse }
     * 
     */
    public GetBatchStatusResponse createGetBatchStatusResponse() {
        return new GetBatchStatusResponse();
    }

    /**
     * Create an instance of {@link GetBatchStatus }
     * 
     */
    public GetBatchStatus createGetBatchStatus() {
        return new GetBatchStatus();
    }

    /**
     * Create an instance of {@link Durum }
     * 
     */
    public Durum createDurum() {
        return new Durum();
    }

    /**
     * Create an instance of {@link Attachment }
     * 
     */
    public Attachment createAttachment() {
        return new Attachment();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBatchStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://earsiv.vedop3.ggm.gov.org/", name = "getBatchStatus")
    public JAXBElement<GetBatchStatus> createGetBatchStatus(GetBatchStatus value) {
        return new JAXBElement<GetBatchStatus>(_GetBatchStatus_QNAME, GetBatchStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBatchStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://earsiv.vedop3.ggm.gov.org/", name = "getBatchStatusResponse")
    public JAXBElement<GetBatchStatusResponse> createGetBatchStatusResponse(GetBatchStatusResponse value) {
        return new JAXBElement<GetBatchStatusResponse>(_GetBatchStatusResponse_QNAME, GetBatchStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendDocumentFileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://earsiv.vedop3.ggm.gov.org/", name = "sendDocumentFileResponse")
    public JAXBElement<SendDocumentFileResponse> createSendDocumentFileResponse(SendDocumentFileResponse value) {
        return new JAXBElement<SendDocumentFileResponse>(_SendDocumentFileResponse_QNAME, SendDocumentFileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendDocumentFile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://earsiv.vedop3.ggm.gov.org/", name = "sendDocumentFile")
    public JAXBElement<SendDocumentFile> createSendDocumentFile(SendDocumentFile value) {
        return new JAXBElement<SendDocumentFile>(_SendDocumentFile_QNAME, SendDocumentFile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EArsivFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://earsiv.vedop3.ggm.gov.org/", name = "EArsivFault")
    public JAXBElement<EArsivFault> createEArsivFault(EArsivFault value) {
        return new JAXBElement<EArsivFault>(_EArsivFault_QNAME, EArsivFault.class, null, value);
    }

}
