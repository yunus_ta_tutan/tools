
package wsClients;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the wsClients package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetAppRespRequest_QNAME = new QName("http://gib.gov.tr/vedop3/eFatura", "getAppRespRequest");
    private final static QName _GetAppRespResponse_QNAME = new QName("http://gib.gov.tr/vedop3/eFatura", "getAppRespResponse");
    private final static QName _EFaturaFault_QNAME = new QName("http://gib.gov.tr/vedop3/eFatura", "EFaturaFault");
    private final static QName _DocumentRequest_QNAME = new QName("http://gib.gov.tr/vedop3/eFatura", "documentRequest");
    private final static QName _DocumentResponse_QNAME = new QName("http://gib.gov.tr/vedop3/eFatura", "documentResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: wsClients
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Base64Binary }
     * 
     */
    public Base64Binary createBase64Binary() {
        return new Base64Binary();
    }

    /**
     * Create an instance of {@link HexBinary }
     * 
     */
    public HexBinary createHexBinary() {
        return new HexBinary();
    }

    /**
     * Create an instance of {@link GetAppRespRequestType }
     * 
     */
    public GetAppRespRequestType createGetAppRespRequestType() {
        return new GetAppRespRequestType();
    }

    /**
     * Create an instance of {@link DocumentReturnType }
     * 
     */
    public DocumentReturnType createDocumentReturnType() {
        return new DocumentReturnType();
    }

    /**
     * Create an instance of {@link DocumentType }
     * 
     */
    public DocumentType createDocumentType() {
        return new DocumentType();
    }

    /**
     * Create an instance of {@link EFaturaFaultType }
     * 
     */
    public EFaturaFaultType createEFaturaFaultType() {
        return new EFaturaFaultType();
    }

    /**
     * Create an instance of {@link GetAppRespResponseType }
     * 
     */
    public GetAppRespResponseType createGetAppRespResponseType() {
        return new GetAppRespResponseType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAppRespRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gib.gov.tr/vedop3/eFatura", name = "getAppRespRequest")
    public JAXBElement<GetAppRespRequestType> createGetAppRespRequest(GetAppRespRequestType value) {
        return new JAXBElement<GetAppRespRequestType>(_GetAppRespRequest_QNAME, GetAppRespRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAppRespResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gib.gov.tr/vedop3/eFatura", name = "getAppRespResponse")
    public JAXBElement<GetAppRespResponseType> createGetAppRespResponse(GetAppRespResponseType value) {
        return new JAXBElement<GetAppRespResponseType>(_GetAppRespResponse_QNAME, GetAppRespResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EFaturaFaultType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gib.gov.tr/vedop3/eFatura", name = "EFaturaFault")
    public JAXBElement<EFaturaFaultType> createEFaturaFault(EFaturaFaultType value) {
        return new JAXBElement<EFaturaFaultType>(_EFaturaFault_QNAME, EFaturaFaultType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gib.gov.tr/vedop3/eFatura", name = "documentRequest")
    public JAXBElement<DocumentType> createDocumentRequest(DocumentType value) {
        return new JAXBElement<DocumentType>(_DocumentRequest_QNAME, DocumentType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentReturnType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gib.gov.tr/vedop3/eFatura", name = "documentResponse")
    public JAXBElement<DocumentReturnType> createDocumentResponse(DocumentReturnType value) {
        return new JAXBElement<DocumentReturnType>(_DocumentResponse_QNAME, DocumentReturnType.class, null, value);
    }

}
