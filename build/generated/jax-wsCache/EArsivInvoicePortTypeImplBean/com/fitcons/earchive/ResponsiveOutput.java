
package com.fitcons.earchive;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ResponsiveOutput complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponsiveOutput">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="outputType" type="{http://fitcons.com/earchive/invoice}ResponsiveOutputType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponsiveOutput", propOrder = {
    "outputType"
})
public class ResponsiveOutput {

    protected ResponsiveOutputType outputType;

    /**
     * Gets the value of the outputType property.
     * 
     * @return
     *     possible object is
     *     {@link ResponsiveOutputType }
     *     
     */
    public ResponsiveOutputType getOutputType() {
        return outputType;
    }

    /**
     * Sets the value of the outputType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsiveOutputType }
     *     
     */
    public void setOutputType(ResponsiveOutputType value) {
        this.outputType = value;
    }

}
