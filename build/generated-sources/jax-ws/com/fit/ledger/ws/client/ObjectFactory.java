
package com.fit.ledger.ws.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.fit.ledger.ws.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SignElectronicLedgerDocumentRequest_QNAME = new QName("http://sapcd.com/ELMT/1.0/WS/Signer", "signElectronicLedgerDocumentRequest");
    private final static QName _SignElectronicLedgerDocumentFault_QNAME = new QName("http://sapcd.com/ELMT/1.0/WS/Signer", "SignElectronicLedgerDocumentFault");
    private final static QName _SignBeratDocumentRequest_QNAME = new QName("http://sapcd.com/ELMT/1.0/WS/Signer", "signBeratDocumentRequest");
    private final static QName _SignSOAPMessageResponse_QNAME = new QName("http://sapcd.com/ELMT/1.0/WS/Signer", "signSOAPMessageResponse");
    private final static QName _SignElectronicLedgerDocumentHeader_QNAME = new QName("http://sapcd.com/ELMT/1.0/WS/Signer", "signElectronicLedgerDocumentHeader");
    private final static QName _SignSOAPMessageHeader_QNAME = new QName("http://sapcd.com/ELMT/1.0/WS/Signer", "signSOAPMessageHeader");
    private final static QName _SignBeratDocumentHeader_QNAME = new QName("http://sapcd.com/ELMT/1.0/WS/Signer", "signBeratDocumentHeader");
    private final static QName _SignBeratDocumentFault_QNAME = new QName("http://sapcd.com/ELMT/1.0/WS/Signer", "signBeratDocumentFault");
    private final static QName _SignSOAPMessageFault_QNAME = new QName("http://sapcd.com/ELMT/1.0/WS/Signer", "signSOAPMessageFault");
    private final static QName _SignElectronicLedgerDocumentResponse_QNAME = new QName("http://sapcd.com/ELMT/1.0/WS/Signer", "signElectronicLedgerDocumentResponse");
    private final static QName _SignBeratDocumentResponse_QNAME = new QName("http://sapcd.com/ELMT/1.0/WS/Signer", "signBeratDocumentResponse");
    private final static QName _SignSOAPMessageRequest_QNAME = new QName("http://sapcd.com/ELMT/1.0/WS/Signer", "signSOAPMessageRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.fit.ledger.ws.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link System }
     * 
     */
    public System createSystem() {
        return new System();
    }

    /**
     * Create an instance of {@link Document }
     * 
     */
    public Document createDocument() {
        return new Document();
    }

    /**
     * Create an instance of {@link SignedDocument }
     * 
     */
    public SignedDocument createSignedDocument() {
        return new SignedDocument();
    }

    /**
     * Create an instance of {@link SignDocumentFault }
     * 
     */
    public SignDocumentFault createSignDocumentFault() {
        return new SignDocumentFault();
    }

    /**
     * Create an instance of {@link SignDocumentResponse }
     * 
     */
    public SignDocumentResponse createSignDocumentResponse() {
        return new SignDocumentResponse();
    }

    /**
     * Create an instance of {@link SignDocumentRequest }
     * 
     */
    public SignDocumentRequest createSignDocumentRequest() {
        return new SignDocumentRequest();
    }

    /**
     * Create an instance of {@link SignDocumentHeaders }
     * 
     */
    public SignDocumentHeaders createSignDocumentHeaders() {
        return new SignDocumentHeaders();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link System.Ip }
     * 
     */
    public System.Ip createSystemIp() {
        return new System.Ip();
    }

    /**
     * Create an instance of {@link Document.Location }
     * 
     */
    public Document.Location createDocumentLocation() {
        return new Document.Location();
    }

    /**
     * Create an instance of {@link Document.Data }
     * 
     */
    public Document.Data createDocumentData() {
        return new Document.Data();
    }

    /**
     * Create an instance of {@link SignedDocument.Location }
     * 
     */
    public SignedDocument.Location createSignedDocumentLocation() {
        return new SignedDocument.Location();
    }

    /**
     * Create an instance of {@link SignedDocument.Data }
     * 
     */
    public SignedDocument.Data createSignedDocumentData() {
        return new SignedDocument.Data();
    }

    /**
     * Create an instance of {@link SignDocumentFault.Log }
     * 
     */
    public SignDocumentFault.Log createSignDocumentFaultLog() {
        return new SignDocumentFault.Log();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SignDocumentRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sapcd.com/ELMT/1.0/WS/Signer", name = "signElectronicLedgerDocumentRequest")
    public JAXBElement<SignDocumentRequest> createSignElectronicLedgerDocumentRequest(SignDocumentRequest value) {
        return new JAXBElement<SignDocumentRequest>(_SignElectronicLedgerDocumentRequest_QNAME, SignDocumentRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SignDocumentFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sapcd.com/ELMT/1.0/WS/Signer", name = "SignElectronicLedgerDocumentFault")
    public JAXBElement<SignDocumentFault> createSignElectronicLedgerDocumentFault(SignDocumentFault value) {
        return new JAXBElement<SignDocumentFault>(_SignElectronicLedgerDocumentFault_QNAME, SignDocumentFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SignDocumentRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sapcd.com/ELMT/1.0/WS/Signer", name = "signBeratDocumentRequest")
    public JAXBElement<SignDocumentRequest> createSignBeratDocumentRequest(SignDocumentRequest value) {
        return new JAXBElement<SignDocumentRequest>(_SignBeratDocumentRequest_QNAME, SignDocumentRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SignDocumentResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sapcd.com/ELMT/1.0/WS/Signer", name = "signSOAPMessageResponse")
    public JAXBElement<SignDocumentResponse> createSignSOAPMessageResponse(SignDocumentResponse value) {
        return new JAXBElement<SignDocumentResponse>(_SignSOAPMessageResponse_QNAME, SignDocumentResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SignDocumentHeaders }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sapcd.com/ELMT/1.0/WS/Signer", name = "signElectronicLedgerDocumentHeader")
    public JAXBElement<SignDocumentHeaders> createSignElectronicLedgerDocumentHeader(SignDocumentHeaders value) {
        return new JAXBElement<SignDocumentHeaders>(_SignElectronicLedgerDocumentHeader_QNAME, SignDocumentHeaders.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SignDocumentHeaders }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sapcd.com/ELMT/1.0/WS/Signer", name = "signSOAPMessageHeader")
    public JAXBElement<SignDocumentHeaders> createSignSOAPMessageHeader(SignDocumentHeaders value) {
        return new JAXBElement<SignDocumentHeaders>(_SignSOAPMessageHeader_QNAME, SignDocumentHeaders.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SignDocumentHeaders }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sapcd.com/ELMT/1.0/WS/Signer", name = "signBeratDocumentHeader")
    public JAXBElement<SignDocumentHeaders> createSignBeratDocumentHeader(SignDocumentHeaders value) {
        return new JAXBElement<SignDocumentHeaders>(_SignBeratDocumentHeader_QNAME, SignDocumentHeaders.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SignDocumentFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sapcd.com/ELMT/1.0/WS/Signer", name = "signBeratDocumentFault")
    public JAXBElement<SignDocumentFault> createSignBeratDocumentFault(SignDocumentFault value) {
        return new JAXBElement<SignDocumentFault>(_SignBeratDocumentFault_QNAME, SignDocumentFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SignDocumentFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sapcd.com/ELMT/1.0/WS/Signer", name = "signSOAPMessageFault")
    public JAXBElement<SignDocumentFault> createSignSOAPMessageFault(SignDocumentFault value) {
        return new JAXBElement<SignDocumentFault>(_SignSOAPMessageFault_QNAME, SignDocumentFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SignDocumentResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sapcd.com/ELMT/1.0/WS/Signer", name = "signElectronicLedgerDocumentResponse")
    public JAXBElement<SignDocumentResponse> createSignElectronicLedgerDocumentResponse(SignDocumentResponse value) {
        return new JAXBElement<SignDocumentResponse>(_SignElectronicLedgerDocumentResponse_QNAME, SignDocumentResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SignDocumentResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sapcd.com/ELMT/1.0/WS/Signer", name = "signBeratDocumentResponse")
    public JAXBElement<SignDocumentResponse> createSignBeratDocumentResponse(SignDocumentResponse value) {
        return new JAXBElement<SignDocumentResponse>(_SignBeratDocumentResponse_QNAME, SignDocumentResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SignDocumentRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sapcd.com/ELMT/1.0/WS/Signer", name = "signSOAPMessageRequest")
    public JAXBElement<SignDocumentRequest> createSignSOAPMessageRequest(SignDocumentRequest value) {
        return new JAXBElement<SignDocumentRequest>(_SignSOAPMessageRequest_QNAME, SignDocumentRequest.class, null, value);
    }

}
