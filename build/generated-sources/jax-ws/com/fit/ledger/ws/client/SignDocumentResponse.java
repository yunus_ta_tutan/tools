
package com.fit.ledger.ws.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SignDocumentResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SignDocumentResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="signedDocument" type="{http://sapcd.com/ELMT/1.0/WS/Signer}SignedDocument"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SignDocumentResponse", propOrder = {
    "signedDocument"
})
public class SignDocumentResponse {

    @XmlElement(required = true)
    protected SignedDocument signedDocument;

    /**
     * Gets the value of the signedDocument property.
     * 
     * @return
     *     possible object is
     *     {@link SignedDocument }
     *     
     */
    public SignedDocument getSignedDocument() {
        return signedDocument;
    }

    /**
     * Sets the value of the signedDocument property.
     * 
     * @param value
     *     allowed object is
     *     {@link SignedDocument }
     *     
     */
    public void setSignedDocument(SignedDocument value) {
        this.signedDocument = value;
    }

}
