
package com.fit.gib.getuserlist;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetPKUserListResult" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPKUserListResult"
})
@XmlRootElement(name = "GetPKUserListResponse")
public class GetPKUserListResponse {

    @XmlElementRef(name = "GetPKUserListResult", namespace = "http://fitcons.com/eInvoice", type = JAXBElement.class, required = false)
    protected JAXBElement<byte[]> getPKUserListResult;

    /**
     * Gets the value of the getPKUserListResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public JAXBElement<byte[]> getGetPKUserListResult() {
        return getPKUserListResult;
    }

    /**
     * Sets the value of the getPKUserListResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public void setGetPKUserListResult(JAXBElement<byte[]> value) {
        this.getPKUserListResult = value;
    }

}
