
package com.fitcons.earchive;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SendInvoiceRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SendInvoiceRequestType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://fitcons.com/earchive/base}BaseArchiveRequest">
 *       &lt;sequence>
 *         &lt;element name="responsiveOutput" type="{http://fitcons.com/earchive/invoice}ResponsiveOutput" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SendInvoiceRequestType", propOrder = {
    "responsiveOutput"
})
public class SendInvoiceRequestType
    extends BaseArchiveRequest
{

    protected ResponsiveOutput responsiveOutput;

    /**
     * Gets the value of the responsiveOutput property.
     * 
     * @return
     *     possible object is
     *     {@link ResponsiveOutput }
     *     
     */
    public ResponsiveOutput getResponsiveOutput() {
        return responsiveOutput;
    }

    /**
     * Sets the value of the responsiveOutput property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsiveOutput }
     *     
     */
    public void setResponsiveOutput(ResponsiveOutput value) {
        this.responsiveOutput = value;
    }

}
