
package com.fitcons.earchive;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ResponsiveOutputType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ResponsiveOutputType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="XML"/>
 *     &lt;enumeration value="HTML"/>
 *     &lt;enumeration value="PDF"/>
 *     &lt;enumeration value="NONE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ResponsiveOutputType")
@XmlEnum
public enum ResponsiveOutputType {

    XML,
    HTML,
    PDF,
    NONE;

    public String value() {
        return name();
    }

    public static ResponsiveOutputType fromValue(String v) {
        return valueOf(v);
    }

}
