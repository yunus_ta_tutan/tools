
package com.fitcons.earchive;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.fitcons.earchive package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SendInvoiceRequestType_QNAME = new QName("http://fitcons.com/earchive/invoice", "sendInvoiceRequestType");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.fitcons.earchive
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SendInvoiceResponseType }
     * 
     */
    public SendInvoiceResponseType createSendInvoiceResponseType() {
        return new SendInvoiceResponseType();
    }

    /**
     * Create an instance of {@link ProcessingFaultType }
     * 
     */
    public ProcessingFaultType createProcessingFaultType() {
        return new ProcessingFaultType();
    }

    /**
     * Create an instance of {@link BaseArchiveRequest }
     * 
     */
    public BaseArchiveRequest createBaseArchiveRequest() {
        return new BaseArchiveRequest();
    }

    /**
     * Create an instance of {@link CustomizationParam }
     * 
     */
    public CustomizationParam createCustomizationParam() {
        return new CustomizationParam();
    }

    /**
     * Create an instance of {@link Result }
     * 
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link GetSignedInvoiceRequestType }
     * 
     */
    public GetSignedInvoiceRequestType createGetSignedInvoiceRequestType() {
        return new GetSignedInvoiceRequestType();
    }

    /**
     * Create an instance of {@link GetInvoiceDocumentRequestType }
     * 
     */
    public GetInvoiceDocumentRequestType createGetInvoiceDocumentRequestType() {
        return new GetInvoiceDocumentRequestType();
    }

    /**
     * Create an instance of {@link GetStatusRequestType }
     * 
     */
    public GetStatusRequestType createGetStatusRequestType() {
        return new GetStatusRequestType();
    }

    /**
     * Create an instance of {@link SendInvoiceResponseType.PreCheckErrorResults }
     * 
     */
    public SendInvoiceResponseType.PreCheckErrorResults createSendInvoiceResponseTypePreCheckErrorResults() {
        return new SendInvoiceResponseType.PreCheckErrorResults();
    }

    /**
     * Create an instance of {@link SendInvoiceResponseType.PreCheckSuccessResults }
     * 
     */
    public SendInvoiceResponseType.PreCheckSuccessResults createSendInvoiceResponseTypePreCheckSuccessResults() {
        return new SendInvoiceResponseType.PreCheckSuccessResults();
    }

    /**
     * Create an instance of {@link GetSignedInvoiceResponseType }
     * 
     */
    public GetSignedInvoiceResponseType createGetSignedInvoiceResponseType() {
        return new GetSignedInvoiceResponseType();
    }

    /**
     * Create an instance of {@link GetInvoiceDocumentResponseType }
     * 
     */
    public GetInvoiceDocumentResponseType createGetInvoiceDocumentResponseType() {
        return new GetInvoiceDocumentResponseType();
    }

    /**
     * Create an instance of {@link SendInvoiceRequestType }
     * 
     */
    public SendInvoiceRequestType createSendInvoiceRequestType() {
        return new SendInvoiceRequestType();
    }

    /**
     * Create an instance of {@link GetStatusResponseType }
     * 
     */
    public GetStatusResponseType createGetStatusResponseType() {
        return new GetStatusResponseType();
    }

    /**
     * Create an instance of {@link PreCheckError }
     * 
     */
    public PreCheckError createPreCheckError() {
        return new PreCheckError();
    }

    /**
     * Create an instance of {@link PreCheckSuccess }
     * 
     */
    public PreCheckSuccess createPreCheckSuccess() {
        return new PreCheckSuccess();
    }

    /**
     * Create an instance of {@link ResponsiveOutput }
     * 
     */
    public ResponsiveOutput createResponsiveOutput() {
        return new ResponsiveOutput();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendInvoiceRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://fitcons.com/earchive/invoice", name = "sendInvoiceRequestType")
    public JAXBElement<SendInvoiceRequestType> createSendInvoiceRequestType(SendInvoiceRequestType value) {
        return new JAXBElement<SendInvoiceRequestType>(_SendInvoiceRequestType_QNAME, SendInvoiceRequestType.class, null, value);
    }

}
