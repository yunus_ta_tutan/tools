
package com.fitcons.earchive;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Detail" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Result" type="{http://fitcons.com/earchive/base}Result"/>
 *         &lt;element name="preCheckErrorResults" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="preCheckError" type="{http://fitcons.com/earchive/invoice}PreCheckError" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="preCheckSuccessResults" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="preCheckSuccess" type="{http://fitcons.com/earchive/invoice}PreCheckSuccess" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "detail",
    "result",
    "preCheckErrorResults",
    "preCheckSuccessResults"
})
@XmlRootElement(name = "sendInvoiceResponseType")
public class SendInvoiceResponseType {

    @XmlElement(name = "Detail", required = true)
    protected String detail;
    @XmlElement(name = "Result", required = true)
    protected Result result;
    protected SendInvoiceResponseType.PreCheckErrorResults preCheckErrorResults;
    protected SendInvoiceResponseType.PreCheckSuccessResults preCheckSuccessResults;

    /**
     * Gets the value of the detail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetail() {
        return detail;
    }

    /**
     * Sets the value of the detail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetail(String value) {
        this.detail = value;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link Result }
     *     
     */
    public Result getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link Result }
     *     
     */
    public void setResult(Result value) {
        this.result = value;
    }

    /**
     * Gets the value of the preCheckErrorResults property.
     * 
     * @return
     *     possible object is
     *     {@link SendInvoiceResponseType.PreCheckErrorResults }
     *     
     */
    public SendInvoiceResponseType.PreCheckErrorResults getPreCheckErrorResults() {
        return preCheckErrorResults;
    }

    /**
     * Sets the value of the preCheckErrorResults property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendInvoiceResponseType.PreCheckErrorResults }
     *     
     */
    public void setPreCheckErrorResults(SendInvoiceResponseType.PreCheckErrorResults value) {
        this.preCheckErrorResults = value;
    }

    /**
     * Gets the value of the preCheckSuccessResults property.
     * 
     * @return
     *     possible object is
     *     {@link SendInvoiceResponseType.PreCheckSuccessResults }
     *     
     */
    public SendInvoiceResponseType.PreCheckSuccessResults getPreCheckSuccessResults() {
        return preCheckSuccessResults;
    }

    /**
     * Sets the value of the preCheckSuccessResults property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendInvoiceResponseType.PreCheckSuccessResults }
     *     
     */
    public void setPreCheckSuccessResults(SendInvoiceResponseType.PreCheckSuccessResults value) {
        this.preCheckSuccessResults = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="preCheckError" type="{http://fitcons.com/earchive/invoice}PreCheckError" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "preCheckError"
    })
    public static class PreCheckErrorResults {

        @XmlElement(required = true)
        protected List<PreCheckError> preCheckError;

        /**
         * Gets the value of the preCheckError property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the preCheckError property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPreCheckError().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link PreCheckError }
         * 
         * 
         */
        public List<PreCheckError> getPreCheckError() {
            if (preCheckError == null) {
                preCheckError = new ArrayList<PreCheckError>();
            }
            return this.preCheckError;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="preCheckSuccess" type="{http://fitcons.com/earchive/invoice}PreCheckSuccess" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "preCheckSuccess"
    })
    public static class PreCheckSuccessResults {

        @XmlElement(required = true)
        protected List<PreCheckSuccess> preCheckSuccess;

        /**
         * Gets the value of the preCheckSuccess property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the preCheckSuccess property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPreCheckSuccess().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link PreCheckSuccess }
         * 
         * 
         */
        public List<PreCheckSuccess> getPreCheckSuccess() {
            if (preCheckSuccess == null) {
                preCheckSuccess = new ArrayList<PreCheckSuccess>();
            }
            return this.preCheckSuccess;
        }

    }

}
