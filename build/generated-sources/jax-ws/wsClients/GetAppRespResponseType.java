
package wsClients;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getAppRespResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getAppRespResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="applicationResponse" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAppRespResponseType", propOrder = {
    "applicationResponse"
})
public class GetAppRespResponseType {

    protected String applicationResponse;

    /**
     * Gets the value of the applicationResponse property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationResponse() {
        return applicationResponse;
    }

    /**
     * Sets the value of the applicationResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationResponse(String value) {
        this.applicationResponse = value;
    }

}
