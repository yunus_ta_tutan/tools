/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package xades4j.production;

import xades4j.providers.KeyingDataProvider;
import xades4j.providers.ValidationDataProvider;

/**
 *
 * @author xuxux
 */
public final class XadesArchiveSigningProfile extends XadesTSigningProfile{
    
    public XadesArchiveSigningProfile(
            KeyingDataProvider keyingProvider,
            ValidationDataProvider validationDataProv)
    {
        super(keyingProvider);
        withBinding(ValidationDataProvider.class, validationDataProv);
    }

    public XadesArchiveSigningProfile(
            KeyingDataProvider keyingProvider,
            Class<? extends ValidationDataProvider> validationDataProvClass)
    {
        super(keyingProvider);
        withBinding(ValidationDataProvider.class, validationDataProvClass);
    }

    public XadesArchiveSigningProfile(
            Class<? extends KeyingDataProvider> keyingProviderClass,
            ValidationDataProvider validationDataProv)
    {
        super(keyingProviderClass);
        withBinding(ValidationDataProvider.class, validationDataProv);
    }

    public XadesArchiveSigningProfile(
            Class<? extends KeyingDataProvider> keyingProviderClass,
            Class<? extends ValidationDataProvider> validationDataProvClass)
    {
        super(keyingProviderClass);
        withBinding(ValidationDataProvider.class, validationDataProvClass);
    }

    @Override
    protected Class<? extends XadesSigner> getSignerClass()
    {
        return SignerA.class;
    }
    
}
