package xades4j.providers.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.MessageDigest;

import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1OutputStream;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DEROutputStream;
import org.bouncycastle.asn1.cmp.PKIStatus;
import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.tsp.TimeStampResponse;
import org.bouncycastle.tsp.TimeStampToken;

import tr.gov.tubitak.uekae.esya.api.asn.pkixtsp.ETimeStampResponse;
import tr.gov.tubitak.uekae.esya.api.infra.tsclient.TSClient;
import tr.gov.tubitak.uekae.esya.api.infra.tsclient.TSSettings;
import xades4j.UnsupportedAlgorithmException;
import xades4j.providers.MessageDigestEngineProvider;
import xades4j.providers.TimeStampTokenGenerationException;
import xades4j.providers.TimeStampTokenProvider;

//import com.fit.einvoice.signer.core.SignHelper;
//import com.fit.einvoice.signer.utils.LogUtils;
import com.google.inject.Inject;

public class AuthenticatedTimeStampTokenProvider implements TimeStampTokenProvider {

	private static final String LIVE_URL = "http://zd.kamusm.gov.tr/";
	private static final String TEST_URL = "http://tzd.kamusm.gov.tr/";
	private final MessageDigestEngineProvider messageDigestProvider;
	private String tsaUrl;
	private int tsaId = 3942;
	private String tsaPass = "P9gK!a4?5V";

	@Inject
	public AuthenticatedTimeStampTokenProvider(MessageDigestEngineProvider messageDigestProvider) {
		this(messageDigestProvider, "http://tzd.kamusm.gov.tr/", 3942, "P9gK!a4?5V");
//		LogUtils.addInfo("URL, used for timestamp is:" + (SignHelper.SIGNING_MODE.equalsIgnoreCase(SignHelper.SIGNING_MODE_PKCS11) ? LIVE_URL : TEST_URL));
		// this(messageDigestProvider, "http://zd.kamusm.gov.tr/", 3942,
		// "P9gK!a4?5V");

	}

	public AuthenticatedTimeStampTokenProvider(MessageDigestEngineProvider messageDigestProvider, String tsaUrl, int tsaId, String tsaPass) {
//		System.out.println("XUXUX>>>OP is berat:" + SignHelper.IS_BERAT_SIGNING_OP + " and will use--> tmstmp_user:" + Integer.parseInt(SignHelper.TIMESTAMP_USER) + " pass:" + SignHelper.TIMESTAMP_PASS);
		this.messageDigestProvider = messageDigestProvider;
		this.tsaUrl = tsaUrl;
		this.tsaId = tsaId;
		this.tsaPass = tsaPass;
	}

	@Override
	public final TimeStampTokenRes getTimeStampToken(byte[] tsDigestInput, String digestAlgUri) throws TimeStampTokenGenerationException {
		// timestamp
		try {
			System.err.println("DefaultTimeStampTokenProvider getTimeStampToken() : 1");
			MessageDigest md = messageDigestProvider.getEngine(digestAlgUri);
			byte[] digest = md.digest(tsDigestInput);

			
			System.err.println("xuxux>>>timestamp server:" + tsaUrl);
			TSSettings tssettings = new TSSettings(tsaUrl, tsaId, tsaPass);
			TSClient tsc = new TSClient();
			tsc.setDefaultSettings(tssettings);

			System.err.println("DefaultTimeStampTokenProvider getTimeStampToken() : 2");
			ETimeStampResponse ersp = tsc.timestamp(digest, tssettings);
			byte[] zd = ersp.getEncoded();

			/*
			 * 
			 */

			ASN1InputStream aIn = new ASN1InputStream(zd);
			DERObject obj = aIn.readObject();
			ASN1Sequence asn1Sequence = (ASN1Sequence) obj;
			ByteArrayOutputStream bOut = new ByteArrayOutputStream();
			DEROutputStream dOut = new DEROutputStream(bOut);
			dOut.write(obj.getDEREncoded());

			zd = bOut.toByteArray();
			aIn.close();
			dOut.close();
			/*
			 * 
			 */

			TimeStampResponse tsResponse = new TimeStampResponse(zd);

			System.err.println("DefaultTimeStampTokenProvider getTimeStampToken() : 3");
			if (tsResponse.getStatus() != PKIStatus.GRANTED && tsResponse.getStatus() != PKIStatus.GRANTED_WITH_MODS) {
				throw new TimeStampTokenGenerationException("Time stamp token not granted. " + tsResponse.getStatusString());
			}
			System.err.println("DefaultTimeStampTokenProvider getTimeStampToken() : 4" + tsResponse.getStatusString());
			TimeStampToken tsToken = tsResponse.getTimeStampToken();
			return new TimeStampTokenRes(toASN1(asn1Sequence.getObjectAt(1).getDERObject().getDEREncoded()), tsToken.getTimeStampInfo().getGenTime());
		} catch (UnsupportedAlgorithmException ex) {
			throw new TimeStampTokenGenerationException("Digest algorithm not supported", ex);
		} catch (TSPException ex) {
			throw new TimeStampTokenGenerationException("Invalid time stamp response", ex);
		} catch (IOException ex) {
			throw new TimeStampTokenGenerationException("Encoding error", ex);
		} catch (Exception ex) {
			throw new TimeStampTokenGenerationException("Error", ex);
		}
	}

	public static byte[] toASN1(byte[] data) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream(400);
			ASN1OutputStream encoder = new ASN1OutputStream(baos);

			encoder.write(data);
			encoder.close();
			return baos.toByteArray();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
