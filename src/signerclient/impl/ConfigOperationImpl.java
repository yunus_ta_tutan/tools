/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.impl;

import com.xuxux.utils.commons.MyXmlUtil;
import com.xuxux.utils.files.MyFileUtil;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import signerclient.factory.ImplFactory;
import signerclient.models.CERTVKNMAPPINGROOT;
import signerclient.models.TokenConfParams;
import signerclient.services.ConfigOperationService;
import signerclient.services.FileOperationsService;
import signerclient.utils.SerializingUtil;
import signerclient.utils.StringUtil;


/**
 *
 * @author xuxux
 */
public class ConfigOperationImpl implements ConfigOperationService {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ConfigOperationImpl.class);
    FileOperationsService fileOperationsService = ImplFactory.fileOperationsInstance();

    static final String CONFIG_PATH = "config/";
    static final String TOKEN_CONFIG_FILE = "token-conf.xml";
    static final String CERT_VKN_MAPPING_FILE = "cert_vkn_mapper.xml";

  
    @Override
    public void saveConfiguration(Map<String, String> configParams, String earcSignerPath,String customPath) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            String selectedDir = earcSignerPath;
            File confDir = new File(selectedDir + "\\" + CONFIG_PATH + customPath + "/");
            File confFile = new File(selectedDir + "\\" + CONFIG_PATH + customPath + "/" + TOKEN_CONFIG_FILE);
            Document context = null;
            if (confFile == null || !confFile.exists()) {
                confDir.mkdirs();
                context = MyXmlUtil.parse(new ByteArrayInputStream(fileOperationsService.getDefaultFileAsByteArray(CONFIG_PATH + TOKEN_CONFIG_FILE)));
            } else {
                context = MyXmlUtil.parse(new FileInputStream(selectedDir + "\\" + CONFIG_PATH + customPath + "/" + TOKEN_CONFIG_FILE));
            }
            context.getElementsByTagName(TokenConfParams.SLOT_LIST_INDEX.toString()).item(0).setTextContent(configParams.get(TokenConfParams.SLOT_LIST_INDEX.toString()));
            context.getElementsByTagName(TokenConfParams.CARD_TYPE.toString()).item(0).setTextContent(configParams.get(TokenConfParams.CARD_TYPE.toString()));
            context.getElementsByTagName(TokenConfParams.PROJECT.toString()).item(0).setTextContent(configParams.get(TokenConfParams.PROJECT.toString()));
            context.getElementsByTagName(TokenConfParams.USE_PKCS11.toString()).item(0).setTextContent(configParams.get(TokenConfParams.USE_PKCS11.toString()));
            context.getElementsByTagName(TokenConfParams.ALIAS.toString()).item(0).setTextContent(configParams.get(TokenConfParams.ALIAS.toString()));
            context.getElementsByTagName(TokenConfParams.ARCMA3_DIR.toString()).item(0).setTextContent(configParams.get(TokenConfParams.ARCMA3_DIR.toString()));
            context.getElementsByTagName(TokenConfParams.PFX_PASS.toString()).item(0).setTextContent(configParams.get(TokenConfParams.PFX_PASS.toString()));
            context.getElementsByTagName(TokenConfParams.PKCS11_PASS.toString()).item(0).setTextContent(configParams.get(TokenConfParams.PKCS11_PASS.toString()));
            context.getElementsByTagName(TokenConfParams.PROVIDER_PATH.toString()).item(0).setTextContent(configParams.get(TokenConfParams.PROVIDER_PATH.toString()));
            MyXmlUtil.transform(context, baos);
            baos.flush();
            MyFileUtil.writeByteArrayToFile(baos.toByteArray(), confFile.getPath());
        } catch (Exception ex) {
            LOGGER.error(StringUtil.traceException(ex));
            throw ex;
        }
    }

    @Override
    public Map<String, String> getSavedConfiguration() throws Exception {
        Map<String, String> confMap = new HashMap<String, String>(11);
        try {
            File confFile = new File(fileOperationsService.getClassPath(this.getClass()) + "\\" + CONFIG_PATH + TOKEN_CONFIG_FILE);
            Document context = null;
            if (confFile != null && confFile.exists()) {
                context = MyXmlUtil.parse(new FileInputStream(confFile));
            } else {
                context = MyXmlUtil.parse(new ByteArrayInputStream(fileOperationsService.getDefaultFileAsByteArray(CONFIG_PATH + TOKEN_CONFIG_FILE)));
            }
            confMap.put(TokenConfParams.SLOT_LIST_INDEX.toString(), context.getElementsByTagName(TokenConfParams.SLOT_LIST_INDEX.toString()).item(0).getTextContent());
            confMap.put(TokenConfParams.CARD_TYPE.toString(), context.getElementsByTagName(TokenConfParams.CARD_TYPE.toString()).item(0).getTextContent());
            confMap.put(TokenConfParams.PROJECT.toString(), context.getElementsByTagName(TokenConfParams.PROJECT.toString()).item(0).getTextContent());
            confMap.put(TokenConfParams.USE_PKCS11.toString(), context.getElementsByTagName(TokenConfParams.USE_PKCS11.toString()).item(0).getTextContent());
            confMap.put(TokenConfParams.ALIAS.toString(), context.getElementsByTagName(TokenConfParams.ALIAS.toString()).item(0).getTextContent());
            confMap.put(TokenConfParams.ARCMA3_DIR.toString(), context.getElementsByTagName(TokenConfParams.ARCMA3_DIR.toString()).item(0).getTextContent());
            confMap.put(TokenConfParams.PFX_PASS.toString(), context.getElementsByTagName(TokenConfParams.PFX_PASS.toString()).item(0).getTextContent());
            confMap.put(TokenConfParams.PKCS11_PASS.toString(), context.getElementsByTagName(TokenConfParams.PKCS11_PASS.toString()).item(0).getTextContent());
            confMap.put(TokenConfParams.PROVIDER_PATH.toString(), context.getElementsByTagName(TokenConfParams.PROVIDER_PATH.toString()).item(0).getTextContent());
            return confMap;
        } catch (Exception ex) {
            LOGGER.error(StringUtil.traceException(ex));
            throw ex;
        }
    }

    @Override
    public void saveCertVknMapping(CERTVKNMAPPINGROOT.CUSTOMER[] customers, String absoluteCustomPath) throws Exception {
        try {
            CERTVKNMAPPINGROOT certvknmappingroot = new CERTVKNMAPPINGROOT();
            String workingDir = fileOperationsService.getClassPath(this.getClass());
            File mappingDir = null;
            File mappingFile = null;
            if (null == absoluteCustomPath) {
                mappingDir = new File(workingDir + "\\" + CONFIG_PATH);
                mappingFile = new File(workingDir + "\\" + CONFIG_PATH + CERT_VKN_MAPPING_FILE);
            } else {
                mappingDir = new File(absoluteCustomPath + "\\" + CONFIG_PATH + "/");
                mappingFile = new File(absoluteCustomPath + "\\" + CONFIG_PATH + "/" + CERT_VKN_MAPPING_FILE);
            }
            if (mappingFile == null || !mappingFile.exists()) {
                mappingDir.mkdirs();
            }

            for (int i = 0; i < customers.length; i++) {
                certvknmappingroot.getCUSTOMER().add(customers[i]);
            }
            SerializingUtil.marshal(certvknmappingroot, mappingFile);
        } catch (Exception e) {
            LOGGER.error(StringUtil.traceException(e));
            throw e;
        }
    }

    @Override
    public Map<String, String> getSavedCertVknMapping(String customPath) throws Exception {
        try {
            Map<String,String> mapping = new HashMap<String, String>(1);
            String workingDir = fileOperationsService.getClassPath(this.getClass());
            File mappingDir = null;
            File mappingFile = null;
            if (null == customPath) {
                mappingDir = new File(workingDir + "\\" + CONFIG_PATH);
                mappingFile = new File(workingDir + "\\" + CONFIG_PATH + CERT_VKN_MAPPING_FILE);
            } else {
                mappingDir = new File(workingDir + "\\" + CONFIG_PATH + customPath + "/");
                mappingFile = new File(workingDir + "\\" + CONFIG_PATH + customPath + "/" + CERT_VKN_MAPPING_FILE);
            }
            if (mappingFile == null || !mappingFile.exists()) {
                mappingDir.mkdirs();
            } else {
                CERTVKNMAPPINGROOT certvknmappingroot = SerializingUtil.unmarshal(CERTVKNMAPPINGROOT.class, mappingFile);
                for (CERTVKNMAPPINGROOT.CUSTOMER customer : certvknmappingroot.getCUSTOMER()) {
                       mapping.put(customer.getVKN(), customer.getSERIAL());
                }
            }
            return mapping;

        } catch (Exception e) {
            LOGGER.error(StringUtil.traceException(e));
            throw e;
        }

    }

}
