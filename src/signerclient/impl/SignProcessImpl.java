/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.impl;

import com.xuxux.utils.commons.MyXmlUtil;
import com.xuxux.utils.files.MyFileUtil;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.JTextPane;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerException;
import org.apache.commons.codec.binary.Base64;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import signerclient.helpers.xadesHelpers.XadesSignOperation;
import signerclient.services.SignProcessService;
import signerclient.tokenService.XuxuxDeviceProvider;
import signerclient.tokenService.XuxuxKeyingDataProviderImpl;
import signerclient.tokenService.XuxuxPfxKeyingDataProviderImpl;
import signerclient.utils.LogUtil;
import signerclient.utils.SOAPUtil;
import signerclient.ws.SignerResult;
import signerclient.wsclient.Invoker;

import tr.gov.tubitak.uekae.esya.api.asn.x509.ECertificate;
import tr.gov.tubitak.uekae.esya.api.crypto.util.KeyUtil;
import tr.gov.tubitak.uekae.esya.api.signature.SignatureType;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.Context;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.DigestMethod;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.SignatureMethod;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.TransformType;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.XMLSignature;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.XMLSignatureException;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.model.Transform;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.model.Transforms;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.model.keyinfo.KeyValue;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.model.xades.ClaimedRole;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.model.xades.SignerRole;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.util.XmlUtil;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.UUID;

import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import signerclient.utils.StringUtil;
import tr.gov.tubitak.uekae.esya.api.smartcard.example.XuxuxHsmManager;
import tr.gov.tubitak.uekae.esya.api.xades.example.utils.SampleBase;

/**
 *
 * @author xuxux
 */
public class SignProcessImpl extends SampleBase implements SignProcessService {

    public static final Object lock = new Object();
    public static JTextPane logPane = null;

    private static Logger LOGGER = LoggerFactory.getLogger(SignProcessImpl.class);

    @Override
    public Document getDefaultDocument() throws Exception {
        try {
            ClassLoader cl = this.getClass().getClassLoader();
            InputStream in = cl.getResourceAsStream("METAINF/fitunsignedenv.xml");
            Document document = MyXmlUtil.parse(in);
            return document;
        } catch (ParserConfigurationException pe) {
            throw new ParserConfigurationException(pe.toString());
        } catch (SAXException se) {
            throw new SAXException(se);
        } catch (IOException ioe) {
            throw new IOException(ioe.toString());
        } catch (Exception e) {
            throw new Exception(e.toString());
        }

    }

    @Override
    public SignerResult invokeSignMethod(String dataToBeSigned, String endPoint) throws Exception {
        try {
            return Invoker.sign(dataToBeSigned, endPoint);
        } catch (Exception e) {
            throw new Exception(e.toString());
        }
    }

    @Override
    public String changeSenderVkn(Document doc, String vkn) throws Exception {
        try {
            Element root = doc.getDocumentElement();
            Element sender = (Element) root.getElementsByTagName("sh:Sender").item(0);
            NodeList contactInfList = sender.getElementsByTagName("sh:ContactInformation");
            for (int i = 0; i < contactInfList.getLength(); i++) {
                Element contactInf = (Element) contactInfList.item(i);
                Node identifier = contactInf.getElementsByTagName("sh:ContactTypeIdentifier").item(0);
                if ("VKN_TCKN".equalsIgnoreCase(identifier.getTextContent())) {
                    Node contact = contactInf.getElementsByTagName("sh:Contact").item(0);
                    contact.setTextContent(vkn);
                    break;
                }
            }
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            MyXmlUtil.transform(doc, baos);
            byte[] data = baos.toByteArray();
            Base64 base64Util = new Base64();
            String dataToBeSigned = base64Util.encodeAsString(data);
            return dataToBeSigned;
        } catch (DOMException de) {
            throw new Exception(de.toString());
        } catch (TransformerException te) {
            throw new TransformerException(te.toString());
        } catch (Exception e) {
            throw new Exception(e.toString());
        }
    }

    @Override
    public byte[] localXadesBesSign(Document doc, String alias, int mode) throws Exception {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ByteArrayOutputStream os2 = new ByteArrayOutputStream();
        InputStream is = null;
        XadesSignOperation op = null;
        byte[] signedArray = null;
        try {
            if (mode == 0)
                XuxuxKeyingDataProviderImpl.setSelectedAlias(alias);
            if (mode == 1)
                XuxuxPfxKeyingDataProviderImpl.setSelectedAlias(alias);
            System.out.println("Signing begins at:" + new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(new Date()));
            Node signingNode = doc.getDocumentElement();
            MyXmlUtil.transform(signingNode, os2);
            is = new ByteArrayInputStream(os2.toByteArray());
            op = new XadesSignOperation(is, os);
            op.SetRole("Tedarikçi");
            op.SetUBLFormat(true);
            synchronized (lock) {
                /**
                 * xuxux archive org : op.Sign(alias, (mode == 0 ? false :
                 * true));
                 */
                op.Sign(alias, (mode == 0 ? false : true));
                os.flush();
                signedArray = os.toByteArray();
            }
            return signedArray;
        } catch (Exception e) {
            LogUtil.addError(logPane, e.toString());
            throw e;
        } finally {
            if (null != os)
                os.close();
            os = null;
            if (null != os2)
                os2.close();
            os2 = null;
            if (null != is)
                is.close();
            is = null;
            op = null;
            signedArray = null;
        }

    }

    @Override
    public Document localWssSignatureForSendDocument(String alias, String pass, String providerPath, String zippedFilePath) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        SOAPMessage message = SOAPUtil.getSoapMessageTemplete(zippedFilePath);
//        String conf = "name = AKIS\nlibrary = " + providerPath + "\ndisabledMechanisms={ CKM_SHA1_RSA_PKCS }";
        String conf = "name = AKIS\nlibrary = " + providerPath + "\nslotListIndex=2";
        XuxuxDeviceProvider xdp = new XuxuxDeviceProvider(new ByteArrayInputStream(conf.getBytes()), pass);
        xdp.load(new ByteArrayInputStream(conf.getBytes()), pass, "PKCS11", null);
        Document document = SOAPUtil.createWSSrequest(xdp, message, alias, pass);
        xdp.unload();
        return document;
    }

    @Override
    public Document localWssSignatureForGetBatchStatus(String alias, String pass, String providerPath, String paketId) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        SOAPMessage message = SOAPUtil.getBatchStatusSoapMessageTemplete(paketId);
//        String conf = "name = AKIS\nlibrary = " + providerPath + "\ndisabledMechanisms={ CKM_SHA1_RSA_PKCS }";
        String conf = "name = AKIS\nlibrary = " + providerPath + "\nslotListIndex=2";
        XuxuxDeviceProvider xdp = new XuxuxDeviceProvider(new ByteArrayInputStream(conf.getBytes()), pass);
        xdp.load(new ByteArrayInputStream(conf.getBytes()), pass, "PKCS11", null);
        Document document = SOAPUtil.createWSSrequest(xdp, message, alias, pass);
        xdp.unload();
        return document;
    }

    @Override
    public byte[] localXadesASignArchiveReportWithMA3(byte[] zippedData, String root_dir, String pfx_pass, String pkcs11_pass) throws Exception {
        try {
            org.w3c.dom.Document raporDoc = MyXmlUtil.parse(new ByteArrayInputStream(MyFileUtil.unzip(zippedData)));

            // get the element that signature will be added
            Element signatureElem = (Element) raporDoc.getDocumentElement().getElementsByTagName("earsiv:baslik").item(0);

            // generate signature
            Context context = createContext(root_dir, null, pkcs11_pass);
            context.setDocument(raporDoc);
            XMLSignature signature = new XMLSignature(context, false);
            // attach signature to envelope structure
            signatureElem.appendChild(signature.getElement());

            // use enveloped signature transform
            Transforms transforms = new Transforms(context);
            transforms.addTransform(new Transform(context, TransformType.ENVELOPED.getUrl()));

            // add whole document(="") with envelope transform, with SHA256
            // and don't include it into signature(false)
            signature.addDocument("", "text/xml", transforms, DigestMethod.SHA_256, false);

            signature.getSignedInfo().setSignatureMethod(SignatureMethod.RSA_SHA256);

            // false-true gets non-qualified certificates while true-false gets
            // qualified ones
//            X509Certificate cert = JSmartCardManager.getInstance().getSignatureCertificate(false, true);
            X509Certificate cert = XuxuxHsmManager.getInstance().getSignatureCertificate(false, true);

            // add certificate to show who signed the document
            signature.addKeyInfo(new ECertificate(cert.getEncoded()));

            // add signer role information
            SignerRole rol = new SignerRole(context, new ClaimedRole[]{new ClaimedRole(context, "Supplier")});
            signature.getQualifyingProperties().getSignedSignatureProperties().setSignerRole(rol);

            // e-fatura standards want public key info to be in the signature
            PublicKey pk = KeyUtil.decodePublicKey(new ECertificate(cert.getEncoded()).getSubjectPublicKeyInfo());
            signature.getKeyInfo().add(new KeyValue(context, pk));

            // add signing time
            signature.getQualifyingProperties().getSignedSignatureProperties().setSigningTime(getTime());

            /**
             * xuxux
             */
            // set policy info defined and required by profile
//			signature.setPolicyIdentifier(OID_POLICY_P4, "Uzun Dönemli ve ÇİSDuP Kontrollü Güvenli Elektronik İmza Politikası",
//					"http://www.tk.gov.tr/bilgi_teknolojileri/elektronik_imza/dosyalar/Elektronik_Imza_Kullanim_Profilleri_Rehberi.pdf");
//            signature.sign(JSmartCardManager.getInstance().getSigner(PIN, cert));
            signature.sign(XuxuxHsmManager.getInstance().getSigner(PIN, cert));

            // upgrade to XL
            signature.upgrade(SignatureType.ES_XL);

            // xuxux - upgrade to A
            signature.upgradeToXAdES_A();
			// signature.addArchiveTimeStamp();

            // e-fatura standards want signatureID to be same with cbc:URI
            // get signatureID from e-fatura
//			String signatureID = ((Element) (raporDoc.getDocumentElement().getElementsByTagName("cbc:URI").item(0))).getTextContent();
            String signatureID = "#Signature_" + UUID.randomUUID().toString();
            String signatureIDwoNumberSign = signatureID.substring(1);

            // change original signatureID
            Element dsSignature = (Element) (raporDoc.getDocumentElement().getElementsByTagName("ds:Signature").item(0));
            dsSignature.setAttribute("Id", signatureIDwoNumberSign);

            Element xadesQualifyingProperties = (Element) (raporDoc.getDocumentElement().getElementsByTagName("xades:QualifyingProperties").item(0));
            xadesQualifyingProperties.setAttribute("Target", signatureID);

            // write to outputstream
            Source source = new DOMSource(raporDoc);
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            transformer.transform(source, new StreamResult(baos));
            baos.flush();
            return baos.toByteArray();

        } catch (XMLSignatureException x) {
            LOGGER.error(StringUtil.traceException(x));
            x.printStackTrace();
            throw x;
        } catch (IOException x) {
            LOGGER.error(StringUtil.traceException(x));
            x.printStackTrace();
            throw x;
        }
    }

    @Override
    public byte[] localXadesASignArchiveInvoiceWithMA3(byte[] zippedData, String root_dir, String pfx_pass, String pkcs11_pass) throws Exception {
        try {
            // // read XML file from document
            // DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            // dbf.setNamespaceAware(true);
            // DocumentBuilder db = dbf.newDocumentBuilder();
            // // read from file, it can be stream too
            // org.w3c.dom.Document faturaDoc = db.parse(new File("D:\\StaticFiles\\arc\\GIB2014000000001.xml"));
            org.w3c.dom.Document faturaDoc = MyXmlUtil.parse(new ByteArrayInputStream(MyFileUtil.unzip(zippedData)));

            // get the element that signature will be added
            Element extContent = (Element) faturaDoc.getDocumentElement().getElementsByTagName("ext:ExtensionContent").item(0);

            // generate signature
            Context context = createContext(root_dir, null, pkcs11_pass);
            context.setDocument(faturaDoc);
            XMLSignature signature = new XMLSignature(context, false);
            // attach signature to envelope structure
            extContent.appendChild(signature.getElement());

            // use enveloped signature transform
            Transforms transforms = new Transforms(context);
            transforms.addTransform(new Transform(context, TransformType.ENVELOPED.getUrl()));
            // add whole document(="") with envelope transform, with SHA256
            // and don't include it into signature(false)
            signature.addDocument("", "text/xml", transforms, DigestMethod.SHA_256, false);

            signature.getSignedInfo().setSignatureMethod(SignatureMethod.RSA_SHA256);

            // false-true gets non-qualified certificates while true-false gets
            // qualified ones
            /**
             * xuxux - original code was : X509Certificate cert =
             * JSmartCardManager.getInstance().getSignatureCertificate(false,
             * true);
             */
//            X509Certificate cert = JSmartCardManager.getInstance().getSignatureCertificate(false, true);
            X509Certificate cert = XuxuxHsmManager.getInstance().getSignatureCertificate(false, true);

            // add certificate to show who signed the document
            signature.addKeyInfo(new ECertificate(cert.getEncoded()));

            // add signer role information
            SignerRole rol = new SignerRole(context, new ClaimedRole[]{new ClaimedRole(context, "Supplier")});
            signature.getQualifyingProperties().getSignedSignatureProperties().setSignerRole(rol);

            // e-fatura standards want public key info to be in the signature
            PublicKey pk = KeyUtil.decodePublicKey(new ECertificate(cert.getEncoded()).getSubjectPublicKeyInfo());
            signature.getKeyInfo().add(new KeyValue(context, pk));

            // add signing time
            signature.getQualifyingProperties().getSignedSignatureProperties().setSigningTime(getTime());

            /**
             * xuxux
             */
            // set policy info defined and required by profile
            // signature.setPolicyIdentifier(OID_POLICY_P4, "Uzun Dönemli ve ÇİSDuP Kontrollü Güvenli Elektronik İmza Politikası",
            // "http://www.tk.gov.tr/bilgi_teknolojileri/elektronik_imza/dosyalar/Elektronik_Imza_Kullanim_Profilleri_Rehberi.pdf");
//            signature.sign(JSmartCardManager.getInstance().getSigner(PIN, cert));
            signature.sign(XuxuxHsmManager.getInstance().getSigner(PIN, cert));
            /**
             * original code is signature.upgrade(SignatureType.ES_XL);
             * signature.upgradeToXAdES_A();
             */
            // upgrade to XL
            signature.upgrade(SignatureType.ES_XL);

            // xuxux - upgrade to A
            signature.upgradeToXAdES_A();
            // signature.addArchiveTimeStamp();

            // e-fatura standards want signatureID to be same with cbc:URI
            // get signatureID from e-fatura
            String signatureID = ((Element) (faturaDoc.getDocumentElement().getElementsByTagName("cbc:URI").item(0))).getTextContent();
            String signatureIDwoNumberSign = signatureID.substring(1);

            // change original signatureID
            Element dsSignature = (Element) (faturaDoc.getDocumentElement().getElementsByTagName("ds:Signature").item(0));
            dsSignature.setAttribute("Id", signatureIDwoNumberSign);

            Element xadesQualifyingProperties = (Element) (faturaDoc.getDocumentElement().getElementsByTagName("xades:QualifyingProperties").item(0));
            xadesQualifyingProperties.setAttribute("Target", signatureID);

            // write to outputstream
            Source source = new DOMSource(faturaDoc);
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            transformer.transform(source, new StreamResult(baos));
            baos.flush();
            return baos.toByteArray();
        } catch (XMLSignatureException x) {
            LOGGER.error(StringUtil.traceException(x));
            throw x;
        } catch (IOException x) {
            LOGGER.error(StringUtil.traceException(x));
            x.printStackTrace();
            throw x;
        }
    }
    
       @Override
    public byte[] localPAdesSignPDFWithMA3(byte[] pdfData, String root_dir, String pfx_pass, String pkcs11_pass) throws Exception {
           try {
//               SignatureContainer pc = SignatureFactory.readContainer(SignatureFormat.PAdES, new FileInputStream(""), createContext(root_dir, pfx_pass, pkcs11_pass));
               
               return null;
           } catch (Exception e) {
             LOGGER.error(StringUtil.traceException(e));
            e.printStackTrace();
            throw e;  
           }
    }

    private static XMLGregorianCalendar getTime() {
        Calendar cal = new GregorianCalendar();
        cal.get(Calendar.SECOND);
        return XmlUtil.createDate(cal);
    }

}
