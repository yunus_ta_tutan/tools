/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.impl;

import com.xuxux.utils.files.MyFileUtil;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IssueDateType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.UUIDType;
import oasis.names.specification.ubl.schema.xsd.invoice_2.InvoiceType;

import org.slf4j.LoggerFactory;
import org.unece.cefact.namespaces.standardbusinessdocumentheader.StandardBusinessDocument;

import signerclient.services.InvGeneratorService;
import signerclient.utils.DateUtil;
import signerclient.utils.GenericMarshallUnmarshallUtil;
import signerclient.utils.StringUtil;

/**
 *
 * @author xuxux
 */
public class InvGeneratorImpl implements InvGeneratorService {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(InvGeneratorImpl.class);

    public InvGeneratorImpl() {

    }

    @Override
    public void generateZippedInvoices(String targetFolderPath, int zipCount, long xmlInZipCount, int startFrom, Date issueDate) throws Exception {
        InvoiceType templete;
        Long initialInvNo = 2014000000000L;
        Random random = new Random();
        UUIDType uuidType = null;
        IDType idType = null;
        IssueDateType issueDateType = null;
        ByteArrayOutputStream baos = null;
        ZipOutputStream zos = null;
        BufferedOutputStream bos = null;
        FileOutputStream fos = null;
        try {
            String _zipfilepath = null;
            String zipfilepath = null;
            templete = GenericMarshallUnmarshallUtil.unmarshall(InvoiceType.class, MyFileUtil.readBytesFromFile(new File(targetFolderPath + "/template.xml")));
            int until = startFrom + zipCount;
            for (int i = startFrom; i < until; i++) {
                _zipfilepath = targetFolderPath + "/_" + (i) + ".zip";
                zipfilepath = targetFolderPath + "/" + (i) + ".zip";
                fos = new FileOutputStream(_zipfilepath);
                bos = new BufferedOutputStream(fos);
                zos = new ZipOutputStream(bos);
                for (long j = 0; j < xmlInZipCount; j++) {
                    uuidType = new UUIDType();
                    uuidType.setValue(UUID.randomUUID().toString());
                    templete.setUUID(uuidType);
                    idType = new IDType();
                    idType.setValue("GIB" + initialInvNo++);
                    templete.setID(idType);
                    issueDateType = new IssueDateType();
                    issueDateType.setValue(DateUtil.toXmlDate(issueDate));
                    templete.setIssueDate(issueDateType);
                    baos = new ByteArrayOutputStream();
                    String out = GenericMarshallUnmarshallUtil.marshall(templete);
                    baos.write(out.getBytes("UTF-8"));
                    zos.putNextEntry(new ZipEntry(uuidType.getValue() + ".xml"));
                    baos.flush();
                    zos.write(baos.toByteArray());
                    zos.closeEntry();
                    zos.flush();
                    bos.flush();
                    fos.flush();
                }
                close(zos);
                close(bos);
                close(fos);

                File file = new File(_zipfilepath);
                File file2 = new File(zipfilepath);
                if (file2.exists()) {
                    continue;
                }
                file.renameTo(file2);
                LOGGER.debug("File created: " + file2.getName());
            }
        } catch (JAXBException e) {
            LOGGER.error(StringUtil.traceException(e));
            throw e;
        } catch (ClassNotFoundException e) {
            LOGGER.error(StringUtil.traceException(e));
            throw e;
        } catch (IOException e) {
            LOGGER.error(StringUtil.traceException(e));
            throw e;
        } catch (DatatypeConfigurationException e) {
            LOGGER.error(StringUtil.traceException(e));
            throw e;
        } catch (Exception e) {
            LOGGER.error(StringUtil.traceException(e));
            throw e;
        } finally {
            close(zos);
            close(fos);
            close(baos);
            close(bos);
        }
    }

    public static IOException close(Closeable c) {
        if (c != null) {
            try {
                c.close();
            } catch (IOException e) {
                return e;
            }
        }
        return null;
    }

    public static void addToZipFile(byte[] data, String entryName, ZipOutputStream zos) throws IOException {
        ZipEntry zipEntry = new ZipEntry(entryName);
        zos.putNextEntry(zipEntry);
        zos.write(data, 0, data.length);
        zos.closeEntry();
    }

    private static int getRandomInteger(int aStart, int aEnd, Random aRandom) {
        if (aStart > aEnd) {
            throw new IllegalArgumentException("Start cannot exceed End.");
        }
        // get the range, casting to long to avoid overflow problems
        long range = (long) aEnd - (long) aStart + 1;
        // compute a fraction of the range, 0 <= frac < range
        long fraction = (long) (range * aRandom.nextDouble());
        return (int) (fraction + aStart);
    }

    @Override
    public void generateZippedEnvelope(String targetFolderPath, int zipCount, long xmlInZipCount, int startFrom, Date issueDate) throws Exception {

        StandardBusinessDocument templateSBD;
        InvoiceType templateINV;
        List<InvoiceType> invList = new ArrayList<InvoiceType>();
        Long initialInvNo = 2014000000000L;
        Random random = new Random();
        UUIDType uuidType = null;
        IDType idType = null;
        IssueDateType issueDateType = null;
        ByteArrayOutputStream baos = null;
        ZipOutputStream zos = null;
        BufferedOutputStream bos = null;
        FileOutputStream fos = null;

        try {
            String _zipfilepath = null;
            String zipfilepath = null;
            int until = startFrom + zipCount;

            for (int x = startFrom; x < until; x++) {
                templateSBD = GenericMarshallUnmarshallUtil.unmarshallSbdWithoutSchemaValidation(StandardBusinessDocument.class, MyFileUtil.readBytesFromFile(new File(targetFolderPath + "/templateSBD.xml")));
                invList = new ArrayList<InvoiceType>();
                _zipfilepath = targetFolderPath + "/_" + (x) + ".zip";
                zipfilepath = targetFolderPath + "/" + (x) + ".zip";
                fos = new FileOutputStream(_zipfilepath);
                bos = new BufferedOutputStream(fos);
                zos = new ZipOutputStream(bos);
                templateSBD.getStandardBusinessDocumentHeader().getDocumentIdentification().setInstanceIdentifier(UUID.randomUUID().toString());
                templateSBD.getAny().getElements().get(0).setElementCount((int) xmlInZipCount);
                zos.putNextEntry(new ZipEntry(templateSBD.getStandardBusinessDocumentHeader().getDocumentIdentification().getInstanceIdentifier() + ".xml"));
                for (int i = 0; i < xmlInZipCount; i++) {
                    templateINV = GenericMarshallUnmarshallUtil.unmarshallSbdWithoutSchemaValidation(InvoiceType.class, MyFileUtil.readBytesFromFile(new File(targetFolderPath + "/templateINV.xml")));
                    uuidType = new UUIDType();
                    uuidType.setValue(UUID.randomUUID().toString());
                    templateINV.setUUID(uuidType);
                    idType = new IDType();
                    idType.setValue("GIB" + initialInvNo++);
                    templateINV.setID(idType);
                    issueDateType = new IssueDateType();
                    issueDateType.setValue(DateUtil.toXmlDate(issueDate));
                    templateINV.setIssueDate(issueDateType);
                    invList.add(i, templateINV);
                }

                for (int i = 0; i < invList.size(); i++) {
                    templateSBD.getAny().getElements().get(0).getElementList().getAny().add(i, invList.get(i));

                }

                baos = new ByteArrayOutputStream();
                String out = GenericMarshallUnmarshallUtil.marshallSbd(templateSBD);

                baos.write(out.getBytes("UTF-8"));

                baos.flush();
                zos.write(baos.toByteArray());
                zos.closeEntry();
                zos.flush();
                bos.flush();
                fos.flush();

                close(zos);
                close(bos);
                close(fos);
                File file = new File(_zipfilepath);
                File file2 = new File(zipfilepath);

                file.renameTo(file2);
            }

        } catch (Exception e) {
            LOGGER.error(StringUtil.traceException(e));
            throw e;
        } finally {
            close(zos);
            close(fos);
            close(baos);
            close(bos);
        }

    }

}
