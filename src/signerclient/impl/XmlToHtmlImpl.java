/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.impl;

import com.xuxux.utils.commons.MyCryptoUtil;
import com.xuxux.utils.commons.MyXmlUtil;
import com.xuxux.utils.files.MyFileUtil;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import signerclient.services.XmlToHtmlService;
import signerclient.utils.BOMUtil;

/**
 *
 * @author xuxux
 */
public class XmlToHtmlImpl implements XmlToHtmlService {

    @Override
    public byte[] convertFromXmlToHtml(File xmlFile, String xsltFilePath, boolean isDefaultXslt) throws Exception {
        /*
         * html e dönüşüm
         */
        ByteArrayInputStream bais = null;
        InputStreamReader reader = null;
        try {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            Document document = MyXmlUtil.parse(new FileInputStream(xmlFile));
            /**
             * xuxux - test fatura gorunurken namespace li xml lerde hata
             * almasindan kaynakli test
             */
            /**
             * XUXUX - Birden fazla additionalDocumentReference olduğunda
             * doğru(xslt iceren) node un alınmasi lazim
             */
            if (isDefaultXslt) {
                NodeList addDocRefs = document.getElementsByTagNameNS("*", "AdditionalDocumentReference");
                String xslt = null;
                if (addDocRefs != null) {
                    for (int i = 0; i < addDocRefs.getLength(); i++) {
                        Element addDocRef = (Element) addDocRefs.item(i);
                        if (addDocRef != null) {
                            Element attach = (Element) addDocRef.getElementsByTagNameNS("*", "Attachment").item(0);
                            if (attach != null) {
                                xslt = attach.getElementsByTagNameNS("*", "EmbeddedDocumentBinaryObject").item(0).getTextContent();
                            }
                        }
                    }
                }
                bais = new ByteArrayInputStream(BOMUtil.removeBOM(MyCryptoUtil.commonsDecodeBase64byteArray(xslt), BOMUtil.UTF8_BOM));
                reader = new InputStreamReader(bais, "UTF-8");
                MyXmlUtil.transformToHtml(new StreamSource(reader), new StreamSource(xmlFile), new StreamResult(os));
            } else {
                bais = new ByteArrayInputStream(BOMUtil.removeBOM(MyFileUtil.readBytesFromFile(new File(xsltFilePath)), BOMUtil.UTF8_BOM));
                reader = new InputStreamReader(bais, "UTF-8");
                MyXmlUtil.transformToHtml(new StreamSource(reader), new StreamSource(xmlFile), new StreamResult(os));
            }
            
            return os.toByteArray();
        } catch (Exception e) {
            throw e;
        } finally {
            if(bais != null)
                bais.close();
            if(reader != null)
                reader.close();
            bais = null;
            reader = null;
        }
    }
}
