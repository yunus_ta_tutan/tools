/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.impl;

import com.xuxux.utils.commons.MyCryptoUtil;
import com.xuxux.utils.commons.MyXmlUtil;
import com.xuxux.utils.files.MyExcelUtil;
import com.xuxux.utils.files.MyFileUtil;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.List;
import javax.swing.JTextPane;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import signerclient.filters.XmlFileFilter;
import signerclient.models.ExtractedUblInfo;
import signerclient.services.FolderExtractService;
import signerclient.utils.LogUtil;

/**
 *
 * @author xuxux
 */
public class FolderExtractImpl implements FolderExtractService {

    private static JTextPane logPane;

    /**
     * @param aLogPane the logPane to set
     */
    public static void setLogPane(JTextPane aLogPane) {
        logPane = aLogPane;
    }

    @Override
    public String[] getAllXmlFileNamesUnderFolder(String folderPath) throws Exception {
        try {
            File folder = new File(folderPath);
            return folder.list(new XmlFileFilter());
        } catch (Exception e) {
            throw new Exception(e.toString());
        }

    }

    @Override
    public ExtractedUblInfo[] extractInfosFromFile(String filePath) throws Exception {
        ExtractedUblInfo[] extractedUblInfos = null;
        FileInputStream fin = null;
        ByteArrayInputStream bin = null;
        try {
            NodeList commonNodeList = null;
            fin = new FileInputStream(filePath);
            Document document = null;
            try {
                document = MyXmlUtil.parse(fin);
            } catch (Exception e) {
                return null;
            }
            
            Node binaryData = document.getElementsByTagNameNS("*", "binaryData").item(0);
            bin = new ByteArrayInputStream(MyFileUtil.unzip(MyCryptoUtil.decode(binaryData.getTextContent())));
            Document innerDocument = null;
            try {
                innerDocument = MyXmlUtil.parse(bin);
            } catch (Exception e) {
                return null;
            }
            
            // envelopeID
            String envelopeID = MyXmlUtil.getFirstNode(innerDocument.getElementsByTagNameNS("*", "InstanceIdentifier")).getTextContent();
            //envelopeTYPE
            String envelopeTYPE = MyXmlUtil.getFirstNode(innerDocument.getElementsByTagNameNS("*", "Type")).getTextContent();
            //envelopeCreationDateAndTime
            String envelopeCreationDateAndTime = MyXmlUtil.getFirstNode(innerDocument.getElementsByTagNameNS("*", "CreationDateAndTime")).getTextContent();
            //senderVKN
            String senderVKN = null;
            Element senderElement = (Element) MyXmlUtil.getFirstNode(innerDocument.getElementsByTagNameNS("*", "Sender"));
            commonNodeList = senderElement.getElementsByTagNameNS("*", "ContactInformation");
            for (int i = 0; i < commonNodeList.getLength(); i++) {
                Element e = (Element) commonNodeList.item(i);
                if ("VKN_TCKN".equals(MyXmlUtil.getFirstContext(e.getElementsByTagNameNS("*", "ContactTypeIdentifier"))))
                    senderVKN = MyXmlUtil.getFirstContext(e.getElementsByTagNameNS("*", "Contact"));
            }
            //senderTITLE
            String senderTITLE = null;
            commonNodeList = senderElement.getElementsByTagNameNS("*", "ContactInformation");
            for (int i = 0; i < commonNodeList.getLength(); i++) {
                Element e = (Element) commonNodeList.item(i);
                if ("UNVAN".equals(MyXmlUtil.getFirstContext(e.getElementsByTagNameNS("*", "ContactTypeIdentifier"))))
                    senderTITLE = MyXmlUtil.getFirstContext(e.getElementsByTagNameNS("*", "Contact"));
            }
            //receiverVKN
            String receiverVKN = null;
            Element receiverElement = (Element) MyXmlUtil.getFirstNode(innerDocument.getElementsByTagNameNS("*", "Receiver"));
            commonNodeList = receiverElement.getElementsByTagNameNS("*", "ContactInformation");
            for (int i = 0; i < commonNodeList.getLength(); i++) {
                Element e = (Element) commonNodeList.item(i);
                if ("VKN_TCKN".equals(MyXmlUtil.getFirstContext(e.getElementsByTagNameNS("*", "ContactTypeIdentifier"))))
                    receiverVKN = MyXmlUtil.getFirstContext(e.getElementsByTagNameNS("*", "Contact"));
            }
            //receiverTITLE
            String receiverTITLE = null;
            commonNodeList = receiverElement.getElementsByTagNameNS("*", "ContactInformation");
            for (int i = 0; i < commonNodeList.getLength(); i++) {
                Element e = (Element) commonNodeList.item(i);
                if ("UNVAN".equals(MyXmlUtil.getFirstContext(e.getElementsByTagNameNS("*", "ContactTypeIdentifier"))))
                    receiverTITLE = MyXmlUtil.getFirstContext(e.getElementsByTagNameNS("*", "Contact"));
            }
            Element elements = (Element) innerDocument.getElementsByTagNameNS("*", "Elements").item(0);
            Element elementList = (Element) elements.getElementsByTagNameNS("*","ElementList").item(0);
            //elementType
            String elementType = elements.getElementsByTagNameNS("*", "ElementType").item(0).getTextContent().trim();
            int elementCount = Integer.parseInt(elements.getElementsByTagNameNS("*", "ElementCount").item(0).getTextContent().trim());

            if (null != elementType && "INVOICE".equalsIgnoreCase(elementType)) {
                NodeList invoiceNodelist = elementList.getChildNodes();
                int length = invoiceNodelist.getLength();
                extractedUblInfos = new ExtractedUblInfo[elementCount];
                int i = 0;
                for (int x = 0; x < length; x++) {
                    Node invoiceNode = invoiceNodelist.item(x);
                    if (invoiceNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element invoice = (Element) invoiceNode;
                        if ("OtomatikTest".equalsIgnoreCase(receiverTITLE) || null == receiverVKN) {
                            extractedUblInfos[i] = new ExtractedUblInfo();
                            extractedUblInfos[i].setEnvelopeID(envelopeID);
                            extractedUblInfos[i].setEnvelopeDate(envelopeCreationDateAndTime);
                            extractedUblInfos[i].setEnvelopeType(envelopeTYPE);
                            extractedUblInfos[i].setInvoiceId("fake");
                            extractedUblInfos[i].setIssueDate("fake");
                            extractedUblInfos[i].setReceiverTitle(receiverTITLE);
                            extractedUblInfos[i].setReceiverVKN(receiverVKN);
                            extractedUblInfos[i].setSenderTitle(senderTITLE);
                            extractedUblInfos[i].setSenderVKN(senderVKN);
                            extractedUblInfos[i].setTotalAmount("fake");
                            extractedUblInfos[i].setTotalTaxAmount("fake");
                            i++;
                            continue;
                        }
                        //invoiceID
                        String invoiceID = MyXmlUtil.getFirstContext(invoice.getElementsByTagNameNS("*", "ID"));
                        //issueDateTime
                        String issueDateTime = MyXmlUtil.getFirstContext(invoice.getElementsByTagNameNS("*", "IssueDate"));
                        issueDateTime += (" " + MyXmlUtil.getFirstContext(invoice.getElementsByTagNameNS("*", "IssueTime")));
                        //payableAmount
                        Element legalMonetaryTotalElement = (Element) MyXmlUtil.firstNode(invoice.getElementsByTagNameNS("*", "LegalMonetaryTotal"));
                        String payableAmount = MyXmlUtil.getFirstContext(legalMonetaryTotalElement.getElementsByTagNameNS("*", "PayableAmount"));
                        //TaxAmount
                        Element taxTotal = (Element) MyXmlUtil.firstNode(invoice.getElementsByTagNameNS("*", "TaxTotal"));
                        String taxAmount = MyXmlUtil.getFirstContext(taxTotal.getElementsByTagNameNS("*", "TaxAmount"));

                        extractedUblInfos[i] = new ExtractedUblInfo();
                        extractedUblInfos[i].setEnvelopeID(envelopeID);
                        extractedUblInfos[i].setEnvelopeDate(envelopeCreationDateAndTime);
                        extractedUblInfos[i].setEnvelopeType(envelopeTYPE);
                        extractedUblInfos[i].setInvoiceId(invoiceID);
                        extractedUblInfos[i].setIssueDate(issueDateTime);
                        extractedUblInfos[i].setReceiverTitle(receiverTITLE);
                        extractedUblInfos[i].setReceiverVKN(receiverVKN);
                        extractedUblInfos[i].setSenderTitle(senderTITLE);
                        extractedUblInfos[i].setSenderVKN(senderVKN);
                        extractedUblInfos[i].setTotalAmount(payableAmount);
                        extractedUblInfos[i].setTotalTaxAmount(taxAmount);
                        i++;
                    }
                }

            } else if (null != elementType && "APPLICATIONRESPONSE".equalsIgnoreCase(elementType.trim()) && "POSTBOXENVELOPE".equalsIgnoreCase(envelopeTYPE)) {
                NodeList nl = elementList.getChildNodes();
                int length = nl.getLength();
                extractedUblInfos = new ExtractedUblInfo[elementCount];
                int i = 0;
                for (int x = 0; x < length; x++) {
                    Node invoiceNode = nl.item(x);
                    if (invoiceNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element applicationResponse = (Element) invoiceNode;
                        //baprID
                        String baprID = MyXmlUtil.getFirstContext(applicationResponse.getElementsByTagNameNS("*", "ID"));
                        //issueDateTime
                        String issueDateTime = MyXmlUtil.getFirstContext(applicationResponse.getElementsByTagNameNS("*", "IssueDate"));
                        issueDateTime += (" " + MyXmlUtil.getFirstContext(applicationResponse.getElementsByTagNameNS("*", "IssueTime")));
                        //senderTITLE
                        //receiverTITLE

                        //ResponseCode
                        Element documentResponse = (Element) MyXmlUtil.firstNode(applicationResponse.getElementsByTagNameNS("*", "DocumentResponse"));
                        Element response = (Element) MyXmlUtil.firstNode(documentResponse.getElementsByTagNameNS("*", "Response"));
                        String responseCode = MyXmlUtil.getFirstContext(response.getElementsByTagNameNS("*", "ResponseCode")) + "-" + MyXmlUtil.getFirstContext(response.getElementsByTagNameNS("*", "Description"));

                        extractedUblInfos[i] = new ExtractedUblInfo();
                        extractedUblInfos[i].setEnvelopeID(envelopeID);
                        extractedUblInfos[i].setEnvelopeDate(envelopeCreationDateAndTime);
                        extractedUblInfos[i].setEnvelopeType(envelopeTYPE);
                        extractedUblInfos[i].setInvoiceId(baprID);
                        extractedUblInfos[i].setIssueDate(issueDateTime);
                        extractedUblInfos[i].setReceiverTitle(receiverTITLE);
                        extractedUblInfos[i].setReceiverVKN(receiverVKN);
                        extractedUblInfos[i].setSenderTitle(senderTITLE);
                        extractedUblInfos[i].setSenderVKN(senderVKN);
                        extractedUblInfos[i].setBaprResponse(responseCode);
                        extractedUblInfos[i].setTotalAmount("");
                        extractedUblInfos[i].setTotalTaxAmount("");
                        i++;
                    }
                }
            } else if (null != elementType && "APPLICATIONRESPONSE".equalsIgnoreCase(elementType.trim()) && "SYSTEMENVELOPE".equalsIgnoreCase(envelopeTYPE)) {
                NodeList nl = elementList.getChildNodes();
                int length = nl.getLength();
                extractedUblInfos = new ExtractedUblInfo[elementCount];
                int i = 0;
                for (int x = 0; x < length; x++) {
                    Node invoiceNode = nl.item(x);
                    if (invoiceNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element applicationResponse = (Element) invoiceNode;
                        //saprID
                        String saprID = MyXmlUtil.getFirstContext(applicationResponse.getElementsByTagNameNS("*", "ID"));
                        //issueDateTime
                        String issueDateTime = MyXmlUtil.getFirstContext(applicationResponse.getElementsByTagNameNS("*", "IssueDate"));
                        issueDateTime += (" " + MyXmlUtil.getFirstContext(applicationResponse.getElementsByTagNameNS("*", "IssueTime")));
                        //senderTITLE
                        //receiverTITLE
                        extractedUblInfos[i] = new ExtractedUblInfo();
                        extractedUblInfos[i].setEnvelopeID(envelopeID);
                        extractedUblInfos[i].setEnvelopeDate(envelopeCreationDateAndTime);
                        extractedUblInfos[i].setEnvelopeType(envelopeTYPE);
                        extractedUblInfos[i].setInvoiceId(saprID);
                        extractedUblInfos[i].setIssueDate(issueDateTime);
                        extractedUblInfos[i].setReceiverTitle(receiverTITLE);
                        extractedUblInfos[i].setReceiverVKN(receiverVKN);
                        extractedUblInfos[i].setSenderTitle(senderTITLE);
                        extractedUblInfos[i].setSenderVKN(senderVKN);
                        extractedUblInfos[i].setTotalAmount("");
                        extractedUblInfos[i].setTotalTaxAmount("");
                        i++;
                    }
                }
            }

        } catch (Exception e) {
            LogUtil.addError(logPane, e.toString());
            e.printStackTrace();
        } finally {
            if (null != fin)
                fin.close();
            fin = null;
            if (null != bin)
                bin.close();
            bin = null;
        }
        return extractedUblInfos;
    }

    @Override
    public void getExcelList(List<ExtractedUblInfo> list, OutputStream os, String sheetName) throws Exception {

        String[] fields = {"envelopeID", "envelopeType", "envelopeDate", "senderVKN", "senderTitle", "receiverVKN", "receiverTitle", "invoiceId", "issueDate", "totalAmount", "totalTaxAmount", "baprResponse"};

        HSSFWorkbook exlWb = null;
        HSSFSheet exlDataSheet = null;
        try {
            exlWb = new HSSFWorkbook();
            exlDataSheet = exlWb.createSheet(sheetName);
            HSSFCellStyle headerCellStyle = setHeaderStyle(exlWb);
            HSSFRow headerRow = exlDataSheet.createRow(0);
            for (int j = 0; j < fields.length; j++) {
                HSSFCell headerCell = headerRow.createCell(j);
                headerCell.setCellStyle(headerCellStyle);
                headerCell.setCellValue(new HSSFRichTextString(fields[j]));
            }
            int i = 1;
            for (ExtractedUblInfo obj : list) {
                HSSFRow row = exlDataSheet.createRow(i);
                row.createCell(0).setCellValue(new HSSFRichTextString(obj.getEnvelopeID()));
                row.createCell(1).setCellValue(new HSSFRichTextString(obj.getEnvelopeType()));
                row.createCell(2).setCellValue(new HSSFRichTextString(obj.getEnvelopeDate()));
                row.createCell(3).setCellValue(new HSSFRichTextString(obj.getSenderVKN()));
                row.createCell(4).setCellValue(new HSSFRichTextString(obj.getSenderTitle()));
                row.createCell(5).setCellValue(new HSSFRichTextString(obj.getReceiverVKN()));
                row.createCell(6).setCellValue(new HSSFRichTextString(obj.getReceiverTitle()));
                row.createCell(7).setCellValue(new HSSFRichTextString(obj.getInvoiceId()));
                row.createCell(8).setCellValue(new HSSFRichTextString(obj.getIssueDate()));
                row.createCell(9).setCellValue(new HSSFRichTextString(obj.getTotalAmount()));
                row.createCell(10).setCellValue(new HSSFRichTextString(obj.getTotalTaxAmount()));
                row.createCell(11).setCellValue(new HSSFRichTextString(obj.getBaprResponse()));

                i++;
            }
            exlWb.write(os);
        } catch (Exception e) {
            throw new Exception(e.toString());
        } finally {
            if (null != os)
                os.close();
            os = null;
        }


    }

    private static HSSFCellStyle setHeaderStyle(HSSFWorkbook sampleWorkBook) {
        HSSFFont font = sampleWorkBook.createFont();
        font.setFontName(HSSFFont.FONT_ARIAL);
        font.setColor(IndexedColors.PLUM.getIndex());
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        HSSFCellStyle cellStyle = sampleWorkBook.createCellStyle();
        cellStyle.setFont(font);
        return cellStyle;
    }
}
