package signerclient.impl;

import com.xuxux.utils.files.MyFileUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.regex.Pattern;

import signerclient.services.FileOperationsService;

public class FileOperationsImpl implements FileOperationsService {

    @Override
    public String getSqlitePath() {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        URL url = loader.getResource("META-INF/sign.sqlite");
        String path = url.getFile();
        path = path.replaceAll("%20", " ");
        path = path.replaceAll("%7E", "~");
        return path;
    }

    @Override
    public String getFilePathUnderMetaINF(String fileName) {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        URL url = loader.getResource("METAINF/" + fileName);
        String path = url.getFile();
        path = path.replaceAll("%20", " ");
        path = path.replaceAll("%7E", "~");
        return path;
    }

    @Override
    public String getFilePathUnderMetaINFAsDecoded(String fileName) {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        URL url = loader.getResource("META-INF/" + fileName);
        String path = url.getFile();
        System.err.println("XUXUX1>>>" + path);
        path = path.replaceAll("%20", " ");
        path = path.replaceAll("%7E", "~");
        System.err.println("XUXUX2>>>" + path);
        return path;
    }

    @Override
    public String convertPathString(String path) {
        String result = "";
        try {
            result = path;
            result = result.replaceAll(Pattern.quote("\\"), "/");
            result = result.replaceAll(Pattern.quote("//"), "/");
            result = result.replaceAll(Pattern.quote("\\/"), "/");
            result = result.replaceAll(Pattern.quote("/\\"), "/");
            result = result.replaceAll(Pattern.quote("//"), "/");
            result = result.replaceAll(Pattern.quote("///"), "/");
            result = result.replaceAll(Pattern.quote("//"), "/");
            result = result.replaceAll(Pattern.quote("///"), "/");
        } catch (Exception e) {
            result = path;
        }
        return result;

    }

    @Override
    public byte[] readBytesFromFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        // Get the size of the file
        long length = file.length();

        // You cannot create an array using a long type.
        // It needs to be an int type.
        // Before converting to an int type, check
        // to ensure that file is not larger than Integer.MAX_VALUE.
        if (length > Integer.MAX_VALUE) {
            throw new IOException("Could not completely read file " + file.getName() + " as it is too long (" + length + " bytes, max supported "
                    + Integer.MAX_VALUE + ")");
        }

        // Create the byte array to hold the data
        byte[] bytes = new byte[(int) length];

        // Read in the bytes
        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }

        // Ensure all the bytes have been read in
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }

        // Close the input stream and return bytes
        is.close();
        return bytes;
    }

    @Override
    public byte[] getDefaultFileAsByteArray(String fileName) throws Exception {
        try {
            ClassLoader cl = this.getClass().getClassLoader();
            InputStream in = cl.getResourceAsStream("METAINF/" + fileName);
            return MyFileUtil.readInputstreamAsByteArray(in);
        } catch (IOException ioe) {
            throw new IOException(ioe.toString());
        } catch (Exception e) {
            throw new Exception(e.toString());
        }
    }

    @Override
    public String getProxyConfSaveDirectory() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getClassPath(Class clazz) throws Exception {
        File homeDir = new File(clazz.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
        if (homeDir.isDirectory()) {
            return homeDir.getPath();
        } else {
            return homeDir.getParent();
        }
    }

}
