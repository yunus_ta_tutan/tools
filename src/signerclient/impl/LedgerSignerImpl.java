/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.impl;

import com.fit.ledger.ws.client.SignDocumentHeaders;
import com.fit.ledger.ws.client.SignDocumentRequest;
import com.fit.ledger.ws.client.SignDocumentResponse;
import com.fit.ledger.ws.client.SignSOAPMessageFault;
import com.xuxux.utils.commons.MyXmlUtil;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.ws.Holder;
import org.apache.commons.codec.binary.Base64;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import signerclient.services.LedgerSignerService;
import signerclient.wsclient.Invoker;

/**
 *
 * @author xuxux
 */
public class LedgerSignerImpl implements LedgerSignerService {

    @Override
    public SignDocumentResponse callSignLedger(Holder<SignDocumentHeaders> headers, SignDocumentRequest parameters, String endPoint) throws Exception {
        try {
            return Invoker.callSignLedger(headers, parameters, endPoint);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public SignDocumentResponse callSignBerat(Holder<SignDocumentHeaders> headers, SignDocumentRequest parameters, String endPoint) throws Exception {
        try {
            return Invoker.callSignBerat(headers, parameters, endPoint);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public SignDocumentResponse callSignSoapMessage(Holder<SignDocumentHeaders> headers, SignDocumentRequest parameters, String endPoint) throws SignSOAPMessageFault {
        try {
            return Invoker.callsignSOAPMessage(headers, parameters, endPoint);
        } catch (SignSOAPMessageFault ssmf) {
            throw ssmf;
        } 
    }

    @Override
    public String changeSenderVkn(Document doc, String vkn) throws Exception {
        try {
            Element root = doc.getDocumentElement();
            Element sender = (Element) root.getElementsByTagName("sh:Sender").item(0);
            NodeList contactInfList = sender.getElementsByTagName("sh:ContactInformation");
            for (int i = 0; i < contactInfList.getLength(); i++) {
                Element contactInf = (Element) contactInfList.item(i);
                Node identifier = contactInf.getElementsByTagName("sh:ContactTypeIdentifier").item(0);
                if ("VKN_TCKN".equalsIgnoreCase(identifier.getTextContent())) {
                    Node contact = contactInf.getElementsByTagName("sh:Contact").item(0);
                    contact.setTextContent(vkn);
                    break;
                }
            }
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            MyXmlUtil.transform(doc, baos);
            byte[] data = baos.toByteArray();
            Base64 base64Util = new Base64();
            String dataToBeSigned = base64Util.encodeAsString(data);
            return dataToBeSigned;
        } catch (DOMException de) {
            throw new Exception(de.toString());
        } catch (TransformerException te) {
            throw new TransformerException(te.toString());
        } catch (Exception e) {
            throw new Exception(e.toString());
        }
    }

    @Override
    public Document getDefaultDocument(boolean isBerat) throws Exception {
        try {
            ClassLoader cl = this.getClass().getClassLoader();
            InputStream in = cl.getResourceAsStream(isBerat ? "METAINF/fitunsignedledger.tmp" : "METAINF/fitunsignedberat.tmp");
            Document document = MyXmlUtil.parse(in);
            return document;
        } catch (ParserConfigurationException pe) {
            throw new ParserConfigurationException(pe.toString());
        } catch (SAXException se) {
            throw new SAXException(se);
        } catch (IOException ioe) {
            throw new IOException(ioe.toString());
        } catch (Exception e) {
            throw new Exception(e.toString());
        }
    }
}
