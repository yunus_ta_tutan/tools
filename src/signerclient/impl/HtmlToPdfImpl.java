/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import org.zefer.pd4ml.PD4ML;
import signerclient.services.HtmlToPdfService;

/**
 *
 * @author xuxux
 */
public class HtmlToPdfImpl implements HtmlToPdfService {

    @Override
    public byte[] convertFromHtmlToPdf(byte[] htmlData) throws Exception {
        InputStreamReader inr = null;
        ByteArrayOutputStream bos = null;
        try {
            // Converting from HTML TO PDF
            PD4ML html = new PD4ML();
            html.setPageInsets(new java.awt.Insets(0, 0, 0, 0));
            html.setHtmlWidth(1000);
            html.useTTF("java:fonts", true);
//            html.addStyle("BODY{margin:0;padding:0}", true);
//            html.useTTF( "/windows/fonts", true );
//            html.setDefaultTTFs("Times New Roman", "Arial", "Courier New");
            html.setAuthorName("FIT");
            bos = new ByteArrayOutputStream();
            html.enableDebugInfo();
            inr = new InputStreamReader(new ByteArrayInputStream(htmlData), "UTF-8");
            html.render(inr, bos);
            return bos.toByteArray();
        } catch (Exception e) {
            throw e;
        }finally{
            if(inr != null)
                inr.close();
            if(bos != null)
                bos.close();
            inr = null;
            bos = null;
                
        }
    }
}
