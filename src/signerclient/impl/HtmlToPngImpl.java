/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.impl;

import java.awt.Dimension;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import org.zefer.pd4ml.PD4Constants;
import org.zefer.pd4ml.PD4ML;
import signerclient.services.HtmlToPngService;

/**
 *
 * @author xuxux
 */
public class HtmlToPngImpl implements HtmlToPngService{

    @Override
    public byte[] convertFromHtmlToPng(byte[] htmlData, int height, int width) throws Exception {
         
        InputStreamReader inr = null;
        ByteArrayOutputStream bos = null;
        try {
            PD4ML pd4ml = new PD4ML();
            Dimension format = PD4Constants.A4;
            Dimension landscapeA4 = pd4ml.changePageOrientation(PD4Constants.A4);
            pd4ml.setPageSize(landscapeA4);
            pd4ml.setPageInsets(new java.awt.Insets(0, 0, 0, 0));
            pd4ml.addStyle("BODY{margin:0;padding:0}", true);
            pd4ml.setPageSizeMM(new Dimension(210, 297));
            pd4ml.setHtmlWidth(width);
            pd4ml.useTTF("java:fonts", true);
            pd4ml.outputFormat(PD4Constants.PNG24);
            pd4ml.setAuthorName("FIT");
            bos = new ByteArrayOutputStream();
            inr = new InputStreamReader(new ByteArrayInputStream(htmlData), "UTF-8");
            pd4ml.render(inr, bos);
            return bos.toByteArray();
        } catch (Exception e) {
            throw e;
        } finally {
            if (inr != null) {
                inr.close();
            }
            if (bos != null) {
                bos.close();
            }
            inr = null;
            bos = null;

        }
    }
    
    
}
