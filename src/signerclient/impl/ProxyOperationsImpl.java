/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.impl;

import com.xuxux.utils.commons.MyCryptoUtil;
import com.xuxux.utils.commons.MyStringUtil;
import com.xuxux.utils.commons.MyXmlUtil;
import com.xuxux.utils.files.MyFileUtil;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.xml.parsers.ParserConfigurationException;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import signerclient.exceptions.ProxyConfException;
import signerclient.factory.ImplFactory;
import signerclient.models.ProxyParams;
import signerclient.services.FileOperationsService;
import signerclient.services.ProxyOperationsService;

/**
 *
 * @author xuxux
 */
public class ProxyOperationsImpl implements ProxyOperationsService {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ProxyOperationsImpl.class);
    FileOperationsService fileOperationsService = ImplFactory.fileOperationsInstance();
    static final String PROXY_CONFIG_PATH = "proxy/proxy-conf.xml";
    static final String PROXY_CONFIG_DIR = "proxy";

    @Override
    public void saveProxyConfiguration(Map<String, String> proxyParams) throws ProxyConfException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            File confDir = new File(System.getProperty("user.home") + "\\" + PROXY_CONFIG_DIR);
            File confFile = new File(System.getProperty("user.home") + "\\" + PROXY_CONFIG_PATH);
            Document context = null;
            if(confFile == null || !confFile.exists()){
                confDir.mkdirs();
                context = MyXmlUtil.parse(new ByteArrayInputStream(fileOperationsService.getDefaultFileAsByteArray(PROXY_CONFIG_PATH)));
            }else {
                context = MyXmlUtil.parse(new FileInputStream(System.getProperty("user.home") + "\\" + PROXY_CONFIG_PATH));
            }
            
            context.getElementsByTagName(ProxyParams.PROXY_SET.toString()).item(0).setTextContent(proxyParams.get(ProxyParams.PROXY_SET.toString()));
            context.getElementsByTagName(ProxyParams.PROXY_HOSTNAME.toString()).item(0).setTextContent(proxyParams.get(ProxyParams.PROXY_HOSTNAME.toString()));
            context.getElementsByTagName(ProxyParams.PROXY_PORT.toString()).item(0).setTextContent(proxyParams.get(ProxyParams.PROXY_PORT.toString()));
            context.getElementsByTagName(ProxyParams.PROXY_USERNAME.toString()).item(0).setTextContent(proxyParams.get(ProxyParams.PROXY_USERNAME.toString()));
            context.getElementsByTagName(ProxyParams.PROXY_PASSWORD.toString()).item(0).setTextContent(proxyParams.get(ProxyParams.PROXY_PASSWORD.toString()));
            MyXmlUtil.transform(context, baos);
            baos.flush();
            MyFileUtil.writeByteArrayToFile(baos.toByteArray(), confFile.getPath());
        } catch (ParserConfigurationException ex) {
            LOGGER.error(ex.toString());
            throw new ProxyConfException(ProxyConfException.PCE_PARAMETER_SAVE_ERROR + ">>>ParserConfigurationException:", ex.getCause());
        } catch (SAXException ex) {
            LOGGER.error(ex.toString());
            throw new ProxyConfException(ProxyConfException.PCE_PARAMETER_SAVE_ERROR + ">>>SAXException:", ex.getCause());
        } catch (IOException ex) {
            LOGGER.error(ex.toString());
            throw new ProxyConfException(ProxyConfException.PCE_PARAMETER_SAVE_ERROR + ">>>IOException:", ex.getCause());
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            ex.printStackTrace();
            throw new ProxyConfException(ProxyConfException.PCE_PARAMETER_SAVE_ERROR + ">>>Exception:", ex.getCause());
        }finally{
            
        }
    }

    @Override
    public Map<String, String> getSavedProxyConfiguration() throws ProxyConfException {
        Map<String, String> confMap = new HashMap<String, String>(5);
        try {
//            File proxyConfDir = new File(System.getProperty("user.home") + "\\proxy");
//            if(!proxyConfDir.exists())
//                proxyConfDir.mkdir();
            File confFile = new File(System.getProperty("user.home") + "\\" + PROXY_CONFIG_PATH);
            Document context = null;
            if(confFile != null && confFile.exists()){
                context = MyXmlUtil.parse(new FileInputStream(confFile));
            }else{
                context = MyXmlUtil.parse(new ByteArrayInputStream(fileOperationsService.getDefaultFileAsByteArray(PROXY_CONFIG_PATH)));
            }
            
            confMap.put(ProxyParams.PROXY_SET.toString(), context.getElementsByTagName(ProxyParams.PROXY_SET.toString()).item(0).getTextContent());
            confMap.put(ProxyParams.PROXY_HOSTNAME.toString(), context.getElementsByTagName(ProxyParams.PROXY_HOSTNAME.toString()).item(0).getTextContent());
            confMap.put(ProxyParams.PROXY_PORT.toString(), context.getElementsByTagName(ProxyParams.PROXY_PORT.toString()).item(0).getTextContent());
            confMap.put(ProxyParams.PROXY_USERNAME.toString(), context.getElementsByTagName(ProxyParams.PROXY_USERNAME.toString()).item(0).getTextContent());
            confMap.put(ProxyParams.PROXY_PASSWORD.toString(), context.getElementsByTagName(ProxyParams.PROXY_PASSWORD.toString()).item(0).getTextContent());
            return confMap;
        } catch (ParserConfigurationException ex) {
            LOGGER.error(ex.toString());
            throw new ProxyConfException(ProxyConfException.PCE_GET_PARAMETER_ERROR + ">>>ParserConfigurationException:", ex.getCause());
        } catch (SAXException ex) {
            LOGGER.error(ex.toString());
            throw new ProxyConfException(ProxyConfException.PCE_GET_PARAMETER_ERROR + ">>>SAXException:", ex.getCause());
        } catch (IOException ex) {
            LOGGER.error(ex.toString());
            throw new ProxyConfException(ProxyConfException.PCE_GET_PARAMETER_ERROR + ">>>IOException:", ex.getCause());
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            throw new ProxyConfException(ProxyConfException.PCE_GET_PARAMETER_ERROR + ">>>Exception:", ex.getCause());
        }
    }

    @Override
    public void setSystemProxyConfiguration(Map<String, String> proxyParams) throws ProxyConfException {
        try {
            String hostname = (String) proxyParams.get(ProxyParams.PROXY_HOSTNAME.toString());
            String port = (String) proxyParams.get(ProxyParams.PROXY_PORT.toString());
            final String username = (String) proxyParams.get(ProxyParams.PROXY_USERNAME.toString());
            final String password = (String) proxyParams.get(ProxyParams.PROXY_PASSWORD.toString());
            if ((!MyStringUtil.isEmpty(hostname)) && (!MyStringUtil.isEmpty(port))) {
                Properties proxyProps = new Properties();
                proxyProps.setProperty("http.proxySet", "true");
                proxyProps.setProperty("http.proxyHost", hostname);
                proxyProps.setProperty("http.proxyPort", port);
                if ((!MyStringUtil.isEmpty(username)) && (!MyStringUtil.isEmpty(password))) {
                    proxyProps.setProperty("http.proxyUser", MyCryptoUtil.encode(username.getBytes()));
                    proxyProps.setProperty("http.proxyPassword", MyCryptoUtil.encode(password.getBytes()));
                    Authenticator.setDefault(new Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(username, password.toCharArray());
                        }
                    });
                }
                Properties systemProperties = System.getProperties();
                Enumeration<?> iterator = proxyProps.propertyNames();
                while (iterator.hasMoreElements()) {
                    String s = (String) iterator.nextElement();
                    systemProperties.put(s, proxyProps.get(s));
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.toString());
            throw new ProxyConfException(ProxyConfException.PCE_SET_SYSTEM_PARAMETER_ERROR + ">>>Exception:", e.getCause());
        }

    }
    

}
