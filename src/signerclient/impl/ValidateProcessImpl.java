/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.impl;

import com.xuxux.utils.commons.MyCryptoUtil;
import com.xuxux.utils.commons.MyXmlUtil;
import com.xuxux.utils.files.MyFileUtil;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.codec.binary.Base64;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import signerclient.services.ValidateProcessService;
import signerclient.ws.ValidatorResult;
import signerclient.wsclient.Invoker;

/**
 *
 * @author xuxux
 */
public class ValidateProcessImpl implements ValidateProcessService {

    @Override
    public String getDefaultDocument() throws Exception {
        ByteArrayOutputStream baos = null;
        try {
            ClassLoader cl = this.getClass().getClassLoader();
            java.io.InputStream in = cl.getResourceAsStream("METAINF/fitsignedenv.xml");
            Document document = MyXmlUtil.parse(in);

            baos = new ByteArrayOutputStream();
            MyXmlUtil.transform(document, baos);
            byte[] data = baos.toByteArray();
            return MyCryptoUtil.commonsEncodeBase64(data);
        } catch (ParserConfigurationException pe) {
            throw new ParserConfigurationException(pe.toString());
        } catch (SAXException se) {
            throw new SAXException(se);
        } catch (IOException ioe) {
            throw new IOException(ioe.toString());
        } catch (Exception e) {
            throw new Exception(e.toString());
        } finally {
            if (baos != null)
                baos.close();
            baos = null;
        }
    }

    @Override
    public ValidatorResult invokeValidateMethod(String dataToBeValidated, String endpoint) throws Exception {
        try {
            return Invoker.validate(dataToBeValidated, endpoint);
        } catch (Exception e) {
            throw new Exception(e.toString());
        }
    }

    @Override
    public String getSelectedDocument(Document xmlDoc) throws Exception {
        ByteArrayOutputStream baos = null;
        try {
            baos = new ByteArrayOutputStream();
            MyXmlUtil.transform(xmlDoc, baos);
            byte[] data = baos.toByteArray();
            return MyCryptoUtil.commonsEncodeBase64(data);
        } catch (Exception e) {
            throw new Exception(e.toString());
        } finally {
            if (baos != null)
                baos.close();
            baos = null;
        }
    }

    @Override
    public Document encapsulateInvWithEnv(Document inv) throws Exception {
        
        try {
            ClassLoader cl = this.getClass().getClassLoader();
            java.io.InputStream in = cl.getResourceAsStream("METAINF/capsule.xml");
            Document capsule = MyXmlUtil.parse(in);
            Node invoiceNode = inv.getElementsByTagNameNS("*", "Invoice").item(0);
            String preInv = invoiceNode.getPrefix();
            System.out.println("xuxux>>>namespaceuri:" + invoiceNode.getNamespaceURI());
//            capsule.createAttributeNS(invoiceNode.getNamespaceURI(), preInv);
            if(null != preInv){
                Element invElem = (Element)invoiceNode;
                String prefix = invElem.getPrefix();
                System.out.println("xuxux>>>invoice prefix: " + prefix);
                NamedNodeMap namedNodeMap = capsule.getDocumentElement().getAttributes();
                System.out.println("*******Named NodeMap of capsule");
                for(int i = 0; i < namedNodeMap.getLength(); i++){
                    System.out.println("xuxux>>>" + i + ":" + namedNodeMap.item(i).getNodeName());
                }
                namedNodeMap = inv.getDocumentElement().getAttributes();
                System.out.println("*******Named NodeMap of invoice");
                for(int i = 0; i < namedNodeMap.getLength(); i++){
                    System.out.println("xuxux>>>" + i + ":" + namedNodeMap.item(i).getNodeName());
                }
            }
            Node elementList = capsule.getElementsByTagNameNS("*","ElementList").item(0);
            elementList.appendChild(capsule.importNode(invoiceNode, true));
            Element capsuleRoot = capsule.getDocumentElement();
            capsuleRoot.setAttributeNS(invoiceNode.getNamespaceURI(), "xmlns", preInv);
            MyXmlUtil.prettyPrint(capsule);
            return capsule;
        } catch (Exception e) {
           throw new Exception(e);
        }
    }

    @Override
    public String getSelectedDocumentFromDocumentResponse(Document xmlDoc) throws Exception {
        ByteArrayOutputStream baos = null;
        try {
            Element documentRequest = xmlDoc.getDocumentElement();
            Document envDoc = MyXmlUtil.parse(new ByteArrayInputStream(MyFileUtil.unzip(MyCryptoUtil.commonsDecodeBase64byteArray(documentRequest.getElementsByTagName("binaryData").item(0).getTextContent()))));
            baos = new ByteArrayOutputStream();
            MyXmlUtil.transform(envDoc, baos);
            byte[] data = baos.toByteArray();
            return MyCryptoUtil.commonsEncodeBase64(data);
        } catch (Exception e) {
            throw new Exception(e.toString());
        }finally{
            if(baos != null){
                baos.flush();
                baos.close();
            }
            baos = null;
                
            
        }
    }
    
    
}
