/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.services;

import java.util.Map;
import signerclient.models.CERTVKNMAPPINGROOT;

/**
 *
 * @author xuxux
 */
public interface ConfigOperationService {
    
    public void saveConfiguration(Map<String, String> configParams,String earcSignerPath,String customPath) throws Exception;
    
    public Map<String, String> getSavedConfiguration() throws Exception;
    
     public void saveCertVknMapping(CERTVKNMAPPINGROOT.CUSTOMER[] customers, String absoluteCustomPath) throws Exception;
    
    public Map<String, String> getSavedCertVknMapping(String customPath) throws Exception;
}
