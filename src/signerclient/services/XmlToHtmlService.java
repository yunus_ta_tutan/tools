/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.services;

import java.io.File;

/**
 *
 * @author xuxux
 */
public interface XmlToHtmlService {
    
   public byte[] convertFromXmlToHtml(File xmlFile, String xsltFilePath, boolean isDefaultXslt) throws Exception;
    
}
