/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.services;

/**
 *
 * @author xuxux
 */
public interface HtmlToPdfService {

    public byte[] convertFromHtmlToPdf(byte[] htmlData) throws Exception;
    
}
