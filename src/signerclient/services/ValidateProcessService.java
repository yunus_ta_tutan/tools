/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.services;

import org.w3c.dom.Document;
import signerclient.ws.ValidatorResult;

/**
 *
 * @author xuxux
 */
public interface ValidateProcessService {
    
    public String getDefaultDocument() throws Exception;
    
    public String getSelectedDocument(Document xmlDoc) throws Exception;
    
    public ValidatorResult invokeValidateMethod(String dataToBeValidated, String endpoint) throws Exception;
    
    public Document encapsulateInvWithEnv(Document inv) throws Exception;
    
    public String getSelectedDocumentFromDocumentResponse(Document xmlDoc) throws Exception;
    
}
