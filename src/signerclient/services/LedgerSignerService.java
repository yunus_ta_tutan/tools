/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.services;

import com.fit.ledger.ws.client.SignDocumentHeaders;
import com.fit.ledger.ws.client.SignDocumentRequest;
import com.fit.ledger.ws.client.SignDocumentResponse;
import com.fit.ledger.ws.client.SignSOAPMessageFault;
import javax.xml.ws.Holder;
import org.w3c.dom.Document;

/**
 *
 * @author xuxux
 */
public interface LedgerSignerService {

    public SignDocumentResponse callSignLedger(Holder<SignDocumentHeaders> headers, SignDocumentRequest parameters, String endPoint) throws Exception;

    public SignDocumentResponse callSignBerat(Holder<SignDocumentHeaders> headers, SignDocumentRequest parameters, String endPoint) throws Exception;

    public SignDocumentResponse callSignSoapMessage(Holder<SignDocumentHeaders> headers, SignDocumentRequest parameters, String endPoint) throws SignSOAPMessageFault;

    public String changeSenderVkn(Document doc, String vkn) throws Exception;

    public Document getDefaultDocument(boolean isBerat) throws Exception;
}
