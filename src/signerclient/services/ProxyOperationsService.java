/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.services;

import java.util.Map;
import signerclient.exceptions.ProxyConfException;

/**
 *
 * @author xuxux
 */
public interface ProxyOperationsService {
    
    public void saveProxyConfiguration(Map<String,String> proxyParams) throws ProxyConfException;
    
    public Map<String,String> getSavedProxyConfiguration() throws ProxyConfException;
    
    public void setSystemProxyConfiguration(Map<String,String> proxyParams) throws ProxyConfException;
    
}
