/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.services;

/**
 *
 * @author xuxux
 */
public interface HtmlToPngService {
    
    public byte[] convertFromHtmlToPng(byte[] htmlData, int height,int width) throws Exception;
    
}
