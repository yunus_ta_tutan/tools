/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.services;

import java.util.Date;

/**
 *
 * @author xuxux
 */
public interface InvGeneratorService {

    public void generateZippedInvoices(String targetFolderPath, int zipCount, long xmlInZipCount, int startFrom, Date issueDate) throws Exception;

    public void generateZippedEnvelope(String targetFolderPath, int zipCount, long xmlInZipCount, int startFrom, Date issueDate) throws Exception;

}
