package signerclient.services;

import java.io.File;
import java.io.IOException;

public interface FileOperationsService {

    public String getSqlitePath();

    public String getFilePathUnderMetaINF(String fileName);

    public String getFilePathUnderMetaINFAsDecoded(String fileName);

    public String convertPathString(String path);

    public byte[] readBytesFromFile(File file) throws IOException;

    public byte[] getDefaultFileAsByteArray(String fileName) throws Exception;
    
    public String getProxyConfSaveDirectory() throws Exception;
    
    public String getClassPath(Class clazz) throws Exception;
    
}
