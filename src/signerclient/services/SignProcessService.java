/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.services;

import org.w3c.dom.Document;
import signerclient.ws.SignerResult;

/**
 *
 * @author xuxux
 */
public interface SignProcessService {
    
    public Document getDefaultDocument() throws Exception;
    
    public SignerResult invokeSignMethod(String dataToBeSigned, String endPoint) throws Exception;
    
    public String changeSenderVkn(Document doc, String vkn) throws Exception;
    
    public byte[] localXadesBesSign(Document doc, String alias,int mode) throws Exception;
    
    public Document localWssSignatureForSendDocument(String alias,String pass,String providerPath,String zippedFilePath) throws Exception;
    
    public Document localWssSignatureForGetBatchStatus(String alias, String pass,String providerPath,String paketId) throws Exception;
    
    public byte[] localXadesASignArchiveReportWithMA3(byte[] zippedData,String root_dir, String pfx_pass,String pkcs11_pass) throws Exception;
    
    public byte[] localXadesASignArchiveInvoiceWithMA3(byte[] zippedData, String root_dir, String pfx_pass, String pkcs11_pass) throws Exception;
    
    public byte[] localPAdesSignPDFWithMA3(byte[] pdfData, String root_dir, String pfx_pass, String pkcs11_pass) throws Exception;
    
}
