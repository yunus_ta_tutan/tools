/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.services;

import java.io.OutputStream;
import java.util.List;
import signerclient.models.ExtractedUblInfo;

/**
 *
 * @author xuxux
 */
public interface FolderExtractService {
    public String[] getAllXmlFileNamesUnderFolder(String folderPath) throws Exception;
    
    public ExtractedUblInfo[] extractInfosFromFile(String filePath) throws Exception;
    
    public void getExcelList(List<ExtractedUblInfo> list, OutputStream os, String sheetName) throws Exception;
    
    
}
