package signerclient.test;

import com.xuxux.utils.commons.MyXmlUtil;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;

import org.w3c.dom.Document;

import tr.gov.tubitak.uekae.esya.api.asn.x509.ECertificate;
import tr.gov.tubitak.uekae.esya.api.common.util.LicenseUtil;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.Context;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.ValidationResult;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.XMLSignature;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.XMLSignatureException;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.config.Config;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.document.DOMDocument;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.resolver.OfflineResolver;

public class XuxuxVerifyOperation {

    protected static String ROOT_DIR = "D:/StaticFiles/MA3_JAVA_API_202";										// root directory of project
    protected static String CONFIG = "D:/StaticFiles/MA3_JAVA_API_202/config";								// config file path
    protected static String BASE_DIR = "D:/StaticFiles/MA3_JAVA_API_202";										// base directory where signatures created
    // protected static String ROOT_DIR ="D:\\Workspaces\\indigo\\SignerWorkspace\\signer\\src\\META-INF"; // root directory of project
    // protected static String CONFIG = "D:\\Workspaces\\indigo\\SignerWorkspace\\signer\\src\\META-INF\\config"; // config file path
    // protected static String BASE_DIR = "D:\\Workspaces\\indigo\\SignerWorkspace\\signer\\src\\META-INF"; // base directory where signatures created
    protected static String POLICY_FILE;																				// certificate validation policy file path
    protected static String POLICY_FILE_CRL;																			// path of policy file without OCSP
    protected static String PFX_FILE;																					// PFX path
    protected static String PFX_PASS;																					// PFX password
    protected static String PIN;																						// smartcard PIN
    protected static ECertificate CERTIFICATE;																				// certificate of PFX
    protected static PrivateKey PRIVATE_KEY;																				// private key of PFX
    protected static OfflineResolver POLICY_RESOLVER;																			// policy resolver for profile examples
    public static final int[] OID_POLICY_P2 = new int[]{2, 16, 792, 1, 61, 0, 1, 5070, 3, 1, 1};
    public static final int[] OID_POLICY_P3 = new int[]{2, 16, 792, 1, 61, 0, 1, 5070, 3, 2, 1};
    public static final int[] OID_POLICY_P4 = new int[]{2, 16, 792, 1, 61, 0, 1, 5070, 3, 3, 1};
    private static final String ENVELOPE_XML = // sample XML document used for enveloped signature
            "<envelope>\n" + "  <data id=\"data1\">\n" + "    <item>Item 1</item>\n" + "    <item>Item 2</item>\n"
            + "    <item>Item 3</item>\n" + "  </data>\n" + "</envelope>\n";

    /**
     * Initialize paths and other variables
     */
    static {

        // URL root = SampleBase.class.getResource("../../../../../../../../../");
        // String classPath = root.getPath();
        // File binDir = new File(classPath);

        // ROOT_DIR = binDir.getParent();
        ROOT_DIR = "D:/StaticFiles/MA3_JAVA_API_202";
        BASE_DIR = ROOT_DIR + "/testdata/";
        CONFIG = ROOT_DIR + "/config/xmlsignature-config.xml";
        POLICY_FILE = ROOT_DIR + "/config/certval-policy.xml";
        POLICY_FILE_CRL = ROOT_DIR + "/config/certval-policy-test-crl.xml";
        PFX_FILE = ROOT_DIR + "/sertifika deposu/072801_test2.pfx";
        PFX_PASS = "072801";
        PIN = "12345";

        System.out.println("Base dir : " + BASE_DIR);

        POLICY_RESOLVER = new OfflineResolver();
        POLICY_RESOLVER.register("urn:oid:2.16.792.1.61.0.1.5070.3.1.1", ROOT_DIR + "/config/profiller/Elektronik_Imza_Kullanim_Profilleri_Rehberi.pdf",
                "text/plain");
        POLICY_RESOLVER.register("urn:oid:2.16.792.1.61.0.1.5070.3.2.1", ROOT_DIR + "/config/profiller/Elektronik_Imza_Kullanim_Profilleri_Rehberi.pdf",
                "text/plain");
        POLICY_RESOLVER.register("urn:oid:2.16.792.1.61.0.1.5070.3.3.1", ROOT_DIR + "/config/profiller/Elektronik_Imza_Kullanim_Profilleri_Rehberi.pdf",
                "text/plain");

        try {

            KeyStore ks = KeyStore.getInstance("PKCS12");

            ks.load(new FileInputStream(PFX_FILE), PFX_PASS.toCharArray());

            String alias = ks.aliases().nextElement();
            PRIVATE_KEY = (PrivateKey) ks.getKey(alias, PFX_PASS.toCharArray());
            Certificate cert = ks.getCertificate(alias);
            CERTIFICATE = new ECertificate(cert.getEncoded());
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    public static Context createContext() {

        Context context = null;
        try {
            context = new Context(BASE_DIR);
            context.setConfig(new Config(CONFIG));
            
        } catch (XMLSignatureException e) {
            e.printStackTrace();
        }
        return context;
    }

    public static ValidationResult validate(Document document) throws Exception {
        Context context = createContext();
        context.addExternalResolver(POLICY_RESOLVER);
        InputStream inputStream = new FileInputStream("D:\\Workspaces\\indigo\\SignerWorkspace\\FitSignerApi\\src\\lisans\\BES_lisans.xml");
        LicenseUtil.setLicenseXml(inputStream);
        XMLSignature signature = XMLSignature.parse(new DOMDocument(document, ""), context);
        // no parameters, use the certificate in key info
        ValidationResult result = signature.verify();
        System.out.println(result.toXml());
        return result;
    }

    public static void main(String[] args) {
        try {
            Document document2 = MyXmlUtil.parse(new FileInputStream("C:\\Users\\xuxux\\Desktop\\TarikAbi\\burak.xml"));
            XuxuxVerifyOperation.validate(document2);
//            
//            Document document1 = MyXmlUtil.parse(new FileInputStream("C:\\Users\\xuxux\\Desktop\\TarikAbi\\fatura.xml"));
//            XuxuxVerifyOperation.validate(document1);

//            Document document = MyXmlUtil.parse(new FileInputStream("C:\\Users\\xuxux\\Desktop\\TarikAbi\\PMC2014000000618.xml"));
//            XuxuxVerifyOperation.validate(document);

        } catch (Exception e) {
           e.printStackTrace();
        }
    }
}
