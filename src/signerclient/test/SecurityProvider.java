/*
 * $Id: prod/jprov_sfnt/samples/safenet/ptkj/samples/provider/SecurityProvider.java 1.1 2009/11/05 10:30:06EST Sorokine, Joseph (jsorokine) Exp  $
 * $Author: Sorokine, Joseph (jsorokine) $
 *
 * Copyright (c) 2002 Safenet Technologies
 * All Rights Reserved - Proprietary Information of Safenet Technologies
 * Not to be Construed as a Published Work.
 *
 * $Source: prod/jprov_sfnt/samples/safenet/ptkj/samples/provider/SecurityProvider.java $
 * $Revision: 1.1 $
 * $Date: 2009/11/05 10:30:06EST $
 * $State: Exp $
 */
package signerclient.test;

import javax.crypto.*;
import javax.crypto.spec.*;

import java.security.*;
import java.security.interfaces.*;
import java.security.spec.*;

import java.util.*;

public class SecurityProvider
{
    public SecurityProvider(String name) throws Exception
    {
        m_provider = Security.getProvider(name);

        if (m_provider == null)
        {
            throw new Exception("Provider " + name + " not found");
        }

        loadProperties();
    }

    public Provider getProvider()           {return m_provider;}

    public Set getCiphers()                 {return (Set)m_ciphers.clone();}
    public Vector getKeyGenerators()        {return (Vector)m_keyGenerators.clone();}
    public Vector getKeyPairGenerators()    {return (Vector)m_keyPairGenerators.clone();}
    public Vector getDigests()              {return (Vector)m_digests.clone();}
    public Vector getSignatures()           {return (Vector)m_signatures.clone();}
    public Vector getMacs()                 {return (Vector)m_macs.clone();}
    public Vector getKeyAgreements()        {return (Vector)m_keyAgreements.clone();}
    public Vector getSecretKeyFactories()   {return (Vector)m_secretKeyFactories.clone();}
    public Vector getKeyFactories()         {return (Vector)m_keyFactories.clone();}
    public Vector getAlgorithmParameters()  {return (Vector)m_algorithmParameters.clone();}
    public Vector getKeyStores()            {return (Vector)m_keyStores.clone();}
    public Vector getSecureRandoms()        {return (Vector)m_secureRandoms.clone();}
    public Vector getMisc()                 {return (Vector)m_misc.clone();}

    public String getName()                 {return m_provider.getName();}

    public void dumpCiphers()
    {
        System.out.println("Ciphers:");
        dumpProperties(m_ciphers);
    }

    public void dumpKeyGenerators()
    {
        System.out.println("KeyGenerators:");
        dumpProperties(m_keyGenerators);
    }

    public void dumpKeyPairGenerators()
    {
        System.out.println("KeyPairGenerators:");
        dumpProperties(m_keyPairGenerators);
    }

    public void dumpDigests()
    {
        System.out.println("Digests:");
        dumpProperties(m_digests);
    }

    public void dumpSignatures()
    {
        System.out.println("Signatures:");
        dumpProperties(m_signatures);
    }

    public void dumpMacs()
    {
        System.out.println("Macs:");
        dumpProperties(m_macs);
    }

    public void dumpKeyAgreements()
    {
        System.out.println("KeyAgreements:");
        dumpProperties(m_keyAgreements);
    }

    public void dumpSecretKeyFactories()
    {
        System.out.println("SecretKeyFactories:");
        dumpProperties(m_secretKeyFactories);
    }

    public void dumpKeyFactories()
    {
        System.out.println("KeyFactories:");
        dumpProperties(m_keyFactories);
    }

    public void dumpAlgorithmParameters()
    {
        System.out.println("AlgorithmParameters:");
        dumpProperties(m_algorithmParameters);
    }

    public void dumpKeyStores()
    {
        System.out.println("KeyStores:");
        dumpProperties(m_keyStores);
    }

    public void dumpSecureRandoms()
    {
        System.out.println("SecureRandoms:");
        dumpProperties(m_secureRandoms);
    }

    public void dumpMisc()
    {
        System.out.println("Miscelaneous:");
        dumpProperties(m_misc);
    }

    public void dumpAll()
    {
        dumpCiphers();
        dumpDigests();
        dumpMacs();
        dumpSignatures();
        dumpSecretKeyFactories();
        dumpKeyFactories();
        dumpKeyGenerators();
        dumpKeyAgreements();
        dumpKeyPairGenerators();
        dumpAlgorithmParameters();
        dumpSecureRandoms();
        dumpKeyStores();
        dumpMisc();
    }

    void dumpProperties(Set set)
    {
        String property = null;

        for (Iterator it = set.iterator(); it.hasNext(); )
        {
            property = (String)it.next();
            System.out.println("    " + property);
        }
    }

    void loadProperties()
    {
        String property = null;

        m_ciphers = new TreeSet();
        m_keyGenerators = new TreeSet();
        m_keyPairGenerators = new TreeSet();
        m_digests = new TreeSet();
        m_signatures = new TreeSet();
        m_macs = new TreeSet();
        m_keyAgreements = new TreeSet();
        m_secretKeyFactories = new TreeSet();
        m_keyFactories = new TreeSet();
        m_algorithmParameters = new TreeSet();
        m_keyStores = new TreeSet();
        m_secureRandoms = new TreeSet();
        m_misc = new TreeSet();

        for (Enumeration e = m_provider.propertyNames(); e.hasMoreElements(); )
        {
            property = (String)e.nextElement();

            if (property.startsWith("Cipher.") || property.startsWith("Alg.Alias.Cipher."))
            {
                m_ciphers.add(property.substring(property.indexOf("Cipher.")+"Cipher.".length()));
            }
            else if (property.startsWith("KeyGenerator.") || property.startsWith("Alg.Alias.KeyGenerator."))
            {
                m_keyGenerators.add(property.substring(property.indexOf("KeyGenerator.")+"KeyGenerator.".length()));
            }
            else if (property.startsWith("KeyPairGenerator.") || property.startsWith("Alg.Alias.KeyPairGenerator."))
            {
                m_keyPairGenerators.add(property.substring(property.indexOf("KeyPairGenerator.")+"KeyPairGenerator.".length()));
            }
            else if (property.startsWith("MessageDigest.") || property.startsWith("Alg.Alias.MessageDigest."))
            {
                m_digests.add(property.substring(property.indexOf("MessageDigest.")+"MessageDigest.".length()));
            }
            else if (property.startsWith("Signature.") || property.startsWith("Alg.Alias.Signature."))
            {
                m_signatures.add(property.substring(property.indexOf("Signature.")+"Signature.".length()));
            }
            else if (property.startsWith("Mac.") || property.startsWith("Alg.Alias.Mac."))
            {
                m_macs.add(property.substring(property.indexOf("Mac.")+"Mac.".length()));
            }
            else if (property.startsWith("KeyAgreement.") || property.startsWith("Alg.Alias.KeyAgreement."))
            {
                m_keyAgreements.add(property.substring(property.indexOf("KeyAgreement.")+"KeyAgreement.".length()));
            }
            else if (property.startsWith("SecretKeyFactory.") || property.startsWith("Alg.Alias.SecretKeyFactory."))
            {
                m_secretKeyFactories.add(property.substring(property.indexOf("SecretKeyFactory.")+"SecretKeyFactory.".length()));
            }
            else if (property.startsWith("KeyFactory.") || property.startsWith("Alg.Alias.KeyFactory."))
            {
                m_keyFactories.add(property.substring(property.indexOf("KeyFactory.")+"KeyFactory.".length()));
            }
            else if (property.startsWith("AlgorithmParameters.") || property.startsWith("Alg.Alias.AlgorithmParameters."))
            {
                m_algorithmParameters.add(property.substring(property.indexOf("AlgorithmParameters.")+"AlgorithmParameters.".length()));
            }
            else if (property.startsWith("KeyStore.") || property.startsWith("Alg.Alias.KeyStore."))
            {
                m_keyStores.add(property.substring(property.indexOf("KeyStore.")+"Keystore.".length()));
            }
            else if (property.startsWith("SecureRandom."))
            {
                m_secureRandoms.add(property.substring(property.indexOf("SecureRandom.")+"SecureRandom.".length()));
            }
            else
            {
                m_misc.add(property);
            }
        }
    }

    Provider m_provider;

    TreeSet m_ciphers;
    TreeSet m_keyGenerators;
    TreeSet m_keyPairGenerators;
    TreeSet m_digests;
    TreeSet m_signatures;
    TreeSet m_macs;
    TreeSet m_keyAgreements;
    TreeSet m_secretKeyFactories;
    TreeSet m_keyFactories;
    TreeSet m_algorithmParameters;
    TreeSet m_keyStores;
    TreeSet m_secureRandoms;
    TreeSet m_misc;
}
