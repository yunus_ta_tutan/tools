/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.test.ws;

import com.xuxux.utils.files.MyFileUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import signerclient.factory.ImplFactory;
import signerclient.services.SignProcessService;

/**
 *
 * @author xuxux
 */
//@javax.jws.WebService(
//                      serviceName = "eArsivSignerService",
//                      portName = "eArsivSignerPort",
//                      targetNamespace = "http://signature.fitcons.com",
//                      wsdlLocation = "http://signature.fitcons.com/WSAdresi?wsdl",
//                      endpointInterface = "com.fit.earsiv.signer.service.ws.EArsivSignaturePortType")
@javax.jws.WebService(
        serviceName = "eArsivSignerService",
        portName = "eArsivSignerPort",
        targetNamespace = "http://signature.fitcons.com",
        endpointInterface = "signerclient.test.ws.EArsivSignaturePortType")
public class EArsivSignaturePortTypeImpl implements EArsivSignaturePortType {
    
    private static String configPath;

    private static final Logger LOGGER = LoggerFactory.getLogger(EArsivSignaturePortTypeImpl.class);
    
    public EArsivSignaturePortTypeImpl(){
        configPath = "D:/ARCMA3";
    }
    
    public EArsivSignaturePortTypeImpl(String path){
        configPath = path;
    }

    /* (non-Javadoc)
     * @see com.fit.earsiv.signer.service.ws.EArsivSignaturePortType#signInvoice(com.fit.earsiv.signer.service.ws.SignInvoiceRequestType  parameter )*
     */
    @Override
    public SignInvoiceResponseType signInvoice(SignInvoiceRequestType parameter) throws EArsivInvoiceFaultMessage {
        return null;
    }

    /* (non-Javadoc)
     * @see com.fit.earsiv.signer.service.ws.EArsivSignaturePortType#signReport(com.fit.earsiv.signer.service.ws.SignReportRequestType  parameter )*
     */
    @Override
    public SignReportResponseType signReport(SignReportRequestType parameter) throws EArsivReportFaultMessage {
        LOGGER.info("Executing operation signInvoice");
        SignProcessService signProcessService = ImplFactory.signProcessInstance();
        SignReportResponseType signReportResponseType;
        try {
            byte[] signedXml = signProcessService.localXadesASignArchiveReportWithMA3(parameter.getBinaryData(), "C:/ARCMA3", null, "975998");
            signReportResponseType = new SignReportResponseType();
            signReportResponseType.setDetail("Signed successfully");
            SignReportResponseType.Result r = new SignReportResponseType.Result(signerclient.test.ws.Result.SUCCESS);
            signReportResponseType.setResult(r);
            signReportResponseType.setSignedData(MyFileUtil.zip("SignedReportData", signedXml));
            return signReportResponseType;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
    }

}
