/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.test.ws;

import com.fit.gib.getuserlist.GIBGetUserList;
import com.fit.gib.getuserlist.ICGIBGetUserList;
import com.xuxux.utils.files.MyFileUtil;
import javax.xml.ws.BindingProvider;
import signerclient.utils.CryptoUtil;

/**
 *
 * @author xuxux
 */
public class GibGetUserListMain {
    
    public static void main(String[] args) {
        GIBGetUserList getUserList = new GIBGetUserList();
		
		ICGIBGetUserList icgibGetUserList = getUserList.getHttpSoap11();
		((BindingProvider) icgibGetUserList).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, "https://efaturagibws.fitbulut.com/ICGIBUserList/GIBGetUserList.svc");
		
		      MyFileUtil.writeByteArrayToFile(icgibGetUserList.getPKUserList(), "D:/TMP/out.zip");
    }
    
}
