/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.test.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 *
 * @author xuxux
 */
@WebService(targetNamespace = "http://signature.fitcons.com", name = "eArsivSignaturePortType")
@XmlSeeAlso({ObjectFactory.class})
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface EArsivSignaturePortType {

    @WebResult(name = "signInvoiceResponseType", targetNamespace = "http://signature.fitcons.com", partName = "parameter")
    @WebMethod(action = "signInvoice")
    public SignInvoiceResponseType signInvoice(
            @WebParam(partName = "parameter", name = "signInvoiceRequestType", targetNamespace = "http://signature.fitcons.com") SignInvoiceRequestType parameter
    ) throws EArsivInvoiceFaultMessage;

    @WebResult(name = "signReportResponseType", targetNamespace = "http://signature.fitcons.com", partName = "parameter")
    @WebMethod(action = "signReport")
    public SignReportResponseType signReport(
            @WebParam(partName = "parameter", name = "signReportRequestType", targetNamespace = "http://signature.fitcons.com") SignReportRequestType parameter
    ) throws EArsivReportFaultMessage;
}
