
package signerclient.test.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Detail" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Result">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Result" type="{http://signature.fitcons.com}result"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="signedData" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "detail",
    "result",
    "signedData"
})
@XmlRootElement(name = "signInvoiceResponseType")
public class SignInvoiceResponseType {

    @XmlElement(name = "Detail", required = true)
    protected String detail;
    @XmlElement(name = "Result", required = true)
    protected SignInvoiceResponseType.Result result;
    @XmlElement(required = true)
    protected byte[] signedData;

    /**
     * Gets the value of the detail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetail() {
        return detail;
    }

    /**
     * Sets the value of the detail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetail(String value) {
        this.detail = value;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link SignInvoiceResponseType.Result }
     *     
     */
    public SignInvoiceResponseType.Result getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link SignInvoiceResponseType.Result }
     *     
     */
    public void setResult(SignInvoiceResponseType.Result value) {
        this.result = value;
    }

    /**
     * Gets the value of the signedData property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getSignedData() {
        return signedData;
    }

    /**
     * Sets the value of the signedData property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setSignedData(byte[] value) {
        this.signedData = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Result" type="{http://signature.fitcons.com}result"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "result"
    })
    public static class Result {
    	
    	public Result(){
    		this(signerclient.test.ws.Result.SUCCESS);
    	}
    	
    	public Result(signerclient.test.ws.Result result){
    		this.result = result;
    	}

        @XmlElement(name = "Result", required = true)
        protected signerclient.test.ws.Result result;

        /**
         * Gets the value of the result property.
         * 
         * @return
         *     possible object is
         *     {@link signerclient.test.ws.Result }
         *     
         */
        public signerclient.test.ws.Result getResult() {
            return result;
        }

        /**
         * Sets the value of the result property.
         * 
         * @param value
         *     allowed object is
         *     {@link signerclient.test.ws.Result }
         *     
         */
        public void setResult(signerclient.test.ws.Result value) {
            this.result = value;
        }

    }

}
