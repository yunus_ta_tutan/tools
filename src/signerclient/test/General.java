/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.test;

import com.xuxux.utils.commons.MyXmlUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author xuxux
 */
public class General {

    private static final Logger LOGGER = LoggerFactory.getLogger(General.class);

    public static void main(String atgs[]) {
        
        String FOLDER = "C:\\Users\\xuxux\\Desktop\\aaaaaa_sefikj\\";
        try {
            Document env_doc = MyXmlUtil.parse(new FileInputStream(new File(FOLDER + "81F43FD0-B21D-4751-87A7-685656967208.xml")));
            NodeList inv_list =  env_doc.getElementsByTagNameNS("*", "Invoice");
            for(int i = 0; i < inv_list.getLength(); i++){
                Node inv = inv_list.item(i);
                MyXmlUtil.transform(inv, new FileOutputStream(FOLDER + i + ".xml"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
