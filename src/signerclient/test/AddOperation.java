/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.test;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 *
 * @author xuxux
 */
@WebService
public class AddOperation {
    
    @WebMethod
    public int add(int a, int b){
        return a + b;
    }
    
}
