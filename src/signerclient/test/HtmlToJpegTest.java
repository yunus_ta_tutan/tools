/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.test;

import com.xuxux.utils.files.MyFileUtil;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 *
 * @author xuxux
 */
public class HtmlToJpegTest {
    
    private static String testFolder = "C:\\Users\\xuxux\\Desktop\\test\\";
    
    public static void main(String[] args) {
//        HtmlImageGenerator imageGenerator = new HtmlImageGenerator();
////        imageGenerator.loadHtml("<b>Hello World!</b> Please goto <a title=\"Goto Google\" href=\"http://www.google.com\">Google</a>.");
//        try {
//            imageGenerator.loadHtml(MyFileUtil.readFileAsString(testFolder + "Pronet.html"));
//            
//        } catch (IOException ex) {
//            ex.printStackTrace();
//            return;
//        }
//        imageGenerator.saveAsImage(testFolder + "pronet.png");
//        imageGenerator.saveAsHtmlWithMap(testFolder + "hello-world.html", testFolder + "pronet2.png");

        JEditorPane ed;
        try {
//            ed = new JEditorPane(new URL("https://earsivtest.ingbank.com.tr/clientEArsivServicesPort.svc?wsdl"));
            ed = new JEditorPane("text/html", MyFileUtil.readFileAsString(testFolder + "Pronet.html"));
            ed.setSize(500, 500);
            BufferedImage image = new BufferedImage(ed.getWidth(), ed.getHeight(),
                    BufferedImage.TYPE_INT_ARGB);
            SwingUtilities.paintComponent(image.createGraphics(),
                    ed,
                    new JPanel(),
                    0, 0, image.getWidth(), image.getHeight());
            ImageIO.write((RenderedImage) image, "png", new File(testFolder + "google.png"));
        } catch (IOException ex) {
            Logger.getLogger(HtmlToJpegTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
