/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.test;

import com.xuxux.utils.commons.MyCryptoUtil;
import com.xuxux.utils.commons.MyXmlUtil;
import com.xuxux.utils.files.MyFileUtil;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import signerclient.factory.ImplFactory;
import signerclient.services.ValidateProcessService;

/**
 *
 * @author xuxux
 */
public class extracter {

    public static void main(String[] args) throws Exception {
        ByteArrayOutputStream baos = null;
        try {
            FileInputStream fis = new FileInputStream("C:\\Users\\xuxux\\Desktop\\test\\d0937a99-cb91-4c73-a15f-018b28161a3d.XML");
            Document xmlDoc = MyXmlUtil.parse(fis);
            ValidateProcessService validateProcessService = ImplFactory.validateProcessInstance();
//           MyFileUtil.writeByteArrayToFile(MyFileUtil.unzip(MyCryptoUtil.commonsDecodeBase64byteArray(xmlDoc.getElementsByTagName("binaryData").item(0).getTextContent())), "D:/test11");
            Document envDoc = MyXmlUtil.parse(new ByteArrayInputStream(MyFileUtil.unzip(MyCryptoUtil.commonsDecodeBase64byteArray(xmlDoc.getElementsByTagNameNS("*","binaryData").item(0).getTextContent()))));
            baos = new ByteArrayOutputStream();
            MyXmlUtil.transform(envDoc, baos);
            baos.flush();
            byte[] data = baos.toByteArray();
            MyFileUtil.writeByteArrayToFile(data, "D:/test11");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (baos != null) {
                baos.flush();
                baos.close();
            }
            baos = null;
        }
    }
}
