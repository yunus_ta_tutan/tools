/*
 * $Id: prod/jprov_sfnt/samples/safenet/ptkj/samples/provider/ProviderDetails.java 1.1 2009/11/05 10:30:05EST Sorokine, Joseph (jsorokine) Exp  $
 * $Author: Sorokine, Joseph (jsorokine) $
 *
 * Copyright (c) 2002 Safenet Technologies
 * All Rights Reserved - Proprietary Information of Safenet Technologies
 * Not to be Construed as a Published Work.
 *
 * $Source: prod/jprov_sfnt/samples/safenet/ptkj/samples/provider/ProviderDetails.java $
 * $Revision: 1.1 $
 * $Date: 2009/11/05 10:30:05EST $
 * $State: Exp $
 */
package signerclient.test;

import au.com.safenet.crypto.provider.SAFENETProvider;
import com.xuxux.utils.files.MyFileUtil;
import java.io.File;
import java.security.*;
//import au.com.safenet.crypto.provider.SAFENETProvider;
import java.io.IOException;
import java.math.BigInteger;
import java.security.cert.CertificateException;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextPane;
import signerclient.tokenService.XuxuxKeyingDataProviderImpl;

/**
 * Provider Details. Options include the ability to list the names of all the
 * loaded providers or to display the details of a named provider.
 */
public class ProviderDetails {

    /**
     * simplified use of System.out.println
     */
    static void println(String s) {
        System.out.println(s);
    }

    static void usage() {
        println("java ...ProviderDetails  [-list] -providerName <providername>");
        println("");
        println("list               list available providers");
        println("providername       Name of an existing provider");
        println("");

        System.exit(1);
    }

    public static void test01() {
        XuxuxKeyingDataProviderImpl.setProviderPath("C:/Windows/System32/cryptoki.dll");
        XuxuxKeyingDataProviderImpl.setLogPane(new JTextPane());
        try {
            XuxuxKeyingDataProviderImpl.refreshAllTokens();
        } catch (Exception ex) {
            Logger.getLogger(ProviderDetails.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) throws Exception {
//        test01();
        byte[] data = MyFileUtil.readBytesFromFile(new File("C:\\Users\\QQx23\\Documents\\Earsivefaturaentegrasyon\\ef604f9e-8e64-572a-a6a9-cb2303579f19.zip"));
        
        MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(data, 0, data.length);
		BigInteger bi = new BigInteger(1, md.digest());
		System.out.println(String.format("%1$032X", bi));
    }

    public static void providerTest() {
        // Initialize WrappingKeyStore
        Provider p = new au.com.safenet.crypto.provider.slot0.SAFENETProvider();
        p.setProperty("name", "SAFENET");
        p.setProperty("library", "C:/Windows/System32/cryptoki.dll");
        p.setProperty("slot", 1 + "");
        if (null != Security.getProvider("SAFENET"))
            Security.removeProvider("SAFENET");
        Security.addProvider(p);

        println("cryptoki initialized : " + p.getInfo());
        KeyStore ks = null;
        try {
            ks = KeyStore.getInstance(null, p);
            ks.load(null, "8b8b8b8D".toCharArray());
            Enumeration<String> aliases = ks.aliases();
            while (aliases.hasMoreElements()) {
                String aliasKey = aliases.nextElement();
                System.out.println("Alias is:" + aliasKey);
                System.out.println(ks.getCertificate(aliasKey));
            }
        } catch (KeyStoreException ex) {
            ex.printStackTrace();
            return;
        } catch (IOException ex) {
            ex.printStackTrace();
            return;
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
            return;
        } catch (CertificateException ex) {
            ex.printStackTrace();
            return;
        }


        Provider[] pArray = Security.getProviders();
        for (Provider ps : pArray) {
            System.out.println(ps.getName());
        }

    }

    private static Provider loadProvider(long slotID, String alias) {

        try {
            System.out.println("PROVIDER>>> loading provider at slot: " + slotID);
            String conf = "name = shell\nlibrary = " + "C:/Windows/System32/cryptoki.dll" + "\nslotListIndex = " + slotID + "\ndescription = desc";
            Provider p = new SAFENETProvider();
            p.setProperty("name", "SAFENET");
            p.setProperty("library", "C:/Windows/System32/cryptoki.dll");
            p.setProperty("slot", slotID + "");
            Provider[] providers = Security.getProviders();
            for (Provider prov : providers) {
                System.out.println("XUXUX>>>Provider name:" + prov.getName());
            }
            System.out.println("-----------------------------");

            return p;

        } catch (Exception ex) {
            System.out.println("PROVIDER>>> error loading provider at slot: " + slotID + " " + ex.getMessage());
            ex.printStackTrace();
        }
        return null;
    }

    public static byte[] testLoadProviderAndSign(String alias) throws Exception {
        Provider prov = loadProvider(1, alias);
        System.out.println(prov);

        Security.addProvider(prov);
        if (null != Security.getProvider("SAFENET")) {
            System.out.println("name is  :" + prov.getInfo());
        }
        PrivateKey pkey = loadPrivateKey(prov, alias);
        System.out.println(pkey);

        Signature signature = Signature.getInstance("SHA256withRSA", prov);
        signature.initSign(pkey);
        signature.update(new byte[20]);
        return signature.sign();
    }

    static Provider AddProvider(String providerClass) throws Exception {
        Class c = Class.forName(providerClass);

        Provider p = (Provider) c.newInstance();

        Security.addProvider(p);

        return p;
    }

    private static PrivateKey loadPrivateKey(Provider p11Prov, String keyAlias) {

        try {
            System.out.println("PROVIDER>>> loading private key for alias: " + keyAlias + " pin: " + "8b8b8b8D");
            KeyStore ks = KeyStore.getInstance("CRYPTOKI", p11Prov);
            ks.load(null, "1234".toCharArray());

            Enumeration aliases = ks.aliases();
            while (aliases.hasMoreElements()) {
                String alias = (String) aliases.nextElement();

                System.out.println("PROVIDER>>> found alias: " + alias);

            }
            return (PrivateKey) ks.getKey(keyAlias, "1234".toCharArray());
        } catch (Exception ex) {
            System.out.println("PROVIDER>>> error loading private key for alias: " + keyAlias + " error: " + ex.getMessage());
            ex.printStackTrace();
        }
        return null;
    }
}
