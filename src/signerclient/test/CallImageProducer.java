/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.test;

import java.net.MalformedURLException;
import java.net.URL;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

/**
 *
 * @author xuxux
 */
public class CallImageProducer {
    
    private String urlRoot;
    private String servletRoot;
    private String url;
    
    public static URL cretaeUrl(String urlRoot, String servletRoot, String companyNo,String senderCompanyNo, String receiverCompanyNo, String invoiceNo, String documentType, String outputType) throws MalformedURLException{
        StringBuilder builder = new StringBuilder();
        builder.append(urlRoot);
        if(urlRoot.endsWith("/"))
        {
            builder.append(servletRoot);
        }else{
            builder.append("/").append(servletRoot).append("?");
        }
        builder.append("companyNo=").append(companyNo).append("&").append("senderCompanyNo=").append(senderCompanyNo).append("&").append("receiverCompanyNo=").append(receiverCompanyNo).append("&").append("invoiceNo=").append(invoiceNo).append("&").append("documentType=").append(documentType).append("&").append("outputType=").append(outputType);
        System.out.println(builder.toString());
        URL url = new URL(builder.toString());
        return url;
    }
    
    public static void main(String[] args){
        try {
           URL url = cretaeUrl("http://piserver:51000", "einvoice/ImageProducer", "cn", "scn", "rcn", "in", "dt", "ot");
            System.out.println(url.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
