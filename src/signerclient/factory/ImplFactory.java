/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.factory;

import javax.swing.JTextPane;
import signerclient.impl.ConfigOperationImpl;
import signerclient.impl.FileOperationsImpl;
import signerclient.impl.FolderExtractImpl;
import signerclient.impl.HtmlToPdfImpl;
import signerclient.impl.InvGeneratorImpl;
import signerclient.impl.LedgerSignerImpl;
import signerclient.impl.ProxyOperationsImpl;
import signerclient.impl.SignProcessImpl;
import signerclient.impl.ValidateProcessImpl;
import signerclient.impl.XmlToHtmlImpl;
import signerclient.services.ConfigOperationService;
import signerclient.services.FileOperationsService;
import signerclient.services.FolderExtractService;
import signerclient.services.HtmlToPdfService;
import signerclient.services.InvGeneratorService;
import signerclient.services.LedgerSignerService;
import signerclient.services.ProxyOperationsService;
import signerclient.services.SignProcessService;
import signerclient.services.ValidateProcessService;
import signerclient.services.XmlToHtmlService;

/**
 *
 * @author xuxux
 */
public class ImplFactory {

    public static SignProcessService signProcessInstance() {
        return new SignProcessImpl();
    }

    public static ValidateProcessService validateProcessInstance() {
        return new ValidateProcessImpl();
    }

    public static FolderExtractService folderExtractInstance(JTextPane pane) {
        FolderExtractImpl folderExtractImpl = new FolderExtractImpl();
        folderExtractImpl.setLogPane(pane);
        return folderExtractImpl;
    }
    
    public static InvGeneratorService invGeneratorInstance(){
        return new InvGeneratorImpl();
    }

    public static LedgerSignerService ledgerSignerInstance() {
        return new LedgerSignerImpl();
    }

    public static FileOperationsService fileOperationsInstance() {
        return new FileOperationsImpl();
    }

    public static XmlToHtmlService xmlToHtmlInstance() {
        return new XmlToHtmlImpl();
    }

    public static HtmlToPdfService htmlToPdfInstance() {
        return new HtmlToPdfImpl();
    }
    
    public static ProxyOperationsService proxyOperationsInstance(){
        return new ProxyOperationsImpl();
    }
    
    public static ConfigOperationService confitOperationsInstance(){
        return new ConfigOperationImpl();
    }
    
}
