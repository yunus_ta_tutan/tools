/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.wsclient;

import javax.xml.ws.BindingProvider;
import wsClients.DocumentReturnType;
import wsClients.EFaturaFaultMessage;

/**
 *
 * @author xuxux
 */
public class ResendInvoker {

    public static DocumentReturnType sendDocument(wsClients.DocumentType document,String endpoint) throws EFaturaFaultMessage {
        wsClients.EFatura service = new wsClients.EFatura();
        wsClients.EFaturaPortType port = service.getEFaturaSoap12();
        ((BindingProvider) port).getRequestContext().put(
                    BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpoint);
        return port.sendDocument(document);
    }
    
}
