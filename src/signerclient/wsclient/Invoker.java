/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.wsclient;

import com.fit.ledger.ws.client.SignDocumentHeaders;
import com.fit.ledger.ws.client.SignDocumentRequest;
import com.fit.ledger.ws.client.SignDocumentResponse;
import com.fit.ledger.ws.client.SignSOAPMessageFault;
import com.fit.ledger.ws.client.SignerService;
import com.fitcons.earchive.EArsivInvoicePortType;
import com.fitcons.earchive.EArsivInvoiceService;
import com.fitcons.earchive.GetInvoiceDocumentFault;
import com.fitcons.earchive.GetInvoiceDocumentRequestType;
import com.fitcons.earchive.GetInvoiceDocumentResponseType;
import com.fitcons.earchive.SendEnvelopeFault;
import com.fitcons.earchive.SendInvoiceFault;
import com.fitcons.earchive.SendInvoiceRequestType;
import com.fitcons.earchive.SendInvoiceResponseType;
import java.io.StringWriter;
import java.io.Writer;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import signerclient.ws.ServiceException;
import signerclient.ws.ServiceException_Exception;
import signerclient.ws.Signer;
import signerclient.ws.SignerResult;
import signerclient.ws.ValidatorResult;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import signerclient.test.ws.EArsivInvoiceFaultMessage;

/**
 *
 * @author xuxux
 */
public class Invoker {

    public static SignerResult sign(java.lang.String envelope, String endPoint) throws ServiceException_Exception {
        try {
            signerclient.ws.SignerService service = new signerclient.ws.SignerService();
            Signer port = service.getSignerPort();
            ((BindingProvider) port).getRequestContext().put(
                    BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);
            return port.sign(envelope);
        } catch (Exception e) {
            throw new ServiceException_Exception(e.toString(), new ServiceException());
        }

    }

    public static ValidatorResult validate(java.lang.String envelope, String endPoint) throws ServiceException_Exception {
        try {
            signerclient.ws.SignerService service = new signerclient.ws.SignerService();

            Signer port = service.getSignerPort();
            ((BindingProvider) port).getRequestContext().put(
                    BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);

//            ((BindingProvider) port).getRequestContext().put(BindingProviderProperties.REQUEST_TIMEOUT, 1200000);
//            ((BindingProvider) port).getRequestContext().put(JAXWSProperties.CONNECT_TIMEOUT, 1200000);
            return port.validate(envelope);
        } catch (Exception e) {
            throw new ServiceException_Exception(e.toString(), new ServiceException());
        }

    }

    public static SignDocumentResponse callSignLedger(Holder<SignDocumentHeaders> headers, SignDocumentRequest parameters, String endPoint) throws Exception {
        try {
            SignerService ledgerSignerService = new SignerService();
            com.fit.ledger.ws.client.Signer port = ledgerSignerService.getSignerPort();
            ((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);
            return port.signElectronicLedgerDocument(headers, parameters);
        } catch (Exception e) {
            throw e;
        }
    }

    public static SignDocumentResponse callSignBerat(Holder<SignDocumentHeaders> headers, SignDocumentRequest parameters, String endPoint) throws Exception {
        try {
            SignerService ledgerSignerService = new SignerService();
            com.fit.ledger.ws.client.Signer port = ledgerSignerService.getSignerPort();
            ((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);
            return port.signBeratDocument(headers, parameters);
        } catch (Exception e) {
            throw e;
        }
    }

    public static SignDocumentResponse callsignSOAPMessage(Holder<SignDocumentHeaders> headers, SignDocumentRequest parameters, String endPoint) throws SignSOAPMessageFault {
        try {
            SignerService ledgerSignerService = new SignerService();
            com.fit.ledger.ws.client.Signer port = ledgerSignerService.getSignerPort();
            ((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);
            return port.signSOAPMessage(headers, parameters);
        } catch (SignSOAPMessageFault ssmf) {
            throw ssmf;
        } 
    }

    public static SendInvoiceResponseType callSendInvoice(SendInvoiceRequestType request, String endPoint, final String ws_user, final String ws_pass) throws EArsivInvoiceFaultMessage, SendInvoiceFault {
        Authenticator myAuth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(ws_user, ws_pass.toCharArray());
            }
        };

        try {
            Authenticator.setDefault(myAuth);
            EArsivInvoiceService eArsivInvoiceService = new EArsivInvoiceService();
            EArsivInvoicePortType port = eArsivInvoiceService.getEArsivInvoicePort();
//        endPoint += "?source=WS&ip=SignerWsClient&VKN_TCKN=" + request.getSenderID() + "&un=" + ws_user;
            ((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);
            ((BindingProvider) port).getRequestContext().put(BindingProvider.USERNAME_PROPERTY, ws_user);
            ((BindingProvider) port).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, ws_pass);
            return port.sendInvoice(request);
        } catch (SendInvoiceFault sif) {
            sif.printStackTrace();
            EArsivInvoiceFaultMessage arsivInvoiceFaultMessage = new EArsivInvoiceFaultMessage(sif.getMessage(), sif);
            throw arsivInvoiceFaultMessage;
        } catch (Exception e) {
            e.printStackTrace();
            EArsivInvoiceFaultMessage arsivInvoiceFaultMessage = new EArsivInvoiceFaultMessage(e.getMessage(), e);
            throw arsivInvoiceFaultMessage;
        }

    }

    public static SendInvoiceResponseType callSendEnvelope(SendInvoiceRequestType request, String endPoint, final String ws_user, final String ws_pass) throws EArsivInvoiceFaultMessage, SendInvoiceFault, SendEnvelopeFault {

        Authenticator myAuthenticator = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(ws_user, ws_pass.toCharArray());
            }
        };
        try {
            Authenticator.setDefault(myAuthenticator);
            EArsivInvoiceService eArsivInvoiceService = new EArsivInvoiceService();
            EArsivInvoicePortType port = eArsivInvoiceService.getEArsivInvoicePort();
//        if(!MyStringUtil.isNullOrEmpty(ws_user) && "_FILE".equalsIgnoreCase(ws_user)){
//            endPoint += "?source=FILE&ip=SignerWsClient&VKN_TCKN=" + request.getSenderID() + "&un=" + ws_user;
//        }else{
//            endPoint += "?source=WS&ip=SignerWsClient&VKN_TCKN=" + request.getSenderID() + "&un=" + ws_user;
//        }

            ((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);
            ((BindingProvider) port).getRequestContext().put(BindingProvider.USERNAME_PROPERTY, ws_user);
            ((BindingProvider) port).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, ws_pass);

            return port.sendEnvelope(request);
        } catch (SendEnvelopeFault sef) {
            sef.printStackTrace();
            EArsivInvoiceFaultMessage arsivInvoiceFaultMessage = new EArsivInvoiceFaultMessage(sef.getMessage(), sef);
            throw arsivInvoiceFaultMessage;
        } catch (Exception e) {
            e.printStackTrace();
            EArsivInvoiceFaultMessage arsivInvoiceFaultMessage = new EArsivInvoiceFaultMessage(e.getMessage(), e);
            throw arsivInvoiceFaultMessage;
        }
    }

    public static GetInvoiceDocumentResponseType callGetInvoiceDocument(GetInvoiceDocumentRequestType request, String endPoint, final String ws_user, final String ws_pass) throws EArsivInvoiceFaultMessage, SendInvoiceFault, SendEnvelopeFault {

        Authenticator myAuthenticator = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(ws_user, ws_pass.toCharArray());
            }
        };
        try {
            Authenticator.setDefault(myAuthenticator);
            EArsivInvoiceService eArsivInvoiceService = new EArsivInvoiceService();
            EArsivInvoicePortType port = eArsivInvoiceService.getEArsivInvoicePort();

            ((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);
            ((BindingProvider) port).getRequestContext().put(BindingProvider.USERNAME_PROPERTY, ws_user);
            ((BindingProvider) port).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, ws_pass);

            return port.getInvoiceDocument(request);
        } catch (GetInvoiceDocumentFault gidf) {
            gidf.printStackTrace();
            EArsivInvoiceFaultMessage arsivInvoiceFaultMessage = new EArsivInvoiceFaultMessage(gidf.getMessage(), gidf);
            throw arsivInvoiceFaultMessage;
        } catch (Exception e) {
            e.printStackTrace();
            EArsivInvoiceFaultMessage arsivInvoiceFaultMessage = new EArsivInvoiceFaultMessage(e.getMessage(), e);
            throw arsivInvoiceFaultMessage;
        }
    }

    public static <T> String marshal(T obj) throws JAXBException {
        StringWriter sw = new StringWriter();
        marshal(obj, sw);
        return sw.toString();
    }

    public static <T> void marshal(T obj, Writer wr) throws JAXBException {
        JAXBContext ctx = JAXBContext.newInstance(obj.getClass());
        Marshaller m = ctx.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        m.marshal(obj, wr);
    }

}
