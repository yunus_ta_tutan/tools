/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.filters;

import java.io.File;
import java.io.FilenameFilter;


/**
 *
 * @author xuxux
 */
public class XmlFileFilter implements FilenameFilter{

    @Override
    public boolean accept(File dir, String name) {
        try {
            if(name.endsWith(".xml") || name.endsWith(".XML"))
                return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
}
