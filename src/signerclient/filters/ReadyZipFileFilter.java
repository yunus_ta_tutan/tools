/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.filters;

import java.io.File;
import java.io.FilenameFilter;

/**
 *
 * @author xuxux
 */
public class ReadyZipFileFilter implements FilenameFilter{

    @Override
    public boolean accept(File dir, String name) {
        if(!name.startsWith("_") && name.endsWith(".zip"))
            return true;
        return false;
    }
    
}
