/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.filters;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author xuxux
 */
public class XmlJFileChooserFilter extends FileFilter{

    @Override
    public boolean accept(File f) {
        if(f.exists() && f.isFile() && (f.getName().endsWith(".xml") || f.getName().endsWith(".XML")))
            return true;
        return false;
    }

    @Override
    public String getDescription() {
        return "Choose an xml file to be signed.";
    }
    
}
