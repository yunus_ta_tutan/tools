/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.filters;

import java.io.File;
import javax.swing.filechooser.FileFilter;
import signerclient.utils.LogUtil;

/**
 *
 * @author xuxux
 */
public class XsltFileFilter extends FileFilter{

    @Override
    public boolean accept(File f) {
        try {
            if(f.exists() && f.isFile() && f.getName().endsWith(".xslt"))
                return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public String getDescription() {
        return "Accepts all xslt files and rejects all other things";
    }
    
}
