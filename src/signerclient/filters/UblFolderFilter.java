/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.filters;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author xuxux
 */
public class UblFolderFilter extends FileFilter{

    @Override
    public boolean accept(File f) {
        try {
            if(f.exists() && f.isDirectory())
                return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public String getDescription() {
        return "Accepts Just Folders";
    }
    
}
