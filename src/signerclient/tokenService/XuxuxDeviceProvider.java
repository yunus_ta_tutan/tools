/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.tokenService;

import org.apache.ws.security.components.crypto.Merlin;
import java.util.Properties;
import org.apache.ws.security.components.crypto.CredentialException;
import org.apache.ws.security.util.Loader;

import sun.security.pkcs11.SunPKCS11;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.cert.CertStore;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.X509CRL;
import java.util.Collections;
import java.util.Enumeration;
import java.util.logging.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author xuxux
 */
public class XuxuxDeviceProvider extends Merlin {

    final static Logger logger = LoggerFactory.getLogger(XuxuxDeviceProvider.class);
    static KeyStore ks;

    public XuxuxDeviceProvider() {

    }

    public XuxuxDeviceProvider(InputStream input, String storepass) {
        try {
            SunPKCS11 akisProvider = new SunPKCS11(input);
        Security.addProvider(akisProvider);
            ks = KeyStore.getInstance("PKCS11", akisProvider);
            ks.load(input, (storepass == null || storepass.length() == 0) ? new char[0] : storepass.toCharArray());
            Enumeration<String> aliases = ks.aliases();
            while (aliases.hasMoreElements()) {
                String alias = aliases.nextElement();
                logger.info(alias);
            }
        } catch (IOException e) {
            logger.debug(e.getMessage(), e);
        } catch (NoSuchAlgorithmException ex) {
            java.util.logging.Logger.getLogger(XuxuxDeviceProvider.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CertificateException ex) {
            java.util.logging.Logger.getLogger(XuxuxDeviceProvider.class.getName()).log(Level.SEVERE, null, ex);
        } catch (KeyStoreException ex) {
            java.util.logging.Logger.getLogger(XuxuxDeviceProvider.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public XuxuxDeviceProvider(Properties properties) throws CredentialException, IOException {
        this(properties, Loader.getClassLoader(Merlin.class));
    }

    public XuxuxDeviceProvider(Properties properties, ClassLoader loader) throws CredentialException, IOException {
        loadProperties(properties, loader);
    }

    @Override
    public void loadProperties(Properties properties, ClassLoader loader) throws CredentialException, IOException {
        if (properties == null) {
            return;
        }
        this.properties = properties;
        //
        // Load the provider(s)
        //
        String provider = properties.getProperty(CRYPTO_KEYSTORE_PROVIDER);
        if (provider != null) {
            provider = provider.trim();
        }
        String certProvider = properties.getProperty(CRYPTO_CERT_PROVIDER);
        if (certProvider != null) {
            setCryptoProvider(certProvider);
        }
        //
        // Load the KeyStore
        //
        String alias = properties.getProperty(KEYSTORE_ALIAS);
        if (alias != null) {
            alias = alias.trim();
            defaultAlias = alias;
        }
        String keyStoreLocation = properties.getProperty(KEYSTORE_FILE);
        if (keyStoreLocation == null) {
            keyStoreLocation = properties.getProperty(OLD_KEYSTORE_FILE);
        }
        String keyStorePassword = properties.getProperty(KEYSTORE_PASSWORD, "security");
        if (keyStorePassword != null) {
            keyStorePassword = keyStorePassword.trim();
        }
        String keyStoreType = properties.getProperty(KEYSTORE_TYPE, KeyStore.getDefaultType());
        if (keyStoreType != null) {
            keyStoreType = keyStoreType.trim();
        }
        if (keyStoreLocation != null) {
            keyStoreLocation = keyStoreLocation.trim();
            InputStream is = loadInputStream(loader, keyStoreLocation);

            try {
                keystore = load(is, keyStorePassword, provider, keyStoreType);
                logger.debug("The KeyStore " + keyStoreLocation + " of type " + keyStoreType + " has been loaded");
            } finally {
                if (is != null) {
                    is.close();
                }
            }
        } else {
            keystore = load(null, keyStorePassword, provider, keyStoreType);
        }

        //
        // Load the TrustStore
        //
        String trustStorePassword = properties.getProperty(TRUSTSTORE_PASSWORD, "changeit");
        if (trustStorePassword != null) {
            trustStorePassword = trustStorePassword.trim();
        }
        String trustStoreType = properties.getProperty(TRUSTSTORE_TYPE, KeyStore.getDefaultType());
        if (trustStoreType != null) {
            trustStoreType = trustStoreType.trim();
        }
        String loadCacerts = properties.getProperty(LOAD_CA_CERTS, "false");
        if (loadCacerts != null) {
            loadCacerts = loadCacerts.trim();
        }
        String trustStoreLocation = properties.getProperty(TRUSTSTORE_FILE);
        if (trustStoreLocation != null) {
            trustStoreLocation = trustStoreLocation.trim();
            InputStream is = loadInputStream(loader, trustStoreLocation);

            try {
                truststore = load(is, trustStorePassword, provider, trustStoreType);
                logger.debug("The TrustStore " + trustStoreLocation + " of type " + trustStoreType + " has been loaded");
                loadCACerts = false;
            } finally {
                if (is != null) {
                    is.close();
                }
            }
        } else if (Boolean.valueOf(loadCacerts).booleanValue()) {
            String cacertsPath = System.getProperty("java.home") + "/lib/security/cacerts";
            if (cacertsPath != null) {
                cacertsPath = cacertsPath.trim();
            }
            InputStream is = new FileInputStream(cacertsPath);
            try {
                String cacertsPasswd = properties.getProperty(TRUSTSTORE_PASSWORD, "changeit");
                if (cacertsPasswd != null) {
                    cacertsPasswd = cacertsPasswd.trim();
                }
                truststore = load(is, cacertsPasswd, null, KeyStore.getDefaultType());
                logger.debug("CA certs have been loaded");
                loadCACerts = true;
            } finally {
                if (is != null) {
                    is.close();
                }
            }
        } else {
            truststore = load(null, trustStorePassword, provider, trustStoreType);
        }
        //
        // Load the CRL file
        //
        String crlLocation = properties.getProperty(X509_CRL_FILE);
        if (crlLocation != null) {
            crlLocation = crlLocation.trim();
            InputStream is = loadInputStream(loader, crlLocation);

            try {
                CertificateFactory cf = getCertificateFactory();
                X509CRL crl = (X509CRL) cf.generateCRL(is);

                if (provider == null || provider.length() == 0) {
                    crlCertStore = CertStore.getInstance("Collection", new CollectionCertStoreParameters(Collections.singletonList(crl)));
                } else {
                    crlCertStore = CertStore.getInstance("Collection", new CollectionCertStoreParameters(Collections.singletonList(crl)), provider);
                }
                logger.debug("The CRL " + crlLocation + " has been loaded");
            } catch (Exception e) {
                logger.debug(e.getMessage(), e);
                throw new CredentialException(CredentialException.IO_ERROR, "ioError00", e);
            } finally {
                if (is != null) {
                    is.close();
                }
            }
        }
    }

    /**
     * Loads the keystore from an <code>InputStream </code>.
     * <p/>
     *
     * @param input <code>InputStream</code> to read from
     * @throws CredentialException
     */
    @Override
    public KeyStore load(InputStream input, String storepass, String provider, String type) throws CredentialException {
        if (ks != null)
            return ks;

        SunPKCS11 akisProvider = new SunPKCS11(input);
        Security.addProvider(akisProvider);

        try {
            if (provider == null || provider.length() == 0) {
                ks = KeyStore.getInstance(type);
            } else {
                ks = KeyStore.getInstance("PKCS11", akisProvider);
            }

            ks.load(input, (storepass == null || storepass.length() == 0) ? new char[0] : storepass.toCharArray());
            Enumeration<String> aliases = ks.aliases();
            while (aliases.hasMoreElements()) {
                String alias = aliases.nextElement();
                logger.info(alias);
            }
        } catch (IOException e) {
            logger.debug(e.getMessage(), e);
            throw new CredentialException(CredentialException.IO_ERROR, "ioError00", e);
        } catch (GeneralSecurityException e) {
            logger.debug(e.getMessage(), e);
            throw new CredentialException(CredentialException.SEC_ERROR, "secError00", e);
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
            throw new CredentialException(CredentialException.FAILURE, "error00", e);
        }
        return ks;
    }

    
    @Override
    public KeyStore getKeyStore(){
        return this.ks;
    }
    
    public void unload(){
        Security.removeProvider(ks.getProvider().getName());
        ks = null;
    }
}
