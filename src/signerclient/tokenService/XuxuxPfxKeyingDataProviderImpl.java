package signerclient.tokenService;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import signerclient.factory.ImplFactory;
import signerclient.helpers.xadesHelpers.CertInfo;
import signerclient.services.FileOperationsService;
import signerclient.services.SignProcessService;
import signerclient.utils.UIutils;


import xades4j.providers.KeyingDataProvider;
import xades4j.providers.SigningCertChainException;
import xades4j.providers.SigningKeyException;
import xades4j.verification.UnexpectedJCAException;

public class XuxuxPfxKeyingDataProviderImpl implements KeyingDataProvider {
    
    private static XuxuxPfxKeyingDataProviderImpl instance = null;
    private static KeyStore keystore = null;
    private static PrivateKey privateKey = null;
    private static X509Certificate certificate = null;
    private static Map<String, CertInfo> certificateMap = new HashMap<String, CertInfo>();
    private static String certAlias = null;
    private static String selectedAlias = null;
    private static boolean isInitialized = false;
    
    public static XuxuxPfxKeyingDataProviderImpl getInstance() {
        
        if (instance == null || isInitialized == false) {
            instance = new XuxuxPfxKeyingDataProviderImpl();
            loadPfx();
            setIsInitialized(true);
        }
        return instance;
    }
    
    private static byte[] getPfxInputStream() {
//        String location = "META-INF/testmm.pfx";
        // String location = "532638-testkurum01@test.com.tr.pfx";
        FileOperationsService fileOperationsService = ImplFactory.fileOperationsInstance();
        try {
            return fileOperationsService.getDefaultFileAsByteArray("pfx/testmm.pfx");
        } catch (Exception ex) {
            return null;
        }
//        return Thread.currentThread().getContextClassLoader().getResourceAsStream(location);
    }
    
    private static String getPfxPassword() {
        return "532638";
    }

    /**
     * Load pfx file to be used as keystore
     */
    public static void loadPfx() {
        if (isInitialized == false) {
            InputStream is = null;
            String password = "";
            try {
                // NOTE: these can be sent as parameters
//            is = getPfxInputStream();
                is = new ByteArrayInputStream(getPfxInputStream());
                
                password = UIutils.askForPin("Pin Giriniz!");

                // load the keystore
                keystore = KeyStore.getInstance("pkcs12", "SunJSSE");
                keystore.load(is, password.toCharArray());
                getCertificateMap().clear();
                
                Enumeration<String> aliasEnum = keystore.aliases();
                while (aliasEnum.hasMoreElements()) {
                    // cache cert, pkey and alias
                    String alias = aliasEnum.nextElement();
                    System.out.println("pfx alias:" + alias);
                    certAlias = alias;
                    certificate = (X509Certificate) keystore.getCertificate(alias);
                    privateKey = (PrivateKey) keystore.getKey(alias, password.toCharArray());
                    CertInfo ci = new CertInfo();
                    ci.setAlias(alias);
                    ci.setCert(certificate);
                    ci.setPrivateKey(privateKey);
                    getCertificateMap().put(alias, ci);
                }
                setIsInitialized(true);
            } catch (Exception e) {
                System.out.println("**Error: could not load pfx");
                e.printStackTrace();
            } finally {
                if (is != null) try {
                        is.close();
                    } catch (Exception e2) {
                        // TODO: handle exception
                    }
            }
        }
        
    }

    /**
     * @return the certificate
     */
    public static X509Certificate getCertificate() {
        return getCertificateMap().get(selectedAlias).getCert();
    }

    /**
     * @return the certAlias
     */
    public static String getCertAlias() {
        return certAlias;
    }

    /**
     * @param aSelectedAlias the selectedAlias to set
     */
    public static void setSelectedAlias(String aSelectedAlias) {
        selectedAlias = aSelectedAlias;
    }

    /**
     * @return the certificateMap
     */
    public static Map<String, CertInfo> getCertificateMap() {
        return certificateMap;
    }

    /**
     * @return the isInitialized
     */
    public static boolean isIsInitialized() {
        return isInitialized;
    }

    /**
     * @param aIsInitialized the isInitialized to set
     */
    public static void setIsInitialized(boolean aIsInitialized) {
        isInitialized = aIsInitialized;
    }
    
    @Override
    public List<X509Certificate> getSigningCertificateChain() throws SigningCertChainException, UnexpectedJCAException {
        // return certificate
        return Arrays.asList(getCertificate());
    }
    
    @Override
    public PrivateKey getSigningKey(X509Certificate arg0) throws SigningKeyException, UnexpectedJCAException {
        return getCertificateMap().get(selectedAlias).getPrivateKey();
    }
    
    public static void main(String[] args) throws Exception {
//
//		Document document = DomUtil.parse(new FileInputStream("U:/xmlsignature-config.xml"));
//		document.getDocumentElement().normalize();
//
//		Element validation = (Element) document.getElementsByTagName("validation").item(0);
//		Element certificateValidationPolicyFile_NES = (Element) validation.getElementsByTagName("certificate-validation-policy-file").item(0);
//		certificateValidationPolicyFile_NES.setTextContent("\""
//				+ "C:/Program Files (x86)/Apache Software Foundation/Tomcat 6.0_Tomcat6(32_bit)/webapps/signer/WEB-INF/classes/META-INF/config/" + "\"");
//		Element certificateValidationPolicyFile_MaliMuhur = (Element) validation.getElementsByTagName("certificate-validation-policy-file").item(1);
//		String texcont = "\"" + "C:/Program Files (x86)/Apache Software Foundation/Tomcat 6.0_Tomcat6(32_bit)/webapps/signer/WEB-INF/classes/META-INF/config/" + "\"";
//		certificateValidationPolicyFile_MaliMuhur.setTextContent(texcont.replaceAll(" ", "+"));
//		ByteArrayOutputStream baos = new ByteArrayOutputStream();
//
//		TransformerFactory transformerFactory = TransformerFactory.newInstance();
//		Transformer transformer = transformerFactory.newTransformer();
//		DOMSource domSource = new DOMSource(document);
//		StreamResult streamResult = new StreamResult(baos);
//		transformer.transform(domSource, streamResult);
//		
//		
//
//		document = DomUtil.parse(new ByteArrayInputStream(baos.toByteArray()));
//		validation = (Element) document.getElementsByTagName("validation").item(0);
//		certificateValidationPolicyFile_NES = (Element) validation.getElementsByTagName("certificate-validation-policy-file").item(0);
//		String path = certificateValidationPolicyFile_NES.getTextContent();
//		System.out.println(URLEncoder.encode(path, "UTF-8"));
        String path = URLEncoder.encode("C:/Program Files/Apache Software Foundation/Tomcat 7.0(64Bit)/webapps/signer/WEB-INF/classes/META-INF/testdata/", "UTF-8");
        System.out.println(URLDecoder.decode(path, "ANSI"));
        // XuxuxPfxKeyingDataProviderImpl.loadPfx();
        //
        // PrivateKey pkey = XuxuxPfxKeyingDataProviderImpl.getInstance().getSigningKey(null);
        // System.out.println(pkey);
        // List certs = XuxuxPfxKeyingDataProviderImpl.getInstance().getSigningCertificateChain();
        // System.out.println(certs);
    }
}
