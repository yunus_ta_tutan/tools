/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.tokenService;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.Security;
import java.security.Signature;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextPane;
import org.apache.commons.lang.ArrayUtils;
import signerclient.exceptions.TokenException;
import signerclient.models.CertInfo;
import signerclient.utils.LogUtil;
import sun.security.pkcs11.wrapper.CK_ATTRIBUTE;
import sun.security.pkcs11.wrapper.CK_C_INITIALIZE_ARGS;
import sun.security.pkcs11.wrapper.CK_SLOT_INFO;
import sun.security.pkcs11.wrapper.PKCS11;
import sun.security.pkcs11.wrapper.PKCS11Constants;
import sun.security.pkcs11.wrapper.PKCS11Exception;
import xades4j.providers.KeyingDataProvider;
import java.io.PrintWriter;
import java.io.StringWriter;
import sun.security.pkcs11.SunPKCS11;

/**
 *
 * @author xuxux
 */
public class XuxuxKeyingDataProviderImpl implements KeyingDataProvider {

    private static XuxuxKeyingDataProviderImpl instance;
    private static Map<String, CertInfo> certificateMap = new HashMap<String, CertInfo>(1);													// xuxux - betek
    private static Map<String, String> pinMap = new HashMap<String, String>();
    private static PKCS11 p11 = null;
    private static String selectedAlias = null;
    private static Map<String, String> vknAliasMap = new HashMap<String, String>();
    private static String providerPath = null;
    private static String[] defaultProvs = {"C:\\windows\\System32\\akisp11.dll", "C:\\windows\\System32\\cknfast.dll"};
    private static JTextPane logPane = null;

    public static XuxuxKeyingDataProviderImpl getInstance() {

        if (instance == null) {
            instance = new XuxuxKeyingDataProviderImpl();
        }
        return instance;
    }

    public static void setProviderPath(String path) {
        providerPath = path;
    }

    public static void setSelectedAlias(String alias) {
        selectedAlias = alias.trim();
    }

    public static void setAliasPin(String alias, String pin) {
        pinMap.put(alias.trim(), pin.trim());
        CertInfo ci = certificateMap.get(alias);
        if (null == ci.privateKey) {
            try {
                ci.privateKey = loadPrivateKey(ci.provider, alias, pin);
            } catch (TokenException ex) {
                Logger.getLogger(XuxuxKeyingDataProviderImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            certificateMap.put(alias, ci);
        }
    }

    public static void setVknAlias(String vkn, String alias) {
        vknAliasMap.put(vkn, alias.trim());
    }

    public static String getSelectedAliasWithVkn(String vkn) {
        return vknAliasMap.get(vkn).toString().trim();
    }

    // --------------------------------------------------------------------------
    @Override
    public List<X509Certificate> getSigningCertificateChain() {
        CertInfo certInfo = certificateMap.get(selectedAlias);
        if (certInfo != null) {
            List<X509Certificate> result = new LinkedList();
            result.add(certInfo.getCert());
            return result;
        }
        LogUtil.addWarning(logPane,"KEYING>>> no certificate found for the selected alias: " + selectedAlias);
        return null;
    }

    @Override
    public PrivateKey getSigningKey(X509Certificate signingCert) {
        CertInfo certInfo = certificateMap.get(selectedAlias);
        if (certInfo != null) {
            Security.removeProvider(certInfo.provider.getName());
            Security.addProvider(certInfo.provider);
            return certInfo.privateKey;
        }
        LogUtil.addWarning(logPane,"KEYING>>> no private key found for the selected alias: " + selectedAlias);
        return null;
    }

    // --------------------------------------------------------------------------
    public static void refreshAllTokens() throws Exception {
        LogUtil.addInfo(logPane, "PROVIDER>>> REFRESHING ALL TOKENS");
        Iterator<CertInfo> it;
        it = certificateMap.values().iterator();
        while (it.hasNext()) {
            CertInfo certInfo = (CertInfo) it.next();
            if (!checkAlive(certInfo)) it.remove();
        }

        PKCS11_RefreshSlots();

        it = certificateMap.values().iterator();
        while (it.hasNext()) {
            CertInfo certInfo = (CertInfo) it.next();
            if (certInfo.privateKey != null) continue; // already exists, not
            // new (prevent too many
            // sessions)
            Provider p11Prov = loadProvider(certInfo.slotID);
            if (p11Prov == null) {
                it.remove();
                continue;
            }
//            PrivateKey key = loadPrivateKey(p11Prov, certInfo.getAlias());
//            certInfo.privateKey = key;
            certInfo.provider = p11Prov;
        }
    }

    public static void PKCS11_RefreshSlots() {
        PKCS11_Init();
        try {
            LogUtil.addInfo(logPane, "PKCS11>>> REFRESH SLOTS");
            long[] slots = p11.C_GetSlotList(false);

            for (long slotID : slots) {
                CK_SLOT_INFO slotInfo = p11.C_GetSlotInfo(slotID);

                if ((slotInfo.flags & PKCS11Constants.CKF_TOKEN_PRESENT) == 0) continue;
                if (checkExists(slotID)) continue; // do not read slot if
                // already exists (prevent
                // too many sessions)

                List<CertInfo> certInfos = PKCS11_ReadSlot(slotID);
                if (null == certificateMap) certificateMap = new HashMap<String, CertInfo>();
                for (CertInfo certInfo : certInfos) {
                    certificateMap.put(certInfo.getAlias().trim(), certInfo);
                }
            }

        } catch (Exception e) {
            LogUtil.addError(logPane, "PKCS11>>> REFRESH SLOTS FAILED: " + e.getMessage());
        }
    }

    private static void PKCS11_Init() {
        if (p11 != null) return;
        try {
            LogUtil.addInfo(logPane, "PKCS11>>> INIT");
            CK_C_INITIALIZE_ARGS initArgs = new CK_C_INITIALIZE_ARGS();
            initArgs.flags = PKCS11Constants.CKF_OS_LOCKING_OK;
            initArgs.pReserved = null;
            if (null != providerPath) {
                p11 = PKCS11.getInstance(providerPath, "C_GetFunctionList", null, false);
            } else {
                for (String path : defaultProvs) {
                    File f = new File(path);
                    if (f.exists() && f.isFile()) {
                        p11 = PKCS11.getInstance(path, "C_GetFunctionList", null, false);
                        break;
                    }
                }
            }

            // p11 = PKCS11.getInstance("C:\\pkcs\\cknfast.dll",
            // "C_GetFunctionList", null, false);
        } catch (Exception e) {
            LogUtil.addError(logPane, e.getMessage());
        }
    }

    private static List<CertInfo> PKCS11_ReadSlot(long slotID) {

        long sessionID = 0;
        List result = new LinkedList();
        try {
            LogUtil.addInfo(logPane, "PKCS11>>> READING SLOT: " + slotID);
            sessionID = p11.C_OpenSession(slotID, PKCS11Constants.CKF_SERIAL_SESSION, null, null);
            LogUtil.addInfo(logPane, "PKCS11>>>sessionID: " + sessionID);

            CK_ATTRIBUTE[] attrs = new CK_ATTRIBUTE[]{new CK_ATTRIBUTE(PKCS11Constants.CKA_TOKEN, true),
                new CK_ATTRIBUTE(PKCS11Constants.CKA_CLASS, PKCS11Constants.CKO_CERTIFICATE)};
            long[] handles = PKCS11_FindObjects(p11, sessionID, attrs);

            for (long handle : handles) {

                attrs = new CK_ATTRIBUTE[]{new CK_ATTRIBUTE(PKCS11Constants.CKA_LABEL)};

                String cka_label = null;
                p11.C_GetAttributeValue(sessionID, handle, attrs);
                if (attrs[0].pValue != null) {
                    cka_label = new String(attrs[0].getCharArray());
                }


                CertInfo info = new CertInfo();
                info.setCert(PKCS11_LoadCert(p11, sessionID, handle));
                info.slotID = slotID;
                info.setAlias(cka_label);
                if (cka_label.contains("FwUpgradeCert")) {
                    LogUtil.addWarning(logPane, "Found FwUpgradeCert and will NOT add it to the certificateMap,");
                    LogUtil.addWarning(logPane, info.toString());
                    continue;
                }
                result.add(info);
                LogUtil.addInfo(logPane, info.toString());

            }
        } catch (Exception e) {
            LogUtil.addError(logPane, "PKCS11>>> ERROR READING SLOT: " + slotID + " " + e.getMessage());
        } 
        return result;
    }

    private static long[] PKCS11_FindObjects(PKCS11 p11, long sessionID, CK_ATTRIBUTE[] attrs) throws PKCS11Exception {

        long[] handles = {};
        /**
         * xuxux - betek test org: p11.C_FindObjectsInit(sessionID, attrs);
         */
        p11.C_FindObjectsInit(sessionID, attrs);
        while (true) {
            long[] h = p11.C_FindObjects(sessionID, 50);
            if (h.length == 0) {
                break;
            }
            handles = ArrayUtils.addAll(handles, h);
        }
        p11.C_FindObjectsFinal(sessionID);
        return handles;
    }

    private static X509Certificate PKCS11_LoadCert(PKCS11 p11, long sessionID, long oHandle) throws PKCS11Exception {

        try {
            CK_ATTRIBUTE[] attrs = new CK_ATTRIBUTE[]{new CK_ATTRIBUTE(PKCS11Constants.CKA_VALUE)};
            p11.C_GetAttributeValue(sessionID, oHandle, attrs);

            byte[] bytes = attrs[0].getByteArray();
            if (bytes == null) {
                throw new CertificateException("unexpectedly retrieved null byte array");
            }
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            return (X509Certificate) cf.generateCertificate(new ByteArrayInputStream(bytes));
        } catch (CertificateException e) {
            e.printStackTrace();
        }
        return null;

    }

    private static Provider loadProvider(long slotID) throws Exception {

        try {
            LogUtil.addInfo(logPane,"PROVIDER>>> loading provider at slot: " + slotID);
            String conf = "name = FITPKCS11\nlibrary = C:\\windows\\system32\\akisp11.dll\nslot = " + slotID;
            if (null != providerPath) {
                conf = "name = FITPKCS11\nlibrary = " + providerPath + "\nslot = " + slotID;
            } else {
                for (String path : defaultProvs) {
                    File f = new File(path);
                    if (f.exists() && f.isFile()) {
                        conf = "name = FITPKCS11\nlibrary = " + path + "\nslot = " + slotID;
                        break;
                    }
                }
            }

            // String conf =
            // "name = FITPKCS11\nlibrary = C:\\pkcs\\cknfast.dll\nslot = " +
            // slotID;

            /**
             * xuxux - betek provider test org: return new
             * sun.security.pkcs11.SunPKCS11(new
             * ByteArrayInputStream(conf.getBytes()));
             */
//            if (null != p11) {
//                p11.C_Finalize(null);
//            }
            Provider p = new SunPKCS11(new ByteArrayInputStream(conf.getBytes()));
//            Provider p = new SAFENETProvider();
//            Security.addProvider(p);
            return p;
        } catch (jprov.cryptoki.CryptokiException e) {
            LogUtil.addError(logPane,"xuxux>>>" + e.toString());
            throw e;
        } catch (Exception ex) {
            LogUtil.addError(logPane,"PROVIDER>>> error loading provider at slot: " + slotID + " " + ex.getMessage());
            throw ex;
        } catch (Throwable t) {
            StringWriter errors = new StringWriter();
            t.printStackTrace(new PrintWriter(errors));
            LogUtil.addError(logPane, "t.toString(): " + t.toString());
            LogUtil.addError(logPane, "t.printStackTrace(): " + errors.toString());
            throw new Exception(t.getMessage(), t.getCause());
        }

    }

    private static boolean checkAlive(CertInfo certInfo) {

        try {
            if (certInfo == null) return false;
            PrivateKey key = certInfo.privateKey;
            if (key == null) return false;

            Signature signature = Signature.getInstance("NONEwithRSA", certInfo.provider);
            signature.initSign(key);
            signature.update(new byte[20]);
            byte[] sig = signature.sign();
            return true;

        } catch (Exception e) {
            LogUtil.addError(logPane,"PROVIDER>>> cert info is not alive, removing: " + certInfo.getAlias());
        }
        return false;
    }

    private static boolean checkExists(long slotID) {
        for (CertInfo certInfo : certificateMap.values()) {
            if (certInfo.slotID == slotID) return true;
        }

        return false;
    }

    private static PrivateKey loadPrivateKey(Provider p11Prov, String keyAlias) {

        try {
            LogUtil.addInfo(logPane,"PROVIDER>>> loading private key for alias: " + keyAlias);
            /**
             * yunus - test org: KeyStore ks = KeyStore.getInstance("PKCS11",
             * p11Prov);
             */
            KeyStore ks = KeyStore.getInstance("PKCS11", p11Prov);
            ks.load(null, pinMap.get(keyAlias.trim()).toCharArray());

            Enumeration aliases = ks.aliases();
            while (aliases.hasMoreElements()) {
                String alias = (String) aliases.nextElement();
                if (!alias.equals(keyAlias)) {
                    // System.out.println("PROVIDER>>> found alias is different from what we expected: "
                    // + keyAlias);
                    continue;
                }
                LogUtil.addInfo(logPane,"PROVIDER>>> found alias: " + alias);
                return (PrivateKey) ks.getKey(alias, pinMap.get(keyAlias.trim()).toCharArray());
            }
        } catch (Exception ex) {
            LogUtil.addError(logPane,"PROVIDER>>> error loading private key for alias: " + keyAlias + " error: " + ex.getMessage());
        }
        return null;
    }

    // --------------------------------------------------------------------------
    public static Map<String, CertInfo> getCertificateMap() {
        return certificateMap;
    }

    public static void setLogPane(JTextPane textPane) {
        logPane = textPane;
    }

    /**
     * Sign the data with the private key corresponding to the specified alias
     *
     * @param dataToSign data to sign
     * @param alias alias of the key
     * @return signature
     */
    public static byte[] sign(byte[] dataToSign, String alias, String pin) throws TokenException {
        CertInfo certInfo = null;
        for (CertInfo ci : certificateMap.values()) {
            if (ci.getAlias().trim().equals(alias.trim())) certInfo = ci;
        }

        //no certinfo for alias
        if (certInfo == null) {
            LogUtil.addWarning(logPane, "KEYING>>> no cert info found for the selected alias: " + alias);
            throw new TokenException(TokenException.EC_NO_CERTINFO_FOR_ALIAS);
        }

        //no privatekey, it might not have been loaded because pin did not exist, try again
        if (certInfo.privateKey == null) {
            LogUtil.addWarning(logPane, "KEYING>>> no private key found for the selected cert info, trying to load pkey for alias: " + alias);
            certInfo.privateKey = loadPrivateKey(certInfo.provider, certInfo.getAlias(), pin);
            certInfo.pin = pin; //save correct pin for later fake pin check
        } else {
            //fake pin check
            if (!certInfo.pin.equals(pin)) throw new TokenException(TokenException.EC_PIN_INCORRECT);
        }

        //switch to the provider (we need to do this because brilliant JDK developers associated slotID with the provider)
        Security.removeProvider(certInfo.provider.getName());
        Security.addProvider(certInfo.provider);
        PrivateKey key = certInfo.privateKey;

        //finally, do the signing
        try {
            Signature signature = Signature.getInstance("NONEwithRSA");
            signature.initSign(key);
            signature.update(dataToSign);
            return signature.sign();
        } catch (Exception ex) {
            LogUtil.addError(logPane, "KEYING>>> signature exception on signing, alias: " + alias + " exception: " + ex.getMessage());
            throw new TokenException(TokenException.EC_SIGNING_FAILED, ex);
        }
    }

    //load private key, throws exceptions for failures
    private static PrivateKey loadPrivateKey(Provider p11Prov, String keyAlias, String pin) throws TokenException {
        try {
            LogUtil.addWarning(logPane, "PROVIDER>>> loading private key for alias: " + keyAlias);
            if (pin == null) {
                LogUtil.addWarning(logPane, "PROVIDER>>> won't load private key, no pin for alias: " + keyAlias);
                throw new TokenException(TokenException.EC_NO_PIN_FOR_ALIAS);
            }

            //get keystore with the provider reference
            KeyStore ks = KeyStore.getInstance("PKCS11", p11Prov);
            ks.load(null, pin.toCharArray()); //why on earth you fucking JDK need pin to just load keystore???

            //search for the alias to make sure that it exist
            Enumeration aliases = ks.aliases();
            while (aliases.hasMoreElements()) {
                String alias = (String) aliases.nextElement();
                if (!alias.trim().equals(keyAlias.trim())) continue;
                LogUtil.addWarning(logPane, "PROVIDER>>> found alias: " + alias);
                return (PrivateKey) ks.getKey(alias, pin.toCharArray());
            }
            throw new TokenException(TokenException.EC_NO_PKEY_FOR_ALIAS);
        } catch (Exception ex) {
            if (ex instanceof TokenException) throw (TokenException) ex; //lazy programmer won't catch all exceptions thrown by keystore
            LogUtil.addError(logPane, "PROVIDER>>> error loading private key for alias: " + keyAlias + " with pin:" + pin +" error: " + ex.getMessage());
            throw new TokenException(TokenException.EC_LOADING_PKEY_FAILED, ex);
        }
    }
}
