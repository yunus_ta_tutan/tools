/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.runnable;

import com.fitcons.earchive.CustomizationParam;
import com.fitcons.earchive.SendInvoiceRequestType;
import com.fitcons.earchive.SendInvoiceResponseType;
import com.xuxux.utils.files.MyFileUtil;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import javax.swing.JTextPane;
import org.slf4j.LoggerFactory;
import signerclient.filters.ReadyZipFileFilter;
import signerclient.test.ws.EArsivInvoiceFaultMessage;
import signerclient.utils.CryptoUtil;
import signerclient.utils.GenericMarshallUnmarshallUtil;
import signerclient.utils.LogUtil;
import signerclient.utils.StringUtil;
import signerclient.wsclient.Invoker;

/**
 *
 * @author QQx23
 */
public class SendEnvelopeCallerThread implements Runnable {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(SendInvoiceCallerThread.class);

    private JTextPane logPane;
    private String endPoint;
    private String sourceFolder;
    private String vknTckn;
    private String branch;
    private String wsusername;
    private String wspassword;

    public SendEnvelopeCallerThread() {
    }

    public SendEnvelopeCallerThread(String endP, String folder, JTextPane p,String vkn_tckn ,String branch, String username, String password) {
        this.logPane = p;
        this.endPoint = endP;
        this.sourceFolder = folder + "/";
        this.vknTckn = vkn_tckn;
        this.branch = branch;
        this.wsusername = username;
        this.wspassword = password;
    }

    private synchronized byte[] getData(String folder) throws FileNotFoundException, IOException {
        File dir = new File(folder);
        if (dir == null || !dir.isDirectory()) {
            throw new FileNotFoundException("Folder :" + folder + " does not exist!");
        }

        String[] files = dir.list(new ReadyZipFileFilter());
        if (!(files != null && files.length > 0)) {
            return null;
        }
        String filename = dir.list(new ReadyZipFileFilter())[0];
        File file = new File(folder + filename);
        byte[] data = MyFileUtil.readBytesFromFile(file);
//        file.renameTo(new File(folder + "_" + filename));
        if (file.delete()) {
            LOGGER.info(file.getName() + " is deleted!");
        } else {
            LOGGER.warn("Delete operation is failed.");
        }
        return data;
    }

    @Override
    public void run() {
        SendInvoiceResponseType response = new SendInvoiceResponseType();
        SendInvoiceRequestType request = new SendInvoiceRequestType();
        request.setDocType("xml");
        request.setSenderID(vknTckn);
        CustomizationParam branchParam = new CustomizationParam();
        branchParam.setParamName("BRANCH");
        branchParam.setParamValue(branch);
        request.getCustomizationParams().add(branchParam);
        request.setReceiverID("2222222222");
        byte[] data = null;
        try {
            data = getData(sourceFolder);
            if (data == null) {
                LOGGER.warn("There is no data to send, Thread will stop!");
                return;
            }

        } catch (Exception ex) {
            LOGGER.error("Error when reading data: " + ex.toString());
            return;
        }
        while (data != null) {
            LOGGER.info("Invoking service...");
            try {
                request.setBinaryData(data);
                request.setHash(CryptoUtil.md5(data));
                response = Invoker.callSendEnvelope(request, endPoint, wsusername, wspassword);
                data = getData(sourceFolder);
                LogUtil.clearPane(logPane);
                LOGGER.debug("SendEnvelope Result:" + response.getResult().getResult().value());
                LogUtil.addInfo(logPane, response.getResult().getResult().value());
                LogUtil.addInfo(logPane, GenericMarshallUnmarshallUtil.marshal(response));
            } catch (EArsivInvoiceFaultMessage ex) {
                LOGGER.error("EArsivInvoiceFaultMessage:" + StringUtil.traceException(ex));
                LogUtil.addError(logPane, ex.getLocalizedMessage());
                return;
            } catch (NoSuchAlgorithmException ex) {
                LOGGER.error("NoSuchAlgorithmException:" + StringUtil.traceException(ex));
                LogUtil.addError(logPane, ex.getLocalizedMessage());
                return;
            } catch (IOException ex) {
                LOGGER.error("IOException:" + StringUtil.traceException(ex));
                LogUtil.addError(logPane, ex.getLocalizedMessage());
                return;
            } catch (Exception ex) {
                LOGGER.error("Exception:" + StringUtil.traceException(ex));
                LogUtil.addError(logPane, ex.getLocalizedMessage());
                return;
            }
        }
        LOGGER.warn("There is no data to send, Thread will stop!");

    }

}
