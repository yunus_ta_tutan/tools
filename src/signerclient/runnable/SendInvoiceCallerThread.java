/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.runnable;

import com.fitcons.earchive.CustomizationParam;
import com.fitcons.earchive.ResponsiveOutput;
import com.fitcons.earchive.ResponsiveOutputType;
import com.fitcons.earchive.SendInvoiceRequestType;
import com.fitcons.earchive.SendInvoiceResponseType;
import com.xuxux.utils.files.MyFileUtil;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import javax.swing.JTextPane;
import org.slf4j.LoggerFactory;
import signerclient.filters.ReadyZipFileFilter;
import signerclient.test.ws.EArsivInvoiceFaultMessage;
import signerclient.utils.CryptoUtil;
import signerclient.utils.GenericMarshallUnmarshallUtil;
import signerclient.utils.LogUtil;
import signerclient.utils.StringUtil;
import signerclient.wsclient.Invoker;

/**
 *
 * @author xuxux
 */
public class SendInvoiceCallerThread implements Runnable {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(SendInvoiceCallerThread.class);

    private JTextPane logPane;
    private String endPoint;
    private String vknTckn;
    private String branch;
    private String sourceFolder;
    private String outputType;
    private String wsUsername;
    private String wsPassWord;

    public SendInvoiceCallerThread() {
    }

    public SendInvoiceCallerThread(String endP, String folder, JTextPane p, String vkn_tckn, String branch, String outputType, String wsusername, String wspassword) {
        this.logPane = p;
        this.endPoint = endP;
        this.sourceFolder = folder + "/";
        this.vknTckn = vkn_tckn;
        this.branch = branch;
        this.outputType = outputType;
        this.wsUsername = wsusername;
        this.wsPassWord = wspassword;

    }

    private synchronized byte[] getData(String folder) throws FileNotFoundException, IOException {
        File dir = new File(folder);
        if (dir == null || !dir.isDirectory()) {
            throw new FileNotFoundException("Folder :" + folder + " does not exist!");
        }

        String[] files = dir.list(new ReadyZipFileFilter());
        if (!(files != null && files.length > 0)) {
            return null;
        }
        String filename = dir.list(new ReadyZipFileFilter())[0];
        File file = new File(folder + filename);
        byte[] data = MyFileUtil.readBytesFromFile(file);
//        file.renameTo(new File(folder + "_" + filename));
        if (file.delete()) {
            LOGGER.info(file.getName() + " is deleted!");
        } else {
            LOGGER.warn("Delete operation is failed.");
        }
        return data;
    }

    @Override
    public void run() {
        SendInvoiceResponseType response = new SendInvoiceResponseType();
        SendInvoiceRequestType request = new SendInvoiceRequestType();
        request.setDocType("xml");
        request.setSenderID(vknTckn);
        CustomizationParam branchParam = new CustomizationParam();
        branchParam.setParamName("BRANCH");
        branchParam.setParamValue(branch);
        request.getCustomizationParams().add(branchParam);
        request.setReceiverID("222222222");
        ResponsiveOutputType responsiveOutputType = ResponsiveOutputType.fromValue(outputType);
        ResponsiveOutput responsiveOutput = new ResponsiveOutput();
        switch (responsiveOutputType) {
            case NONE:
                responsiveOutput.setOutputType(ResponsiveOutputType.NONE);
                break;

            case HTML:
                responsiveOutput.setOutputType(ResponsiveOutputType.HTML);
                break;

            case PDF:
                responsiveOutput.setOutputType(ResponsiveOutputType.PDF);
                break;

            case XML:
                responsiveOutput.setOutputType(ResponsiveOutputType.XML);
                break;

            default:
                responsiveOutput.setOutputType(ResponsiveOutputType.NONE);
        }
        request.setResponsiveOutput(responsiveOutput);
        byte[] data = null;
        try {
            data = getData(sourceFolder);
            if (data == null) {
                LOGGER.warn("There is no data to send, Thread will stop!");
                return;
            }
        } catch (Exception ex) {
            LOGGER.error("Error when reading data: " + ex.toString());
            return;
        }
        while (data != null) {

            LOGGER.info("Invoking service...");
            try {
                request.setBinaryData(data);
                request.setHash(CryptoUtil.md5(data));

                response = Invoker.callSendInvoice(request, endPoint, wsUsername, wsPassWord);

                data = null;
                data = getData(sourceFolder);
                LogUtil.clearPane(logPane);
                LOGGER.debug("SendInvoice Result:" + response.getResult().getResult().value());
                LogUtil.addInfo(logPane, response.getResult().getResult().value());
                LogUtil.addInfo(logPane, GenericMarshallUnmarshallUtil.marshal(response));
            } catch (EArsivInvoiceFaultMessage ex) {
                LogUtil.addError(logPane, "EArsivInvoiceFaultMessage:" + StringUtil.traceException(ex));
                return;
            } catch (NoSuchAlgorithmException ex) {
                LogUtil.addError(logPane, "NoSuchAlgorithmException:" + StringUtil.traceException(ex));
                return;
            } catch (IOException ex) {
                LogUtil.addError(logPane, "IOException:" + StringUtil.traceException(ex));
                return;
            } catch (Exception ex) {
                LogUtil.addError(logPane, "Exception:" + StringUtil.traceException(ex));
                return;
            }
        }
        LOGGER.warn("There is no data to send, Thread will stop!");

    }

    public static void main(String[] args) {
    }

}
