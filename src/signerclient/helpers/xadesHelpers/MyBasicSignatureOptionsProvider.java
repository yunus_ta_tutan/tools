package signerclient.helpers.xadesHelpers;

import xades4j.providers.BasicSignatureOptionsProvider;

public class MyBasicSignatureOptionsProvider implements BasicSignatureOptionsProvider {

	private final boolean includeSigningCertificate;
    private final boolean includePublicKey;
    private final boolean signSigningCertificate;

    public MyBasicSignatureOptionsProvider(boolean includeSigningCertificate, boolean includePublicKey, boolean signSigningCertificate)
    {
        this.includeSigningCertificate = includeSigningCertificate;
        this.includePublicKey = includePublicKey;
        this.signSigningCertificate = signSigningCertificate;
    }

    @Override
    public boolean includeSigningCertificate()
    {
        return this.includeSigningCertificate;
    }

    @Override
    public boolean includePublicKey()
    {
        return this.includePublicKey;
    }

    @Override
    public boolean signSigningCertificate()
    {
        return this.signSigningCertificate;
    }

}
