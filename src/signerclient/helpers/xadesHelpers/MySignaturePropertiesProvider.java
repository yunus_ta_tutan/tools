package signerclient.helpers.xadesHelpers;

import xades4j.properties.SignerRoleProperty;
import xades4j.properties.SigningTimeProperty;
import xades4j.providers.SignaturePropertiesCollector;
import xades4j.providers.SignaturePropertiesProvider;

public class MySignaturePropertiesProvider implements SignaturePropertiesProvider {

	String _role;
	public void setSignerRole(String role)
	{
		_role = role;
	}
	
	@Override
	public void provideProperties(SignaturePropertiesCollector signedPropsCol) {
		if(_role != null && _role.isEmpty() == false){
			signedPropsCol.setSignerRole(new SignerRoleProperty(_role));
		}
		signedPropsCol.setSigningTime(new SigningTimeProperty());
	}

}
