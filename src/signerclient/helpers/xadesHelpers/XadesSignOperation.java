package signerclient.helpers.xadesHelpers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.xml.transform.TransformerFactoryConfigurationError;

import org.apache.xml.security.transforms.Transforms;
import org.w3c.dom.*;

import signerclient.tokenService.XuxuxKeyingDataProviderImpl;
import signerclient.tokenService.XuxuxPfxKeyingDataProviderImpl;

import xades4j.production.DataObjectReference;
import xades4j.production.SignedDataObjects;
import xades4j.production.XadesArchiveSigningProfile;
import xades4j.production.XadesBesSigningProfile;
import xades4j.production.XadesSigner;
import xades4j.properties.DataObjectDesc;
import xades4j.properties.DataObjectTransform;
import xades4j.providers.KeyingDataProvider;
import xades4j.providers.SigningCertChainException;
import xades4j.providers.ValidationData;
import xades4j.providers.ValidationDataException;
import xades4j.providers.ValidationDataProvider;
import xades4j.providers.impl.AuthenticatedTimeStampTokenProvider;
import xades4j.verification.UnexpectedJCAException;

public class XadesSignOperation {

    InputStream _inputStream;
    OutputStream _outStream;
    boolean _isStream;
    String _cert;
    String _role;
    boolean _UBLFormat;

    public XadesSignOperation(String inputPath, String outPath) throws FileNotFoundException {
        _inputStream = new FileInputStream(inputPath);
        _outStream = new FileOutputStream(outPath);
        _isStream = false;
    }

    public XadesSignOperation(InputStream input, OutputStream out) throws FileNotFoundException {
        _inputStream = input;
        _outStream = out;
        _isStream = true;
    }

    public void SetCertificateAlias(String cert) {
        _cert = cert;
    }

    public void SetRole(String role) {
        _role = role;
    }

    public void SetUBLFormat(boolean b) {
        _UBLFormat = b;
    }

    public void Sign(String signatureId, boolean withPfx) throws TransformerFactoryConfigurationError, Exception {
        /*
         * bu methoddaki parametre (signatureId) ido da ortaya çıkan uri ile
         * id uyusmazligi nedeniyle eklenmistir
         */
        Document doc = SignatureServicesBase.getDocument(_inputStream);

        // fmg>>: replacing with pfx provider
        KeyingDataProvider keyingProvider = null;
        if (withPfx) {
            // pfx signinf
            System.out.println("Will sign with FitPfxKeyingDataProvider");
            keyingProvider = XuxuxPfxKeyingDataProviderImpl.getInstance();
        } else {
            // regular pkcs11 signing
            System.out.println("Will sign with FitKeyingDataProvider");
            keyingProvider = XuxuxKeyingDataProviderImpl.getInstance();
        }
        
        XadesBesSigningProfile pr = new XadesBesSigningProfile(keyingProvider);

        pr.withAlgorithmsProvider(Sha1AlgProvider.class);
        pr.withBasicSignatureOptionsProvider(new MyBasicSignatureOptionsProvider(true, true, true));

        MySignaturePropertiesProvider propProv = new MySignaturePropertiesProvider();

        propProv.setSignerRole(_role);
        pr.withSignaturePropertiesProvider(propProv);

        XadesSigner signer = pr.newSigner();

        String refUri = "";
        DataObjectDesc dataObjRef = new DataObjectReference(refUri).withTransform(new DataObjectTransform(Transforms.TRANSFORM_ENVELOPED_SIGNATURE));

        Element elementToSign;

        if (_UBLFormat) {
            NodeList l = doc.getElementsByTagNameNS("*", "ExtensionContent");
            if (l.getLength() <= 0) {
                throw new Exception("Can not find ExtensionContent node!");
            }
            elementToSign = (Element) l.item(0);
        } else {
            elementToSign = doc.getDocumentElement();
        }

        // sondaki "signatureId" parametresi ido da ortaya çıkan id-uri uyusmazligini cozmek için girilmiştir.
        // signer.sign(new SignedDataObjects(dataObjRef), elementToSign);
        signer.sign(new SignedDataObjects(dataObjRef), elementToSign, signatureId);
        // new Enveloped(signer).sign(elemenToSign);
        SignatureServicesBase.outputDocument(doc, _outStream);

        if (!_isStream) {
            _inputStream.close();
            _outStream.close();
        }
    }

    public void archiveSign(String signatureId, boolean withPfx) throws TransformerFactoryConfigurationError, Exception {
        /*
         * bu methoddaki parametre (signatureId) ido da ortaya çıkan uri ile
         * id uyusmazligi nedeniyle eklenmistir
         */
        Document doc = SignatureServicesBase.getDocument(_inputStream);

        // fmg>>: replacing with pfx provider
        KeyingDataProvider keyingProvider = null;
        if (withPfx) {
            // pfx signinf
            System.out.println("Will sign with FitPfxKeyingDataProvider");
            keyingProvider = XuxuxPfxKeyingDataProviderImpl.getInstance();
        } else {
            // regular pkcs11 signing
            System.out.println("Will sign with FitKeyingDataProvider");
            keyingProvider = XuxuxKeyingDataProviderImpl.getInstance();
        }

        /**
         * xuxux archive
         */
        final KeyingDataProvider kdp = keyingProvider;
//        XadesBesSigningProfile pr = new XadesBesSigningProfile(kdp);
        XadesArchiveSigningProfile archiveSigningProfile = new XadesArchiveSigningProfile(keyingProvider, new ValidationDataProvider() {

            @Override
            public ValidationData getValidationData(List<X509Certificate> certChainFragment) throws ValidationDataException {
                try {
                    return new ValidationData(kdp.getSigningCertificateChain());
                } catch (SigningCertChainException ex) {
                    throw new ValidationDataException("message:SigningCertChainException" , ex.getCause());
                } catch (UnexpectedJCAException ex) {
                    throw new ValidationDataException("message:UnexpectedJCAException" , ex.getCause());
                }
            }
        });

        archiveSigningProfile.withAlgorithmsProvider(Sha1AlgProvider.class);
        archiveSigningProfile.withBasicSignatureOptionsProvider(new MyBasicSignatureOptionsProvider(true, true, true));

        archiveSigningProfile.withTimeStampTokenProvider(AuthenticatedTimeStampTokenProvider.class);
        MySignaturePropertiesProvider propProv = new MySignaturePropertiesProvider();

        propProv.setSignerRole(_role);
        archiveSigningProfile.withSignaturePropertiesProvider(propProv);

        XadesSigner signer = archiveSigningProfile.newSigner();

        String refUri = "";
        DataObjectDesc dataObjRef = new DataObjectReference(refUri).withTransform(new DataObjectTransform(Transforms.TRANSFORM_ENVELOPED_SIGNATURE));

        Element elementToSign;

        if (_UBLFormat) {
            NodeList l = doc.getElementsByTagNameNS("*", "ExtensionContent");
            if (l.getLength() <= 0) {
                throw new Exception("Can not find ExtensionContent node!");
            }
            elementToSign = (Element) l.item(0);
        } else {
            elementToSign = doc.getDocumentElement();
        }

        // sondaki "signatureId" parametresi ido da ortaya çıkan id-uri uyusmazligini cozmek için girilmiştir.
        // signer.sign(new SignedDataObjects(dataObjRef), elementToSign);
        signer.sign(new SignedDataObjects(dataObjRef), elementToSign, signatureId);

        // new Enveloped(signer).sign(elemenToSign);
        SignatureServicesBase.outputDocument(doc, _outStream);

        if (!_isStream) {
            _inputStream.close();
            _outStream.close();
        }
    }

}
