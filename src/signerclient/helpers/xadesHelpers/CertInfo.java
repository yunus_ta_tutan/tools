package signerclient.helpers.xadesHelpers;

import java.security.PrivateKey;
import java.security.Provider;
import java.security.cert.X509Certificate;

import sun.security.x509.KeyUsageExtension;

public class CertInfo {

    private String alias;
    private X509Certificate cert;
    private PrivateKey privateKey;
    public long slotID;
    public Provider provider;
    private KeyUsageExtension keyUsageExt;
    private String digital_signature;

    @Override
    public String toString() {

        // TODO Auto-generated method stub
        return "\nalias: " + getAlias() + "\nslotID: " + slotID + "\n cert subjectdn:" + getCert().getSubjectDN().getName() + "\n private key:" + getPrivateKey()
                + "\n#####";
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public X509Certificate getCert() {
        return cert;
    }

    public void setCert(X509Certificate cert) {
        this.cert = cert;
    }

    public String getDigital_signature() {
        return digital_signature;
    }

    public void setDigital_signature(String digital_signature) {
        this.digital_signature = digital_signature;
    }

    public KeyUsageExtension getKeyUsageExt() {
        return keyUsageExt;
    }

    public void setKeyUsageExt(KeyUsageExtension keyUsageExt) {
        this.keyUsageExt = keyUsageExt;
    }

    /**
     * @return the privateKey
     */
    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    /**
     * @param privateKey the privateKey to set
     */
    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }
}
