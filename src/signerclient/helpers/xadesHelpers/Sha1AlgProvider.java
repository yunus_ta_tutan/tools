package signerclient.helpers.xadesHelpers;

import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;

import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.spec.SignatureMethodParameterSpec;

import org.apache.xml.security.algorithms.MessageDigestAlgorithm;

import xades4j.UnsupportedAlgorithmException;
import xades4j.providers.impl.DefaultAlgorithmsProvider;

public class Sha1AlgProvider extends DefaultAlgorithmsProvider {
	@Override
	public String getSignatureAlgorithm(String keyAlgorithmName) throws UnsupportedAlgorithmException {
		if (keyAlgorithmName.equals("RSA")) {
			XMLSignatureFactory factory = XMLSignatureFactory.getInstance();
			try {
				SignatureMethod sm = factory.newSignatureMethod("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256", (SignatureMethodParameterSpec) null);
				// System.out.println("aha geldi : " + sm.getAlgorithm());
				return sm.getAlgorithm();
			} catch (NoSuchAlgorithmException e) {
				System.err.println("NoSuchAlgorithmException : " + e.toString());
				e.printStackTrace();
			} catch (InvalidAlgorithmParameterException e) {
				System.err.println("InvalidAlgorithmParameterException : " + e.toString());
				e.printStackTrace();
			}

			// return "http:l//www.w3.org/2001/04/xmldsig-more#rsa-sha256";

		}
		else if (!keyAlgorithmName.equals("RSA")) {
			return SignatureMethod.RSA_SHA1;

		}

		return super.getSignatureAlgorithm(keyAlgorithmName);

	}

	@Override
	public String getDigestAlgorithmForDataObjsReferences() {
		// return MessageDigestAlgorithm.ALGO_ID_DIGEST_SHA1;
		return MessageDigestAlgorithm.ALGO_ID_DIGEST_SHA256;
	}

	public static void main(String[] args) {
		System.out.println(SignatureMethod.RSA_SHA1);

		XMLSignatureFactory factory = XMLSignatureFactory.getInstance();
		try {
			SignatureMethod sm = factory.newSignatureMethod("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256", (SignatureMethodParameterSpec) null);
			System.out.println(sm.getAlgorithm());
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
