package signerclient.helpers.xadesHelpers;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

public class SignatureServicesBase {

	public static Document getDocument(String fileName) throws Exception {
		DocumentBuilderFactory factory = null;
		DocumentBuilder builder;
		try {
			factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			builder = factory.newDocumentBuilder();
			return builder.parse(new FileInputStream(fileName));
		} catch (ParserConfigurationException ex) {
			throw new NullPointerException(
					"SignatureServicesTestBase init failed:" + ex.getMessage());
		} finally {
			builder = null;
			factory = null;
		}
	}

	public static Document getDocument(InputStream inStream) throws Exception {
		DocumentBuilderFactory factory = null;
		DocumentBuilder builder;
		try {
			factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			builder = factory.newDocumentBuilder();
			return builder.parse(inStream);
		} catch (ParserConfigurationException ex) {
			throw new NullPointerException(
					"SignatureServicesTestBase init failed:" + ex.getMessage());
		} finally {
			builder = null;
			factory = null;
		}
	}

	public static Document getNewDocument() throws Exception {
		DocumentBuilderFactory factory = null;
		DocumentBuilder builder;
		try {
			factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			builder = factory.newDocumentBuilder();
			return builder.newDocument();
		} catch (ParserConfigurationException ex) {
			throw new NullPointerException(
					"SignatureServicesTestBase init failed:" + ex.getMessage());
		} finally {
			builder = null;
			factory = null;
		}
	}

	public static void outputDocument(Document doc, String fileName)
			throws Exception {

		TransformerFactory tf = null;
		FileOutputStream out = null;
		try {
			tf = TransformerFactory.newInstance();
			out = new FileOutputStream(fileName);
			tf.newTransformer().transform(new DOMSource(doc),
					new StreamResult(out));
		} catch (Exception e) {
			throw e;
		} finally {
			tf = null;
			out.close();
		}

	}

	public static void outputDocument(Document doc, OutputStream outStream)
			throws Exception {

		TransformerFactory tf = null;
		try {
			tf = TransformerFactory.newInstance();
			tf.newTransformer().transform(new DOMSource(doc),
					new StreamResult(outStream));
		} catch (Exception e) {
			throw e;
		} finally {
			tf = null;
		}
	}
}