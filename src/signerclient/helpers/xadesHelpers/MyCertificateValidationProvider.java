package FIT.Signer.helpers.Xades;

import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import xades4j.providers.CertificateValidationException;
import xades4j.providers.CertificateValidationProvider;
import xades4j.providers.ValidationData;
import xades4j.verification.UnexpectedJCAException;

public class MyCertificateValidationProvider implements CertificateValidationProvider {

	public MyCertificateValidationProvider()
	{
	}
	
	public ValidationData validate(X509CertSelector certSelector, Date date,
			Collection<X509Certificate> certCol)
			throws CertificateValidationException, UnexpectedJCAException {
		
		ArrayList<X509Certificate> list = new ArrayList<X509Certificate>();
		
		Iterator<X509Certificate> it = certCol.iterator();
		while(it.hasNext()){
			X509Certificate cert = it.next();
			list.add(cert);
	
		}
		
		return new ValidationData(list);
	}

}
