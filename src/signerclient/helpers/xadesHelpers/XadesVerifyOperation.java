package signerclient.helpers.xadesHelpers;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.KeyStore;
import java.security.cert.CRLException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509CRL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.xml.security.utils.Constants;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import xades4j.properties.ObjectIdentifier;
import xades4j.properties.data.SigningTimeData;
import xades4j.providers.CertificateValidationProvider;
import xades4j.providers.SignaturePolicyDocumentProvider;
import xades4j.providers.impl.PKIXCertificateValidationProvider;
import xades4j.utils.FileSystemDirectoryCertStore;
import xades4j.verification.PropertyInfo;
import xades4j.verification.XAdESVerificationResult;
import xades4j.verification.XadesVerificationProfile;
import xades4j.verification.XadesVerifier;
import signerclient.factory.ImplFactory;
import signerclient.services.FileOperationsService;

//import com.fit.einvoice.signer.factory.ServiceFactory;
//import com.fit.einvoice.signer.services.FileOperationsService;

public class XadesVerifyOperation extends SignatureServicesBase {

	static SignaturePolicyDocumentProvider		policyDocumentFinder;
	public static CertificateValidationProvider	validationProviderMySigs;
	static protected XadesVerificationProfile	verificationProfile;
	public static X509CRL						rootCRL;
	public static X509CRL						subcaCRL;

	public static boolean						crlControl	= false;

	static {
		try {
			policyDocumentFinder = new SignaturePolicyDocumentProvider() {
				@Override
				public InputStream getSignaturePolicyDocumentStream(ObjectIdentifier sigPolicyId) {
					return new ByteArrayInputStream("Test policy input stream".getBytes());
				}
			};
			// first disable crl checing (in case we cannot download crl)
			setCRLChecking(false);

			// crl downloading thread
			Timer timer = new Timer(true);
			timer.scheduleAtFixedRate(new TimerTask() {
				@Override
				public void run() {
					FileOperationsService fileOperations = ImplFactory.fileOperationsInstance();
					try {
						if (isUrlReachable("depo.kamusm.gov.tr")) {
							System.out.println("depo.kamusm.gov.tr is reachable, will download root and subca crl files");
							downloadUrl("http://depo.kamusm.gov.tr/kurumsal/kurumsal-s1.crl", fileOperations.getFilePathUnderMetaINF("crl/root.crl"));
							downloadUrl("http://depo.kamusm.gov.tr/kurumsal/mmeshs-s1.crl", fileOperations.getFilePathUnderMetaINF("crl/subca.crl"));
							setCRLChecking(true);
							crlControl = true;
						}
						else {
							setCRLChecking(false);
							System.out.println("depo.kamusm.gov.tr is NOT reachable, will NOT download root and subca crl files");
						}

					} catch (Exception e) {
						System.err.println(e.toString());
						setCRLChecking(false);
					}

					// yasardaki internet erisimi olmayan imza sunucusunda ortaya cikan hata(cannot find valid certification path) sebebiyle kaldirildi
					// try {
					// File fileRootCrl = new File(fileOperations.getFilePathUnderMetaINF("crl/root.crl"));
					// File fileSubcaCrl = new File(fileOperations.getFilePathUnderMetaINF("crl/subca.crl"));
					//
					// if (fileRootCrl != null && fileSubcaCrl != null && fileRootCrl.exists() && fileSubcaCrl.exists()) {
					// setCRLChecking(true);
					// crlControl = true;
					// }else{
					// setCRLChecking(false);
					// }
					// } catch (Exception e2) {
					// try {
					// setCRLChecking(false);
					// } catch (Exception e) {
					// System.err.println(e.toString());
					// }
					// System.err.println(e2.toString());
					// }

				}
			}, 1000, 24 * 60 * 60 * 1000);
		} catch (Exception ex) {
			throw new NullPointerException("XadesVerifyOperation init failed: " + ex.getMessage());
		}
	}

	public static boolean isUrlReachable(String hostnameOrIP) {
		Socket socket = null;
		boolean reachable = false;
		try {
			socket = new Socket(hostnameOrIP, 80);
			reachable = true;
		} catch (UnknownHostException e) {
			reachable = false;
			System.err.println(e.toString());
		} catch (IOException e) {
			reachable = false;
			System.err.println(e.toString());
		} finally {
			if (socket != null) try {
				socket.close();
			} catch (IOException e) {
			}
		}
		return reachable;
	}

	private static void downloadUrl(String urlString, String file) throws Exception {
		InputStream is = null;
		OutputStream os = null;
		try {
			URL url = new URL(urlString);
			is = url.openStream();
			os = new FileOutputStream(file);

			byte[] buffer = new byte[4096];
			int n = 0;
			while (-1 != (n = is.read(buffer))) {
				os.write(buffer, 0, n);
			}
		} finally {
			if (is != null) try {
				is.close();
			} catch (IOException ex) {
			}
			if (os != null) try {
				os.close();
			} catch (IOException ex) {
			}
		}
	}

	public static void setCRLChecking(boolean enabled) {
		System.out.println("CRL_checking is >>> " + enabled);
		InputStream rootCrlInput = null;
		InputStream subcaCrlInput = null;
		try {
			FileOperationsService fileOperations = ImplFactory.fileOperationsInstance();
			FileSystemDirectoryCertStore certStore = new FileSystemDirectoryCertStore(fileOperations.getFilePathUnderMetaINF("crl"));
			KeyStore ks = KeyStore.getInstance("jks");
			ks.load(new FileInputStream(fileOperations.getFilePathUnderMetaINF("crl/trust.ks")), "123456".toCharArray());
			validationProviderMySigs = new PKIXCertificateValidationProvider(ks, enabled, certStore.getStore());
			System.out.println(certStore.getStore());
			verificationProfile = new XadesVerificationProfile(XadesVerifyOperation.validationProviderMySigs);
			System.out.println("*** CRL checking enabled: " + enabled);

			rootCrlInput = new FileInputStream(fileOperations.getFilePathUnderMetaINF("crl/root.crl"));
			subcaCrlInput = new FileInputStream(fileOperations.getFilePathUnderMetaINF("crl/subca.crl"));

			CertificateFactory factory = CertificateFactory.getInstance("X.509");

			rootCRL = (X509CRL) factory.generateCRL(rootCrlInput);
			subcaCRL = (X509CRL) factory.generateCRL(subcaCrlInput);
		} catch (Exception e) {
			System.err.println(e.toString());
		} finally {
			try {
				if (rootCrlInput != null) {
					rootCrlInput.close();
				}
			} catch (Exception e2) {
				System.err.println(e2.toString());
			}
			try {
				if (subcaCrlInput != null) {
					subcaCrlInput.close();
				}
			} catch (Exception e2) {
				System.err.println(e2.toString());
			}
			subcaCrlInput = null;
			rootCrlInput = null;
		}

	}

	public static ArrayList<XadesVerificationResults> verifySignature(String sigFileName) throws Exception {
		InputStream inS = new FileInputStream(sigFileName);
		ArrayList<XadesVerificationResults> res = verifySignature(inS);
		inS.close();
		return res;
	}

	public static ArrayList<XadesVerificationResults> verifySignature(InputStream inStream) throws Exception {
		XAdESVerificationResult r = verifySignature(inStream, verificationProfile);

		ArrayList<XadesVerificationResults> resultList = new ArrayList<XadesVerificationResults>();
		XadesVerificationResults res = new XadesVerificationResults();
		res.SigningCertificate = r.getValidationCertificate();

		Iterator<PropertyInfo> it = r.getPropertiesAndData().iterator();
		while (it.hasNext()) {
			PropertyInfo i = it.next();
			if (i.getPropertyData().getClass() == SigningTimeData.class) {
				SigningTimeData d = (SigningTimeData) i.getPropertyData();
				res.SigningDate = d.getSigningTime();
				break;
			}
		}

		resultList.add(res);
		return resultList;
	}

	protected static XAdESVerificationResult verifySignature(InputStream inS, XadesVerificationProfile p) throws Exception {
		Element signatureNode = getSigElement(getDocument(inS));
		return verifySignature(signatureNode, p);
	}

	protected static XAdESVerificationResult verifySignature(Element sigElem, XadesVerificationProfile p) throws Exception {
		XadesVerifier vr = p.newVerifier();
		XAdESVerificationResult res = vr.verify(sigElem, null);
		return res;
	}

	static protected Element getSigElement(Document doc) throws Exception {
		return (Element) doc.getElementsByTagNameNS(Constants.SignatureSpecNS, Constants._TAG_SIGNATURE).item(0);
	}

	protected static KeyStore createAndLoadJKSKeyStore(String path, String pwd) throws Exception {
//		path = C_CertStore.certsLocation + path;
		FileInputStream fis = new FileInputStream(path);
		KeyStore ks = KeyStore.getInstance("jks");
		ks.load(fis, pwd.toCharArray());
		fis.close();
		return ks;
	}

	protected static FileSystemDirectoryCertStore createDirectoryCertStore() throws CertificateException, CRLException {
		return new FileSystemDirectoryCertStore("");
	}

}
