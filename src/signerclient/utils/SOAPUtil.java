/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.utils;

import com.fit.earsiv.rapor.ws.Attachment;
import com.fit.earsiv.rapor.ws.GetBatchStatus;
import com.fit.earsiv.rapor.ws.SendDocumentFile;
import com.xuxux.utils.commons.MyXmlUtil;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.security.KeyStoreException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.WSEncryptionPart;
import org.apache.ws.security.WSSConfig;
import org.apache.ws.security.WSSecurityException;
import org.apache.ws.security.components.crypto.Crypto;
import org.apache.ws.security.components.crypto.CryptoFactory;
import org.apache.ws.security.message.WSSecHeader;
import org.apache.ws.security.message.WSSecSignature;
import org.apache.ws.security.message.WSSecTimestamp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import signerclient.tokenService.XuxuxDeviceProvider;

/**
 *
 * @author xuxux
 */
public class SOAPUtil {

    static final Logger LOGGER = LoggerFactory.getLogger(SOAPUtil.class);

    public static SOAPMessage getSoapMessageTemplete(String zippedFilePath) throws JAXBException, ParserConfigurationException, SOAPException {

        File zippedFile = new File(zippedFilePath);
        if (!zippedFile.exists() || !zippedFile.isFile()) {
            LOGGER.warn("please select a zipped file!");
            throw new UnsupportedOperationException("selected file is not a zipped file");
        }

        org.w3c.dom.Document fileDoc = MyXmlUtil.newDocument();

        SendDocumentFile sendDocumentFile = new SendDocumentFile();
        Attachment attachment = new Attachment();
        FileDataSource fileDataSource = new FileDataSource(zippedFile);
        DataHandler dataHandler = new DataHandler(fileDataSource);
        attachment.setFileName(zippedFile.getName());
        attachment.setBinaryData(dataHandler);
        sendDocumentFile.setAttachment(attachment);

        JAXBContext jaxbContext = JAXBContext.newInstance(SendDocumentFile.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(sendDocumentFile, fileDoc);

        MessageFactory messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
        SOAPMessage message = messageFactory.createMessage();
        message.getSOAPBody().addDocument(fileDoc);
        return message;

    }
    
    public static SOAPMessage getBatchStatusSoapMessageTemplete(String paketId) throws JAXBException, ParserConfigurationException, SOAPException {

        org.w3c.dom.Document fileDoc = MyXmlUtil.newDocument();

        GetBatchStatus getBatchStatus = new GetBatchStatus();
        getBatchStatus.setPaketId(paketId);

        JAXBContext jaxbContext = JAXBContext.newInstance(GetBatchStatus.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(getBatchStatus, fileDoc);

        MessageFactory messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
        SOAPMessage message = messageFactory.createMessage();
        message.getSOAPBody().addDocument(fileDoc);
        return message;

    }

    public static Document createWSSrequest(XuxuxDeviceProvider xdp, SOAPMessage message, String alias, String pass) throws WSSecurityException, KeyStoreException, CertificateEncodingException {
        final int signatureValidityTime = 3000;
        X509Certificate signingCert = (X509Certificate) xdp.getKeyStore().getCertificate(alias);
        WSSConfig config = new WSSConfig();
        config.setWsiBSPCompliant(false);

        WSSecSignature builder = new WSSecSignature(config);
        builder.setSignatureAlgorithm("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");
        builder.setX509Certificate(signingCert);
        builder.setUserInfo(alias, pass);
        builder.setUseSingleCertificate(true);
        builder.setKeyIdentifierType(WSConstants.BST_DIRECT_REFERENCE);

        Document document = message.getSOAPPart();

        WSSecHeader secHeader = new WSSecHeader();
        secHeader.setMustUnderstand(true);
        secHeader.insertSecurityHeader(document);

        WSSecTimestamp timestamp = new WSSecTimestamp();
        timestamp.setTimeToLive(signatureValidityTime);
        document = timestamp.build(document, secHeader);

        List<WSEncryptionPart> parts = new ArrayList<WSEncryptionPart>();
        WSEncryptionPart timestampPart = new WSEncryptionPart("Timestamp", WSConstants.WSU_NS, "");
        WSEncryptionPart bodyPart = new WSEncryptionPart(WSConstants.ELEM_BODY, WSConstants.URI_SOAP11_ENV, "");
        parts.add(timestampPart);
        parts.add(bodyPart);

        builder.setParts(parts);

        Properties properties = new Properties();
        properties.setProperty("org.apache.ws.security.crypto.provider", "signerclient.tokenService.XuxuxDeviceProvider");

        Crypto crypto = CryptoFactory.getInstance(properties);
        ((XuxuxDeviceProvider) crypto).setKeyStore(xdp.getKeyStore());

        crypto.loadCertificate(new ByteArrayInputStream(signingCert.getEncoded()));
        Document returnDoc = builder.build(document, crypto, secHeader);
        return returnDoc;
    }

    public static SOAPMessage invoke(Document doc, String url) {
        try {
            SOAPMessage request = toSOAPMessage(doc);

            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            SOAPMessage soapResponse = soapConnection.call(request, url);

            // Process the SOAP Response
//            printSOAPResponse(soapResponse);
            soapConnection.close();
            return soapResponse;
        } catch (Exception e) {
            LOGGER.error(StringUtil.traceException(e));
            e.printStackTrace();
        }
        return null;
    }

    private static void printSOAPResponse(SOAPMessage soapResponse) throws Exception {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        Source sourceContent = soapResponse.getSOAPPart().getContent();
        System.out.print("\nResponse SOAP Message = ");
        StreamResult result = new StreamResult(System.out);
        transformer.transform(sourceContent, result);
    }

    public static final String prettyPrint(Document xml) throws Exception {
        Transformer tf = TransformerFactory.newInstance().newTransformer();
        tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        tf.setOutputProperty(OutputKeys.INDENT, "yes");
        Writer out = new StringWriter();
        tf.transform(new DOMSource(xml), new StreamResult(out));
        return out.toString();
    }

    public static Document toDocument(SOAPMessage soapMsg) throws TransformerConfigurationException, TransformerException, SOAPException, IOException {
        Source src = soapMsg.getSOAPPart().getContent();
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        DOMResult result = new DOMResult();
        transformer.transform(src, result);
        return (Document) result.getNode();
    }

    public static SOAPMessage toSOAPMessage(Document doc) throws TransformerConfigurationException, TransformerException, SOAPException, IOException {
        DOMSource src = new DOMSource(doc);
        MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
        SOAPMessage soapMsg = mf.createMessage();
        soapMsg.getSOAPPart().setContent(src);
        return soapMsg;
    }
}
