/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.utils;

import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

/**
 *
 * @author XUXUX
 */
public class LogUtil extends JTextPane {

    static final Color D_Black = Color.getHSBColor(0.000f, 0.000f, 0.000f);
    static final Color D_Red = Color.getHSBColor(0.000f, 1.000f, 0.502f);
    static final Color D_Blue = Color.getHSBColor(0.667f, 1.000f, 0.502f);
    static final Color D_Magenta = Color.getHSBColor(0.833f, 1.000f, 0.502f);
    static final Color D_Green = Color.getHSBColor(0.333f, 1.000f, 0.502f);
    static final Color D_Yellow = Color.getHSBColor(0.167f, 1.000f, 0.502f);
    static final Color D_Cyan = Color.getHSBColor(0.500f, 1.000f, 0.502f);
    static final Color D_White = Color.getHSBColor(0.000f, 0.000f, 0.753f);
    static final Color B_Black = Color.getHSBColor(0.000f, 0.000f, 0.502f);
    static final Color B_Red = Color.getHSBColor(0.000f, 1.000f, 1.000f);
    static final Color B_Blue = Color.getHSBColor(0.667f, 1.000f, 1.000f);
    static final Color B_Magenta = Color.getHSBColor(0.833f, 1.000f, 1.000f);
    static final Color B_Green = Color.getHSBColor(0.333f, 1.000f, 1.000f);
    static final Color B_Yellow = Color.getHSBColor(0.167f, 1.000f, 1.000f);
    static final Color B_Cyan = Color.getHSBColor(0.500f, 1.000f, 1.000f);
    static final Color B_White = Color.getHSBColor(0.000f, 0.000f, 1.000f);
    static final Color cReset = Color.getHSBColor(0.000f, 0.000f, 1.000f);
    static Color colorCurrent = cReset;
    static String remaining = "";
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss:SSS");

    public SimpleDateFormat getDateFormat() {
        return dateFormat;
    }

    public synchronized static void addInfo(JTextPane pane, String msg) {

        //appendToPane(pane, '\n' + "INFO : " + dateFormat.format(new Date()) + " -> " + msg, Color.white);
        appendANSI(pane, '\n' + "INF : " + dateFormat.format(new Date()) + " -> " + msg, Color.green);
    }

    public synchronized static void addError(JTextPane pane, String msg) {

        // appendToPane(pane, '\n' + "ERROR : " + dateFormat.format(new Date()) + " -> " + msg, Color.red);
        appendANSI(pane, '\n' + "ERR : " + dateFormat.format(new Date()) + " -> " + msg, Color.red);
    }

    public synchronized static void addWarning(JTextPane pane, String msg) {

        // appendToPane(pane, '\n' + "WARNING : " + dateFormat.format(new Date()) + " -> " + msg, Color.orange);
        appendANSI(pane, '\n' + "WRN : " + dateFormat.format(new Date()) + " -> " + msg, Color.orange);
    }

    public synchronized static void clearPane(JTextPane pane) {
        pane.setEditable(true);
        pane.setText("");
        pane.setEditable(false);
    }

    private static void appendToPane(JTextPane tp, String msg, Color c) {
        
        tp.setEditable(true);
        StyleContext sc = StyleContext.getDefaultStyleContext();
        AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

        aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
        aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

        int len = tp.getDocument().getLength();
        tp.setCaretPosition(len);
        tp.setCharacterAttributes(aset, false);
        tp.replaceSelection(msg);
        tp.setEditable(false);
        
    }

    public static void appendANSI(JTextPane pane, String s, Color c) { // convert ANSI color codes first
        int aPos = 0;   // current char position in addString
        int aIndex = 0; // index of next Escape sequence
        int mIndex = 0; // index of "m" terminating Escape sequence
        String tmpString = "";
        boolean stillSearching = true; // true until no more Escape sequences
        String addString = remaining + s;
        remaining = "";
        colorCurrent = Color.white;
        if (addString.length() > 0) {
            aIndex = addString.indexOf("\u001B"); // find first escape
            if (aIndex == -1) { // no escape/color change in this string, so just send it with current color
                // append(colorCurrent, addString);
                appendToPane(pane, addString, c);
                return;
            }
// otherwise There is an escape character in the string, so we must process it

            if (aIndex > 0) { // Escape is not first char, so send text up to first escape
                tmpString = addString.substring(0, aIndex);
                // append(colorCurrent, tmpString);
                appendToPane(pane, tmpString, colorCurrent);
                aPos = aIndex;
            }
// aPos is now at the beginning of the first escape sequence

            stillSearching = true;
            while (stillSearching) {
                mIndex = addString.indexOf("m", aPos); // find the end of the escape sequence
                if (mIndex < 0) { // the buffer ends halfway through the ansi string!
                    remaining = addString.substring(aPos, addString.length());
                    stillSearching = false;
                    continue;
                } else {
                    tmpString = addString.substring(aPos, mIndex + 1);
                    colorCurrent = getANSIColor(tmpString);
                }
                aPos = mIndex + 1;
// now we have the color, send text that is in that color (up to next escape)

                aIndex = addString.indexOf("\u001B", aPos);

                if (aIndex == -1) { // if that was the last sequence of the input, send remaining text
                    tmpString = addString.substring(aPos, addString.length());
                    //append(colorCurrent, tmpString);
                    appendToPane(pane, tmpString, colorCurrent);
                    stillSearching = false;
                    continue; // jump out of loop early, as the whole string has been sent now
                }

                // there is another escape sequence, so send part of the string and prepare for the next
                tmpString = addString.substring(aPos, aIndex);
                aPos = aIndex;
                //append(colorCurrent, tmpString);
                appendToPane(pane, tmpString, colorCurrent);

            } // while there's text in the input buffer
        }
    }

    public static Color getANSIColor(String ANSIColor) {
        if (ANSIColor.equals("\u001B[30m")) {
            return D_Black;
        } else if (ANSIColor.equals("\u001B[31m")) {
            return D_Red;
        } else if (ANSIColor.equals("\u001B[32m")) {
            return D_Green;
        } else if (ANSIColor.equals("\u001B[33m")) {
            return D_Yellow;
        } else if (ANSIColor.equals("\u001B[34m")) {
            return D_Blue;
        } else if (ANSIColor.equals("\u001B[35m")) {
            return D_Magenta;
        } else if (ANSIColor.equals("\u001B[36m")) {
            return D_Cyan;
        } else if (ANSIColor.equals("\u001B[37m")) {
            return D_White;
        } else if (ANSIColor.equals("\u001B[30;0m")) {
            return D_Black;
        } else if (ANSIColor.equals("\u001B[31;0m")) {
            return D_Red;
        } else if (ANSIColor.equals("\u001B[32;0m")) {
            return D_Green;
        } else if (ANSIColor.equals("\u001B[33;0m")) {
            return D_Yellow;
        } else if (ANSIColor.equals("\u001B[34;0m")) {
            return D_Blue;
        } else if (ANSIColor.equals("\u001B[35;0m")) {
            return D_Magenta;
        } else if (ANSIColor.equals("\u001B[36;0m")) {
            return D_Cyan;
        } else if (ANSIColor.equals("\u001B[37;0m")) {
            return D_White;
        } else if (ANSIColor.equals("\u001B[30;1m")) {
            return B_Black;
        } else if (ANSIColor.equals("\u001B[31;1m")) {
            return B_Red;
        } else if (ANSIColor.equals("\u001B[32;1m")) {
            return B_Green;
        } else if (ANSIColor.equals("\u001B[33;1m")) {
            return B_Yellow;
        } else if (ANSIColor.equals("\u001B[34;1m")) {
            return B_Blue;
        } else if (ANSIColor.equals("\u001B[35;1m")) {
            return B_Magenta;
        } else if (ANSIColor.equals("\u001B[36;1m")) {
            return B_Cyan;
        } else if (ANSIColor.equals("\u001B[37;1m")) {
            return B_White;
        } else if (ANSIColor.equals("\u001B[0m")) {
            return cReset;
        } else if (ANSIColor.equals("\u001B[37;1m")) {
            return B_White;
        } else {
            return B_White;
        }
    }
    
    
    public static void main(String[] args){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss:SSS");
        System.out.println(dateFormat.format(new Date()));
        System.out.println(dateFormat.format(new Date()));
    }
}
