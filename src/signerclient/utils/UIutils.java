/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.utils;

import javax.swing.JOptionPane;

/**
 *
 * @author xuxux
 */
public class UIutils {
    
     public static String askForPin(String description) {
        String enteredPin = JOptionPane.showInputDialog(description);
        return enteredPin;
    }
    
}
