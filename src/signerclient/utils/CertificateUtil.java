/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.utils;

import java.security.Principal;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;
import java.util.HashMap;
import org.bouncycastle.asn1.misc.MiscObjectIdentifiers;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.jce.provider.JCEECPublicKey;
import org.bouncycastle.jce.provider.JDKDSAPublicKey;

/**
 *
 * @author xuxux
 */
public class CertificateUtil {
    
     /** KeyUsage constants */
    public static final int DIGITALSIGNATURE = 0;
    public static final int NONREPUDIATION = 1;
    public static final int KEYENCIPHERMENT = 2;
    public static final int DATAENCIPHERMENT = 3;
    public static final int KEYAGREEMENT = 4;
    public static final int KEYCERTSIGN = 5;
    public static final int CRLSIGN = 6;
    public static final int ENCIPHERONLY = 7;
    public static final int DECIPHERONLY = 8;

    public static final String[] KEYUSAGETEXTS = { 
        "DIGITALSIGNATURE",
        "NONREPUDIATION",
        "KEYENCIPHERMENT",
        "DATAENCIPHERMENT",
        "KEYAGREEMENT",
        "KEYCERTSIGN",
        "CRLSIGN",
        "ENCIPHERONLY",
        "DECIPHERONLY" };

    /** Extended key usage constants */
    public static final int ANYEXTENDEDKEYUSAGE = 0;
    public static final int SERVERAUTH = 1;
    public static final int CLIENTAUTH = 2;
    public static final int CODESIGNING = 3;
    public static final int EMAILPROTECTION = 4;
    public static final int IPSECENDSYSTEM = 5;
    public static final int IPSECTUNNEL = 6;
    public static final int IPSECUSER = 7;
    public static final int TIMESTAMPING = 8;
    public static final int SMARTCARDLOGON = 9;
    public static final int OCSPSIGNING = 10;

    public static final String[] EXTENDEDKEYUSAGEOIDSTRINGS = {
        "1.3.6.1.5.5.7.3.0",
        "1.3.6.1.5.5.7.3.1",
        "1.3.6.1.5.5.7.3.2",
        "1.3.6.1.5.5.7.3.3",
        "1.3.6.1.5.5.7.3.4",
        "1.3.6.1.5.5.7.3.5",
        "1.3.6.1.5.5.7.3.6",
        "1.3.6.1.5.5.7.3.7",
        "1.3.6.1.5.5.7.3.8",
        "1.3.6.1.4.1.311.20.2.2",
        "1.3.6.1.5.5.7.3.9" };

    public static final String[] EXTENDEDKEYUSAGETEXTS = {
        "ANYEXTENDEDKEYUSAGE",
        "SERVERAUTH",
        "CLIENTAUTH",
        "CODESIGNING",
        "EMAILPROTECTION",
        "IPSECENDSYSTEM",
        "IPSECTUNNEL",
        "IPSECUSER",
        "TIMESTAMPING",
        "SMARTCARDLOGON",
        "OCSPSIGNER" };

    private static final int SUBALTNAME_OTHERNAME = 0;
    private static final int SUBALTNAME_RFC822NAME = 1;
    private static final int SUBALTNAME_DNSNAME = 2;
    private static final int SUBALTNAME_X400ADDRESS = 3;
    private static final int SUBALTNAME_DIRECTORYNAME = 4;
    private static final int SUBALTNAME_EDIPARTYNAME = 5;
    private static final int SUBALTNAME_URI = 6;
    private static final int SUBALTNAME_IPADDRESS = 7;
    private static final int SUBALTNAME_REGISTREDID = 8;
    
    public static String getKeyUsageAsText(X509Certificate certificate){
        if (certificate == null)
            return null;

        String kuText = "";
        boolean[] keyusage = certificate.getKeyUsage();
        if (keyusage == null) return "";
        if (keyusage[0]) kuText += "digitalSignature";
        if (keyusage[1]) kuText += (kuText.equals("")?"":", ") + "nonRepudiation";
        if (keyusage[2]) kuText += (kuText.equals("")?"":", ") + "keyEncipherment";
        if (keyusage[3]) kuText += (kuText.equals("")?"":", ") + "dataEncipherment";
        if (keyusage[4]) kuText += (kuText.equals("")?"":", ") + "keyAgreement";
        if (keyusage[5]) kuText += (kuText.equals("")?"":", ") + "keyCertSign";
        if (keyusage[6]) kuText += (kuText.equals("")?"":", ") + "cRLSign";
        if (keyusage[7]) kuText += (kuText.equals("")?"":", ") + "encipherOnly";
        if (keyusage[8]) kuText += (kuText.equals("")?"":", ") + "decipherOnly";
        return kuText;
    }
    
     public static String getNSCertTypeAsText(X509Certificate certificate){
        if (certificate == null)
            return null;

        byte[] nct = certificate.getExtensionValue(MiscObjectIdentifiers.netscapeCertType.getId());
		
        if (nct == null) return "";
        String nctText = "";
        if (nct[0]==0) nctText += "SSLClient";
        if (nct[1]==0) nctText += (nctText.equals("")?"":", ") + "SSLServer";
        if (nct[2]==0) nctText += (nctText.equals("")?"":", ") + "S/MIME";
        if (nct[3]==0) nctText += (nctText.equals("")?"":", ") + "Object Signing";
        if (nct[4]==0) nctText += (nctText.equals("")?"":", ") + "Reserved";
        if (nct[5]==0) nctText += (nctText.equals("")?"":", ") + "SSL CA";
        if (nct[6]==0) nctText += (nctText.equals("")?"":", ") + "S/MIME CA";
        if (nct[7]==0) nctText += (nctText.equals("")?"":", ") + "Object Signing CA";
        return nctText;
    }

     
     public static String getExtendedKeyUsageAsText(X509Certificate certificate){
        java.util.List extendedkeyusage = null;

        HashMap<String, String>   extendedkeyusageoidtotextmap = null;
        String[] EXTENDEDKEYUSAGEOIDSTRINGS = { "2.5.29.37.0",
                "1.3.6.1.5.5.7.3.0",
                "1.3.6.1.5.5.7.3.1",
                "1.3.6.1.5.5.7.3.2",
                "1.3.6.1.5.5.7.3.3",
                "1.3.6.1.5.5.7.3.4",
                "1.3.6.1.5.5.7.3.5",
                "1.3.6.1.5.5.7.3.6",
                "1.3.6.1.5.5.7.3.7",
                "1.3.6.1.5.5.7.3.8",
                "1.3.6.1.4.1.311.20.2.2",
        "1.3.6.1.5.5.7.3.9"};

        String[] EXTENDEDKEYUSAGETEXTS = { "All usages",
                "All usages",
                "Server authentication",
                "Client authentication",
                "Code signing",
                "Email protection",
                "IPSec end system",
                "IPSec tunnel",
                "IPSec user",
                "Timestamping",
                "Smartcard Logon",
        "OCSP signer"};

        // Build HashMap of Extended KeyUsage OIDs (String) to Text representation (String)
        extendedkeyusageoidtotextmap = new HashMap<String, String>();
        for(int i=0; i < EXTENDEDKEYUSAGETEXTS.length; i++)
            extendedkeyusageoidtotextmap.put(EXTENDEDKEYUSAGEOIDSTRINGS[i], EXTENDEDKEYUSAGETEXTS[i]);
        try{
            extendedkeyusage = certificate.getExtendedKeyUsage();
        } catch(java.security.cert.CertificateParsingException e){
            return null;
        }
        if(extendedkeyusage == null)
            extendedkeyusage = new java.util.ArrayList();

        /*String[] returnval = new String[extendedkeyusage.size()];
        for(int i1=0; i1 < extendedkeyusage.size(); i1++){
          returnval[i1] = (String) extendedkeyusageoidtotextmap.get(extendedkeyusage.get(i1));
        }*/

        String returnval = "";
        for(int i=0; i < extendedkeyusage.size(); i++)
            returnval += (returnval.equals("")?"":", ") + extendedkeyusageoidtotextmap.get(extendedkeyusage.get(i));
        return returnval;
    }
     
      public static String getDNAsShortText(Principal dn) {
        X509Principal X509dn = new X509Principal(dn.getName());
        //log.debug("extracting short name from dn " + X509dn);
        if (X509dn!=null && X509dn.getValues(X509Principal.CN).size()>0)
            return X509dn.getValues(X509Principal.CN).get(0).toString();
        else {
            String str_dn = dn.getName();
            int last_equal = str_dn.lastIndexOf("=");
            if (last_equal>=0)
                return str_dn.substring( last_equal+1, str_dn.length());
            return str_dn;
        }
    }
      
        public static String getPublicKeyInfo(PublicKey pk) {
        int keysize = 0;
        String format = pk.getAlgorithm();
        if (pk instanceof RSAPublicKey) {
            RSAPublicKey rsapk = (RSAPublicKey) pk;
            keysize = (rsapk.getModulus().toByteArray().length -1) * 8;
        }
        if (pk instanceof JCEECPublicKey) {
            JCEECPublicKey ecpubkey = (JCEECPublicKey) pk;
            keysize = ecpubkey.getQ().getX().getFieldSize();
            //ECParameterSpec ecpspecs = ecpubkey.getParams();
            format = "ECDSA";
            //if ( ecpspecs instanceof ECNamedCurveSpec ) {
            //	ECNamedCurveSpec ecncspec = (ECNamedCurveSpec) ecpspecs;
            //	format += " (" + ecncspec.getName() + ")";
            //}
        }
        if (pk instanceof JDKDSAPublicKey) {
            JDKDSAPublicKey dsapubkey = (JDKDSAPublicKey) pk;
            keysize = dsapubkey.getY().bitLength();
        }

        return  format + " " + keysize + "bits";
    }
}
