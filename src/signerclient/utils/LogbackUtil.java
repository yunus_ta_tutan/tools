/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.utils;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy;
import com.xuxux.utils.commons.MyStringUtil;

/**
 *
 * @author xuxux
 */
public class LogbackUtil {

    private static final String logPath = "/log/signerClient/signerClient.log";
    private static final String logPathArchive = "/log/signerClient/archive/signerClient-%d{yyyy-MM-dd}.log.gz";
    private static final String logPattern = "%d{HH:mm:ss.SSS, Turkey} [%thread] %-5level %logger{35} - %msg%n";
    private static final int maxFileHistory = 20;

    public static void configureLogBack(String userDir) {
        if(MyStringUtil.isEmpty(userDir))
            userDir = "C:/";
        Logger rootLogger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        loggerContext.reset();

        RollingFileAppender<ILoggingEvent> rfAppender = new RollingFileAppender<ILoggingEvent>();
        rfAppender.setContext(loggerContext);
        rfAppender.setFile(userDir + logPath);
        rfAppender.setAppend(true);

        TimeBasedRollingPolicy<ILoggingEvent> tbRollingPolicy = new TimeBasedRollingPolicy<ILoggingEvent>();
        tbRollingPolicy.setContext(loggerContext);
        tbRollingPolicy.setFileNamePattern(userDir + logPathArchive);
        tbRollingPolicy.setParent(rfAppender);
        tbRollingPolicy.setMaxHistory(maxFileHistory);
        tbRollingPolicy.start();
        rfAppender.setRollingPolicy(tbRollingPolicy);

        PatternLayoutEncoder patternLayoutEncoder = new PatternLayoutEncoder();
        patternLayoutEncoder.setContext(loggerContext);
        patternLayoutEncoder.setPattern(logPattern);
        patternLayoutEncoder.start();
        rfAppender.setEncoder(patternLayoutEncoder);

        rfAppender.start();

        rootLogger.addAppender(rfAppender);
    }

}
