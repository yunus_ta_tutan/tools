/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.utils;

import javax.swing.table.DefaultTableModel;
import signerclient.models.CERTVKNMAPPINGROOT;

/**
 *
 * @author xuxux
 */
public class ObjectMapper {
    
    public static DefaultTableModel convertTokenConfigToDefaultTableModel(CERTVKNMAPPINGROOT.CUSTOMER customer) throws Exception{
        throw new Exception("this method has not yet been implemented!");
    }
    
    public static CERTVKNMAPPINGROOT.CUSTOMER[] convertCertserialTableModelToCustomer(DefaultTableModel model) throws Exception{
        CERTVKNMAPPINGROOT.CUSTOMER[] customers = new CERTVKNMAPPINGROOT.CUSTOMER[model.getRowCount()];
        for (int i = 0; i < model.getRowCount(); i++){
            CERTVKNMAPPINGROOT.CUSTOMER customer = new CERTVKNMAPPINGROOT.CUSTOMER();
            customer.setVKN(model.getValueAt(i, 0).toString());
            customer.setSERIAL(model.getValueAt(i, 1).toString());
            customer.setALIAS(model.getValueAt(i, 2).toString());
            customer.setSLOT(model.getValueAt(i, 3).toString());
            customer.setPIN(model.getValueAt(i, 4).toString());
            customers[i] = customer;
        }
        return customers;
    }
    
}
