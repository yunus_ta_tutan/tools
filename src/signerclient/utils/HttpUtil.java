/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.utils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;

/**
 *
 * @author xuxux
 */
public class HttpUtil {

    private static final String USER_AGENT = "User-Agent: Apache-HttpClient/4.1.1 (java 1.5)";

    // HTTP GET request
    public static void sendGet(String url) throws Exception {

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println(response.toString());

    }

    // HTTP POST request
    public static String sendPost(String url, byte[] dataToBeSent) throws Exception {
        String up = "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:efat=\"http://gib.gov.tr/vedop3/eFatura\" xmlns:xm=\"http://www.w3.org/2005/05/xmlmime\"><soap:Header/><soap:Body>";
        String data = new String(dataToBeSent, "UTF-8");
        String down = "</soap:Body></soap:Envelope>";
        
        String alldata = up + data + down;

        URL obj = new URL(url);
        URLConnection connection = obj.openConnection();
        HttpURLConnection con = (HttpURLConnection) connection;

        System.out.println(new String(dataToBeSent, "UTF-8"));

        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        byte[] buffer = new byte[dataToBeSent.length];
        buffer = dataToBeSent;
        bout.write(buffer);
        bout.flush();
        byte[] b = bout.toByteArray();
        System.out.println("b.length:" + b.length);

        //add reuqest header

        con.setRequestProperty("Content-Length", String.valueOf(alldata.getBytes().length));
        con.setRequestProperty("Content-Type", "application/soap+xml;charset=UTF-8");
        con.setRequestProperty("Accept-Encoding", "gzip,deflate");
        con.setRequestProperty("Accept", "text/xml");
        con.setRequestProperty("transfer-encoding", "chunked");


        con.setRequestMethod("POST");
        con.setDoOutput(true);
        con.setDoInput(true);

        OutputStream out = con.getOutputStream();
        out.write(alldata.getBytes());
        out.flush();
        out.close();

        InputStreamReader isr = new InputStreamReader(con.getInputStream(), "UTF-8");
        BufferedReader in = new BufferedReader(isr);
        String outputString = "";
        String responseString = "";
        while ((outputString = in.readLine()) != null) {
            responseString = responseString + outputString;
        }

        //print result
        return responseString;

    }

    public static void main(String[] args) {
        try {
            sendPost("", new byte[20]);
        } catch (Exception ex) {
            System.err.println(ex.toString());
        }
    }
}
