package signerclient.utils;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.commons.codec.binary.Base64;

/**
 * @author xuxux
 * 
 */
public class CryptoUtil {

	public static String md5(byte[] value) throws NoSuchAlgorithmException {
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("MD5");
			digest.update(value, 0, value.length);
			BigInteger i = new BigInteger(1, digest.digest());
			return String.format("%1$032X", i);
		} finally {
			digest = null;
		}
	}

	public static String encode(byte[] value) {
		Base64 encoder;
		try {
			encoder = new Base64(true);
			return encoder.encodeAsString(value);
		} finally {
			encoder = null;
		}
               
	}

	public static byte[] decode(String value) throws IOException {
		Base64 decoder;
		try {
			decoder = new Base64(true);
			return decoder.decode(value);
		} finally {
			decoder = null;
		}
	}

	public static void main(String[] args) {

		Base64 base64;
		try {
			base64 = new Base64(true);
			String txt = "yunus";
			String encoded = base64.encodeAsString(txt.getBytes());
                        System.out.println(encoded);
                        byte[] decodedBytes = base64.decode(encoded);
                        System.out.println(new String(decodedBytes));
		} catch (Exception e){
                    System.err.println("hata");
                }

	}

}
