/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;

/**
 *
 * @author xuxux
 */
public class StringUtil {
    
    public static boolean isNullOrEmpty(String val){
        try {
            if(null == val)
                return true;
            if(val.trim().length() < 1)
                return true;
            return false;
        } catch (Exception e) {
            return true;
        }
    }

    public static String removeFileNameExtension(String fileName) {
        int dotIntex = fileName.lastIndexOf(".");
        if (dotIntex != -1)
            return fileName.substring(0, dotIntex);
        return fileName;
    }
    
    public static String formatString(String value, Object... params) {
		final MessageFormat temp = new MessageFormat(value);
		return temp.format(params);
	}

    public static String traceException(Throwable e) {
        StringBuilder builder = new StringBuilder(512);
        if (null != e.getCause()) {
            builder.append(traceException(e.getCause())).append("\n");;
        } else {
            builder.append(e.toString()).append("\n");;
            for (StackTraceElement ste : e.getStackTrace()) {
                builder.append(ste.toString()).append("\n");
            }
        }
        return builder.toString();
    }

    public static String parseError(Exception e) {
        Throwable cause = e.getCause();
        if (null != cause) {
            return (null != cause.getMessage() ? cause.getMessage() : cause.toString());
        }
        return (null != e.getMessage() ? e.getMessage() : e.toString());
    }

    public static URL cretaeUrl(String urlRoot, String servletRoot, String companyNo, String senderCompanyNo, String receiverCompanyNo, String invoiceNo, String documentType, String outputType) throws MalformedURLException, Exception {
        try {
            StringBuilder builder = new StringBuilder();
            builder.append(urlRoot).append(servletRoot);
            builder.append("companyNo=").append(companyNo).append("&").append("senderCompanyNo=").append(senderCompanyNo).append("&").append("receiverCompanyNo=").append(receiverCompanyNo).append("&").append("invoiceNo=").append(invoiceNo).append("&").append("documentType=").append(documentType).append("&").append("outputType=").append(outputType);
            System.out.println(builder.toString());
            URL url = new URL(builder.toString());
            return url;
        } catch (MalformedURLException mue) {
            mue.printStackTrace();
            throw mue;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }

    }

    public static String readInputStreamAsString(InputStream is) throws IOException {
        InputStreamReader isr = new InputStreamReader(is);
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(isr);
        String read = br.readLine();
        while (read != null) {
            //System.out.println(read);
            sb.append(read);
            read = br.readLine();
        }
        return sb.toString();

    }
}
