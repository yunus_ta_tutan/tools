/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.utils;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author xuxux
 */
public class DateUtil {

    private static final TimeZone timeZone = TimeZone.getTimeZone("Turkey");
    private static final Locale locale = new Locale("tr_TR");

    private static final SimpleDateFormat FORMAT_SHORT_STAMP = new SimpleDateFormat("yyyy-MM");
    private static final SimpleDateFormat FORMAT_SHORT = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat FORMAT_FULL = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");

    private final static SimpleDateFormat DATE_FORMAT_SIMPLE = new SimpleDateFormat("yyyyMMdd");

    static {
        TimeZone.setDefault(timeZone);
        FORMAT_SHORT.setTimeZone(timeZone);
        FORMAT_FULL.setTimeZone(timeZone);
    }

    public static long getTimeDifferenceAsMilliseconds(long lStartTime, long lEndTime) {
        long difference = lEndTime - lStartTime;
        return difference;
    }

    public static XMLGregorianCalendar toXmlDateTime(Date date) throws DatatypeConfigurationException {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        XMLGregorianCalendar xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
        xmlCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
        return xmlCalendar;
    }

    public static XMLGregorianCalendar toXmlDate(Date date) throws DatatypeConfigurationException {
        XMLGregorianCalendar xmlCalendar = toXmlDateTime(date);
        xmlCalendar.setHour(DatatypeConstants.FIELD_UNDEFINED);
        xmlCalendar.setMinute(DatatypeConstants.FIELD_UNDEFINED);
        xmlCalendar.setSecond(DatatypeConstants.FIELD_UNDEFINED);
        xmlCalendar.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
        return xmlCalendar;
    }

    public static Date parseDate(String value) throws Exception {
        Date date = null;
        if (null != value) {
            try {
                date = DATE_FORMAT_SIMPLE.parse(checkCorrectDateFormat(value), new ParsePosition(0));
            } catch (Throwable e) {
                throw new Exception(StringUtil.formatString("Date convertion error for [{0}]", value));
            }
            if (null == date)
                throw new Exception(StringUtil.formatString("Date convertion error for [{0}]", value));
        }
        return date;
    }

    private static String checkCorrectDateFormat(String value) throws Exception {
        if (value.contains("-")) {
            if (value.indexOf("-") == 4 && value.lastIndexOf("-") == 7) {
                return value.replace("-", "");
            }
            throw new Exception();
        }
        return value;
    }

}
