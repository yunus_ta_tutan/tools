/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

/**
 *
 * @author xuxux
 */
public class CompressionUtil {

    public static byte[] inflater(byte[] zippedFile) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Inflater decompresser = new Inflater();
        try {
            decompresser.setInput(zippedFile);
            byte[] buffer = new byte[1024];
            while (!decompresser.finished()) {
                final int decompressedSize = decompresser.inflate(buffer);
                baos.write(buffer, 0, decompressedSize);
            }
            return baos.toByteArray();
        } catch (DataFormatException e) {
            java.lang.System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                decompresser.end();
                baos.close();
            } catch (final IOException e) {
                java.lang.System.out.println(e.getMessage());
            }
        }
        return null;
    }

    // zipliyo
    public static byte[] deflater(byte[] bytes) {
        Deflater deflater = new Deflater(3);
        deflater.setInput(bytes);
        deflater.finish();
        ByteArrayOutputStream bos = new ByteArrayOutputStream(bytes.length);
        byte[] buffer = new byte[1024];
        while (!deflater.finished()) {
            int bytesCompressed = deflater.deflate(buffer);
            bos.write(buffer, 0, bytesCompressed);
        }
        try {
            bos.close();
        } catch (IOException ioe) {
            java.lang.System.out.println("Error while closing the stream : " + ioe);
        }

        return bos.toByteArray();
    }
}
