/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

/**
 *
 * @author xuxux
 */
public class PdfUtil {

    private static final Font FONT = new Font(FontFamily.HELVETICA, 50, Font.BOLD, BaseColor.RED);

    public static byte[] manipulatePdf(byte[] pdfData, String message) throws IOException, DocumentException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();

        PdfReader reader = new PdfReader(pdfData);
        PdfStamper stamper = new PdfStamper(reader, output);

        int n = reader.getNumberOfPages();
        for (int i = 1; i <= n; i++) {
            PdfContentByte over = stamper.getOverContent(i);
            Phrase phrase = new Phrase(message, FONT);
            over.saveState();
            PdfGState gs1 = new PdfGState();
            gs1.setFillOpacity(0.6f); // Yazinin koyulugunu ayarlanir.
            over.setGState(gs1);
            ColumnText.showTextAligned(over, Element.ALIGN_CENTER, phrase, 297, 450, 45);
            over.restoreState();
        }

        stamper.close();
        reader.close();

        return output.toByteArray();
    }

}
