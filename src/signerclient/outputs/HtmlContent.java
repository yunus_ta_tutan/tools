/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.outputs;

/**
 *
 * @author xuxux
 */
import javax.swing.JEditorPane;
import javax.swing.JFrame;

public class HtmlContent extends JFrame {

   public void start(String htmlStr) {
        try {
            JEditorPane ed1 = new JEditorPane("text/html", htmlStr);
            add(ed1);
            setVisible(true);
            setSize(600, 600);
            setDefaultCloseOperation(EXIT_ON_CLOSE);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Some problem has occured" + e.getMessage());
        }
    }
}
