/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient;

import signerclient.ui.SignerClientUI;
import signerclient.utils.LogbackUtil;



/**
 *
 * @author xuxux
 */
public class SignerClient {

    /**
     * @param args the command line arguments
     */
    static {
         LogbackUtil.configureLogBack(System.getProperty("user.home"));
    }
    
    public static void main(String[] args) {
        SignerClientUI clientUI = new SignerClientUI();
        clientUI.setLocationRelativeTo(null);
        clientUI.setVisible(true);
    }
}
