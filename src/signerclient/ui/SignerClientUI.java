/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.ui;

import com.fit.ledger.ws.client.CompressionType;
import com.fit.ledger.ws.client.SignDocumentHeaders;
import com.fit.ledger.ws.client.SignDocumentRequest;
import com.fit.ledger.ws.client.SignDocumentResponse;
import com.fit.ledger.ws.client.SignSOAPMessageFault;
import com.fit.ledger.ws.client.User;
import com.fitcons.earchive.GetInvoiceDocumentRequestType;
import com.fitcons.earchive.GetInvoiceDocumentResponseType;
import com.xuxux.utils.commons.MyCompressionUtil;
import com.xuxux.utils.commons.MyCryptoUtil;
import com.xuxux.utils.commons.MyNumberUtil;
import com.xuxux.utils.commons.MyStringUtil;
import com.xuxux.utils.commons.MyXmlUtil;
import com.xuxux.utils.files.MyFileUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.xml.bind.JAXBElement;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.Endpoint;
import javax.xml.ws.Holder;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import signerclient.exceptions.ProxyConfException;
import signerclient.exceptions.TokenException;
import signerclient.factory.ImplFactory;
import signerclient.filters.DllFileFilter;
import signerclient.filters.UblFolderFilter;
import signerclient.filters.XmlJFileChooserFilter;
import signerclient.filters.XsltFileFilter;
import signerclient.impl.SignProcessImpl;
import signerclient.models.CARD_TYPE;
import signerclient.models.CERTVKNMAPPINGROOT;
import signerclient.models.CertInfo;
import signerclient.models.ExtractedUblInfo;
import signerclient.models.ProxyParams;
import signerclient.models.TokenConfParams;
import signerclient.runnable.SendEnvelopeCallerThread;
import signerclient.runnable.SendInvoiceCallerThread;
import signerclient.services.ConfigOperationService;
import signerclient.services.FileOperationsService;
import signerclient.services.FolderExtractService;
import signerclient.services.HtmlToPdfService;
import signerclient.services.InvGeneratorService;
import signerclient.services.LedgerSignerService;
import signerclient.services.ProxyOperationsService;
import signerclient.services.SignProcessService;
import signerclient.services.ValidateProcessService;
import signerclient.services.XmlToHtmlService;
import signerclient.test.ws.EArsivSignaturePortTypeImpl;
import signerclient.tokenService.XuxuxKeyingDataProviderImpl;
import signerclient.tokenService.XuxuxPfxKeyingDataProviderImpl;
import signerclient.utils.CertificateUtil;
import signerclient.utils.CompressionUtil;
import signerclient.utils.DateUtil;
import signerclient.utils.GenericMarshallUnmarshallUtil;
import signerclient.utils.LogUtil;
import signerclient.utils.ObjectMapper;
import signerclient.utils.PdfUtil;
import signerclient.utils.SOAPUtil;
import signerclient.utils.SocketUtil;
import signerclient.utils.StringUtil;
import signerclient.utils.UIutils;
import signerclient.utils.ZipUtil;
import signerclient.ws.SignerResult;
import signerclient.ws.ValidatorResult;
import signerclient.wsclient.Invoker;
import signerclient.wsclient.ResendInvoker;
import wsClients.Base64Binary;
import wsClients.DocumentReturnType;
import wsClients.DocumentType;

/**
 *
 * @author xuxux
 */
public class SignerClientUI extends javax.swing.JFrame {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(SignerClientUI.class);
    private int xSignTime = 1;
    private int xVerifyTime = 1;
    private int collectionCount = 1000;
    private static final int MEGABYTE = (1024 * 1024);
    private static final String _INVOICENS = "urn:oasis:names:specification:ubl:schema:xsd:Invoice-2";
    private static final String _APPRESPONSENS = "urn:oasis:names:specification:ubl:schema:xsd:ApplicationResponse-2";
    private static final String _STANDARTBDHNS = "http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader";
    private static final String _DOCUMENTREQNS = "http://gib.gov.tr/vedop3/eFatura";
    private String pin = null;

    private Map<String, String> instancePortMap = new HashMap<String, String>();
    Map<String, String> vknCertMap = new HashMap();

    /**
     * Creates new form SignerClientUI
     */
    public SignerClientUI() {
        setTitle("FIT Signer WS Invoker");
        System.setProperty("javax.xml.bind.JAXBContext",
                "com.sun.xml.internal.bind.v2.ContextFactory");
        initComponents();
        arcInvWsEndpointTF.setText("http://pi730cpd:50000/eArsivInvoiceService/EArsivInvoicePortTypeImplBean?source=FILE&ip=SignerWsClient&VKN_TCKN=1111111111&un=xuxux");
        resendAddressTF.setText("http://piserver:51000/EFatura/EFaturaPortTypeImplBean");
        ledgerWSendpointTF.setText("http://localhost:8080/LedgerSigner/services/SignerPort");
        ledgerDataToBeSignedTF.setText("D:\\StaticFiles\\defter_test_data\\imzatest1.tmp");
        ledgerVknTF.setText("3840091158");
        resendFileTF.setText("C:\\Users\\xuxux\\Desktop\\turkcell\\test.XML");
        pkcs11RB.setSelected(true);
        xadesBesRB1.setSelected(true);
        issueDateChooser.setCurrent(Calendar.getInstance());
        issueDateChooser.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
        initProxyDialog();
        initSetupDialog();
    }

    public void initSetupDialog() {
        DefaultListModel<String> model = new DefaultListModel<String>();
        esInstancePortJL.setModel(model);
        earcSignerPathTF.setText("D:\\TMP\\EARC_SIGNER");
    }

    public void initProxyDialog() {
        ProxyOperationsService proxyOperationsService = ImplFactory.proxyOperationsInstance();
        Map<String, String> proxyParams;
        try {
            proxyParams = proxyOperationsService.getSavedProxyConfiguration();
            proxyCheckBox.setSelected(proxyParams.get(ProxyParams.PROXY_SET.toString()).equalsIgnoreCase("true") ? true : false);
            hostnameTF.setText(proxyParams.get(ProxyParams.PROXY_HOSTNAME.toString()));
            portTF.setText(proxyParams.get(ProxyParams.PROXY_PORT.toString()));
            usernameTF1.setText(proxyParams.get(ProxyParams.PROXY_USERNAME.toString()));
            passwordTF1.setText(proxyParams.get(ProxyParams.PROXY_PASSWORD.toString()));
            LOGGER.debug("proxy configuration params initialized successfully");
        } catch (ProxyConfException pce) {
            LOGGER.error(pce.toString());
        } catch (Exception e) {
            LOGGER.error(e.toString());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dllFileChooser = new javax.swing.JFileChooser();
        dataFolderChooser = new javax.swing.JFileChooser();
        excelOutFileChooser = new javax.swing.JFileChooser();
        xmlFileChooser = new javax.swing.JFileChooser();
        documentTypeBG = new javax.swing.ButtonGroup();
        outputTypeBG = new javax.swing.ButtonGroup();
        ledgerSignedFC = new javax.swing.JFileChooser();
        xadesFormatBG = new javax.swing.ButtonGroup();
        keyingDataProviderBG = new javax.swing.ButtonGroup();
        pdfOutFolderChooser = new javax.swing.JFileChooser();
        xsltFileChooser = new javax.swing.JFileChooser();
        proxyDialog = new javax.swing.JDialog();
        jLabel25 = new javax.swing.JLabel();
        saveProxySettingsButton = new javax.swing.JButton();
        proxyCheckBox = new javax.swing.JCheckBox();
        jLabel26 = new javax.swing.JLabel();
        hostnameTF = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        portTF = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        usernameTF1 = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        passwordTF1 = new javax.swing.JTextField();
        documentTypeforXA_BG = new javax.swing.ButtonGroup();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jTabbedPane3 = new javax.swing.JTabbedPane();
        Inv_Env_BG = new javax.swing.ButtonGroup();
        setupEarcSignerJD1 = new javax.swing.JDialog();
        jLabel40 = new javax.swing.JLabel();
        earcSignerPathTF = new javax.swing.JTextField();
        earcSignerFolderChooserJB = new javax.swing.JButton();
        esCancelSetup1 = new javax.swing.JButton();
        esNextSetup1 = new javax.swing.JButton();
        jLabel41 = new javax.swing.JLabel();
        esInstancePortTF = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        esInstancePortJL = new javax.swing.JList();
        esInstanceAddJB = new javax.swing.JButton();
        esInstanceRemoveJB = new javax.swing.JButton();
        earcSignerFolderChooser = new javax.swing.JFileChooser();
        setupEarcSignerJD2 = new javax.swing.JDialog();
        jButton3 = new javax.swing.JButton();
        esNextSetup2 = new javax.swing.JButton();
        jLabel39 = new javax.swing.JLabel();
        archiveRB = new javax.swing.JRadioButton();
        ticketRB = new javax.swing.JRadioButton();
        slotListIndexTF = new javax.swing.JTextField();
        jLabel44 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        cardTypeCB = new javax.swing.JComboBox();
        usePkcs11CB = new javax.swing.JCheckBox();
        usePkcs11JL = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        aliasTF = new javax.swing.JTextField();
        jLabel48 = new javax.swing.JLabel();
        pkcs11PassTF = new javax.swing.JTextField();
        jLabel49 = new javax.swing.JLabel();
        pfxPassTF = new javax.swing.JTextField();
        jLabel50 = new javax.swing.JLabel();
        providerPathTF = new javax.swing.JTextField();
        dllFilePathChooserJB = new javax.swing.JButton();
        projectBG = new javax.swing.ButtonGroup();
        setupEarcSignerJD3 = new javax.swing.JDialog();
        jScrollPane4 = new javax.swing.JScrollPane();
        certSerialJT = new javax.swing.JTable();
        certificateCB = new javax.swing.JComboBox();
        jLabel43 = new javax.swing.JLabel();
        addCertificateJB = new javax.swing.JButton();
        saveCertVknMapJB = new javax.swing.JButton();
        removeCertificateJB = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jTextField1 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        CallSignMethodButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        CallValidateMethodButton = new javax.swing.JButton();
        ClearButton = new javax.swing.JButton();
        jCheckBox1 = new javax.swing.JCheckBox();
        jLabel7 = new javax.swing.JLabel();
        xTimeFormattedTextField = new javax.swing.JFormattedTextField();
        xTimeVerifyTF = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        concurrentSignButton = new javax.swing.JButton();
        concurrentValidateButton = new javax.swing.JButton();
        validateSelectedButton = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        ConnectButton = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        dllBrowseButton = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        testSignXTimeIF = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        xadesFormatPane = new javax.swing.JLayeredPane();
        xadesBesRB1 = new javax.swing.JRadioButton();
        xadesARB1 = new javax.swing.JRadioButton();
        pkcs11RB = new javax.swing.JRadioButton();
        pfxRB = new javax.swing.JRadioButton();
        wssRB = new javax.swing.JRadioButton();
        jPanel7 = new javax.swing.JPanel();
        validationBrowseB = new javax.swing.JButton();
        selectedValidationFileTF = new javax.swing.JTextField();
        validateB = new javax.swing.JButton();
        exportCB = new javax.swing.JCheckBox();
        jPanel9 = new javax.swing.JPanel();
        paketIdTF = new javax.swing.JTextField();
        getBatchStatusButton = new javax.swing.JButton();
        testSignButton = new javax.swing.JButton();
        invRB = new javax.swing.JRadioButton();
        repRB = new javax.swing.JRadioButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        ublFolderTextField = new javax.swing.JTextField();
        ublFolderChooserButton = new javax.swing.JButton();
        testButton = new javax.swing.JButton();
        clearButton = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        counterLabel = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        collectionCountField = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        resendFileTF = new javax.swing.JTextField();
        resendFileBrowseButton = new javax.swing.JButton();
        resendButton = new javax.swing.JButton();
        jLabel13 = new javax.swing.JLabel();
        resendAddressTF = new javax.swing.JTextField();
        CleanButton = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        rootUrlTF = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        companynoTF = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        senderTF = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        receiverTF = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        invoicenoTF = new javax.swing.JTextField();
        jLayeredPane1 = new javax.swing.JLayeredPane();
        invoiceDT_RB = new javax.swing.JRadioButton();
        baprDT_RB = new javax.swing.JRadioButton();
        jLayeredPane2 = new javax.swing.JLayeredPane();
        htmloutputRB = new javax.swing.JRadioButton();
        pdfoutputRB = new javax.swing.JRadioButton();
        pngoutputRB = new javax.swing.JRadioButton();
        xmloutputRB = new javax.swing.JRadioButton();
        xmldownloadRB = new javax.swing.JRadioButton();
        envxmldownloadRB = new javax.swing.JRadioButton();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        usernameTF = new javax.swing.JTextField();
        passwordTF = new javax.swing.JPasswordField();
        jButton2 = new javax.swing.JButton();
        saveDataCB = new javax.swing.JCheckBox();
        ClearButton2 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jLabel21 = new javax.swing.JLabel();
        ledgerWSendpointTF = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        ledgerDataToBeSignedTF = new javax.swing.JTextField();
        ledgerBrowseBT = new javax.swing.JButton();
        callLegderSignerB = new javax.swing.JButton();
        jLabel23 = new javax.swing.JLabel();
        ledgerVknTF = new javax.swing.JTextField();
        saveSignedLedgerCB = new javax.swing.JCheckBox();
        ledgerDefaultInputCB = new javax.swing.JCheckBox();
        clearB = new javax.swing.JButton();
        ledgerIsBeratCB = new javax.swing.JCheckBox();
        jButton6 = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jLabel24 = new javax.swing.JLabel();
        invoiceFolderTF = new javax.swing.JTextField();
        invoiceFolderChooseB = new javax.swing.JButton();
        pdfExportB = new javax.swing.JButton();
        xsltLable = new javax.swing.JLabel();
        xsltFilePathTF = new javax.swing.JTextField();
        xsltFileChooseB = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        embeddedXsltCB = new javax.swing.JCheckBox();
        wsPanel = new javax.swing.JPanel();
        portLabel = new javax.swing.JLabel();
        wsPortTF = new javax.swing.JTextField();
        publishButton = new javax.swing.JButton();
        jPanel10 = new javax.swing.JPanel();
        arcInvWsEndpointTF = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        arcInvFolderTF = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        invoiceFolderChooser = new javax.swing.JButton();
        sendInvoiceBT = new javax.swing.JButton();
        jPanel11 = new javax.swing.JPanel();
        jLabel32 = new javax.swing.JLabel();
        arcInvOutFolderTF = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        arcInvZipCountTF = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        arcInvXmlCountPerZipTF = new javax.swing.JTextField();
        generrateInvBT = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jLabel35 = new javax.swing.JLabel();
        firstFileNameTF = new javax.swing.JTextField();
        issueDateChooser = new datechooser.beans.DateChooserCombo();
        jLabel37 = new javax.swing.JLabel();
        branchTF = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        responsiveOutputCB = new javax.swing.JComboBox();
        ENV_RB = new javax.swing.JRadioButton();
        INV_RB = new javax.swing.JRadioButton();
        vkn_tcknTF = new javax.swing.JTextField();
        jLabel38 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        ws_userTF = new javax.swing.JTextField();
        jLabel51 = new javax.swing.JLabel();
        ws_passTF = new javax.swing.JTextField();
        jPanel12 = new javax.swing.JPanel();
        setupEarcSignerJB = new javax.swing.JButton();
        jPanel13 = new javax.swing.JPanel();
        jLabel52 = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        vknTf = new javax.swing.JTextField();
        callGetInvoiceDocumentButton = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        invoiceIDTA = new javax.swing.JTextArea();
        jLabel54 = new javax.swing.JLabel();
        endPointTF = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextPane1 = new javax.swing.JTextPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        exitMenuItem = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        proxyConfMenuItem = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        aboutMenuItem = new javax.swing.JMenuItem();

        dllFileChooser.setFileFilter(new DllFileFilter());

        dataFolderChooser.setFileFilter(new UblFolderFilter());
        dataFolderChooser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dataFolderChooserActionPerformed(evt);
            }
        });

        xmlFileChooser.setFileFilter(new XmlJFileChooserFilter());
        xmlFileChooser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                xmlFileChooserActionPerformed(evt);
            }
        });

        xsltFileChooser.setFileFilter(new XsltFileFilter());

        proxyDialog.setAlwaysOnTop(true);
        proxyDialog.setModal(true);
        proxyDialog.setResizable(false);

        jLabel25.setFont(new java.awt.Font("Tempus Sans ITC", 1, 18)); // NOI18N
        jLabel25.setText("PROXY SETTINGS");

        saveProxySettingsButton.setText("Save");
        saveProxySettingsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveProxySettingsButtonActionPerformed(evt);
            }
        });

        proxyCheckBox.setText("use proxy");

        jLabel26.setText("hostname:");

        jLabel27.setText("port");

        jLabel28.setText("username:");

        jLabel29.setText("password:");

        javax.swing.GroupLayout proxyDialogLayout = new javax.swing.GroupLayout(proxyDialog.getContentPane());
        proxyDialog.getContentPane().setLayout(proxyDialogLayout);
        proxyDialogLayout.setHorizontalGroup(
            proxyDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(proxyDialogLayout.createSequentialGroup()
                .addGap(99, 99, 99)
                .addGroup(proxyDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel25)
                    .addGroup(proxyDialogLayout.createSequentialGroup()
                        .addComponent(jLabel29)
                        .addGap(18, 18, 18)
                        .addComponent(passwordTF1))
                    .addGroup(proxyDialogLayout.createSequentialGroup()
                        .addComponent(jLabel28)
                        .addGap(18, 18, 18)
                        .addComponent(usernameTF1, javax.swing.GroupLayout.DEFAULT_SIZE, 149, Short.MAX_VALUE))
                    .addComponent(proxyCheckBox)
                    .addGroup(proxyDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(saveProxySettingsButton)
                        .addGroup(proxyDialogLayout.createSequentialGroup()
                            .addGroup(proxyDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel26)
                                .addComponent(jLabel27))
                            .addGap(18, 18, 18)
                            .addGroup(proxyDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(portTF, javax.swing.GroupLayout.DEFAULT_SIZE, 149, Short.MAX_VALUE)
                                .addComponent(hostnameTF)))))
                .addContainerGap(107, Short.MAX_VALUE))
        );
        proxyDialogLayout.setVerticalGroup(
            proxyDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(proxyDialogLayout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(jLabel25)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(proxyCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(proxyDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel26)
                    .addComponent(hostnameTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(proxyDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel27)
                    .addComponent(portTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14)
                .addGroup(proxyDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel28)
                    .addComponent(usernameTF1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(proxyDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel29)
                    .addComponent(passwordTF1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                .addComponent(saveProxySettingsButton)
                .addContainerGap())
        );

        jLabel40.setText("EARC_SIGNER PATH:");

        earcSignerPathTF.setEditable(false);

        earcSignerFolderChooserJB.setText("...");
        earcSignerFolderChooserJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                earcSignerFolderChooserJBActionPerformed(evt);
            }
        });

        esCancelSetup1.setText("Cancel");
        esCancelSetup1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                esCancelSetup1ActionPerformed(evt);
            }
        });

        esNextSetup1.setText("Next");
        esNextSetup1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                esNextSetup1ActionPerformed(evt);
            }
        });

        jLabel41.setText("INSTANCE PORT:");

        esInstancePortJL.setBorder(new javax.swing.border.MatteBorder(null));
        jScrollPane2.setViewportView(esInstancePortJL);

        esInstanceAddJB.setText("Add>>");
        esInstanceAddJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                esInstanceAddJBActionPerformed(evt);
            }
        });

        esInstanceRemoveJB.setText("<<Remove");
        esInstanceRemoveJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                esInstanceRemoveJBActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout setupEarcSignerJD1Layout = new javax.swing.GroupLayout(setupEarcSignerJD1.getContentPane());
        setupEarcSignerJD1.getContentPane().setLayout(setupEarcSignerJD1Layout);
        setupEarcSignerJD1Layout.setHorizontalGroup(
            setupEarcSignerJD1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(setupEarcSignerJD1Layout.createSequentialGroup()
                .addGroup(setupEarcSignerJD1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(setupEarcSignerJD1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(setupEarcSignerJD1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel41)
                            .addComponent(jLabel40))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(setupEarcSignerJD1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(setupEarcSignerJD1Layout.createSequentialGroup()
                                .addComponent(esInstancePortTF, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(46, 46, 46)
                                .addGroup(setupEarcSignerJD1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(esInstanceRemoveJB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(esInstanceAddJB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 148, Short.MAX_VALUE))
                            .addComponent(earcSignerPathTF))
                        .addGap(18, 18, 18))
                    .addGroup(setupEarcSignerJD1Layout.createSequentialGroup()
                        .addGap(224, 224, 224)
                        .addComponent(esCancelSetup1)
                        .addGap(274, 274, 274)
                        .addComponent(esNextSetup1)
                        .addGap(18, 18, Short.MAX_VALUE)))
                .addComponent(earcSignerFolderChooserJB, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(207, Short.MAX_VALUE))
        );
        setupEarcSignerJD1Layout.setVerticalGroup(
            setupEarcSignerJD1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(setupEarcSignerJD1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(setupEarcSignerJD1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(setupEarcSignerJD1Layout.createSequentialGroup()
                        .addGroup(setupEarcSignerJD1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel41)
                            .addComponent(esInstancePortTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(esInstanceAddJB))
                        .addGap(18, 18, 18)
                        .addComponent(esInstanceRemoveJB)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 219, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(setupEarcSignerJD1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel40)
                    .addComponent(earcSignerPathTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(earcSignerFolderChooserJB))
                .addGap(55, 55, 55)
                .addGroup(setupEarcSignerJD1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(esCancelSetup1)
                    .addComponent(esNextSetup1))
                .addGap(42, 42, 42))
        );

        jButton3.setText("Cancel");

        esNextSetup2.setText("Next");
        esNextSetup2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                esNextSetup2ActionPerformed(evt);
            }
        });

        jLabel39.setText("PROJECT:");

        projectBG.add(archiveRB);
        archiveRB.setText("ARCHIVE");

        projectBG.add(ticketRB);
        ticketRB.setText("TICKET");

        jLabel44.setText("SLOT_LIST_INDEX:");

        jLabel45.setText("CARD_TYPE:");

        cardTypeCB.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "THALES", "SAFENET", "UTIMACO", "AKIS" }));

        usePkcs11CB.setSelected(true);
        usePkcs11CB.setText("Use PKCS11");
        usePkcs11CB.setEnabled(false);

        usePkcs11JL.setText("USE PKCS11:");

        jLabel47.setText("ALIAS:");

        jLabel48.setText("PKCS11_PASS:");

        jLabel49.setText("PFX_PASS:");

        jLabel50.setText("PROVIDER_PATH:");

        providerPathTF.setEditable(false);

        dllFilePathChooserJB.setText("...");
        dllFilePathChooserJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dllFilePathChooserJBActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout setupEarcSignerJD2Layout = new javax.swing.GroupLayout(setupEarcSignerJD2.getContentPane());
        setupEarcSignerJD2.getContentPane().setLayout(setupEarcSignerJD2Layout);
        setupEarcSignerJD2Layout.setHorizontalGroup(
            setupEarcSignerJD2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(setupEarcSignerJD2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(setupEarcSignerJD2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel50)
                    .addGroup(setupEarcSignerJD2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(setupEarcSignerJD2Layout.createSequentialGroup()
                            .addGroup(setupEarcSignerJD2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel44)
                                .addComponent(jLabel45)
                                .addComponent(usePkcs11JL))
                            .addGap(18, 18, 18)
                            .addGroup(setupEarcSignerJD2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(setupEarcSignerJD2Layout.createSequentialGroup()
                                    .addGroup(setupEarcSignerJD2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(usePkcs11CB)
                                        .addComponent(cardTypeCB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(esNextSetup2, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(slotListIndexTF)))
                        .addGroup(setupEarcSignerJD2Layout.createSequentialGroup()
                            .addComponent(jLabel39)
                            .addGap(241, 241, 241)
                            .addComponent(archiveRB)
                            .addGap(18, 18, 18)
                            .addComponent(ticketRB))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, setupEarcSignerJD2Layout.createSequentialGroup()
                            .addGroup(setupEarcSignerJD2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel48)
                                .addComponent(jLabel47))
                            .addGap(52, 52, 52)
                            .addGroup(setupEarcSignerJD2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(aliasTF)
                                .addComponent(pkcs11PassTF)))
                        .addGroup(setupEarcSignerJD2Layout.createSequentialGroup()
                            .addComponent(jLabel49)
                            .addGap(81, 81, 81)
                            .addGroup(setupEarcSignerJD2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(setupEarcSignerJD2Layout.createSequentialGroup()
                                    .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(0, 0, Short.MAX_VALUE))
                                .addComponent(providerPathTF)
                                .addComponent(pfxPassTF)))))
                .addGap(18, 18, 18)
                .addComponent(dllFilePathChooserJB, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(311, Short.MAX_VALUE))
        );
        setupEarcSignerJD2Layout.setVerticalGroup(
            setupEarcSignerJD2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, setupEarcSignerJD2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(setupEarcSignerJD2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel39)
                    .addComponent(archiveRB)
                    .addComponent(ticketRB))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(setupEarcSignerJD2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel44)
                    .addComponent(slotListIndexTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(setupEarcSignerJD2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel45)
                    .addComponent(cardTypeCB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(setupEarcSignerJD2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(usePkcs11JL)
                    .addComponent(usePkcs11CB))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(setupEarcSignerJD2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel47)
                    .addComponent(aliasTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(setupEarcSignerJD2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel48)
                    .addComponent(pkcs11PassTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(setupEarcSignerJD2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel49)
                    .addComponent(pfxPassTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(setupEarcSignerJD2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel50)
                    .addGroup(setupEarcSignerJD2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(providerPathTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(dllFilePathChooserJB)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 69, Short.MAX_VALUE)
                .addGroup(setupEarcSignerJD2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(esNextSetup2)
                    .addComponent(jButton3))
                .addContainerGap())
        );

        certSerialJT.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "VKN", "SERIAL", "ALIAS", "SLOT", "PIN"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, false, false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane4.setViewportView(certSerialJT);

        certificateCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                certificateCBActionPerformed(evt);
            }
        });

        jLabel43.setText("Select Certificate:");

        addCertificateJB.setText("Add");
        addCertificateJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addCertificateJBActionPerformed(evt);
            }
        });

        saveCertVknMapJB.setText("Save");
        saveCertVknMapJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveCertVknMapJBActionPerformed(evt);
            }
        });

        removeCertificateJB.setText("Remove");
        removeCertificateJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeCertificateJBActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout setupEarcSignerJD3Layout = new javax.swing.GroupLayout(setupEarcSignerJD3.getContentPane());
        setupEarcSignerJD3.getContentPane().setLayout(setupEarcSignerJD3Layout);
        setupEarcSignerJD3Layout.setHorizontalGroup(
            setupEarcSignerJD3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(setupEarcSignerJD3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(setupEarcSignerJD3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 869, Short.MAX_VALUE)
                    .addGroup(setupEarcSignerJD3Layout.createSequentialGroup()
                        .addComponent(jLabel43)
                        .addGap(117, 117, 117)
                        .addComponent(certificateCB, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, setupEarcSignerJD3Layout.createSequentialGroup()
                        .addComponent(saveCertVknMapJB)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addCertificateJB)
                        .addGap(31, 31, 31)
                        .addComponent(removeCertificateJB)))
                .addContainerGap())
        );
        setupEarcSignerJD3Layout.setVerticalGroup(
            setupEarcSignerJD3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(setupEarcSignerJD3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(setupEarcSignerJD3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(certificateCB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel43))
                .addGap(10, 10, 10)
                .addGroup(setupEarcSignerJD3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addCertificateJB)
                    .addComponent(saveCertVknMapJB)
                    .addComponent(removeCertificateJB))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 293, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(jTable1);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jTextField1.setText("http://localhost:8080/signer/services/SignerPort");
        jTextField1.setToolTipText("");

        jLabel1.setText("WS EndPoint:");

        CallSignMethodButton.setLabel("Call Sign Method Repeatedly");
        CallSignMethodButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CallSignMethodButtonActionPerformed(evt);
            }
        });

        jLabel2.setText("VKN:");

        jTextField2.setText("3840091158");

        CallValidateMethodButton.setLabel("Call Validate Method Repeatedly");
        CallValidateMethodButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CallValidateMethodButtonActionPerformed(evt);
            }
        });

        ClearButton.setText("Clear Log Pane");
        ClearButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ClearButtonActionPerformed(evt);
            }
        });

        jCheckBox1.setText("Show Signed Data");

        jLabel7.setText("X time");

        xTimeFormattedTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        xTimeFormattedTextField.setText("1");

        xTimeVerifyTF.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        xTimeVerifyTF.setText("1");

        jLabel10.setText("X time");

        concurrentSignButton.setText("Call Sign Method Concurrently");

        concurrentValidateButton.setText("Call Validate Method Concurrently");

        validateSelectedButton.setText("Validate Selected Inv/Env...");
        validateSelectedButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validateSelectedButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(xTimeFormattedTextField)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2)
                    .addComponent(xTimeVerifyTF))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField2)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel10))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(CallValidateMethodButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(CallSignMethodButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(concurrentValidateButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jCheckBox1)
                            .addComponent(concurrentSignButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addComponent(validateSelectedButton)
                        .addGap(122, 122, 122)
                        .addComponent(ClearButton)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCheckBox1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CallSignMethodButton)
                    .addComponent(jLabel7)
                    .addComponent(xTimeFormattedTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(concurrentSignButton))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 104, Short.MAX_VALUE)
                        .addComponent(ClearButton)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(CallValidateMethodButton)
                            .addComponent(xTimeVerifyTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10)
                            .addComponent(concurrentValidateButton)
                            .addComponent(validateSelectedButton))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        jTabbedPane1.addTab("Web Service Testing", jPanel1);

        jLabel3.setText("Provider dll:");

        jTextField3.setEditable(false);
        jTextField3.setText("Choose dll with browse button.");

        ConnectButton.setText("Connect To Device");
        ConnectButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ConnectButtonActionPerformed(evt);
            }
        });

        jLabel4.setText("Certificate:");

        jComboBox1.setSelectedItem(null);
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        dllBrowseButton.setText("Browse...");
        dllBrowseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dllBrowseButtonActionPerformed(evt);
            }
        });

        jLabel5.setText("Data to be signed:");

        jTextField4.setEditable(false);

        jButton1.setText("Browse...");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        testSignXTimeIF.setText("1");

        jLabel11.setText("X Time");

        xadesFormatBG.add(xadesBesRB1);
        xadesBesRB1.setText("XADES-BES");
        xadesBesRB1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                xadesBesRB1ActionPerformed(evt);
            }
        });
        xadesFormatPane.add(xadesBesRB1);
        xadesBesRB1.setBounds(10, 10, 100, 29);

        xadesFormatBG.add(xadesARB1);
        xadesARB1.setText("XADES-A");
        xadesFormatPane.add(xadesARB1);
        xadesARB1.setBounds(10, 30, 100, 29);

        keyingDataProviderBG.add(pkcs11RB);
        pkcs11RB.setText("PKCS#11");
        pkcs11RB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pkcs11RBActionPerformed(evt);
            }
        });
        xadesFormatPane.add(pkcs11RB);
        pkcs11RB.setBounds(140, 10, 120, 29);

        keyingDataProviderBG.add(pfxRB);
        pfxRB.setText("PKCS#12(with pfx file)");
        pfxRB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pfxRBActionPerformed(evt);
            }
        });
        xadesFormatPane.add(pfxRB);
        pfxRB.setBounds(140, 40, 150, 29);

        xadesFormatBG.add(wssRB);
        wssRB.setText("WSS");
        xadesFormatPane.add(wssRB);
        wssRB.setBounds(10, 50, 100, 29);

        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        validationBrowseB.setText("Browse...");
        validationBrowseB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validationBrowseBActionPerformed(evt);
            }
        });

        selectedValidationFileTF.setEditable(false);

        validateB.setText("Validate");
        validateB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validateBActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(selectedValidationFileTF, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(validateB, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(validationBrowseB, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(validationBrowseB)
                    .addComponent(selectedValidationFileTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(validateB)
                .addContainerGap())
        );

        exportCB.setText("export");

        getBatchStatusButton.setText("Get Batch Status");
        getBatchStatusButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                getBatchStatusButtonActionPerformed(evt);
            }
        });

        testSignButton.setText("Test Sign");
        testSignButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                testSignButtonActionPerformed(evt);
            }
        });

        documentTypeforXA_BG.add(invRB);
        invRB.setText("invoice");

        documentTypeforXA_BG.add(repRB);
        repRB.setText("report");

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(paketIdTF)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(invRB)
                            .addComponent(repRB))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 122, Short.MAX_VALUE)
                        .addComponent(testSignButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(getBatchStatusButton)))
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(paketIdTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(getBatchStatusButton)
                            .addComponent(testSignButton))
                        .addContainerGap())
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(invRB)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(repRB))))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jComboBox1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 434, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(27, 27, 27)
                                        .addComponent(dllBrowseButton)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(ConnectButton))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 435, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(26, 26, 26)
                                        .addComponent(jButton1)
                                        .addGap(18, 18, 18)
                                        .addComponent(testSignXTimeIF, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel11)))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(xadesFormatPane, javax.swing.GroupLayout.PREFERRED_SIZE, 316, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(33, 33, 33)
                        .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(exportCB)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(26, 26, 26))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ConnectButton)
                    .addComponent(dllBrowseButton)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1)
                    .addComponent(testSignXTimeIF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(exportCB))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(xadesFormatPane, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Local Token Access Testing", jPanel2);

        jLabel6.setText("Input Folder:");

        ublFolderTextField.setEditable(false);

        ublFolderChooserButton.setText("Browse...");
        ublFolderChooserButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ublFolderChooserButtonActionPerformed(evt);
            }
        });

        testButton.setText("Export to Excel");
        testButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                testButtonActionPerformed(evt);
            }
        });

        clearButton.setText("Clear Log Pane");
        clearButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearButtonActionPerformed(evt);
            }
        });

        jLabel8.setText("Record Counter:");

        counterLabel.setText("X");

        jLabel9.setText("Collection Count:");

        collectionCountField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        collectionCountField.setText("10000");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(clearButton)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addComponent(jLabel6)
                            .addGap(27, 27, 27)
                            .addComponent(ublFolderTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 525, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(ublFolderChooserButton)
                            .addGap(18, 18, 18)
                            .addComponent(testButton)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(counterLabel))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(collectionCountField, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(331, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(ublFolderTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ublFolderChooserButton)
                    .addComponent(testButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(collectionCountField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 108, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(counterLabel))
                .addGap(15, 15, 15)
                .addComponent(clearButton)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Excell Export", jPanel3);

        jLabel12.setText("File:");

        resendFileTF.setEditable(false);

        resendFileBrowseButton.setText("Browse...");
        resendFileBrowseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resendFileBrowseButtonActionPerformed(evt);
            }
        });

        resendButton.setText("Resend");
        resendButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resendButtonActionPerformed(evt);
            }
        });

        jLabel13.setText("Resend Address:");

        CleanButton.setText("Clear Log Pane");
        CleanButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CleanButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addGap(18, 18, 18)
                        .addComponent(resendFileTF, javax.swing.GroupLayout.PREFERRED_SIZE, 491, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(resendAddressTF)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(resendButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(resendFileBrowseButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 441, Short.MAX_VALUE)
                .addComponent(CleanButton)
                .addGap(20, 20, 20))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(resendFileTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(resendFileBrowseButton)
                    .addComponent(CleanButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(resendButton)
                    .addComponent(jLabel13)
                    .addComponent(resendAddressTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(192, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("SendToPortal", jPanel4);

        jLabel14.setText("Root URL:");

        rootUrlTF.setText("http://piserver:51000");

        jLabel15.setText("Company No:");

        companynoTF.setText("3840091158");

        jLabel16.setText("Sender Comp. No:");

        senderTF.setText("3840091158");

        jLabel17.setText("Receiver Comp. No:");

        receiverTF.setText("1111111028");

        jLabel18.setText("Invoice No:");

        invoicenoTF.setText("GIB2013000000040");

        jLayeredPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLayeredPane1.setToolTipText("Document Type");
        jLayeredPane1.setName(""); // NOI18N

        documentTypeBG.add(invoiceDT_RB);
        invoiceDT_RB.setText("INVOICE");
        jLayeredPane1.add(invoiceDT_RB);
        invoiceDT_RB.setBounds(20, 10, 100, 29);

        documentTypeBG.add(baprDT_RB);
        baprDT_RB.setText("B_APR");
        jLayeredPane1.add(baprDT_RB);
        baprDT_RB.setBounds(20, 30, 100, 29);

        jLayeredPane2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        outputTypeBG.add(htmloutputRB);
        htmloutputRB.setText("HTML Output");
        jLayeredPane2.add(htmloutputRB);
        htmloutputRB.setBounds(10, 10, 127, 29);

        outputTypeBG.add(pdfoutputRB);
        pdfoutputRB.setText("PDF Output");
        jLayeredPane2.add(pdfoutputRB);
        pdfoutputRB.setBounds(10, 40, 115, 29);

        outputTypeBG.add(pngoutputRB);
        pngoutputRB.setText("PNG Output");
        jLayeredPane2.add(pngoutputRB);
        pngoutputRB.setBounds(10, 70, 117, 29);

        outputTypeBG.add(xmloutputRB);
        xmloutputRB.setText("XML Output");
        jLayeredPane2.add(xmloutputRB);
        xmloutputRB.setBounds(160, 10, 150, 29);

        outputTypeBG.add(xmldownloadRB);
        xmldownloadRB.setText("XML Download");
        jLayeredPane2.add(xmldownloadRB);
        xmldownloadRB.setBounds(160, 40, 137, 29);

        outputTypeBG.add(envxmldownloadRB);
        envxmldownloadRB.setText("Env. XML Download");
        jLayeredPane2.add(envxmldownloadRB);
        envxmldownloadRB.setBounds(160, 70, 173, 29);

        jLabel19.setText("user:");

        jLabel20.setText("pass:");

        usernameTF.setText("IWUSER");

        passwordTF.setText("Ft12Cs");

        jButton2.setText("Make Request");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        saveDataCB.setText("Save Output");

        ClearButton2.setText("Clear Logs");
        ClearButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ClearButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel14)
                    .addComponent(jLabel15)
                    .addComponent(jLabel16)
                    .addComponent(jLabel18)
                    .addComponent(jLabel17))
                .addGap(26, 26, 26)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(rootUrlTF, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                    .addComponent(companynoTF)
                    .addComponent(senderTF)
                    .addComponent(receiverTF)
                    .addComponent(invoicenoTF))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLayeredPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel5Layout.createSequentialGroup()
                            .addComponent(jLabel20)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(passwordTF))
                        .addGroup(jPanel5Layout.createSequentialGroup()
                            .addComponent(jLabel19)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(usernameTF, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(37, 37, 37)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLayeredPane2)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jButton2)
                        .addGap(59, 59, 59)
                        .addComponent(saveDataCB)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 223, Short.MAX_VALUE)
                        .addComponent(ClearButton2)))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLayeredPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton2)
                            .addComponent(saveDataCB)
                            .addComponent(ClearButton2)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel14)
                                    .addComponent(rootUrlTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(companynoTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel15))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(senderTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel16)))
                            .addComponent(jLayeredPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel19)
                                .addComponent(usernameTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(receiverTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel17)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel18)
                            .addComponent(invoicenoTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel20)
                            .addComponent(passwordTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(93, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Get Outputs", jPanel5);

        jLabel21.setText("WS Endpoint:");

        jLabel22.setText("Data to be signed:");

        ledgerDataToBeSignedTF.setEditable(false);

        ledgerBrowseBT.setText("Browse...");
        ledgerBrowseBT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ledgerBrowseBTActionPerformed(evt);
            }
        });

        callLegderSignerB.setText("Call Sign Doc Sevice");
        callLegderSignerB.setActionCommand("Call Sign Doc Sevice");
        callLegderSignerB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                callLegderSignerBActionPerformed(evt);
            }
        });

        jLabel23.setText("VKN:");

        saveSignedLedgerCB.setText("Save Signed Document");

        ledgerDefaultInputCB.setText("Use Default Test Data");
        ledgerDefaultInputCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ledgerDefaultInputCBActionPerformed(evt);
            }
        });

        clearB.setText("Clear Log Pane");
        clearB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearBActionPerformed(evt);
            }
        });

        ledgerIsBeratCB.setText("is Berat?");

        jButton6.setText("Sign Soap Message");
        jButton6.setActionCommand("callSignSoapMessage");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(clearB))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel22)
                            .addComponent(jLabel21)
                            .addComponent(jLabel23))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ledgerWSendpointTF, javax.swing.GroupLayout.PREFERRED_SIZE, 493, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(ledgerVknTF, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(ledgerDataToBeSignedTF, javax.swing.GroupLayout.PREFERRED_SIZE, 493, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jButton6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(callLegderSignerB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(ledgerBrowseBT, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel6Layout.createSequentialGroup()
                                        .addComponent(ledgerDefaultInputCB)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(ledgerIsBeratCB))
                                    .addComponent(saveSignedLedgerCB))))
                        .addGap(0, 120, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ledgerWSendpointTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(ledgerDataToBeSignedTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ledgerBrowseBT)
                    .addComponent(ledgerDefaultInputCB)
                    .addComponent(ledgerIsBeratCB))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(saveSignedLedgerCB)
                    .addComponent(callLegderSignerB)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel23))
                    .addComponent(ledgerVknTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 53, Short.MAX_VALUE)
                .addComponent(clearB)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Ledger Signer WS Client", jPanel6);

        jLabel24.setText("XML input:");

        invoiceFolderTF.setEditable(false);

        invoiceFolderChooseB.setText("Browse...");
        invoiceFolderChooseB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                invoiceFolderChooseBActionPerformed(evt);
            }
        });

        pdfExportB.setText("Export as PDF");
        pdfExportB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pdfExportBActionPerformed(evt);
            }
        });

        xsltLable.setText("XSLT file:");

        xsltFilePathTF.setEditable(false);

        xsltFileChooseB.setText("Browse...");
        xsltFileChooseB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                xsltFileChooseBActionPerformed(evt);
            }
        });

        jButton4.setText("Clear Log Pane");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        embeddedXsltCB.setText("use embedded XSLT");
        embeddedXsltCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                embeddedXsltCBActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel24)
                            .addComponent(xsltLable))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(invoiceFolderTF)
                            .addComponent(xsltFilePathTF, javax.swing.GroupLayout.DEFAULT_SIZE, 463, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(invoiceFolderChooseB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(xsltFileChooseB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(22, 22, 22)
                        .addComponent(embeddedXsltCB)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 226, Short.MAX_VALUE)
                        .addComponent(pdfExportB))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(invoiceFolderTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(invoiceFolderChooseB))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(xsltLable)
                    .addComponent(xsltFilePathTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(xsltFileChooseB)
                    .addComponent(embeddedXsltCB)
                    .addComponent(pdfExportB))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 147, Short.MAX_VALUE)
                .addComponent(jButton4)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Export PDF", jPanel8);

        portLabel.setText("Port:");

        publishButton.setText("Publish");
        publishButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                publishButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout wsPanelLayout = new javax.swing.GroupLayout(wsPanel);
        wsPanel.setLayout(wsPanelLayout);
        wsPanelLayout.setHorizontalGroup(
            wsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(wsPanelLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(portLabel)
                .addGap(18, 18, 18)
                .addComponent(wsPortTF, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(publishButton)
                .addContainerGap(1018, Short.MAX_VALUE))
        );
        wsPanelLayout.setVerticalGroup(
            wsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(wsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(wsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(portLabel)
                    .addComponent(wsPortTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(publishButton))
                .addContainerGap(230, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Run As WS", wsPanel);

        arcInvWsEndpointTF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                arcInvWsEndpointTFActionPerformed(evt);
            }
        });

        jLabel30.setText("endpoint:");

        arcInvFolderTF.setEditable(false);
        arcInvFolderTF.setText("C:\\Users\\xuxux\\Desktop\\1111111111\\envelope");

        jLabel31.setText("source:");

        invoiceFolderChooser.setText("...");
        invoiceFolderChooser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                invoiceFolderChooserActionPerformed(evt);
            }
        });

        sendInvoiceBT.setText("start");
        sendInvoiceBT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendInvoiceBTActionPerformed(evt);
            }
        });

        jLabel32.setText("target:");

        arcInvOutFolderTF.setEditable(false);
        arcInvOutFolderTF.setText("C:\\Users\\xuxux\\Desktop\\1111111111\\envelope");

        jLabel33.setText("zip count:");
        jLabel33.setToolTipText("");

        arcInvZipCountTF.setText("1");

        jLabel34.setText("xml count per zip:");

        arcInvXmlCountPerZipTF.setText("1");

        generrateInvBT.setText("Generate");
        generrateInvBT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generrateInvBTActionPerformed(evt);
            }
        });

        jButton5.setText("...");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jLabel35.setText("first file:");

        firstFileNameTF.setText("1");

        jLabel37.setText("issue_date:");

        branchTF.setText("default");

        jLabel36.setText("BRANCH:");

        jLabel42.setText("OUTPUT:");

        responsiveOutputCB.setEditable(true);
        responsiveOutputCB.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "NONE", "XML", "HTML", "PDF", "PNG" }));

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel34)
                            .addComponent(jLabel33))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(arcInvXmlCountPerZipTF, javax.swing.GroupLayout.DEFAULT_SIZE, 68, Short.MAX_VALUE)
                            .addComponent(arcInvZipCountTF))
                        .addGap(52, 52, 52)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel35)
                            .addComponent(jLabel37))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addComponent(issueDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(generrateInvBT))
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addComponent(firstFileNameTF, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addComponent(jLabel32)
                        .addGap(18, 18, 18)
                        .addComponent(arcInvOutFolderTF, javax.swing.GroupLayout.PREFERRED_SIZE, 490, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 70, Short.MAX_VALUE)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel36)
                            .addComponent(jLabel42))
                        .addGap(37, 37, 37)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(branchTF)
                            .addComponent(responsiveOutputCB, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel32)
                    .addComponent(arcInvOutFolderTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel33)
                    .addComponent(arcInvZipCountTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel35)
                    .addComponent(firstFileNameTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(arcInvXmlCountPerZipTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel37))
                    .addComponent(jLabel34)
                    .addComponent(issueDateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(25, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(branchTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel36))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel42)
                    .addComponent(responsiveOutputCB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(generrateInvBT)
                .addContainerGap())
        );

        Inv_Env_BG.add(ENV_RB);
        ENV_RB.setSelected(true);
        ENV_RB.setText("Envelope");
        ENV_RB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ENV_RBActionPerformed(evt);
            }
        });

        Inv_Env_BG.add(INV_RB);
        INV_RB.setText("Invoice");

        vkn_tcknTF.setText("1111111111");

        jLabel38.setText("VKN/TCKN:");

        jLabel46.setText("ws_user:");

        jLabel51.setText("ws_pass:");

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGap(66, 66, 66)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel30)
                            .addComponent(jLabel31)))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel51)
                            .addComponent(jLabel46))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(arcInvFolderTF, javax.swing.GroupLayout.PREFERRED_SIZE, 515, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(arcInvWsEndpointTF, javax.swing.GroupLayout.PREFERRED_SIZE, 515, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(ENV_RB)
                            .addGroup(jPanel10Layout.createSequentialGroup()
                                .addComponent(invoiceFolderChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(147, 147, 147)))
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel10Layout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addComponent(INV_RB)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel10Layout.createSequentialGroup()
                                .addGap(128, 128, 128)
                                .addComponent(jLabel38)
                                .addGap(26, 26, 26)
                                .addComponent(vkn_tcknTF)
                                .addContainerGap())))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(sendInvoiceBT)
                            .addComponent(ws_userTF, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
                            .addComponent(ws_passTF))
                        .addGap(41, 41, 41)
                        .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(arcInvWsEndpointTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel30)
                    .addComponent(ENV_RB)
                    .addComponent(INV_RB))
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(arcInvFolderTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel31)
                            .addComponent(invoiceFolderChooser))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel10Layout.createSequentialGroup()
                                .addComponent(sendInvoiceBT)
                                .addGap(10, 10, 10)
                                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel46)
                                    .addComponent(ws_userTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel51)
                                    .addComponent(ws_passTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(vkn_tcknTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel38))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("EarcWsClient", jPanel10);

        setupEarcSignerJB.setText("Start Setup...");
        setupEarcSignerJB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setupEarcSignerJBActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGap(545, 545, 545)
                .addComponent(setupEarcSignerJB)
                .addContainerGap(592, Short.MAX_VALUE))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                .addContainerGap(126, Short.MAX_VALUE)
                .addComponent(setupEarcSignerJB)
                .addGap(120, 120, 120))
        );

        jTabbedPane1.addTab("EARC_SIGNER Setup", jPanel12);

        jLabel52.setText("VKN:");

        jLabel53.setText("INV_ID:");

        vknTf.setText("1111111111");

        callGetInvoiceDocumentButton.setText("CALL");
        callGetInvoiceDocumentButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                callGetInvoiceDocumentButtonActionPerformed(evt);
            }
        });

        invoiceIDTA.setColumns(20);
        invoiceIDTA.setRows(5);
        jScrollPane5.setViewportView(invoiceIDTA);

        jLabel54.setText("ENDPOINT:");

        endPointTF.setText("http://pi730cpd:50000/eArsivInvoiceService/EArsivInvoicePortTypeImplBean");

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel52)
                    .addComponent(jLabel53))
                .addGap(25, 25, 25)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(callGetInvoiceDocumentButton)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(vknTf, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel54)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(endPointTF))
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 938, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(228, Short.MAX_VALUE))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel52)
                    .addComponent(vknTf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel54)
                    .addComponent(endPointTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(jLabel53)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(callGetInvoiceDocumentButton)
                .addContainerGap())
        );

        jTabbedPane1.addTab("EARC_getInvoiceDocument", jPanel13);

        jTextPane1.setBackground(new java.awt.Color(51, 51, 51));
        jTextPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jScrollPane1.setViewportView(jTextPane1);

        jMenu1.setText("File");

        exitMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        exitMenuItem.setText("Exit");
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(exitMenuItem);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");

        proxyConfMenuItem.setText("Proxy Configuration");
        proxyConfMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                proxyConfMenuItemActionPerformed(evt);
            }
        });
        jMenu2.add(proxyConfMenuItem);

        jMenuBar1.add(jMenu2);

        helpMenu.setText("Help");

        aboutMenuItem.setText("About");
        helpMenu.add(aboutMenuItem);

        jMenuBar1.add(helpMenu);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
            .addComponent(jScrollPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jTabbedPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void CallSignMethodButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CallSignMethodButtonActionPerformed
        if (null == xTimeFormattedTextField.getText() || xTimeFormattedTextField.getText().trim().length() < 1 || !MyNumberUtil.isNumber(xTimeFormattedTextField.getText())) {
            LogUtil.addError(jTextPane1, "X time field must be numeric!");
            return;
        }
        if (null == jTextField1.getText() || jTextField1.getText().length() < 1) {
            LogUtil.addWarning(jTextPane1, "You must input an EndPoint address for 'SignerService' web service!");
            return;
        }
        if (null == jTextField2.getText() || jTextField2.getText().length() < 1) {
            LogUtil.addWarning(jTextPane1, "You must input a VKN which was already defined on 'Signer app'!");
            return;
        }
        xSignTime = Math.abs(Integer.parseInt(xTimeFormattedTextField.getText()));
        new Thread(new Runnable() {
            @Override
            public void run() {
                CallSignMethodButton.setEnabled(false);
                CallValidateMethodButton.setEnabled(false);
                jTextField1.setEnabled(false);
                jTextField2.setEnabled(false);
                ClearButton.setEnabled(false);
                jCheckBox1.setEnabled(false);
                LogUtil.addWarning(jTextPane1, "CALLING SIGN METHOD OF " + jTextField1.getText() + " WEB SERVICE!");
                for (int i = 0; i < xSignTime; i++) {
                    SignerResult result = null;
                    try {
                        SignProcessService signProcessService = ImplFactory.signProcessInstance();
                        Document defaultDocument = signProcessService.getDefaultDocument();
                        String dataToBeSigned = signProcessService.changeSenderVkn(defaultDocument, jTextField2.getText());
                        result = signProcessService.invokeSignMethod(dataToBeSigned, jTextField1.getText());
                        int signResult = result.getResult();
                        if (signResult == 1) {
                            LogUtil.addInfo(jTextPane1, "signing result is: " + signResult + " - SUCCESSFULL!");
                            if (jCheckBox1.isSelected()) {
                                LogUtil.addInfo(jTextPane1, "Signed Data is:\n" + result.getEnvelope());
                            }
                        } else {
                            JAXBElement<String> note = result.getNote();
                            if (null != note) {
                                LogUtil.addWarning(jTextPane1, note.getValue());
                            }
                        }

                    } catch (Exception e) {
                        LogUtil.addError(jTextPane1, e.toString());
                        if (null != result && null != result.getNote()) {
                            LogUtil.addWarning(jTextPane1, result.getNote().getValue());
                        }
                    }
                }

                CallSignMethodButton.setEnabled(true);
                CallValidateMethodButton.setEnabled(true);
                jTextField1.setEnabled(true);
                jTextField2.setEnabled(true);
                ClearButton.setEnabled(true);
                jCheckBox1.setEnabled(true);
            }
        }).start();
    }//GEN-LAST:event_CallSignMethodButtonActionPerformed

    private void CallValidateMethodButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CallValidateMethodButtonActionPerformed

        xVerifyTime = Math.abs(Integer.parseInt(xTimeVerifyTF.getText()));
        new Thread(new Runnable() {
            @Override
            public void run() {
                CallSignMethodButton.setEnabled(false);
                CallValidateMethodButton.setEnabled(false);
                jTextField1.setEnabled(false);
                jTextField2.setEnabled(false);
                ClearButton.setEnabled(false);
                jCheckBox1.setEnabled(false);

                if (null == jTextField1.getText() || jTextField1.getText().length() < 1) {
                    LogUtil.addError(jTextPane1, "INVALID ENDPOINT!");
                    return;
                }
                LogUtil.addWarning(jTextPane1, "CALLING VALIDATE METHOD OF " + jTextField1.getText() + " WEB SERVICE!");
                ValidateProcessService validateProcessService = ImplFactory.validateProcessInstance();
                for (int i = 0; i < xVerifyTime; i++) {
                    ValidatorResult validatorResult = null;
                    try {
                        String dataToBeValidated = validateProcessService.getDefaultDocument();
                        validatorResult = validateProcessService.invokeValidateMethod(dataToBeValidated, jTextField1.getText());
                        int valResult = validatorResult.getResult();
                        LogUtil.addInfo(jTextPane1, "validation result is: " + valResult + " - SUCCESSFULL!");
                        JAXBElement<String> note = validatorResult.getNote();
                        LogUtil.addInfo(jTextPane1, note.getValue());
                    } catch (Exception e) {
                        LogUtil.addError(jTextPane1, e.toString());
                        if (null != validatorResult && null != validatorResult.getNote()) {
                            LogUtil.addError(jTextPane1, validatorResult.getNote().getValue());
                        }
                    }
                }
                CallSignMethodButton.setEnabled(true);
                CallValidateMethodButton.setEnabled(true);
                jTextField1.setEnabled(true);
                jTextField2.setEnabled(true);
                ClearButton.setEnabled(true);
                jCheckBox1.setEnabled(true);
            }
        }).start();
    }//GEN-LAST:event_CallValidateMethodButtonActionPerformed

    private void ClearButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ClearButtonActionPerformed
        try {
            LogUtil.clearPane(jTextPane1);
        } catch (Exception e) {
            LogUtil.addError(jTextPane1, e.toString());
        }
    }//GEN-LAST:event_ClearButtonActionPerformed

    private void dllBrowseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dllBrowseButtonActionPerformed
        File defaultFile = new File("C:\\Windows\\System32\\akisp11.dll");
        if (null != defaultFile && defaultFile.exists()) {
            dllFileChooser.setSelectedFile(defaultFile);
        }

        int returnVal = dllFileChooser.showOpenDialog(null);
        File dllFile = dllFileChooser.getSelectedFile();
        while (!dllFile.exists() && returnVal == JFileChooser.APPROVE_OPTION) {
            returnVal = dllFileChooser.showOpenDialog(null);
            dllFile = dllFileChooser.getSelectedFile();
            LogUtil.addWarning(jTextPane1, "Choosen file does not exist!");
        }
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            jTextField3.setText(dllFile.getPath());
            XuxuxKeyingDataProviderImpl.setProviderPath(dllFile.getPath());
            LogUtil.addInfo(jTextPane1, "Provider module is selected!");
        } else {
            LogUtil.addWarning(jTextPane1, "Provider selection is cancelled!");
        }
    }//GEN-LAST:event_dllBrowseButtonActionPerformed

    private void ConnectButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ConnectButtonActionPerformed
        jComboBox1.removeAllItems();
        if (pkcs11RB.isSelected()) {
            // PKCS#11 implementation
            try {
                XuxuxKeyingDataProviderImpl.setLogPane(jTextPane1);
                XuxuxKeyingDataProviderImpl.refreshAllTokens();
                Map<String, CertInfo> certMap = XuxuxKeyingDataProviderImpl.getCertificateMap();
                for (String key : certMap.keySet()) {
                    jComboBox1.addItem(key);
                }
            } catch (Exception e) {
                LogUtil.addError(jTextPane1, "Error while accessing! Err:" + e.toString() + " CAUSE:" + e.getCause());
                StringWriter errors = new StringWriter();
                e.printStackTrace(new PrintWriter(errors));
                LogUtil.addError(jTextPane1, "e.toString(): " + e.toString());
                LogUtil.addError(jTextPane1, "e.printStackTrace(): " + errors.toString());
            }
        } else if (pfxRB.isSelected()) {
            // PKCS#12(pfx) implementation
            try {
                XuxuxPfxKeyingDataProviderImpl.loadPfx();
                Map<String, signerclient.helpers.xadesHelpers.CertInfo> certMap = XuxuxPfxKeyingDataProviderImpl.getCertificateMap();
                for (String key : certMap.keySet()) {
                    jComboBox1.addItem(key);
                }
            } catch (Exception e) {
            }
        } else {
            LogUtil.addWarning(jTextPane1, "PKCS implementation must be selected! Operation has been stopped.");
        }
    }//GEN-LAST:event_ConnectButtonActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void ublFolderChooserButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ublFolderChooserButtonActionPerformed
        File defaultFolder = new File("D:\\EFATURA\\ARSIV\\RESEND\\IN\\");
        if (null != defaultFolder && defaultFolder.exists()) {
            dataFolderChooser.setSelectedFile(defaultFolder);
        }
        dataFolderChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = dataFolderChooser.showOpenDialog(null);
        if (returnVal == dataFolderChooser.APPROVE_OPTION) {
            File folder = dataFolderChooser.getSelectedFile();
            //This is where a real application would open the file.
            ublFolderTextField.setText(folder.getPath());
            LogUtil.addInfo(jTextPane1, folder.getPath() + " directory is selected.");
        } else {
            LogUtil.addWarning(jTextPane1, "Input folder selection cancelled!");
        }
    }//GEN-LAST:event_ublFolderChooserButtonActionPerformed

    private void dataFolderChooserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dataFolderChooserActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_dataFolderChooserActionPerformed

    private void testButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_testButtonActionPerformed

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    int outFileNameIndex = 1;
                    String firstOutputFileName = null;
                    File selectedFile = null;
                    collectionCount = Integer.parseInt(collectionCountField.getText());
                    if (null != ublFolderTextField.getText() && ublFolderTextField.getText().length() > 1) {
                        if (excelOutFileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                            selectedFile = excelOutFileChooser.getSelectedFile();
                        } else {
                            LogUtil.addWarning(jTextPane1, "Export cancelled!");
                            return;
                        }
                        FolderExtractService folderExtractService = ImplFactory.folderExtractInstance(jTextPane1);
                        List<ExtractedUblInfo> fullList = new ArrayList<ExtractedUblInfo>(10);
                        int counter = 0;
                        int labelCounter = 0;
                        String files[] = folderExtractService.getAllXmlFileNamesUnderFolder(ublFolderTextField.getText());
                        for (String file : files) {
                            labelCounter++;
                            counter++;
                            counterLabel.setText(labelCounter + " adet");
                            if (counter <= collectionCount) {
                                List<ExtractedUblInfo> tmpList = null;
                                try {
                                    tmpList = Arrays.asList(folderExtractService.extractInfosFromFile(ublFolderTextField.getText() + "/" + file));
                                } catch (Exception e) {
                                    LogUtil.addError(jTextPane1, e.toString());
                                    continue;
                                }
                                fullList.addAll(tmpList);
                            }
                            if (counter >= collectionCount) {
                                counter = 0;
                                selectedFile = new File(excelOutFileChooser.getSelectedFile().getParent() + "\\" + StringUtil.removeFileNameExtension(excelOutFileChooser.getSelectedFile().getName()) + outFileNameIndex++ + ".xls");
                                folderExtractService.getExcelList(fullList, new FileOutputStream(selectedFile), "Test");
                                fullList = null;
                                fullList = new ArrayList<ExtractedUblInfo>(10);
                            }
                        }

                        if (counter > 0) {
                            selectedFile = new File(excelOutFileChooser.getSelectedFile().getParent() + "\\" + StringUtil.removeFileNameExtension(excelOutFileChooser.getSelectedFile().getName()) + outFileNameIndex++ + ".xls");
                            folderExtractService.getExcelList(fullList, new FileOutputStream(selectedFile), "Test");
                            fullList = null;
                            fullList = new ArrayList<ExtractedUblInfo>(10);
                            counter = 0;
                        }
                    } else {
                        JOptionPane.showMessageDialog(rootPane, "Please select an input folder!", "Warning", JOptionPane.WARNING_MESSAGE);
                    }
                    LogUtil.addInfo(jTextPane1, "Export completed!");

                } catch (Exception e) {
                    LogUtil.addError(jTextPane1, e.toString());
                    e.printStackTrace();
                }
            }
        }).start();

    }//GEN-LAST:event_testButtonActionPerformed

    private void clearButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearButtonActionPerformed
        try {
            counterLabel.setText("");
            LogUtil.clearPane(jTextPane1);
        } catch (Exception e) {
            LogUtil.addError(jTextPane1, e.toString());
        }
    }//GEN-LAST:event_clearButtonActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        int returnVal = xmlFileChooser.showDialog(this, "Choose");
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            jTextField4.setText(xmlFileChooser.getSelectedFile().getPath());
        } else {
            jTextField4.setText(null);
            LogUtil.addWarning(jTextPane1, "File choosing operation cancelled!");
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void testSignButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_testSignButtonActionPerformed
        new Thread(new Runnable() {
            @Override
            public void run() {
                pin = null;
                byte[] signedData = null;
                try {
                    SignProcessService signProcessService = ImplFactory.signProcessInstance();
                    int xTime = Integer.parseInt(testSignXTimeIF.getText());
                    if (xadesBesRB1.isSelected()) {
                        int mode = -1;
                        if (pkcs11RB.isSelected()) {
                            XuxuxKeyingDataProviderImpl.setAliasPin(jComboBox1.getSelectedItem().toString(), UIutils.askForPin("Pin giriniz!"));
                            XuxuxKeyingDataProviderImpl.setVknAlias("3840091158", jComboBox1.getSelectedItem().toString());
                            mode = 0;
                        } else if (pfxRB.isSelected()) {
                            XuxuxPfxKeyingDataProviderImpl.setSelectedAlias(jComboBox1.getSelectedItem().toString());
                            mode = 1;
                        } else {
                            LogUtil.addWarning(jTextPane1, "Please select PKCS mode! Operation has been stopped.");
                            return;
                        }
                        // xades BES imza
                        SignProcessImpl.logPane = jTextPane1;
                        for (int i = 0; i < xTime; i++) {
                            signedData = signProcessService.localXadesBesSign(MyXmlUtil.parse(new FileInputStream(jTextField4.getText())), jComboBox1.getSelectedItem().toString(), mode);
                            if (!exportCB.isSelected()) {
                                LogUtil.addInfo(jTextPane1, "length:" + signedData.length);
                            } else {
                                if (excelOutFileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                                    String destination = excelOutFileChooser.getSelectedFile().getParent() + "\\" + StringUtil.removeFileNameExtension(excelOutFileChooser.getSelectedFile().getName()) + ".xml";
                                    MyFileUtil.writeByteArrayToFile(signedData, destination);
                                    LogUtil.addInfo(jTextPane1, "Signed data has been written to: " + destination);
                                } else {
                                    LogUtil.addWarning(jTextPane1, "Export of signed data has been cancelled!");
                                    return;
                                }
                            }
                        }

                    } else if (xadesARB1.isSelected()) {
                        // burada xades - A imza atılacak
                        if (MyStringUtil.isEmpty(testSignXTimeIF.getText())) {
                            LogUtil.addWarning(jTextPane1, "Please enter X Time parameter!!!");
                            return;
                        }
                        if (!MyNumberUtil.isNumber(testSignXTimeIF.getText())) {
                            LogUtil.addWarning(jTextPane1, "X Time parameter must be numeric!!!");
                            return;
                        }
                        if (pin == null) {
                            pin = UIutils.askForPin("PIN?");
                        }

                        for (int i = 0; i < xTime; i++) {
                            /**
                             * TODO: burada ma3 ile xades-A imza atilacak
                             */
                            if (repRB.isSelected()) {
                                // rapor dosyasi icin
                                signedData = signProcessService.localXadesASignArchiveReportWithMA3(MyFileUtil.readBytesFromFile(xmlFileChooser.getSelectedFile()), UIutils.askForPin("CONFIG PATH?"), null, pin);
                            } else if (invRB.isSelected()) {
                                // fatura dosyasi icin
                                signedData = signProcessService.localXadesASignArchiveInvoiceWithMA3(MyFileUtil.readBytesFromFile(xmlFileChooser.getSelectedFile()), UIutils.askForPin("CONFIG PATH?"), null, pin);
                            } else {
                                LogUtil.addWarning(jTextPane1, "Please select document type: 'invoice' or 'report'!");
                                return;
                            }

                            if (!exportCB.isSelected()) {
                                LogUtil.addInfo(jTextPane1, "length:" + signedData.length);
                            } else {
                                if (excelOutFileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                                    String destination = excelOutFileChooser.getSelectedFile().getParent() + "\\" + StringUtil.removeFileNameExtension(excelOutFileChooser.getSelectedFile().getName()) + ".xml";
                                    MyFileUtil.writeByteArrayToFile(signedData, destination);
                                    LogUtil.addInfo(jTextPane1, "Signed data has been written to: " + destination);
                                } else {
                                    LogUtil.addWarning(jTextPane1, "Export of signed data has been cancelled!");
                                    return;
                                }
                            }
                        }
                    } else if (wssRB.isSelected()) {
                        //burada earsiv rapor gonderim web servisi icin soap mesajı imzalanacak
                        Document signedRequest = signProcessService.localWssSignatureForSendDocument(jComboBox1.getSelectedItem().toString(), UIutils.askForPin("Pin Giriniz..."), jTextField3.getText(), jTextField4.getText());
                        if (!exportCB.isSelected()) {
                            LogUtil.addInfo(jTextPane1, "Invoking web service with signed soap message, will print response to the console...");
                            SOAPMessage response = SOAPUtil.invoke(signedRequest, UIutils.askForPin("Enter web service URL!"));
                            LogUtil.addInfo(jTextPane1, SOAPUtil.prettyPrint(SOAPUtil.toDocument(response)));
                        } else {
                            if (excelOutFileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                                String destination = excelOutFileChooser.getSelectedFile().getParent() + "\\" + StringUtil.removeFileNameExtension(excelOutFileChooser.getSelectedFile().getName()) + ".xml";
                                MyXmlUtil.transform(signedRequest, new FileOutputStream(destination));
                                LogUtil.addInfo(jTextPane1, "Signed data has been written to: " + destination);
                            } else {
                                LogUtil.addWarning(jTextPane1, "Export of signed data has been cancelled!");
                                return;
                            }
                        }
                    }

                } catch (TokenException tex) {
                    LOGGER.error(StringUtil.traceException(tex));
                    LogUtil.addError(jTextPane1, "GÜMMMMMMM!" + tex.getErrorCode());
                } catch (Exception ex) {
                    LOGGER.error(StringUtil.traceException(ex));
                    LogUtil.addError(jTextPane1, "GÜMMMMMMM2!" + ex.getMessage());
                }
            }
        }).start();
    }//GEN-LAST:event_testSignButtonActionPerformed

    private void resendFileBrowseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resendFileBrowseButtonActionPerformed
        int returnVal = xmlFileChooser.showDialog(this, "Choose xml file!");
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            resendFileTF.setText(xmlFileChooser.getSelectedFile().getPath());
        } else {
            resendFileTF.setText(null);
            LogUtil.addWarning(jTextPane1, "File choosing operation cancelled!");
        }
    }//GEN-LAST:event_resendFileBrowseButtonActionPerformed

    private void resendButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resendButtonActionPerformed
        if (resendAddressTF.getText().length() < 1 || resendFileTF.getText().length() < 1) {
            LogUtil.addError(jTextPane1, "Incorrect parameters!");
            return;
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                MemoryMXBean memoryBean = ManagementFactory.getMemoryMXBean();

                FileInputStream fis = null;
                File inFile = null;
                try {
                    resendAddressTF.setEditable(false);
                    resendButton.setEnabled(false);
                    resendFileBrowseButton.setEnabled(false);

                    inFile = new File(resendFileTF.getText());
                    fis = new FileInputStream(inFile);
                    Document document = MyXmlUtil.parse(fis);
                    String file = MyXmlUtil.getFirstContext(document.getElementsByTagName("fileName"));
                    String data = MyXmlUtil.getFirstContext(document.getElementsByTagName("binaryData"));
                    String hash = MyXmlUtil.getFirstContext(document.getElementsByTagName("hash"));

                    DocumentType documentType = new DocumentType();
                    Base64Binary base64Binary = new Base64Binary();
                    base64Binary.setValue(MyCryptoUtil.commonsDecodeBase64byteArray(data));
                    base64Binary.setContentType("application/octet-stream");
                    documentType.setBinaryData(base64Binary);
                    documentType.setFileName(file);
                    documentType.setHash(hash);
                    DocumentReturnType documentReturnType = ResendInvoker.sendDocument(documentType, resendAddressTF.getText());
                    LogUtil.addInfo(jTextPane1, "HASH:" + documentReturnType.getHash() + "  MSG:" + documentReturnType.getMsg());

                } catch (IOException ex) {
                    LogUtil.addError(jTextPane1, ex.toString());
                } catch (Exception ex) {
                    LogUtil.addError(jTextPane1, ex.toString());
                } catch (OutOfMemoryError er) {
                    LogUtil.addError(jTextPane1, er.toString());
                    MemoryUsage heapUsage = memoryBean.getHeapMemoryUsage();
                    long maxMemory = heapUsage.getMax() / MEGABYTE;
                    long usedMemory = heapUsage.getUsed() / MEGABYTE;
                    LogUtil.addError(jTextPane1, " : Memory Use :" + usedMemory + "M/" + maxMemory + "M");
                } finally {
                    try {
                        resendAddressTF.setEditable(true);
                        resendButton.setEnabled(true);
                        resendFileBrowseButton.setEnabled(true);
                        if (fis != null) {
                            fis.close();
                        }
                        fis = null;

                    } catch (IOException ex) {
                        LogUtil.addError(jTextPane1, ex.toString());
                    }
                }
            }
        }).start();

    }//GEN-LAST:event_resendButtonActionPerformed

    private void CleanButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CleanButtonActionPerformed
        try {
            LogUtil.clearPane(jTextPane1);
        } catch (Exception e) {
            LogUtil.addError(jTextPane1, e.toString());
        }
    }//GEN-LAST:event_CleanButtonActionPerformed

    private void validateSelectedButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_validateSelectedButtonActionPerformed
        xVerifyTime = Math.abs(Integer.parseInt(xTimeVerifyTF.getText()));
        new Thread(new Runnable() {
            @Override
            public void run() {
                CallSignMethodButton.setEnabled(false);
                CallValidateMethodButton.setEnabled(false);
                jTextField1.setEnabled(false);
                jTextField2.setEnabled(false);
                ClearButton.setEnabled(false);
                jCheckBox1.setEnabled(false);

                if (null == jTextField1.getText() || jTextField1.getText().length() < 1) {
                    LogUtil.addError(jTextPane1, "INVALID ENDPOINT!");
                    return;
                }
                LogUtil.addWarning(jTextPane1, "CALLING VALIDATE METHOD OF " + jTextField1.getText() + " WEB SERVICE!");
                ValidateProcessService validateProcessService = ImplFactory.validateProcessInstance();
                FileInputStream fis = null;
                for (int i = 0; i < xVerifyTime; i++) {
                    ValidatorResult validatorResult = null;
                    String dataToBeValidated = null;
                    try {
                        /**
                         * bu kısımda xml fatura ya da zarf dosyadan okuanacak
                         */
                        int returnVal = xmlFileChooser.showDialog(null, "Choose xml file!");
                        if (returnVal == JFileChooser.APPROVE_OPTION) {
                            fis = new FileInputStream(xmlFileChooser.getSelectedFile().getPath());
                            Document xmlDoc = MyXmlUtil.parse(fis);
                            LogUtil.addWarning(jTextPane1, xmlDoc.getDocumentElement().getNamespaceURI());

                            if (_INVOICENS.equalsIgnoreCase(xmlDoc.getDocumentElement().getNamespaceURI()) || _APPRESPONSENS.equalsIgnoreCase(xmlDoc.getDocumentElement().getNamespaceURI())) {
                                /**
                                 * burada fatura/bapr oldugunu soyle
                                 */
                                LogUtil.addInfo(jTextPane1, "Sending INVOICE/BAPR for validation");
                                dataToBeValidated = validateProcessService.getSelectedDocument(xmlDoc);
                            } else if (_STANDARTBDHNS.equalsIgnoreCase(xmlDoc.getDocumentElement().getNamespaceURI())) {
                                /**
                                 * burda da zarf oldugunu soyle
                                 */
                                LogUtil.addInfo(jTextPane1, "Sending ENVELOPE for validation");
                                dataToBeValidated = validateProcessService.getSelectedDocument(xmlDoc);
                            } else if (_DOCUMENTREQNS.equalsIgnoreCase(xmlDoc.getDocumentElement().getNamespaceURI())) {
                                /**
                                 * burada resend den alinan document request
                                 * oldugunu soyle
                                 */
                                LogUtil.addInfo(jTextPane1, "Sending Document Request for validation(data has benn taken from resend folder)");
                                dataToBeValidated = validateProcessService.getSelectedDocumentFromDocumentResponse(xmlDoc);
                            } else {
                                LogUtil.addError(jTextPane1, "Unknown document format!!!");
                                return;
                            }
                        } else {
                            resendFileTF.setText(null);
                            LogUtil.addWarning(jTextPane1, "File choosing operation cancelled!");
                            return;
                        }
                        /**
                         * bu kısımda xml fatura ya da zarf dosyadan okuanacak
                         */
                        LogUtil.addInfo(jTextPane1, "Calling validation method of web service...");
                        validatorResult = validateProcessService.invokeValidateMethod(dataToBeValidated, jTextField1.getText());
                        int valResult = validatorResult.getResult();
                        JAXBElement<String> note = validatorResult.getNote();
                        if (valResult == 1) {
                            LogUtil.addInfo(jTextPane1, "validation result is: " + valResult + " - SUCCESSFULL!");
                            LogUtil.addInfo(jTextPane1, note.getValue());
                        } else {
                            LogUtil.addError(jTextPane1, "validation result is: " + valResult + " - FAILED!");
                            LogUtil.addError(jTextPane1, note.getValue());
                        }

                    } catch (Exception e) {
                        LogUtil.addError(jTextPane1, e.toString());
                        if (null != validatorResult && null != validatorResult.getNote()) {
                            LogUtil.addError(jTextPane1, validatorResult.getNote().getValue());
                        }
                    } finally {
                        try {
                            if (fis != null) {
                                fis.close();
                            }
                            fis = null;
                        } catch (IOException ex) {
                            Logger.getLogger(SignerClientUI.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                CallSignMethodButton.setEnabled(true);
                CallValidateMethodButton.setEnabled(true);
                jTextField1.setEnabled(true);
                jTextField2.setEnabled(true);
                ClearButton.setEnabled(true);
                jCheckBox1.setEnabled(true);
            }
        }).start();
    }//GEN-LAST:event_validateSelectedButtonActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        long start = System.currentTimeMillis();
        try {
            if (MyStringUtil.isNullOrEmpty(rootUrlTF.getText())) {
                LogUtil.addWarning(jTextPane1, "Root URL can not be empty!");
                return;
            }
            if (MyStringUtil.isNullOrEmpty(companynoTF.getText())) {
                LogUtil.addWarning(jTextPane1, "Company No can not be empty!");
                return;
            }
            if (MyStringUtil.isNullOrEmpty(senderTF.getText())) {
                LogUtil.addWarning(jTextPane1, "Sender Company No can not be empty!");
                return;
            }
            if (MyStringUtil.isNullOrEmpty(receiverTF.getText())) {
                LogUtil.addWarning(jTextPane1, "Receiver Company No can not be empty!");
                return;
            }
            if (MyStringUtil.isNullOrEmpty(invoicenoTF.getText())) {
                LogUtil.addWarning(jTextPane1, "Invoice No can not be empty!");
                return;
            }

            String documentType = null;
            if (invoiceDT_RB.isSelected()) {
                documentType = "1";
            } else if (baprDT_RB.isSelected()) {
                documentType = "2";
            } else {
                LogUtil.addWarning(jTextPane1, "Please Select Document Type parameter!");
                return;
            }

            String outputType = null;
            String extension = null;
            if (htmloutputRB.isSelected()) {
                outputType = "1";
                extension = ".html";
            } else if (pdfoutputRB.isSelected()) {
                outputType = "2";
                extension = ".pdf";
            } else if (pngoutputRB.isSelected()) {
                outputType = "3";
                extension = ".png";
            } else if (xmloutputRB.isSelected()) {
                outputType = "4";
                extension = ".xml";
            } else if (xmldownloadRB.isSelected()) {
                outputType = "5";
                extension = ".xml";
            } else if (envxmldownloadRB.isSelected()) {
                outputType = "6";
                extension = ".xml";
            } else {
                LogUtil.addWarning(jTextPane1, "Please select Output Type parameter!");
                return;
            }
            String servletRoot = rootUrlTF.getText().endsWith("/") ? "einvoice/ImageProducer?" : "/einvoice/ImageProducer?";

            URL url = StringUtil.cretaeUrl(rootUrlTF.getText(), servletRoot, companynoTF.getText(), senderTF.getText(), receiverTF.getText(), invoicenoTF.getText(), documentType, outputType);
            LogUtil.addInfo(jTextPane1, "Request URL: " + url.toString());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//            connection.setConnectTimeout(5000);
//            connection.setReadTimeout(15000);
            String userpass = usernameTF.getText() + ":" + new String(passwordTF.getPassword());
            LogUtil.addInfo(jTextPane1, userpass);
            String basicAuth = "Basic " + new String(MyCryptoUtil.commonsEncodeBase64(userpass.getBytes("UTF-8")));
            connection.setRequestProperty("Authorization", basicAuth);
            connection.connect();
            byte[] data = IOUtils.toByteArray(connection.getInputStream());
            LogUtil.addInfo(jTextPane1, "Received data length:" + data.length);
            if (saveDataCB.isSelected()) {
                File selectedFile = null;
                if (excelOutFileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                    selectedFile = excelOutFileChooser.getSelectedFile();
                    MyFileUtil.writeByteArrayToFile(data, excelOutFileChooser.getSelectedFile().getParent() + "\\" + StringUtil.removeFileNameExtension(excelOutFileChooser.getSelectedFile().getName()) + extension);
                } else {
                    LogUtil.addWarning(jTextPane1, "Saving cancelled!");
                }
            }

            LogUtil.addInfo(jTextPane1, "Finished in " + DateUtil.getTimeDifferenceAsMilliseconds(start, System.currentTimeMillis()) + " milliseconds!!!");

        } catch (Exception e) {
            LogUtil.addError(jTextPane1, e.toString());
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void ClearButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ClearButton2ActionPerformed
        try {
            LogUtil.clearPane(jTextPane1);
        } catch (Exception e) {
            LogUtil.addError(jTextPane1, e.toString());
        }
    }//GEN-LAST:event_ClearButton2ActionPerformed

    private void callLegderSignerBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_callLegderSignerBActionPerformed
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (MyStringUtil.isNullOrEmpty(ledgerDataToBeSignedTF.getText())) {
                        LogUtil.addWarning(jTextPane1, "Please select the ledger document!!");
                        return;
                    }

                    if (MyStringUtil.isNullOrEmpty(ledgerWSendpointTF.getText())) {
                        LogUtil.addWarning(jTextPane1, "Invalid Endpoint!!");
                        return;
                    }

                    if (MyStringUtil.isNullOrEmpty(ledgerVknTF.getText())) {
                        LogUtil.addWarning(jTextPane1, "VKN can not be empty!! Please type a VKN number which you have defined already in the signer application!!!");
                        return;
                    }

                    StringBuilder sb = new StringBuilder();
                    sb.append("\nType=1\n");
                    SignDocumentHeaders signDocumentHeaders = new SignDocumentHeaders();
                    // type
                    signDocumentHeaders.setType("1");

                    User user = new User();
                    // username
                    user.setUsername("xuxux");
                    sb.append("Username=xuxux\n");
                    signDocumentHeaders.setRequester(user);
                    // token
                    sb.append("Token=" + ledgerVknTF.getText() + "\n");
                    signDocumentHeaders.setToken(ledgerVknTF.getText());

                    com.fit.ledger.ws.client.Document.Location location = new com.fit.ledger.ws.client.Document.Location();
                    // location
                    sb.append("Location=D:/test.txt\n").append("CompressionType=ZIP\n");
                    location.setValue("D:/test.txt");
                    location.setCompression(CompressionType.ZIP);

                    com.fit.ledger.ws.client.Document document = new com.fit.ledger.ws.client.Document();
                    com.fit.ledger.ws.client.Document.Data data = new com.fit.ledger.ws.client.Document.Data();
                    data.setCompression(CompressionType.ZIP);
                    // data to be signed
                    if (ledgerDefaultInputCB.isSelected()) {
                        FileOperationsService fileOperationsService = ImplFactory.fileOperationsInstance();
                        if (ledgerIsBeratCB.isSelected()) {
                            // berat
                            data.setValue(MyCompressionUtil.deflater(fileOperationsService.getDefaultFileAsByteArray("fitunsignedberat.tmp")));
                            // name of document
                            document.setName("BeratDocument");
                            sb.append("Document name=BeratDocument\n");
                        } else {
                            // not berat
                            data.setValue(MyCompressionUtil.deflater(fileOperationsService.getDefaultFileAsByteArray("fitunsignedledger.tmp")));
                            // name of document
                            document.setName("LedgerDocument");
                            sb.append("Document name=LedgerDocument\n");
                        }

                        LogUtil.addInfo(jTextPane1, "Will Sign default document!");
                    } else {
                        LogUtil.addInfo(jTextPane1, "Will Sign selected document: " + ledgerDataToBeSignedTF.getText());
                        data.setValue(MyCompressionUtil.deflater(MyFileUtil.readBytesFromFile(new File(ledgerDataToBeSignedTF.getText()))));
                    }

                    sb.append("Deflated data to be signed length=" + data.getValue().length + "\n");
                    document.setData(data);
                    document.setLocation(location);

                    com.fit.ledger.ws.client.System.Ip ip = new com.fit.ledger.ws.client.System.Ip();
                    // ip
                    sb.append("Ip=127.0.0.1\n");
                    ip.setValue("127.0.0.1");

                    com.fit.ledger.ws.client.System system = new com.fit.ledger.ws.client.System();
                    system.setIp(ip);

                    SignDocumentRequest parameters = new SignDocumentRequest();
                    parameters.getDocument().add(document);
                    parameters.setSystem(system);
                    javax.xml.ws.Holder<SignDocumentHeaders> holder = new Holder<SignDocumentHeaders>();

                    holder.value = signDocumentHeaders;
                    LedgerSignerService ledgerSignerService = ImplFactory.ledgerSignerInstance();
                    // endpoint
                    sb.append("Endpoint=" + ledgerWSendpointTF.getText() + "\n");
                    LogUtil.addInfo(jTextPane1, sb.toString());
                    SignDocumentResponse response = null;
                    if (ledgerIsBeratCB.isSelected()) {
                        // call sign berat method of WS
                        response = ledgerSignerService.callSignBerat(holder, parameters, ledgerWSendpointTF.getText());
                    } else {
                        // sign ledger method of WS
                        response = ledgerSignerService.callSignLedger(holder, parameters, ledgerWSendpointTF.getText());
                    }
                    System.out.println(response.getSignedDocument().getName());

                    LogUtil.addInfo(jTextPane1, "Document signed in the server side and received back with the length of:" + response.getSignedDocument().getData().getValue().length);
                    if (saveSignedLedgerCB.isSelected()) {
                        if (ledgerSignedFC.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                            File selectedFile = ledgerSignedFC.getSelectedFile();
                            System.out.println("selectedFile.getPath():" + selectedFile.getPath());
                            MyFileUtil.writeByteArrayToFile(CompressionUtil.inflater(response.getSignedDocument().getData().getValue()), FilenameUtils.removeExtension(selectedFile.getPath()) + ".xml");
                            LogUtil.addInfo(jTextPane1, "XML file has been written to: " + selectedFile.getPath() + ".xml");
                        } else {
                            LogUtil.addWarning(jTextPane1, "Save cancelled!");
                            return;
                        }
                    }
                } catch (Exception e) {
                    LogUtil.addError(jTextPane1, e.toString());
                    e.printStackTrace();
                }
            }
        }).start();
    }//GEN-LAST:event_callLegderSignerBActionPerformed

    private void ledgerBrowseBTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ledgerBrowseBTActionPerformed
        try {
            if (ledgerSignedFC.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                File selectedFile = ledgerSignedFC.getSelectedFile();
                System.out.println("selectedFile.getPath():" + selectedFile.getPath());
                LogUtil.addInfo(jTextPane1, selectedFile.getPath() + " is selected to be signed!");
                ledgerDataToBeSignedTF.setText(selectedFile.getPath());
            } else {
                LogUtil.addWarning(jTextPane1, "selection cancelled!");
                ledgerDataToBeSignedTF.setText(null);
                return;
            }
        } catch (Exception e) {
            LogUtil.addError(jTextPane1, e.toString());
        }
    }//GEN-LAST:event_ledgerBrowseBTActionPerformed

    private void clearBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearBActionPerformed
        try {
            LogUtil.clearPane(jTextPane1);
        } catch (Exception e) {
            LogUtil.addError(jTextPane1, e.toString());
        }
    }//GEN-LAST:event_clearBActionPerformed

    private void ledgerDefaultInputCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ledgerDefaultInputCBActionPerformed
        try {
            if (ledgerDefaultInputCB.isSelected()) {
                ledgerDataToBeSignedTF.setText("Default");
                ledgerBrowseBT.setEnabled(false);
            } else {
                ledgerBrowseBT.setEnabled(true);
                ledgerDataToBeSignedTF.setText(null);
            }
        } catch (Exception e) {
        }
    }//GEN-LAST:event_ledgerDefaultInputCBActionPerformed

    private void pkcs11RBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pkcs11RBActionPerformed
        jLabel3.setText("Provider dll:");
        jTextField3.setText("Choose dll with browse button.");
        ConnectButton.setText("Connect to Device");
    }//GEN-LAST:event_pkcs11RBActionPerformed

    private void pfxRBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pfxRBActionPerformed
        jLabel3.setText("Provider pfx:");
        jTextField3.setText("Select pfx with browse button.");
        ConnectButton.setText("Read pfx file");
    }//GEN-LAST:event_pfxRBActionPerformed

    private void validationBrowseBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_validationBrowseBActionPerformed
        int returnVal = xmlFileChooser.showDialog(this, "Choose");
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            selectedValidationFileTF.setText(xmlFileChooser.getSelectedFile().getPath());
        } else {
            selectedValidationFileTF.setText(null);
            LogUtil.addWarning(jTextPane1, "File choosing operation cancelled!");
        }
    }//GEN-LAST:event_validationBrowseBActionPerformed

    private void validateBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_validateBActionPerformed
//        if (null != selectedValidationFileTF.getText()) {
//            Validation v = new Validation();
//            ValidationResult result = null;
//            try {
//                result = v.validate(selectedValidationFileTF.getText());
//            } catch (Exception ex) {
//                LogUtil.addError(jTextPane1, ex.getMessage());
//            }
//            if (result.getType() == ValidationResultType.VALID)
//                LogUtil.addInfo(jTextPane1, result.toXml());
//            else
//                LogUtil.addError(jTextPane1, result.toXml());
//        } else {
//            LogUtil.addWarning(jTextPane1, "Please select an xml file to be validated!");
//        }
    }//GEN-LAST:event_validateBActionPerformed

    private void invoiceFolderChooseBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_invoiceFolderChooseBActionPerformed
        File defaultFolder = new File("D:\\EFATURA\\ARSIV\\RESEND\\IN\\");
        if (null != defaultFolder && defaultFolder.exists()) {
            dataFolderChooser.setSelectedFile(defaultFolder);
        }
        dataFolderChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = dataFolderChooser.showOpenDialog(null);
        if (returnVal == dataFolderChooser.APPROVE_OPTION) {
            File folder = dataFolderChooser.getSelectedFile();
            //This is where a real application would open the file.
            invoiceFolderTF.setText(folder.getPath());
            LogUtil.addInfo(jTextPane1, folder.getPath() + " directory is selected.");
        } else {
            LogUtil.addWarning(jTextPane1, "Input folder selection cancelled!");
        }
    }//GEN-LAST:event_invoiceFolderChooseBActionPerformed

    private void pdfExportBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pdfExportBActionPerformed
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (MyStringUtil.isEmpty(invoiceFolderTF.getText())) {
                        LogUtil.addWarning(jTextPane1, "Please select invoice folder !!!");
                        return;
                    }
                    if (!embeddedXsltCB.isSelected() && MyStringUtil.isEmpty(xsltFilePathTF.getText())) {
                        LogUtil.addWarning(jTextPane1, "Please select xslt File");
                        return;
                    }

                    pdfOutFolderChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                    if (null != invoiceFolderTF.getText() && invoiceFolderTF.getText().length() > 1) {
                        int returnVal = pdfOutFolderChooser.showOpenDialog(null);
                        if (returnVal == JFileChooser.APPROVE_OPTION) {
                            File pdfOutFolder = pdfOutFolderChooser.getSelectedFile();
                            FolderExtractService folderExtractService = ImplFactory.folderExtractInstance(jTextPane1);
                            String files[] = null;
                            try {
                                files = folderExtractService.getAllXmlFileNamesUnderFolder(invoiceFolderTF.getText());
                            } catch (Exception e) {
                                LogUtil.addError(jTextPane1, e.getMessage());
                                return;
                            }
                            HtmlToPdfService htmlToPdfService = ImplFactory.htmlToPdfInstance();
                            XmlToHtmlService xmlToHtmlService = ImplFactory.xmlToHtmlInstance();
                            byte[] pdfData = null;
                            byte[] htmlData = null;
                            for (String file : files) {
                                LogUtil.addInfo(jTextPane1, "processing " + file + " file...");
                                try {
                                    htmlData = xmlToHtmlService.convertFromXmlToHtml(new File(invoiceFolderTF.getText() + "/" + file), xsltFilePathTF.getText(), embeddedXsltCB.isSelected());
                                    pdfData = htmlToPdfService.convertFromHtmlToPdf(htmlData);
                                    MyFileUtil.writeByteArrayToFile(htmlData, pdfOutFolder.getPath() + "/" + FilenameUtils.removeExtension(file) + ".html");
                                    MyFileUtil.writeByteArrayToFile(pdfData, pdfOutFolder.getPath() + "/" + FilenameUtils.removeExtension(file) + ".pdf");
//                                    LogUtil.addInfo(jTextPane1, "file " + file + "has been converted succesfully!");
                                } catch (Exception e) {
                                    LogUtil.addError(jTextPane1, "Error while processing file:" + file);
                                    LogUtil.addError(jTextPane1, e.getMessage());
                                }
                            }
                            LogUtil.addInfo(jTextPane1, "HTML->PDF conversion completed! " + files.length + " files have been converted. Exported files are under:" + pdfOutFolder.getAbsolutePath());
                        } else {
                            LogUtil.addWarning(jTextPane1, "Export cancelled!");
                            return;
                        }

                    }
                } catch (Exception e) {
                    LogUtil.addError(jTextPane1, e.toString());
                }

            }
        }).start();
    }//GEN-LAST:event_pdfExportBActionPerformed

    private void xmlFileChooserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_xmlFileChooserActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_xmlFileChooserActionPerformed

    private void xsltFileChooseBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_xsltFileChooseBActionPerformed
        int returnVal = xsltFileChooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            xsltFilePathTF.setText(xsltFileChooser.getSelectedFile().getPath());
            LogUtil.addInfo(jTextPane1, "XSLT file is selected!");
        } else {
            LogUtil.addWarning(jTextPane1, "XSLT selection is cancelled!");
        }
    }//GEN-LAST:event_xsltFileChooseBActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        LogUtil.clearPane(jTextPane1);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void embeddedXsltCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_embeddedXsltCBActionPerformed
        if (embeddedXsltCB.isSelected()) {
            xsltFileChooseB.setEnabled(false);
        } else {
            xsltFileChooseB.setEnabled(true);
        }
    }//GEN-LAST:event_embeddedXsltCBActionPerformed

    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        System.exit(0);
    }//GEN-LAST:event_exitMenuItemActionPerformed

    private void proxyConfMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_proxyConfMenuItemActionPerformed
        proxyDialog.pack();
        proxyDialog.setLocationRelativeTo(null);
        proxyDialog.setTitle("Proxy Configuration");
        proxyDialog.setVisible(true);
    }//GEN-LAST:event_proxyConfMenuItemActionPerformed

    private void saveProxySettingsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveProxySettingsButtonActionPerformed
        ProxyOperationsService proxyOperationsService = ImplFactory.proxyOperationsInstance();
        Map<String, String> confMap = new HashMap<String, String>(5);
        confMap.put(ProxyParams.PROXY_SET.toString(), proxyCheckBox.isSelected() ? "true" : "false");
        confMap.put(ProxyParams.PROXY_HOSTNAME.toString(), hostnameTF.getText());
        confMap.put(ProxyParams.PROXY_PORT.toString(), portTF.getText());
        confMap.put(ProxyParams.PROXY_USERNAME.toString(), usernameTF1.getText());
        confMap.put(ProxyParams.PROXY_PASSWORD.toString(), passwordTF1.getText());
        try {
            proxyOperationsService.saveProxyConfiguration(confMap);
            LOGGER.debug("proxy configuration params saved successfully!");
            proxyOperationsService.setSystemProxyConfiguration(confMap);
            LOGGER.debug("proxy configuration params set successfully!");
            proxyDialog.setVisible(false);
            this.toFront();
        } catch (ProxyConfException ex) {
            LOGGER.error(ex.toString());
        }
    }//GEN-LAST:event_saveProxySettingsButtonActionPerformed

    private void getBatchStatusButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_getBatchStatusButtonActionPerformed
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    SignProcessService signProcessService = ImplFactory.signProcessInstance();
                    Document signedRequest = signProcessService.localWssSignatureForGetBatchStatus(jComboBox1.getSelectedItem().toString(), UIutils.askForPin("Pin Giriniz..."), jTextField3.getText(), paketIdTF.getText());
                    if (!exportCB.isSelected()) {
                        LogUtil.addInfo(jTextPane1, "Invoking web service with signed soap message, will print response to the console...");
                        SOAPMessage response = SOAPUtil.invoke(signedRequest, UIutils.askForPin("Enter web service URL!"));
                        LogUtil.addInfo(jTextPane1, SOAPUtil.prettyPrint(SOAPUtil.toDocument(response)));
                    } else {
                        if (excelOutFileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                            String destination = excelOutFileChooser.getSelectedFile().getParent() + "\\" + StringUtil.removeFileNameExtension(excelOutFileChooser.getSelectedFile().getName()) + ".xml";
                            MyXmlUtil.transform(signedRequest, new FileOutputStream(destination));
                            LogUtil.addInfo(jTextPane1, "Signed data has been written to: " + destination);
                        } else {
                            LogUtil.addWarning(jTextPane1, "Export of signed data has been cancelled!");
                            return;
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    LogUtil.addError(jTextPane1, ex.toString());
                }
            }
        }).start();
    }//GEN-LAST:event_getBatchStatusButtonActionPerformed

    private void publishButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_publishButtonActionPerformed
        if (MyStringUtil.isEmpty(wsPortTF.getText())) {
            LogUtil.addError(jTextPane1, "Port is required");
            return;
        }
        String endPoint = "http://localhost:" + wsPortTF.getText() + "/ArcSigner/services/eArsivSignerPort";
        Endpoint.publish(endPoint, new EArsivSignaturePortTypeImpl());
        LogUtil.addInfo(jTextPane1, "WS is published at " + endPoint);
    }//GEN-LAST:event_publishButtonActionPerformed

    private void invoiceFolderChooserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_invoiceFolderChooserActionPerformed
        File defaultFolder = new File("D:/TMP/target/billion/");
        if (null != defaultFolder && defaultFolder.exists()) {
            dataFolderChooser.setSelectedFile(defaultFolder);
        }
        dataFolderChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = dataFolderChooser.showOpenDialog(null);
        if (returnVal == dataFolderChooser.APPROVE_OPTION) {
            File folder = dataFolderChooser.getSelectedFile();
            //This is where a real application would open the file.
            arcInvFolderTF.setText(folder.getPath());
            LogUtil.addInfo(jTextPane1, folder.getPath() + " directory is selected.");
        } else {
            LogUtil.addWarning(jTextPane1, "Input folder selection cancelled!");
        }
    }//GEN-LAST:event_invoiceFolderChooserActionPerformed

    private void sendInvoiceBTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendInvoiceBTActionPerformed
        String arcInvFolder = arcInvFolderTF.getText();
        String arcInvWsEndpoint = arcInvWsEndpointTF.getText();
        if (!MyStringUtil.isNullOrEmpty(arcInvFolderTF)) {
            if (!MyStringUtil.isNullOrEmpty(arcInvWsEndpointTF)) {
                ExecutorService executor = Executors.newFixedThreadPool(1);
                if (INV_RB.isSelected()) {
                    Runnable worker = new SendInvoiceCallerThread(arcInvWsEndpoint, arcInvFolder, jTextPane1, vkn_tcknTF.getText(), branchTF.getText(), responsiveOutputCB.getSelectedItem().toString(), ws_userTF.getText(), ws_passTF.getText());
                    executor.execute(worker);
                }
                if (ENV_RB.isSelected()) {
                    Runnable worker = new SendEnvelopeCallerThread(arcInvWsEndpoint, arcInvFolder, jTextPane1, vkn_tcknTF.getText(), branchTF.getText(), ws_userTF.getText(), ws_passTF.getText());
                    executor.execute(worker);
                }
            } else {
                LogUtil.addWarning(jTextPane1, "Please enter the endpoint of SendInvoice service!");
            }
        } else {
            LogUtil.addWarning(jTextPane1, "Please select a folder!");
        }
    }//GEN-LAST:event_sendInvoiceBTActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        File defaultFolder = new File("D:/TMP/target/billion/");
        if (null != defaultFolder && defaultFolder.exists()) {
            dataFolderChooser.setSelectedFile(defaultFolder);
        }
        dataFolderChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = dataFolderChooser.showOpenDialog(null);
        if (returnVal == dataFolderChooser.APPROVE_OPTION) {
            File folder = dataFolderChooser.getSelectedFile();
            //This is where a real application would open the file.
            arcInvOutFolderTF.setText(folder.getPath());
            LogUtil.addInfo(jTextPane1, folder.getPath() + " directory is selected.");
        } else {
            LogUtil.addWarning(jTextPane1, "Input folder selection cancelled!");
        }
    }//GEN-LAST:event_jButton5ActionPerformed

    private void generrateInvBTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generrateInvBTActionPerformed
        if (INV_RB.isSelected()) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    String firstFileName = firstFileNameTF.getText();
                    String arcInvOutFolder = arcInvOutFolderTF.getText();
                    String arcInvZipCount = arcInvZipCountTF.getText();
                    String arcInvXmlCountPerZip = arcInvXmlCountPerZipTF.getText();
                    if (MyStringUtil.isNullOrEmpty(firstFileName)) {
                        LogUtil.addWarning(jTextPane1, "Please select a file name to start from!");
                        return;
                    }
                    if (MyStringUtil.isNullOrEmpty(arcInvOutFolder) || !MyNumberUtil.isNumber(firstFileName)) {
                        LogUtil.addWarning(jTextPane1, "Please select a target folder which contains template.xml file!");
                        return;
                    }
                    if (MyStringUtil.isNullOrEmpty(arcInvZipCount) || !MyNumberUtil.isNumber(arcInvZipCount)) {
                        LogUtil.addWarning(jTextPane1, "Zip Count must not be empty and it must be numeric!");
                        return;
                    }
                    if (MyStringUtil.isNullOrEmpty(arcInvXmlCountPerZip) || !MyNumberUtil.isNumber(arcInvXmlCountPerZip)) {
                        LogUtil.addWarning(jTextPane1, "XML count per zip must not be empty and it must be numeric!");
                        return;
                    }
                    InvGeneratorService generatorService = ImplFactory.invGeneratorInstance();
                    try {
                        generatorService.generateZippedInvoices(arcInvOutFolder, Integer.parseInt(arcInvZipCount), Integer.parseInt(arcInvXmlCountPerZip), Integer.parseInt(firstFileName), issueDateChooser.getSelectedDate().getTime());
                        LogUtil.addInfo(jTextPane1, "File(s) has been created.");
                    } catch (Exception ex) {
                        LogUtil.addError(jTextPane1, "Error when generating files: " + StringUtil.traceException(ex));
                    }
                }
            }).start();
        } else if (ENV_RB.isSelected()) {
            new Thread(new Runnable() {

                @Override
                public void run() {
                    String firstFileName = firstFileNameTF.getText();
                    String arcInvOutFolder = arcInvOutFolderTF.getText();
                    String arcInvZipCount = arcInvZipCountTF.getText();
                    String arcInvXmlCountPerZip = arcInvXmlCountPerZipTF.getText();
                    if (MyStringUtil.isNullOrEmpty(firstFileName)) {
                        LogUtil.addWarning(jTextPane1, "Please select a file name to start from!");
                        return;
                    }
                    if (MyStringUtil.isNullOrEmpty(arcInvOutFolder) || !MyNumberUtil.isNumber(firstFileName)) {
                        LogUtil.addWarning(jTextPane1, "Please select a target folder which contains template.xml file!");
                        return;
                    }
                    if (MyStringUtil.isNullOrEmpty(arcInvZipCount) || !MyNumberUtil.isNumber(arcInvZipCount)) {
                        LogUtil.addWarning(jTextPane1, "Zip Count must not be empty and it must be numeric!");
                        return;
                    }
                    if (MyStringUtil.isNullOrEmpty(arcInvXmlCountPerZip) || !MyNumberUtil.isNumber(arcInvXmlCountPerZip)) {
                        LogUtil.addWarning(jTextPane1, "XML count per zip must not be empty and it must be numeric!");
                        return;
                    }
                    InvGeneratorService generatorService = ImplFactory.invGeneratorInstance();
                    try {
                        generatorService.generateZippedEnvelope(arcInvOutFolder, Integer.parseInt(arcInvZipCount), Integer.parseInt(arcInvXmlCountPerZip), Integer.parseInt(firstFileName), issueDateChooser.getSelectedDate().getTime());
                        LogUtil.addInfo(jTextPane1, "File(s) has been created.");
                    } catch (Exception e) {
                        LogUtil.addError(jTextPane1, "Error when generating files: " + StringUtil.traceException(e));
                    }
                }
            }).start();

        } else {
            LogUtil.addWarning(jTextPane1, "Template dosyasının fatura mı zarf mı olduğunu seçiniz!");
        }


    }//GEN-LAST:event_generrateInvBTActionPerformed

    private void arcInvWsEndpointTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_arcInvWsEndpointTFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_arcInvWsEndpointTFActionPerformed

    private void xadesBesRB1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_xadesBesRB1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_xadesBesRB1ActionPerformed

    private void ENV_RBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ENV_RBActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ENV_RBActionPerformed

    private void setupEarcSignerJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setupEarcSignerJBActionPerformed
        setupEarcSignerJD1.pack();
        setupEarcSignerJD1.setLocationRelativeTo(null);
        setupEarcSignerJD1.setTitle("EARC_SIGNER SETUP");
        setupEarcSignerJD1.setVisible(true);
        this.toBack();
    }//GEN-LAST:event_setupEarcSignerJBActionPerformed

    private void esCancelSetup1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_esCancelSetup1ActionPerformed
        setupEarcSignerJD1.setVisible(false);
        this.toFront();
    }//GEN-LAST:event_esCancelSetup1ActionPerformed

    private void esInstanceAddJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_esInstanceAddJBActionPerformed
        boolean isPortAssignedAlready = false;
        try {
            if (StringUtil.isNullOrEmpty(esInstancePortTF.getText()) || !NumberUtils.isDigits(esInstancePortTF.getText())) {
                throw new Exception("Port is empty or not numeric!");
            }
            Integer port = Integer.parseInt(esInstancePortTF.getText());
            isPortAssignedAlready = SocketUtil.isPortOpen("localhost", port, 100);
            if (isPortAssignedAlready) {
                throw new Exception("Port:" + port + " is being used already by another application!");
            }
            DefaultListModel<String> model = (DefaultListModel<String>) esInstancePortJL.getModel();
            if (instancePortMap.get(port.toString()) == null) {
                instancePortMap.put(port.toString(), port.toString());
                model.addElement(port.toString());
            }
        } catch (Exception e) {
            LogUtil.addError(jTextPane1, e.getMessage());
        }


    }//GEN-LAST:event_esInstanceAddJBActionPerformed

    private void esInstanceRemoveJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_esInstanceRemoveJBActionPerformed

        DefaultListModel model = (DefaultListModel) esInstancePortJL.getModel();
        int selectedIndex = esInstancePortJL.getSelectedIndex();
        if (selectedIndex != -1) {
            instancePortMap.remove(model.get(selectedIndex));
            model.remove(selectedIndex);

        }
    }//GEN-LAST:event_esInstanceRemoveJBActionPerformed

    private void earcSignerFolderChooserJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_earcSignerFolderChooserJBActionPerformed
        File defaultFolder = new File("D:/EARC_SIGNER");
        if (null != defaultFolder && defaultFolder.exists()) {
            earcSignerFolderChooser.setSelectedFile(defaultFolder);
        }
        earcSignerFolderChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = earcSignerFolderChooser.showOpenDialog(null);
        if (returnVal == earcSignerFolderChooser.APPROVE_OPTION) {
            File folder = earcSignerFolderChooser.getSelectedFile();
            //This is where a real application would open the file.
            earcSignerPathTF.setText(folder.getPath());
            LogUtil.addInfo(jTextPane1, folder.getPath() + " directory is selected.");
        } else {
            LogUtil.addWarning(jTextPane1, "Input folder selection cancelled!");
        }
    }//GEN-LAST:event_earcSignerFolderChooserJBActionPerformed

    private void esNextSetup1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_esNextSetup1ActionPerformed
        try {
            FileOperationsService fileOperationsService = ImplFactory.fileOperationsInstance();
            byte[] defaultArcma3ZipBytes = fileOperationsService.getDefaultFileAsByteArray("ARCMA3.zip");
            String workingDir = fileOperationsService.getClassPath(this.getClass());
            MyFileUtil.writeByteArrayToFile(defaultArcma3ZipBytes, workingDir + "/ARCMA3.zip");
            File arcma3ZipFile = new File(workingDir + "/ARCMA3.zip");
            for (Iterator<String> iter = instancePortMap.keySet().iterator(); iter.hasNext();) {
                ZipUtil.unzip(arcma3ZipFile, new File(earcSignerPathTF.getText() + "/ARCMA3/" + iter.next()));
            }
            setupEarcSignerJD2.pack();
            setupEarcSignerJD2.setLocationRelativeTo(null);
            setupEarcSignerJD2.setTitle("EARC_SIGNER SETUP - 2");
            setupEarcSignerJD1.setVisible(false);
            setupEarcSignerJD2.setVisible(true);
        } catch (Exception e) {
            LogUtil.addError(jTextPane1, ExceptionUtils.getStackTrace(e));
            e.printStackTrace();
        }

    }//GEN-LAST:event_esNextSetup1ActionPerformed

    private void esNextSetup2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_esNextSetup2ActionPerformed
        ConfigOperationService configOperationService = ImplFactory.confitOperationsInstance();
        Map<String, String> confMap = new HashMap<String, String>(11);
        String model = cardTypeCB.getSelectedItem().toString();

        confMap.put(TokenConfParams.SLOT_LIST_INDEX.toString(), slotListIndexTF.getText());
        confMap.put(TokenConfParams.CARD_TYPE.toString(), model);
        confMap.put(TokenConfParams.PROJECT.toString(), archiveRB.isSelected() ? "ARCHIVE" : "TICKET");
        confMap.put(TokenConfParams.USE_PKCS11.toString(), usePkcs11CB.isSelected() ? "1" : "0");
        confMap.put(TokenConfParams.ALIAS.toString(), aliasTF.getText());
//        confMap.put(TokenConfParams.ARCMA3_DIR.toString(), (earcSignerPathTF.getText() + "/ARCMA3/").replaceAll("\\\\", "/"));
        confMap.put(TokenConfParams.PFX_PASS.toString(), pfxPassTF.getText());
        confMap.put(TokenConfParams.PKCS11_PASS.toString(), pkcs11PassTF.getText());
        confMap.put(TokenConfParams.PROVIDER_PATH.toString(), providerPathTF.getText());
        try {
            for (Iterator<String> iter = instancePortMap.keySet().iterator(); iter.hasNext();) {
                String instance = iter.next();
                confMap.put(TokenConfParams.ARCMA3_DIR.toString(), (earcSignerPathTF.getText() + "/ARCMA3/" + instance + "/").replaceAll("\\\\", "/"));
                configOperationService.saveConfiguration(confMap, earcSignerPathTF.getText(), instance);
            }
            LogUtil.addInfo(jTextPane1, "configuration params saved successfully!");
            setupEarcSignerJD3.pack();
            setupEarcSignerJD3.setLocationRelativeTo(null);
            setupEarcSignerJD3.setTitle("EARC_SIGNER SETUP - 3");
            setupEarcSignerJD2.setVisible(false);
            setupEarcSignerJD3.setVisible(true);
            File dllFile = dllFileChooser.getSelectedFile();
            XuxuxKeyingDataProviderImpl.setProviderPath(dllFile.getPath());
            XuxuxKeyingDataProviderImpl.setLogPane(jTextPane1);
            XuxuxKeyingDataProviderImpl.refreshAllTokens();
            Map<String, CertInfo> certMap = XuxuxKeyingDataProviderImpl.getCertificateMap();
            List<CertInfo> certInfoList = new ArrayList<CertInfo>();
            for (String key : certMap.keySet()) {
                CertInfo certInfo = certMap.get(key);
                certInfoList.add(certInfo);
            }
            updateCertificates(certInfoList);
        } catch (Exception ex) {
            ex.printStackTrace();
            LogUtil.addError(jTextPane1, ex.toString());
        }
    }//GEN-LAST:event_esNextSetup2ActionPerformed

    public void updateCertificates(List<CertInfo> certInfos) {
        //update model first
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        for (CertInfo certInfo : certInfos) {
            LogUtil.addInfo(jTextPane1, CertificateUtil.getKeyUsageAsText(certInfo.getCert()));
            if (CertificateUtil.getKeyUsageAsText(certInfo.getCert()).contains("digitalSignature")) {
                model.addElement(certInfo);
            }
        }
        certificateCB.setModel(model);

        //set the first certificate as current
        if (certificateCB.getItemCount() == 0) {
            return;
        }
        certificateCB.setSelectedIndex(0);
    }


    private void dllFilePathChooserJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dllFilePathChooserJBActionPerformed
        File defaultFile = new File("C:\\Windows\\System32\\cryptoki.dll");
        if (null != defaultFile && defaultFile.exists()) {
            dllFileChooser.setSelectedFile(defaultFile);
        } else {
            dllFileChooser.setSelectedFile(new File("C:\\Windows\\System32\\"));
        }

        int returnVal = dllFileChooser.showOpenDialog(null);
        File dllFile = dllFileChooser.getSelectedFile();
        while (!dllFile.exists() && returnVal == JFileChooser.APPROVE_OPTION) {
            returnVal = dllFileChooser.showOpenDialog(null);
            dllFile = dllFileChooser.getSelectedFile();
            LogUtil.addWarning(jTextPane1, "Choosen file does not exist!");
        }
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            providerPathTF.setText(dllFile.getPath());
            XuxuxKeyingDataProviderImpl.setProviderPath(dllFile.getPath());
            LogUtil.addInfo(jTextPane1, "Provider module is selected!");
        } else {
            LogUtil.addWarning(jTextPane1, "Provider selection is cancelled!");
        }
    }//GEN-LAST:event_dllFilePathChooserJBActionPerformed

    private void addCertificateJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addCertificateJBActionPerformed
        try {
            CertInfo certInfo = (CertInfo) certificateCB.getModel().getSelectedItem();
            if (certInfo != null) {
                DefaultTableModel defaultTableModel = (DefaultTableModel) certSerialJT.getModel();
                defaultTableModel.addRow(new Object[]{"VKN", certInfo.getCert().getSerialNumber(), certInfo.getAlias(), certInfo.slotID, "PIN"});
            }
        } catch (Exception e) {
            LogUtil.addError(jTextPane1, ExceptionUtils.getStackTrace(e));
        }
    }//GEN-LAST:event_addCertificateJBActionPerformed

    private void certificateCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_certificateCBActionPerformed

    }//GEN-LAST:event_certificateCBActionPerformed

    private void saveCertVknMapJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveCertVknMapJBActionPerformed
        try {
            ConfigOperationService configOperationService = ImplFactory.confitOperationsInstance();
            DefaultTableModel model = (DefaultTableModel) certSerialJT.getModel();
//            Map<String, String> mapping = new HashMap();
//            for (int i = 0; i < model.getRowCount(); i++) {
//                mapping.put(model.getValueAt(i, 0).toString(), model.getValueAt(i, 1).toString());
//            }
//            mapping = new TreeMap(mapping);
            CERTVKNMAPPINGROOT.CUSTOMER[] customers = ObjectMapper.convertCertserialTableModelToCustomer(model);
            configOperationService.saveCertVknMapping(customers, earcSignerPathTF.getText());
            LOGGER.debug("Certificate <-> VKN mapping params saved successfully!");
            this.toFront();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }//GEN-LAST:event_saveCertVknMapJBActionPerformed

    private void removeCertificateJBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeCertificateJBActionPerformed
        try {
            DefaultTableModel defaultTableModel = (DefaultTableModel) certSerialJT.getModel();
            defaultTableModel.removeRow(certSerialJT.getSelectedRow());
        } catch (Exception e) {
            LogUtil.addError(jTextPane1, ExceptionUtils.getStackTrace(e));
        }
    }//GEN-LAST:event_removeCertificateJBActionPerformed

    private void callGetInvoiceDocumentButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_callGetInvoiceDocumentButtonActionPerformed
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    LogUtil.clearPane(jTextPane1);
                    pdfOutFolderChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                    int returnVal = pdfOutFolderChooser.showOpenDialog(null);
                    if (returnVal == JFileChooser.APPROVE_OPTION) {
                        LogUtil.addWarning(jTextPane1, "working for vkn:" + vknTf.getText());
                        String[] invoiceIdArray = invoiceIDTA.getText().split(",");
                        for (String invId : invoiceIdArray) {
                            invId = invId.trim();
                            LogUtil.addInfo(jTextPane1, "working for invoice:" + invId);

                            GetInvoiceDocumentRequestType request = new GetInvoiceDocumentRequestType();
                            request.setVkn(vknTf.getText());
                            request.setInvoiceNumber(invId);
                            GetInvoiceDocumentResponseType response = Invoker.callGetInvoiceDocument(request, endPointTF.getText(), ws_userTF.getText(), ws_passTF.getText());
                            MyFileUtil.writeByteArrayToFile(response.getBinaryData(), pdfOutFolderChooser.getSelectedFile().getPath() + "/" + vknTf.getText() + "_" + invId + ".pdf");
                        }
                    } else {
                        LogUtil.addWarning(jTextPane1, "Export cancelled!");
                        return;
                    }

                } catch (Exception e) {
                    LogUtil.addError(jTextPane1, e.toString());
                }

            }
        }).start();
    }//GEN-LAST:event_callGetInvoiceDocumentButtonActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (MyStringUtil.isNullOrEmpty(ledgerDataToBeSignedTF.getText())) {
                        LogUtil.addWarning(jTextPane1, "Please select the ledger document!!");
                        return;
                    }

                    if (MyStringUtil.isNullOrEmpty(ledgerWSendpointTF.getText())) {
                        LogUtil.addWarning(jTextPane1, "Invalid Endpoint!!");
                        return;
                    }

                    if (MyStringUtil.isNullOrEmpty(ledgerVknTF.getText())) {
                        LogUtil.addWarning(jTextPane1, "VKN can not be empty!! Please type a VKN number which you have defined already in the signer application!!!");
                        return;
                    }

                    StringBuilder sb = new StringBuilder();
                    sb.append("\nType=1\n");
                    SignDocumentHeaders signDocumentHeaders = new SignDocumentHeaders();
                    // type
                    signDocumentHeaders.setType("1");

                    User user = new User();
                    // username
                    user.setUsername("xuxux");
                    sb.append("Username=xuxux\n");
                    signDocumentHeaders.setRequester(user);
                    // token
                    sb.append("Token=" + ledgerVknTF.getText() + "\n");
                    signDocumentHeaders.setToken(ledgerVknTF.getText());

                    com.fit.ledger.ws.client.Document.Location location = new com.fit.ledger.ws.client.Document.Location();
                    // location
                    sb.append("Location=D:/test.txt\n").append("CompressionType=ZIP\n");
                    location.setValue("D:/test.txt");
                    location.setCompression(CompressionType.ZIP);

                    com.fit.ledger.ws.client.Document document = new com.fit.ledger.ws.client.Document();
                    com.fit.ledger.ws.client.Document.Data data = new com.fit.ledger.ws.client.Document.Data();
                    data.setCompression(CompressionType.ZIP);
                    // data to be signed
                    if (ledgerDefaultInputCB.isSelected()) {
                        FileOperationsService fileOperationsService = ImplFactory.fileOperationsInstance();
                            byte[] dataValue = MyCompressionUtil.deflater(fileOperationsService.getDefaultFileAsByteArray("fitsignedledger.xml"));
                            sb.append("Data length before signSoapMessage: " + dataValue.length + "\n");
                            data.setValue(dataValue);
                            // name of document
                            document.setName("SignedLedgerDocument");
                            sb.append("Document name=SignedLedgerDocument\n");
                        

                        LogUtil.addInfo(jTextPane1, "Will Sign default document!");
                    } else {
                        LogUtil.addInfo(jTextPane1, "Will Sign selected document: " + ledgerDataToBeSignedTF.getText());
                        byte[] dataValue = MyCompressionUtil.deflater(MyFileUtil.readBytesFromFile(new File(ledgerDataToBeSignedTF.getText())));
                        sb.append("Data length before signSoapMessage: " + dataValue.length + "\n");
                        data.setValue(dataValue);
                    }

                    sb.append("Deflated data to be signed length=" + data.getValue().length + "\n");
                    document.setData(data);
                    document.setLocation(location);

                    com.fit.ledger.ws.client.System.Ip ip = new com.fit.ledger.ws.client.System.Ip();
                    // ip
                    sb.append("Ip=127.0.0.1\n");
                    ip.setValue("127.0.0.1");

                    com.fit.ledger.ws.client.System system = new com.fit.ledger.ws.client.System();
                    system.setIp(ip);

                    SignDocumentRequest parameters = new SignDocumentRequest();
                    parameters.getDocument().add(document);
                    parameters.setSystem(system);
                    javax.xml.ws.Holder<SignDocumentHeaders> holder = new Holder<SignDocumentHeaders>();

                    holder.value = signDocumentHeaders;
                    LedgerSignerService ledgerSignerService = ImplFactory.ledgerSignerInstance();
                    // endpoint
                    sb.append("Endpoint=" + ledgerWSendpointTF.getText() + "\n");
                    LogUtil.addInfo(jTextPane1, sb.toString());
                    // signSoapMessage method of WS
                    SignDocumentResponse response = ledgerSignerService.callSignSoapMessage(holder, parameters, ledgerWSendpointTF.getText());
                    System.out.println(response.getSignedDocument().getName());

                    LogUtil.addInfo(jTextPane1, "Document signed in the server side and received back with the length of:" + response.getSignedDocument().getData().getValue().length);
                    if (saveSignedLedgerCB.isSelected()) {
                        if (ledgerSignedFC.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                            File selectedFile = ledgerSignedFC.getSelectedFile();
                            System.out.println("selectedFile.getPath():" + selectedFile.getPath());
//                            MyFileUtil.writeByteArrayToFile(CompressionUtil.inflater(response.getSignedDocument().getData().getValue()), FilenameUtils.removeExtension(selectedFile.getPath()) + ".xml");
                            MyFileUtil.writeByteArrayToFile(response.getSignedDocument().getData().getValue(), FilenameUtils.removeExtension(selectedFile.getPath()) + ".xml");
                            LogUtil.addInfo(jTextPane1, "XML file has been written to: " + selectedFile.getPath() + ".xml");
                        } else {
                            LogUtil.addWarning(jTextPane1, "Save cancelled!");
                            return;
                        }
                    }
                } catch (SignSOAPMessageFault ssmf){
                    LogUtil.addError(jTextPane1, "1_SignSOAPMessageFault");
                    try {
                        LogUtil.addError(jTextPane1, GenericMarshallUnmarshallUtil.marshal(ssmf));
                    } catch (Exception e) {
                        LogUtil.addError(jTextPane1, "marshall error:" + e.toString());
                    }
                    LogUtil.addError(jTextPane1, ssmf.toString());
                } catch (Exception e) {
                    LogUtil.addError(jTextPane1, "2_Exception");
                    LogUtil.addError(jTextPane1, e.toString());
                    e.printStackTrace();
                }
            }
        }).start();
    }//GEN-LAST:event_jButton6ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SignerClientUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SignerClientUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SignerClientUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SignerClientUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SignerClientUI().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton CallSignMethodButton;
    private javax.swing.JButton CallValidateMethodButton;
    private javax.swing.JButton CleanButton;
    private javax.swing.JButton ClearButton;
    private javax.swing.JButton ClearButton2;
    private javax.swing.JButton ConnectButton;
    private javax.swing.JRadioButton ENV_RB;
    private javax.swing.JRadioButton INV_RB;
    private javax.swing.ButtonGroup Inv_Env_BG;
    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JButton addCertificateJB;
    private javax.swing.JTextField aliasTF;
    private javax.swing.JTextField arcInvFolderTF;
    private javax.swing.JTextField arcInvOutFolderTF;
    private javax.swing.JTextField arcInvWsEndpointTF;
    private javax.swing.JTextField arcInvXmlCountPerZipTF;
    private javax.swing.JTextField arcInvZipCountTF;
    private javax.swing.JRadioButton archiveRB;
    private javax.swing.JRadioButton baprDT_RB;
    private javax.swing.JTextField branchTF;
    private javax.swing.JButton callGetInvoiceDocumentButton;
    private javax.swing.JButton callLegderSignerB;
    private javax.swing.JComboBox cardTypeCB;
    private javax.swing.JTable certSerialJT;
    private javax.swing.JComboBox certificateCB;
    private javax.swing.JButton clearB;
    private javax.swing.JButton clearButton;
    private javax.swing.JTextField collectionCountField;
    private javax.swing.JTextField companynoTF;
    private javax.swing.JButton concurrentSignButton;
    private javax.swing.JButton concurrentValidateButton;
    private javax.swing.JLabel counterLabel;
    private javax.swing.JFileChooser dataFolderChooser;
    private javax.swing.JButton dllBrowseButton;
    private javax.swing.JFileChooser dllFileChooser;
    private javax.swing.JButton dllFilePathChooserJB;
    private javax.swing.ButtonGroup documentTypeBG;
    private javax.swing.ButtonGroup documentTypeforXA_BG;
    private javax.swing.JFileChooser earcSignerFolderChooser;
    private javax.swing.JButton earcSignerFolderChooserJB;
    private javax.swing.JTextField earcSignerPathTF;
    private javax.swing.JCheckBox embeddedXsltCB;
    private javax.swing.JTextField endPointTF;
    private javax.swing.JRadioButton envxmldownloadRB;
    private javax.swing.JButton esCancelSetup1;
    private javax.swing.JButton esInstanceAddJB;
    private javax.swing.JList esInstancePortJL;
    private javax.swing.JTextField esInstancePortTF;
    private javax.swing.JButton esInstanceRemoveJB;
    private javax.swing.JButton esNextSetup1;
    private javax.swing.JButton esNextSetup2;
    private javax.swing.JFileChooser excelOutFileChooser;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JCheckBox exportCB;
    private javax.swing.JTextField firstFileNameTF;
    private javax.swing.JButton generrateInvBT;
    private javax.swing.JButton getBatchStatusButton;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JTextField hostnameTF;
    private javax.swing.JRadioButton htmloutputRB;
    private javax.swing.JRadioButton invRB;
    private javax.swing.JRadioButton invoiceDT_RB;
    private javax.swing.JButton invoiceFolderChooseB;
    private javax.swing.JButton invoiceFolderChooser;
    private javax.swing.JTextField invoiceFolderTF;
    private javax.swing.JTextArea invoiceIDTA;
    private javax.swing.JTextField invoicenoTF;
    private datechooser.beans.DateChooserCombo issueDateChooser;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JLayeredPane jLayeredPane2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTabbedPane jTabbedPane3;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextPane jTextPane1;
    private javax.swing.ButtonGroup keyingDataProviderBG;
    private javax.swing.JButton ledgerBrowseBT;
    private javax.swing.JTextField ledgerDataToBeSignedTF;
    private javax.swing.JCheckBox ledgerDefaultInputCB;
    private javax.swing.JCheckBox ledgerIsBeratCB;
    private javax.swing.JFileChooser ledgerSignedFC;
    private javax.swing.JTextField ledgerVknTF;
    private javax.swing.JTextField ledgerWSendpointTF;
    private javax.swing.ButtonGroup outputTypeBG;
    private javax.swing.JTextField paketIdTF;
    private javax.swing.JPasswordField passwordTF;
    private javax.swing.JTextField passwordTF1;
    private javax.swing.JButton pdfExportB;
    private javax.swing.JFileChooser pdfOutFolderChooser;
    private javax.swing.JRadioButton pdfoutputRB;
    private javax.swing.JTextField pfxPassTF;
    private javax.swing.JRadioButton pfxRB;
    private javax.swing.JTextField pkcs11PassTF;
    private javax.swing.JRadioButton pkcs11RB;
    private javax.swing.JRadioButton pngoutputRB;
    private javax.swing.JLabel portLabel;
    private javax.swing.JTextField portTF;
    private javax.swing.ButtonGroup projectBG;
    private javax.swing.JTextField providerPathTF;
    private javax.swing.JCheckBox proxyCheckBox;
    private javax.swing.JMenuItem proxyConfMenuItem;
    private javax.swing.JDialog proxyDialog;
    private javax.swing.JButton publishButton;
    private javax.swing.JTextField receiverTF;
    private javax.swing.JButton removeCertificateJB;
    private javax.swing.JRadioButton repRB;
    private javax.swing.JTextField resendAddressTF;
    private javax.swing.JButton resendButton;
    private javax.swing.JButton resendFileBrowseButton;
    private javax.swing.JTextField resendFileTF;
    private javax.swing.JComboBox responsiveOutputCB;
    private javax.swing.JTextField rootUrlTF;
    private javax.swing.JButton saveCertVknMapJB;
    private javax.swing.JCheckBox saveDataCB;
    private javax.swing.JButton saveProxySettingsButton;
    private javax.swing.JCheckBox saveSignedLedgerCB;
    private javax.swing.JTextField selectedValidationFileTF;
    private javax.swing.JButton sendInvoiceBT;
    private javax.swing.JTextField senderTF;
    private javax.swing.JButton setupEarcSignerJB;
    private javax.swing.JDialog setupEarcSignerJD1;
    private javax.swing.JDialog setupEarcSignerJD2;
    private javax.swing.JDialog setupEarcSignerJD3;
    private javax.swing.JTextField slotListIndexTF;
    private javax.swing.JButton testButton;
    private javax.swing.JButton testSignButton;
    private javax.swing.JTextField testSignXTimeIF;
    private javax.swing.JRadioButton ticketRB;
    private javax.swing.JButton ublFolderChooserButton;
    private javax.swing.JTextField ublFolderTextField;
    private javax.swing.JCheckBox usePkcs11CB;
    private javax.swing.JLabel usePkcs11JL;
    private javax.swing.JTextField usernameTF;
    private javax.swing.JTextField usernameTF1;
    private javax.swing.JButton validateB;
    private javax.swing.JButton validateSelectedButton;
    private javax.swing.JButton validationBrowseB;
    private javax.swing.JTextField vknTf;
    private javax.swing.JTextField vkn_tcknTF;
    private javax.swing.JPanel wsPanel;
    private javax.swing.JTextField wsPortTF;
    private javax.swing.JTextField ws_passTF;
    private javax.swing.JTextField ws_userTF;
    private javax.swing.JRadioButton wssRB;
    private javax.swing.JFormattedTextField xTimeFormattedTextField;
    private javax.swing.JTextField xTimeVerifyTF;
    private javax.swing.JRadioButton xadesARB1;
    private javax.swing.JRadioButton xadesBesRB1;
    private javax.swing.ButtonGroup xadesFormatBG;
    private javax.swing.JLayeredPane xadesFormatPane;
    private javax.swing.JFileChooser xmlFileChooser;
    private javax.swing.JRadioButton xmldownloadRB;
    private javax.swing.JRadioButton xmloutputRB;
    private javax.swing.JButton xsltFileChooseB;
    private javax.swing.JFileChooser xsltFileChooser;
    private javax.swing.JTextField xsltFilePathTF;
    private javax.swing.JLabel xsltLable;
    // End of variables declaration//GEN-END:variables
}
