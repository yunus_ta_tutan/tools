/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.exceptions;

/**
 *
 * @author xuxux
 */
public class TokenException extends Exception{
    public static final String EC_NO_CERTINFO_FOR_ALIAS = "ec-no-certinfo-for-alias";
    public static final String EC_NO_PIN_FOR_ALIAS = "ec-no-pin-for-alias";    
    public static final String EC_PIN_INCORRECT = "ec-pin-incorrect";
    public static final String EC_NO_PKEY_FOR_ALIAS = "ec-no-pkey-for-alias";
    public static final String EC_LOADING_PKEY_FAILED = "ec-loading-pkey-failed";
    public static final String EC_SIGNING_FAILED = "ec-signing-failed";
    public static final String EC_SIGNING_CANCELLED = "ec-signing-cancelled";
    public static final String EC_CERT_NOT_SELECTED = "ec-cert-not-selected";
    public static final String EC_CERT_ENCODE_FAILED = "ec-cert-encode-failed";
    private String errorCode;
    private String errorParam;
    
     /**
     * New TokenException with an error code
     */
    public TokenException(String errorCode) {
        super();
        if (errorCode == null) throw new IllegalArgumentException("ServiceException: errorCode cannot be null");
        this.errorCode = errorCode;
    }

    /**
     * New TokenException with an error code and a cause
     */
    public TokenException(String errorCode, Throwable cause) {
        super(cause);
        if (errorCode == null) throw new IllegalArgumentException("ServiceException: errorCode cannot be null");
        this.errorCode = errorCode;
    }

    /**
     * New TokenException with an error code, error parameter and a cause
     */
    public TokenException(String errorCode, String errorParam, Throwable cause) {
        super(cause);
        if (errorCode == null) throw new IllegalArgumentException("TokenException: errorCode cannot be null");
        this.errorCode = errorCode;
        this.errorParam = errorParam;
    }
    
    /**
     * Get the status code for this error.
     * <p>
     * Can be compared with EC_ error codes in this class.
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Get the parameter for this error.
     * Contains additional info / parameters in the error code
     */
    public String getErrorParam() {
        return errorParam;
    }
    
    /**
     * Get all causes
     */
    public static String getAllCauses(Exception ex) {
        Throwable currentCause = ex.getCause();
        StringBuffer causeBuffer = new StringBuffer();
        while (currentCause != null) {
            causeBuffer.append(currentCause.toString());
            currentCause = currentCause.getCause();
            if (currentCause != null) causeBuffer.append(" - ");
        }
        return causeBuffer.toString();
    }

}
