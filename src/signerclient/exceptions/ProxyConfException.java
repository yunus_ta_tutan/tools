/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.exceptions;

/**
 *
 * @author xuxux
 */
public class ProxyConfException extends Exception{
    
    public static final String PCE_ILLEGAL_PARAMETER = "'error code' cannot be null.";
    public static final String PCE_PARAMETER_SAVE_ERROR = "error occured while saving proxy parameters to 'proxy-config.xml'.";
    public static final String PCE_GET_PARAMETER_ERROR = "error occured while getting proxy parameters from 'proxy-config.xml'.";
    public static final String PCE_SET_SYSTEM_PARAMETER_ERROR = "error occured while setting system proxy parameters";
    private String errorCode;
    private String errorParam;
    
    public ProxyConfException(String code){
        super();
        if(null == code) throw new IllegalArgumentException(PCE_ILLEGAL_PARAMETER);
        this.errorCode = code;
    }
    
    public ProxyConfException(String code, Throwable cause){
        super(cause);
        if(null == code) throw new IllegalArgumentException(PCE_ILLEGAL_PARAMETER);
        this.errorCode = code;
    }
    
    public ProxyConfException(String code,String param, Throwable cause){
        super(code, cause);
        if(null == code) throw new IllegalArgumentException(PCE_ILLEGAL_PARAMETER);
        this.errorCode = code;
        this.errorParam = param;
    }
    
    /**
     * Get all causes
     * @param ex
     * @return 
     */
    public String getAllCauses(Exception ex) {
        Throwable currentCause = ex.getCause();
        StringBuilder causeBuffer = new StringBuilder();
        while (currentCause != null) {
            causeBuffer.append(currentCause.toString());
            currentCause = currentCause.getCause();
            if (currentCause != null) causeBuffer.append(" - ");
        }
        return causeBuffer.toString();
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorParam() {
        return errorParam;
    }

    public void setErrorParam(String errorParam) {
        this.errorParam = errorParam;
    }
    
}
