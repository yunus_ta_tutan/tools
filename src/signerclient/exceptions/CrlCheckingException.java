package signerclient.exceptions;

import java.io.Serializable;

/**
 * 
 * @author MDemir
 *
 */

public class CrlCheckingException extends Exception implements Serializable{
	
	private static final long	serialVersionUID	= 1L;

	public CrlCheckingException(String message) {
		super(message);
	}
	
	public CrlCheckingException(Throwable throwable){
		super(throwable);
	}
	
	public CrlCheckingException(String message, Throwable throwable) {
		super(message, throwable);
	}
	
}
