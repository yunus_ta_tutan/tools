/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.models;

/**
 *
 * @author xuxux
 */
public enum ResponsiveOutputType {
    
    XML("XML"),HTML("HTML"),PDF("PDF"),NONE("NONE");
    
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    private ResponsiveOutputType(String type){
        
    }
    
}
