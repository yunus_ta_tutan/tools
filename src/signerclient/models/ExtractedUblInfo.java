/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.models;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author xuxux
 */
public class ExtractedUblInfo implements Serializable{
    
    private String envelopeID;
    private String envelopeType;
    private String envelopeDate;
    private String senderVKN;
    private String senderTitle;
    private String receiverVKN;
    private String receiverTitle;
    private String invoiceId;
    private String issueDate;
    private String totalAmount;
    private String totalTaxAmount;
    private String baprResponse;

    /**
     * @return the envelopeID
     */
    public String getEnvelopeID() {
        return envelopeID;
    }

    /**
     * @param envelopeID the envelopeID to set
     */
    public void setEnvelopeID(String envelopeID) {
        this.envelopeID = envelopeID;
    }

    /**
     * @return the envelopeType
     */
    public String getEnvelopeType() {
        return envelopeType;
    }

    /**
     * @param envelopeType the envelopeType to set
     */
    public void setEnvelopeType(String envelopeType) {
        this.envelopeType = envelopeType;
    }

    /**
     * @return the envelopeDate
     */
    public String getEnvelopeDate() {
        return envelopeDate;
    }

    /**
     * @param envelopeDate the envelopeDate to set
     */
    public void setEnvelopeDate(String envelopeDate) {
        this.envelopeDate = envelopeDate;
    }

    /**
     * @return the senderVKN
     */
    public String getSenderVKN() {
        return senderVKN;
    }

    /**
     * @param senderVKN the senderVKN to set
     */
    public void setSenderVKN(String senderVKN) {
        this.senderVKN = senderVKN;
    }

    /**
     * @return the senderTitle
     */
    public String getSenderTitle() {
        return senderTitle;
    }

    /**
     * @param senderTitle the senderTitle to set
     */
    public void setSenderTitle(String senderTitle) {
        this.senderTitle = senderTitle;
    }

    /**
     * @return the receiverVKN
     */
    public String getReceiverVKN() {
        return receiverVKN;
    }

    /**
     * @param receiverVKN the receiverVKN to set
     */
    public void setReceiverVKN(String receiverVKN) {
        this.receiverVKN = receiverVKN;
    }

    /**
     * @return the receiverTitle
     */
    public String getReceiverTitle() {
        return receiverTitle;
    }

    /**
     * @param receiverTitle the receiverTitle to set
     */
    public void setReceiverTitle(String receiverTitle) {
        this.receiverTitle = receiverTitle;
    }

    /**
     * @return the invoiceId
     */
    public String getInvoiceId() {
        return invoiceId;
    }

    /**
     * @param invoiceId the invoiceId to set
     */
    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    /**
     * @return the issueDate
     */
    public String getIssueDate() {
        return issueDate;
    }

    /**
     * @param issueDate the issueDate to set
     */
    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    /**
     * @return the totalAmount
     */
    public String getTotalAmount() {
        return totalAmount;
    }

    /**
     * @param totalAmount the totalAmount to set
     */
    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     * @return the totalTaxAmount
     */
    public String getTotalTaxAmount() {
        return totalTaxAmount;
    }

    /**
     * @param totalTaxAmount the totalTaxAmount to set
     */
    public void setTotalTaxAmount(String totalTaxAmount) {
        this.totalTaxAmount = totalTaxAmount;
    }

    /**
     * @return the baprResponse
     */
    public String getBaprResponse() {
        return baprResponse;
    }

    /**
     * @param baprResponse the baprResponse to set
     */
    public void setBaprResponse(String baprResponse) {
        this.baprResponse = baprResponse;
    }

   
}
