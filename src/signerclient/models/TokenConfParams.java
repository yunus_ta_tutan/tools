/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.models;

/**
 *
 * @author xuxux
 */
public enum TokenConfParams {
    SLOT_LIST_INDEX,
    CARD_TYPE,
    PROJECT,
    USE_PKCS11,
    ARCMA3_DIR,
    PROVIDER_PATH,
    PKCS11_PASS,
    PFX_PASS,
    ALIAS
    
}
