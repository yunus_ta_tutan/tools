/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.models;

/**
 *
 * @author xuxux
 */
public enum CARD_TYPE {

    THALES("cknfast.dll"), SAFENET("cryptoki.dll"), UTIMACO("cryptoki.dll"), AKIS("akisp11.dll");

    
    private final String dll;

    public String getDll() {
        return dll;
    }
    CARD_TYPE(String dll){
        this.dll = dll;
    }
}
