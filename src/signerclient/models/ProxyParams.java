/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signerclient.models;

/**
 *
 * @author xuxux
 */
public enum ProxyParams {
    PROXY_SET,
    PROXY_HOSTNAME,
    PROXY_PORT,
    PROXY_USERNAME,
    PROXY_PASSWORD
}
