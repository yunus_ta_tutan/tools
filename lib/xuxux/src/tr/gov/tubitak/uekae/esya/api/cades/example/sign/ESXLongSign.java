package tr.gov.tubitak.uekae.esya.api.cades.example.sign;

import junit.framework.TestCase;
import tr.gov.tubitak.uekae.esya.api.asn.x509.ECertificate;
import tr.gov.tubitak.uekae.esya.api.cades.example.testconstans.TestConstants;
import tr.gov.tubitak.uekae.esya.api.cades.example.validation.ValidationUtil;
import tr.gov.tubitak.uekae.esya.api.cmssignature.ISignable;
import tr.gov.tubitak.uekae.esya.api.cmssignature.SignableByteArray;
import tr.gov.tubitak.uekae.esya.api.cmssignature.attribute.EParameters;
import tr.gov.tubitak.uekae.esya.api.cmssignature.signature.BaseSignedData;
import tr.gov.tubitak.uekae.esya.api.cmssignature.signature.ESignatureType;
import tr.gov.tubitak.uekae.esya.api.cmssignature.validation.SignedDataValidationResult;
import tr.gov.tubitak.uekae.esya.api.cmssignature.validation.SignedData_Status;
import tr.gov.tubitak.uekae.esya.api.common.crypto.BaseSigner;
import tr.gov.tubitak.uekae.esya.api.smartcard.example.SmartCardManager;
import tr.gov.tubitak.uekae.esya.asn.util.AsnIO;

import java.util.HashMap;

public class ESXLongSign extends TestCase
{
	/**
	 * creates ESXLong type signature and validate it.
	 * @throws Exception
	 */
	public void testEsxlongSign() throws Exception
	{
		BaseSignedData bs = new BaseSignedData();
		
		ISignable externalContent = new SignableByteArray("test".getBytes());
		bs.addContent(externalContent);
		
		HashMap<String, Object> params = new HashMap<String, Object>();
		
		//if you are using test certificates,without QCStatement,you must set P_CHECK_QC_STATEMENT to false.
		//By default,it is true
		boolean checkQCStatement = TestConstants.getCheckQCStatement();
		
		//necassary for getting signaturetimestamp
		params.put(EParameters.P_TSS_INFO, TestConstants.getTSSettings());
		
		//necessary for validation of signer certificate according to time in signaturetimestamp attribute
		params.put(EParameters.P_CERT_VALIDATION_POLICY, TestConstants.getPolicy());
		
		
		//Get qualified or non-qualified certificate.
		ECertificate cert = SmartCardManager.getInstance().getSignatureCertificate(checkQCStatement, !checkQCStatement);
		BaseSigner signer = SmartCardManager.getInstance().getSigner(TestConstants.getPIN(), cert);
		
		//add signer
		bs.addSigner(ESignatureType.TYPE_ESXLong, cert, signer, null, params);
		
		SmartCardManager.getInstance().logout();
		
		byte [] signedDocument = bs.getEncoded();
		
		AsnIO.dosyayaz(signedDocument, TestConstants.getDirectory() + "testdata/ESXLong-1.p7s");
		
		SignedDataValidationResult sdvr =ValidationUtil.validate(signedDocument, externalContent);
		
		assertEquals(SignedData_Status.ALL_VALID, sdvr.getSDStatus());
	}
}
