package tr.gov.tubitak.uekae.esya.api.cades.example.sign;

import junit.framework.TestCase;
import tr.gov.tubitak.uekae.esya.api.asn.x509.ECertificate;
import tr.gov.tubitak.uekae.esya.api.cades.example.testconstans.TestConstants;
import tr.gov.tubitak.uekae.esya.api.cades.example.validation.ValidationUtil;
import tr.gov.tubitak.uekae.esya.api.certificate.validation.policy.ValidationPolicy;
import tr.gov.tubitak.uekae.esya.api.cmssignature.ISignable;
import tr.gov.tubitak.uekae.esya.api.cmssignature.SignableByteArray;
import tr.gov.tubitak.uekae.esya.api.cmssignature.attribute.EParameters;
import tr.gov.tubitak.uekae.esya.api.cmssignature.attribute.IAttribute;
import tr.gov.tubitak.uekae.esya.api.cmssignature.attribute.SigningTimeAttr;
import tr.gov.tubitak.uekae.esya.api.cmssignature.signature.BaseSignedData;
import tr.gov.tubitak.uekae.esya.api.cmssignature.signature.ESignatureType;
import tr.gov.tubitak.uekae.esya.api.cmssignature.validation.SignedDataValidationResult;
import tr.gov.tubitak.uekae.esya.api.cmssignature.validation.SignedData_Status;
import tr.gov.tubitak.uekae.esya.api.common.crypto.Algorithms;
import tr.gov.tubitak.uekae.esya.api.common.crypto.BaseSigner;
import tr.gov.tubitak.uekae.esya.api.crypto.alg.DigestAlg;
import tr.gov.tubitak.uekae.esya.api.smartcard.example.SmartCardManager;
import tr.gov.tubitak.uekae.esya.asn.util.AsnIO;

import java.security.spec.MGF1ParameterSpec;
import java.security.spec.PSSParameterSpec;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * This class shows creations BES type signature.
 * @author orcun.ertugrul
 */

public class BESSign extends TestCase
{
	/**
	 * creates BES type signature and validate it.
	 * @throws Exception
	 */
	public void testSimpleSign() throws Exception
	{
		BaseSignedData bs = new BaseSignedData();
		ISignable content = new SignableByteArray("test".getBytes());
		bs.addContent(content);
				
		HashMap<String, Object> params = new HashMap<String, Object>();
		
		//if the user does not want certificate validation at generating signature,he can add 
		//P_VALIDATE_CERTIFICATE_BEFORE_SIGNING parameter with its value set to false
		//params.put(EParameters.P_VALIDATE_CERTIFICATE_BEFORE_SIGNING, false);

		//necessary for certificate validation.By default,certificate validation is done 
		params.put(EParameters.P_CERT_VALIDATION_POLICY, TestConstants.getPolicy());
				
		//By default, QC statement is checked,and signature wont be created if it is not a 
		//qualified certificate. 
		boolean checkQCStatement = TestConstants.getCheckQCStatement();
				
		//Get qualified or non-qualified certificate.
		ECertificate cert = SmartCardManager.getInstance().getSignatureCertificate(checkQCStatement, !checkQCStatement);
		BaseSigner signer = SmartCardManager.getInstance().getSigner(TestConstants.getPIN(), cert);
		
		//add signer
		//Since the specified attributes are mandatory for bes,null is given as parameter 
		//for optional attributes
		bs.addSigner(ESignatureType.TYPE_BES, cert , signer, null, params);
		
		SmartCardManager.getInstance().logout();
		
		byte [] signedDocument = bs.getEncoded();
		
		//write the contentinfo to file
		AsnIO.dosyayaz(signedDocument,TestConstants.getDirectory() + "testdata/BES-1.p7s");
		
		SignedDataValidationResult sdvr = ValidationUtil.validate(signedDocument, null);
		System.out.println(sdvr);
		
		assertEquals(SignedData_Status.ALL_VALID, sdvr.getSDStatus());
	}		
	
	/**
	 * creates BES type signature with signing time attribute and validate it.
	 * @throws Exception
	 */
	public void testSigningTimeAttrSign() throws Exception
	{
		BaseSignedData bs = new BaseSignedData();
		ISignable content = new SignableByteArray("test".getBytes());
		bs.addContent(content);
		
		//Since SigningTime attribute is optional,add it to optional attributes list
		List<IAttribute> optionalAttributes = new ArrayList<IAttribute>();
		optionalAttributes.add(new SigningTimeAttr(Calendar.getInstance()));
				
		HashMap<String, Object> params = new HashMap<String, Object>();
		ValidationPolicy policy=  TestConstants.getPolicy();
		
		//necessary for certificate validation.By default,certificate validation is done 
		params.put(EParameters.P_CERT_VALIDATION_POLICY, policy);
		
		//if the user does not want certificate validation,he can add 
		//P_VALIDATE_CERTIFICATE_BEFORE_SIGNING parameter with its value set to false
		//params.put(EParameters.P_VALIDATE_CERTIFICATE_BEFORE_SIGNING, false);
				
		//By default, QC statement is checked,and signature wont be created if it is not a 
		//qualified certificate.
		boolean checkQCStatement = TestConstants.getCheckQCStatement();
		
		//Get qualified or non-qualified certificate.
		ECertificate cert = SmartCardManager.getInstance().getSignatureCertificate(checkQCStatement, !checkQCStatement);
		BaseSigner signer = SmartCardManager.getInstance().getSigner(TestConstants.getPIN(), cert);

		
		//add signer
		//Since the specified attributes are mandatory for bes,null is given as parameter 
		//for optional attributes
		bs.addSigner(ESignatureType.TYPE_BES, cert , signer, optionalAttributes, params);
		
		SmartCardManager.getInstance().logout();
		
		byte [] signedDocument = bs.getEncoded();
		
		//write the contentinfo to file
		AsnIO.dosyayaz(signedDocument,TestConstants.getDirectory() + "testdata/BES-2.p7s");
		
		SignedDataValidationResult sdvr = ValidationUtil.validate(signedDocument, null);
		
		assertEquals(SignedData_Status.ALL_VALID, sdvr.getSDStatus());
	}	
	
	public void testPSSSign() throws Exception
	{
		BaseSignedData bs = new BaseSignedData();
		ISignable content = new SignableByteArray("test".getBytes());
		bs.addContent(content);
				
		HashMap<String, Object> params = new HashMap<String, Object>();
		
		//if the user does not want certificate validation at generating signature,he can add 
		//P_VALIDATE_CERTIFICATE_BEFORE_SIGNING parameter with its value set to false
		//params.put(EParameters.P_VALIDATE_CERTIFICATE_BEFORE_SIGNING, false);

		//necessary for certificate validation.By default,certificate validation is done 
		params.put(EParameters.P_CERT_VALIDATION_POLICY, TestConstants.getPolicy());
				
		//By default, QC statement is checked,and signature wont be created if it is not a 
		//qualified certificate. 
		boolean checkQCStatement = TestConstants.getCheckQCStatement();
		params.put(EParameters.P_VALIDATE_CERTIFICATE_BEFORE_SIGNING, false);
		
		DigestAlg digestAlg = DigestAlg.SHA256;
		PSSParameterSpec spec = new PSSParameterSpec(digestAlg.getName(), "MGF1", new MGF1ParameterSpec(digestAlg.getName()), 
				digestAlg.getDigestLength(), 0);
		
		ECertificate cert = SmartCardManager.getInstance().getSignatureCertificate(checkQCStatement, !checkQCStatement);
		BaseSigner signer = SmartCardManager.getInstance().getSigner(TestConstants.getPIN(), cert, Algorithms.SIGNATURE_RSA_PSS, spec);
		
		bs.addSigner(ESignatureType.TYPE_BES, cert , signer, null, params);
		
		SmartCardManager.getInstance().logout();
		
		byte [] signedDocument = bs.getEncoded();
		
		//write the contentinfo to file
		AsnIO.dosyayaz(signedDocument,TestConstants.getDirectory() + "testdata/BES-3.p7s");
		
		SignedDataValidationResult sdvr = ValidationUtil.validate(signedDocument, null);
		
		System.out.println(sdvr);
		
		assertEquals(SignedData_Status.ALL_VALID, sdvr.getSDStatus());
	}	
}
