package tr.gov.tubitak.uekae.esya.api.cades.example.signwithsmartcardmodule;

import junit.framework.TestCase;
import tr.gov.tubitak.uekae.esya.api.cades.example.testconstans.TestConstants;
import tr.gov.tubitak.uekae.esya.api.common.crypto.BaseSigner;
import tr.gov.tubitak.uekae.esya.api.smartcard.example.JSmartCardManager;
import tr.gov.tubitak.uekae.esya.api.smartcard.signature.PKCS7Signature;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.security.cert.X509Certificate;

public class BESSignWithSmartCardModule extends TestCase
{
	/**
	 * You can create a BES type signature with smart card module.
	 * Also you can not validate certificate before creating signature.
	 */
	public  void testBESSignWithSmartCardModule() throws Exception
	{
		byte [] toBeSigned = "161626 nolu hesaba havaleyi onaylıyorum.".getBytes("UTF-8");
		
		PKCS7Signature pkcsSignature = new PKCS7Signature();
		ByteArrayOutputStream signature = new ByteArrayOutputStream();
		
		X509Certificate cert = JSmartCardManager.getInstance().getSignatureCertificate(true, false);
		BaseSigner signer = JSmartCardManager.getInstance().getSigner(TestConstants.getPIN(), cert);

		ByteArrayInputStream bais = new ByteArrayInputStream(toBeSigned);
		pkcsSignature.signExternalContent(bais, cert, signature, signer);
	}
	
	
}
