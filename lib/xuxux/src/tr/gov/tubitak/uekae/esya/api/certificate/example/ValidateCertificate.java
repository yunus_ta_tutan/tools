package tr.gov.tubitak.uekae.esya.api.certificate.example;

import junit.framework.TestCase;
import tr.gov.tubitak.uekae.esya.api.asn.x509.ECertificate;
import tr.gov.tubitak.uekae.esya.api.certificate.validation.CertificateStatus;
import tr.gov.tubitak.uekae.esya.api.certificate.validation.CertificateValidation;
import tr.gov.tubitak.uekae.esya.api.certificate.validation.ValidationSystem;
import tr.gov.tubitak.uekae.esya.api.certificate.validation.check.certificate.CertificateStatusInfo;
import tr.gov.tubitak.uekae.esya.api.certificate.validation.policy.PolicyReader;
import tr.gov.tubitak.uekae.esya.api.certificate.validation.policy.ValidationPolicy;
import tr.gov.tubitak.uekae.esya.api.smartcard.example.SmartCardManager;

import java.io.FileInputStream;
import java.util.Calendar;

public class ValidateCertificate extends TestCase
{
    private static String POLICY_FILE_NES="./config/certval-policy.xml";
    private static String POLICY_FILE_MM="./config/certval-policy-malimuhur.xml";

    public ValidationPolicy getPolicy(String fileName) throws Exception
    {
			return PolicyReader.readValidationPolicy(new FileInputStream(fileName));
    }
    
	public void testValidateNESCertificate() throws Exception
	{
        boolean QCStatement = true; //Qualified certificate		
		ECertificate cert = SmartCardManager.getInstance().getSignatureCertificate(QCStatement, !QCStatement);
		
		ValidationSystem vs = CertificateValidation.createValidationSystem(getPolicy(POLICY_FILE_NES));
		vs.setBaseValidationTime(Calendar.getInstance());
		CertificateStatusInfo csi = CertificateValidation.validateCertificate(vs, cert);
		if(csi.getCertificateStatus() != CertificateStatus.VALID)
			throw new Exception("Not Verified");
		
	}
	public void testValidateMMCertificate() throws Exception
	{
        boolean QCStatement = false; //Unqualified certificate		
		ECertificate cert = SmartCardManager.getInstance().getSignatureCertificate(QCStatement, !QCStatement);
		
		ValidationSystem vs = CertificateValidation.createValidationSystem(getPolicy(POLICY_FILE_MM));
		vs.setBaseValidationTime(Calendar.getInstance());
		CertificateStatusInfo csi = CertificateValidation.validateCertificate(vs, cert);
		if(csi.getCertificateStatus() != CertificateStatus.VALID)
			throw new Exception("Not Verified");
		
	}
}
