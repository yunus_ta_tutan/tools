package tr.gov.tubitak.uekae.esya.api.xades.example.formats;

import org.junit.Test;

import tr.gov.tubitak.uekae.esya.api.asn.x509.ECertificate;
import tr.gov.tubitak.uekae.esya.api.signature.SignatureType;
import tr.gov.tubitak.uekae.esya.api.smartcard.example.JSmartCardManager;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.Context;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.SignatureMethod;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.XMLSignature;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.XMLSignatureException;
import tr.gov.tubitak.uekae.esya.api.xades.example.utils.SampleBase;
import tr.gov.tubitak.uekae.esya.api.xades.example.validation.Validation;

import java.io.FileOutputStream;
import java.io.IOException;
import java.security.cert.X509Certificate;

/**
 * X1 type signature signing sample
 * @author suleyman.uslu
 */
public class X1 extends SampleBase {

    public static final String SIGNATURE_FILENAME = "x1.xml";

    /**
     * Creates detached X1 type signature
     * @throws Exception
     */
    @Test
    public void createX1() throws Exception
    {
        try {
            // create context with working directory
            Context context = createContext();

            // create signature according to context,
            // with default type (XADES_BES)
            XMLSignature signature = new XMLSignature(context);

            // add document as reference, but do not embed it
            // into the signature (embed=false)
            signature.addDocument("./sample.txt", "text/plain", false);

            signature.getSignedInfo().setSignatureMethod(SignatureMethod.RSA_SHA256);

            // false-true gets non-qualified certificates while true-false gets qualified ones
            X509Certificate cert = JSmartCardManager.getInstance().getSignatureCertificate(true, false);

            // add certificate to show who signed the document
            signature.addKeyInfo(new ECertificate(cert.getEncoded()));

            // now sign it by using smart card
            signature.sign(JSmartCardManager.getInstance().getSigner(PIN, cert));

            // upgrade to X1
            signature.upgrade(SignatureType.ES_X_Type1);

            signature.write(new FileOutputStream(BASE_DIR + SIGNATURE_FILENAME));
        }
        catch (XMLSignatureException x){
            // cannot create signature
            x.printStackTrace();
        }
        catch (IOException x){
            // probably couldn't write to the file
            x.printStackTrace();
        }
    }

    @Test
    public void validate() throws Exception {
        Validation.validate(SIGNATURE_FILENAME);
    }
}
