package tr.gov.tubitak.uekae.esya.api.cades.example.validation;

import junit.framework.TestCase;
import tr.gov.tubitak.uekae.esya.api.cades.example.testconstans.TestConstants;
import tr.gov.tubitak.uekae.esya.api.cmssignature.signature.BaseSignedData;
import tr.gov.tubitak.uekae.esya.asn.util.AsnIO;

/**
 * Gets signer certificate from BasedSignedData
 * @author orcun.ertugrul
 *
 */
public class GetSignerCertificate extends TestCase
{
	/***
	 * Gets certificate of the first signature.
	 * @throws Exception
	 */
	public void testGetCertificate() throws Exception
	{
		byte [] sign = AsnIO.dosyadanOKU(TestConstants.getDirectory() + "testdata/BES-1.p7s");
		BaseSignedData bs = new BaseSignedData(sign);
		System.out.println("Certificate Owner Name: " + bs.getSignerList().get(0).getSignerCertificate().getSubject().getCommonNameAttribute());
		System.out.println("Certificate Owner TC Kimlik No: " + bs.getSignerList().get(0).getSignerCertificate().getSubject().getSerialNumberAttribute());
	}
}
