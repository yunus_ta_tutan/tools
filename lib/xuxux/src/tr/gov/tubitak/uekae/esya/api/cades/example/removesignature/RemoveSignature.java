package tr.gov.tubitak.uekae.esya.api.cades.example.removesignature;

import junit.framework.TestCase;
import tr.gov.tubitak.uekae.esya.api.cades.example.testconstans.TestConstants;
import tr.gov.tubitak.uekae.esya.api.cmssignature.signature.BaseSignedData;
import tr.gov.tubitak.uekae.esya.asn.util.AsnIO;

/**
 * Removes signature. 
 * Firstly run sign operations in order to create signatures to be removed. 
 * @author orcun.ertugrul
 *
 */
public class RemoveSignature extends TestCase
{
	/**
	 * Removes the first counter signature and remains no signer.
	 * @throws Exception
	 */
	public void testRemoveAll() throws Exception
	{
		byte [] content = AsnIO.dosyadanOKU(TestConstants.getDirectory() + "testdata\\counterSignatures.p7s");
		BaseSignedData bs = new BaseSignedData(content);
		bs.getSignerList().get(0).remove();
		byte [] noSign = bs.getEncoded();
		
		BaseSignedData removedBsd = new BaseSignedData(noSign);
		assertEquals(0, removedBsd.getAllSigners().size());
		
	}
	
	/**
	 * Removes the second counter signature and remains one signer.
	 * @throws Exception
	 */
	public void testKeepOne() throws Exception
	{
		byte [] content = AsnIO.dosyadanOKU(TestConstants.getDirectory() + "testdata\\counterSignatures.p7s");
		BaseSignedData bs = new BaseSignedData(content);
		bs.getSignerList().get(0).getCounterSigners().get(0).remove();
		byte [] noSign = bs.getEncoded();
		
		BaseSignedData removedBsd = new BaseSignedData(noSign);
		assertEquals(1, removedBsd.getAllSigners().size());
		
	}
	
	/**
	 * Removes the third counter signature and remains two signer.
	 * @throws Exception
	 */
	public void testKeepTwo() throws Exception
	{
		byte [] content = AsnIO.dosyadanOKU(TestConstants.getDirectory() + "testdata\\counterSignatures.p7s");
		BaseSignedData bs = new BaseSignedData(content);
		
		bs.getSignerList().get(0).getCounterSigners().get(0).getCounterSigners().get(0).remove();
		byte [] noSign = bs.getEncoded();
		
		BaseSignedData removedBsd = new BaseSignedData(noSign);
		assertEquals(2, removedBsd.getAllSigners().size());
	}
	
	/**
	 * Removes the fourth counter signature and remains three signer.
	 * @throws Exception
	 */
	public void testKeepThree() throws Exception
	{
		byte [] content = AsnIO.dosyadanOKU(TestConstants.getDirectory() +"testdata\\counterSignatures.p7s");
		BaseSignedData bs = new BaseSignedData(content);
		bs.getSignerList().get(0).getCounterSigners().get(0).getCounterSigners().get(0).getCounterSigners().get(0).remove();
		byte [] noSign = bs.getEncoded();
		
		BaseSignedData removedBsd = new BaseSignedData(noSign);
		assertEquals(3, removedBsd.getAllSigners().size());
		
	}
	
}
