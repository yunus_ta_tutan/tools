package tr.gov.tubitak.uekae.esya.api.SignatureAPI.example;

import org.junit.Assert;
import org.junit.Test;
import tr.gov.tubitak.uekae.esya.api.asn.x509.ECertificate;
import tr.gov.tubitak.uekae.esya.api.common.crypto.BaseSigner;
import tr.gov.tubitak.uekae.esya.api.signature.*;
import tr.gov.tubitak.uekae.esya.api.smartcard.example.SmartCardManager;

public class AttachDetach {
    @Test
    public void attachNewSignatureToExisting() throws Exception
    {
        Context c = Constants.createContext();
        SignatureContainer sc = SignatureFactory.createContainer(Constants.signatureFormat, c);
        
        boolean checkQCStatement = Constants.getCheckQCStatement();		
		//Get qualified or non-qualified certificate.
		ECertificate cert = SmartCardManager.getInstance().getSignatureCertificate(checkQCStatement, !checkQCStatement);
		BaseSigner signer = SmartCardManager.getInstance().getSigner(Constants.getPIN(), cert);
		
        Signature s1 = sc.createSignature(cert);
        s1.addContent(Constants.getContent(), false);
        s1.sign(signer);

        Signature s2 = sc.createSignature(cert);
        s2.addContent(Constants.getContent(), false);
        s2.sign(signer);
        SmartCardManager.getInstance().logout();
        SignatureContainer existing = Constants.readSignatureContainer("bes_enveloping", c);
        existing.addExternalSignature(s1);
        existing.addExternalSignature(s2);

        Constants.dosyaYaz(existing, "bes_multiple_attached");
    }

    @Test
    public void detachSignature() throws Exception {
        SignatureContainer existing = Constants.readSignatureContainer("bes_multiple_attached");
        // remove third signature!
        existing.getSignatures().get(2).detachFromParent();
        Constants.dosyaYaz(existing, "bes_multiple_detached");
    }

    @Test
    public void detachCounterLeaf() throws Exception {
        SignatureContainer existing = Constants.readSignatureContainer("serial_to_serial_bes");

        // first counter
        Signature cs = existing.getSignatures().get(0).getCounterSignatures().get(0);
        // counter of counter
        Signature cc = cs.getCounterSignatures().get(0);

        cc.detachFromParent();
        Constants.dosyaYaz(existing, "bes_serial_detached_leaf");
    }

    @Test
    public void detachCounterMiddle() throws Exception {
        SignatureContainer existing = Constants.readSignatureContainer("serial_to_serial_bes");

        // first counter
        Signature cs = existing.getSignatures().get(0).getCounterSignatures().get(0);

        cs.detachFromParent();
        Constants.dosyaYaz(existing, "bes_serial_detached_middle");
    }

    // attach to A
    // detach from A
    // detach serial
    // detach subserial

    @Test
    public void validateAttached() throws Exception {
        SignatureContainer sc = Constants.readSignatureContainer("bes_multiple_attached", Constants.createContext());
        Assert.assertEquals(3, sc.getSignatures().size());
        ContainerValidationResult cvr = sc.verifyAll();
        System.out.println(cvr);
        Assert.assertEquals(ContainerValidationResultType.ALL_VALID, cvr.getResultType());
    }

    @Test
    public void validateDetached() throws Exception {
        SignatureContainer sc = Constants.readSignatureContainer("bes_multiple_detached");
        Assert.assertEquals(2, sc.getSignatures().size());
        ContainerValidationResult cvr = sc.verifyAll();
        System.out.println(cvr);
        Assert.assertEquals(ContainerValidationResultType.ALL_VALID, cvr.getResultType());
    }

    @Test
    public void validateDetachedSerialLeaf() throws Exception {
        SignatureContainer sc = Constants.readSignatureContainer("bes_serial_detached_leaf");
        Assert.assertEquals(1, sc.getSignatures().size());
        Assert.assertEquals(1, sc.getSignatures().get(0).getCounterSignatures().size());
        Assert.assertEquals(0, sc.getSignatures().get(0).getCounterSignatures().get(0).getCounterSignatures().size());
        ContainerValidationResult cvr = sc.verifyAll();
        System.out.println(cvr);
        Assert.assertEquals(ContainerValidationResultType.ALL_VALID, cvr.getResultType());
    }

    @Test
    public void validateDetachedSerialMiddle() throws Exception {
        SignatureContainer sc = Constants.readSignatureContainer("bes_serial_detached_middle");
        Assert.assertEquals(1, sc.getSignatures().size());
        Assert.assertEquals(0, sc.getSignatures().get(0).getCounterSignatures().size());
        ContainerValidationResult cvr = sc.verifyAll();
        System.out.println(cvr);
        Assert.assertEquals(ContainerValidationResultType.ALL_VALID, cvr.getResultType());
    }
}
