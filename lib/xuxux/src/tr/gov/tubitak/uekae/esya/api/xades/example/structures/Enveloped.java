package tr.gov.tubitak.uekae.esya.api.xades.example.structures;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.w3c.dom.Document;

import tr.gov.tubitak.uekae.esya.api.asn.x509.ECertificate;
import tr.gov.tubitak.uekae.esya.api.smartcard.example.JSmartCardManager;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.Context;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.SignatureMethod;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.XMLSignature;
import tr.gov.tubitak.uekae.esya.api.xades.example.utils.SampleBase;
import tr.gov.tubitak.uekae.esya.api.xades.example.validation.Validation;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileOutputStream;
import java.security.cert.X509Certificate;

/**
 * Enveloped BES sample
 */
@RunWith(JUnit4.class)
public class Enveloped extends SampleBase
{
    public static final String SIGNATURE_FILENAME = "enveloped.xml";

    /**
     * Create enveloped BES
     * @throws Exception
     */
    @Test
    public void createEnveloped() throws Exception
    {
        // here is our custom envelope XML
        Document envelopeDoc = newEnvelope();


        // create context with working directory
        Context context = createContext();

        // define where signature belongs to
        context.setDocument(envelopeDoc);


        // create signature according to context,
        // with default type (XADES_BES)
        XMLSignature signature = new XMLSignature(context, false);

        // attach signature to envelope
        envelopeDoc.getDocumentElement().appendChild(signature.getElement());

        // add document as reference,
        signature.addDocument("#data1", "text/xml", false);

        signature.getSignedInfo().setSignatureMethod(SignatureMethod.RSA_SHA256);

        // false-true gets non-qualified certificates while true-false gets qualified ones
        X509Certificate cert = JSmartCardManager.getInstance().getSignatureCertificate(true, false);

        // add certificate to show who signed the document
        signature.addKeyInfo(new ECertificate(cert.getEncoded()));

        // now sign it by using smart card
        signature.sign(JSmartCardManager.getInstance().getSigner(PIN, cert));


        // this time we do not use signature.write because we need to write
        // whole document instead of signature
        Source source = new DOMSource(envelopeDoc);
        Transformer transformer = TransformerFactory.newInstance().newTransformer();

        // write to file
        transformer.transform(source, new StreamResult(new FileOutputStream(BASE_DIR + SIGNATURE_FILENAME)));
    }

    @Test
    public void validate() throws Exception {
        Validation.validate(SIGNATURE_FILENAME);
    }

}
