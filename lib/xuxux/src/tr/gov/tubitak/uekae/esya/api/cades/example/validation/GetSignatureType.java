package tr.gov.tubitak.uekae.esya.api.cades.example.validation;

import junit.framework.TestCase;
import tr.gov.tubitak.uekae.esya.api.cades.example.testconstans.TestConstants;
import tr.gov.tubitak.uekae.esya.api.cmssignature.signature.BaseSignedData;
import tr.gov.tubitak.uekae.esya.api.cmssignature.signature.ESignatureType;
import tr.gov.tubitak.uekae.esya.asn.util.AsnIO;

public class GetSignatureType extends TestCase
{
	/**
	 * Getting signature type of a ESA type signature
	 * @throws Exception
	 */
	public void testIsItESA() throws Exception
	{
		byte [] content = AsnIO.dosyadanOKU(TestConstants.getDirectory() + "testdata/ESA-Converted-1.p7s");
		BaseSignedData bs = new BaseSignedData(content);
		
		ESignatureType type= bs.getSignerList().get(0).getType();
		
		assertEquals(type, ESignatureType.TYPE_ESA);
	}
}
