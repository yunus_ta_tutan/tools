package tr.gov.tubitak.uekae.esya.api.SignatureAPI.example;

import org.junit.Test;
import tr.gov.tubitak.uekae.esya.api.asn.x509.ECertificate;
import tr.gov.tubitak.uekae.esya.api.common.crypto.BaseSigner;
import tr.gov.tubitak.uekae.esya.api.signature.*;
import tr.gov.tubitak.uekae.esya.api.smartcard.example.SmartCardManager;

import static org.junit.Assert.assertEquals;

public class Bes {
	@Test
    public void createDetached() throws Exception
    {
        SignatureContainer container = SignatureFactory.createContainer(Constants.signatureFormat, Constants.createContext());
		
        boolean checkQCStatement = Constants.getCheckQCStatement();		
		//Get qualified or non-qualified certificate.
		ECertificate cert = SmartCardManager.getInstance().getSignatureCertificate(checkQCStatement, !checkQCStatement);
		BaseSigner signer = SmartCardManager.getInstance().getSigner(Constants.getPIN(), cert);
		
        Signature signature = container.createSignature(cert);

        signature.addContent(Constants.getContent(), false);

        signature.sign(signer);
        SmartCardManager.getInstance().logout();
        Constants.dosyaYaz(container,"bes_detached");
    }
    @Test
    public void createEnveloping() throws Exception
    {
        SignatureContainer container = SignatureFactory.createContainer(Constants.signatureFormat, Constants.createContext());
        //container.getContext().setConfig(new Config("./config/esya-signature-config.xml"));
        boolean checkQCStatement = Constants.getCheckQCStatement();		
		//Get qualified or non-qualified certificate.
		ECertificate cert = SmartCardManager.getInstance().getSignatureCertificate(checkQCStatement, !checkQCStatement);
		BaseSigner signer = SmartCardManager.getInstance().getSigner(Constants.getPIN(), cert);        
        
        Signature signature = container.createSignature(cert);

        signature.addContent(Constants.getContent(), true);

        signature.sign(signer);
        SmartCardManager.getInstance().logout();
        Constants.dosyaYaz(container, "bes_enveloping");
    }

    @Test
    public void validateDetached() throws Exception
    {
        Context c = Constants.createContext();
        ContainerValidationResult cvr = Validation.validateSignature("bes_detached", c);
        assertEquals(ContainerValidationResultType.ALL_VALID, cvr.getResultType());
        assertEquals(1, cvr.getSignatureValidationResults().size());
    }

    @Test
    public void validateEnveloping() throws Exception
    {
        ContainerValidationResult cvr = Validation.validateSignature("bes_enveloping");
        assertEquals(ContainerValidationResultType.ALL_VALID, cvr.getResultType());
        assertEquals(1, cvr.getSignatureValidationResults().size());
    }
	
}
