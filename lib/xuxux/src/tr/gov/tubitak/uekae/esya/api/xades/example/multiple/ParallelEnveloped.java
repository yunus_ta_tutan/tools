package tr.gov.tubitak.uekae.esya.api.xades.example.multiple;

import org.junit.Test;

import tr.gov.tubitak.uekae.esya.api.asn.x509.ECertificate;
import tr.gov.tubitak.uekae.esya.api.smartcard.example.JSmartCardManager;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.Context;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.SignatureMethod;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.SignedDocument;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.XMLSignature;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.document.Document;
import tr.gov.tubitak.uekae.esya.api.xades.example.utils.SampleBase;
import tr.gov.tubitak.uekae.esya.api.xades.example.validation.Validation;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.resolver.Resolver;

import java.io.FileOutputStream;
import java.security.cert.X509Certificate;

/**
 * Parallel enveloped signature sample
 * @author suleyman.uslu
 */
public class ParallelEnveloped extends SampleBase {

    public static final String SIGNATURE_FILENAME = "parallel_enveloped.xml";

    /**
     * Creates two signatures in a document, that signs same inner data
     * @throws Exception
     */
    @Test
    public void createParallelEnveloped() throws Exception
    {
        Context context = createContext();

        SignedDocument signatures = new SignedDocument(context);

        Document doc = Resolver.resolve("./sample.txt", context);
        String fragment = signatures.addDocument(doc);

        XMLSignature signature1 = signatures.createSignature();

        // add document as inner reference
        signature1.addDocument("#"+fragment, "text/plain", false);

        signature1.getSignedInfo().setSignatureMethod(SignatureMethod.RSA_SHA256);

        // false-true gets non-qualified certificates while true-false gets qualified ones
        X509Certificate cert = JSmartCardManager.getInstance().getSignatureCertificate(true, false);

        // add certificate to show who signed the document
        signature1.addKeyInfo(new ECertificate(cert.getEncoded()));

        // now sign it by using smart card
        signature1.sign(JSmartCardManager.getInstance().getSigner(PIN, cert));

        XMLSignature signature2 = signatures.createSignature();

        // add document as inner reference
        signature2.addDocument("#"+fragment, "text/plain", false);

        signature2.getSignedInfo().setSignatureMethod(SignatureMethod.RSA_SHA256);

        // add certificate to show who signed the document
        signature2.addKeyInfo(new ECertificate(cert.getEncoded()));

        // now sign it by using smart card
        signature2.sign(JSmartCardManager.getInstance().getSigner(PIN, cert));

        // write combined document
        signatures.write(new FileOutputStream(BASE_DIR + SIGNATURE_FILENAME));
    }

    @Test
    public void validate() throws Exception {
        Validation.validateParallel(SIGNATURE_FILENAME);
    }
}
