package tr.gov.tubitak.uekae.esya.api.SignatureAPI.example;

import org.junit.Test;
import org.junit.Assert;

import tr.gov.tubitak.uekae.esya.api.asn.x509.ECertificate;
import tr.gov.tubitak.uekae.esya.api.common.crypto.BaseSigner;
import tr.gov.tubitak.uekae.esya.api.signature.*;
import tr.gov.tubitak.uekae.esya.api.signature.profile.TurkishESigProfiles;
import tr.gov.tubitak.uekae.esya.api.smartcard.example.SmartCardManager;

import java.util.Calendar;

public class TurkishESigProfile {
    @Test
    public void createP1() throws Exception
    {
        SignatureContainer container = SignatureFactory.createContainer(Constants.signatureFormat, Constants.createContext());
		
        boolean checkQCStatement = Constants.getCheckQCStatement();		
		//Get qualified or non-qualified certificate.
		ECertificate cert = SmartCardManager.getInstance().getSignatureCertificate(checkQCStatement, !checkQCStatement);
		BaseSigner signer = SmartCardManager.getInstance().getSigner(Constants.getPIN(), cert);
		
        Signature signature = container.createSignature(cert);
        signature.addContent(Constants.getContent(), false);
        signature.setSigningTime(Calendar.getInstance());

        signature.sign(signer);
        SmartCardManager.getInstance().logout();        
        Constants.dosyaYaz(container, "turkish_profile_p1_bes");
    }

    @Test
    public void createP2() throws Exception
    {
        SignatureContainer container = SignatureFactory.createContainer(Constants.signatureFormat, Constants.createContext());
		
        boolean checkQCStatement = Constants.getCheckQCStatement();		
		//Get qualified or non-qualified certificate.
		ECertificate cert = SmartCardManager.getInstance().getSignatureCertificate(checkQCStatement, !checkQCStatement);
		BaseSigner signer = SmartCardManager.getInstance().getSigner(Constants.getPIN(), cert);
	       
        Signature signature = container.createSignature(cert);
        signature.addContent(Constants.getContent(), false);
        signature.setSigningTime(Calendar.getInstance());
        signature.setSignaturePolicy(TurkishESigProfiles.SIG_POLICY_ID_P2v1);

        signature.sign(signer);
        signature.upgrade(SignatureType.ES_T);
        SmartCardManager.getInstance().logout();        
        Constants.dosyaYaz(container, "turkish_profile_p2_t");
    }

    @Test
    public void createP3() throws Exception
    {

        Context context = Constants.createContext();
        context.getConfig().setCertificateValidationPolicies(Constants.getCRLOnlyPolicy());

        SignatureContainer container = SignatureFactory.createContainer(Constants.signatureFormat, context);
		
        boolean checkQCStatement = Constants.getCheckQCStatement();		
		//Get qualified or non-qualified certificate.
		ECertificate cert = SmartCardManager.getInstance().getSignatureCertificate(checkQCStatement, !checkQCStatement);
		BaseSigner signer = SmartCardManager.getInstance().getSigner(Constants.getPIN(), cert);
	
        Signature signature = container.createSignature(cert);
        signature.addContent(Constants.getContent(), false);
        signature.setSigningTime(Calendar.getInstance());
        signature.setSignaturePolicy(TurkishESigProfiles.SIG_POLICY_ID_P3v1);

        signature.sign(signer);
        signature.upgrade(SignatureType.ES_XL);
        SmartCardManager.getInstance().logout();        
        Constants.dosyaYaz(container, "turkish_profile_p3_xl_crl");
    }

    @Test
    public void createP4() throws Exception
    {
        Context context = Constants.createContext();
        context.getConfig().setCertificateValidationPolicies(Constants.getOCSPOnlyPolicy());

        SignatureContainer container = SignatureFactory.createContainer(Constants.signatureFormat, context);
		
        boolean checkQCStatement = Constants.getCheckQCStatement();		
		//Get qualified or non-qualified certificate.
		ECertificate cert = SmartCardManager.getInstance().getSignatureCertificate(checkQCStatement, !checkQCStatement);
		BaseSigner signer = SmartCardManager.getInstance().getSigner(Constants.getPIN(), cert);
	
        Signature signature = container.createSignature(cert);
        signature.addContent(Constants.getContent(), false);
        signature.setSigningTime(Calendar.getInstance());
        signature.setSignaturePolicy(TurkishESigProfiles.SIG_POLICY_ID_P4v1);

        signature.sign(signer);
        signature.upgrade(SignatureType.ES_XL);
        SmartCardManager.getInstance().logout();       
        Constants.dosyaYaz(container, "turkish_profile_p4_xl_ocsp");
    }

    @Test
    public void createP4WithA() throws Exception
    {
        SignatureContainer sc = Constants.readSignatureContainer("turkish_profile_p4_xl_ocsp");
        sc.getSignatures().get(0).upgrade(SignatureType.ES_A);
        Constants.dosyaYaz(sc, "turkish_profile_p4_a");
    }

    @Test
    public void createP4WithAWithA() throws Exception
    {
        SignatureContainer sc = Constants.readSignatureContainer("turkish_profile_p4_a", Constants.createContext());
        sc.getSignatures().get(0).addArchiveTimestamp();
        Constants.dosyaYaz(sc, "turkish_profile_p4_aa");
    }

    @Test
    public void validateP1() throws Exception
    {
        ContainerValidationResult cvr = Validation.validateSignature("turkish_profile_p1_bes");
        Assert.assertEquals(ContainerValidationResultType.ALL_VALID, cvr.getResultType());
    }

    @Test
    public void validateP2() throws Exception
    {
        ContainerValidationResult cvr = Validation.validateSignature("turkish_profile_p2_t");
        Assert.assertEquals(ContainerValidationResultType.ALL_VALID, cvr.getResultType());
    }

    @Test
    public void validateP3() throws Exception
    {
        ContainerValidationResult cvr = Validation.validateSignature("turkish_profile_p3_xl_crl");
        Assert.assertEquals(ContainerValidationResultType.ALL_VALID, cvr.getResultType());
    }

    @Test
    public void validateP4() throws Exception
    {
        ContainerValidationResult cvr = Validation.validateSignature("turkish_profile_p4_xl_ocsp");
        Assert.assertEquals(ContainerValidationResultType.ALL_VALID, cvr.getResultType());
    }

    @Test
    public void validateP4A() throws Exception
    {
        ContainerValidationResult cvr = Validation.validateSignature("turkish_profile_p4_a");
        Assert.assertEquals(ContainerValidationResultType.ALL_VALID, cvr.getResultType());
    }

    @Test
    public void validateP4AA() throws Exception
    {
        ContainerValidationResult cvr = Validation.validateSignature("turkish_profile_p4_aa");
        Assert.assertEquals(ContainerValidationResultType.ALL_VALID, cvr.getResultType());
    }

}
