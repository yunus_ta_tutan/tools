package tr.gov.tubitak.uekae.esya.api.xades.example.multiple;

import org.junit.Test;

import tr.gov.tubitak.uekae.esya.api.asn.x509.ECertificate;
import tr.gov.tubitak.uekae.esya.api.smartcard.example.JSmartCardManager;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.Context;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.SignatureMethod;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.XMLSignature;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.document.Document;
import tr.gov.tubitak.uekae.esya.api.xades.example.structures.Detached;
import tr.gov.tubitak.uekae.esya.api.xades.example.utils.SampleBase;
import tr.gov.tubitak.uekae.esya.api.xades.example.validation.Validation;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.resolver.Resolver;

import java.io.FileOutputStream;
import java.security.cert.X509Certificate;

/**
 * Counter signature sample
 * @author suleyman.uslu
 */
public class CounterDetached extends SampleBase {

    public static final String SIGNATURE_FILENAME = "counter_detached.xml";

    /**
     * Adds counter signature to a detached one
     * @throws Exception
     */
    @Test
    public void signCounterDetached() throws  Exception
    {
        Context context = createContext();

        // read previously created signature, you need to run Detached.java first
        Document doc = Resolver.resolve(Detached.SIGNATURE_FILENAME, context);
        XMLSignature signature = XMLSignature.parse(doc, context);

        // create counter signature
        XMLSignature counterSignature = signature.createCounterSignature();

        counterSignature.getSignedInfo().setSignatureMethod(SignatureMethod.RSA_SHA256);

        // false-true gets non-qualified certificates while true-false gets qualified ones
        X509Certificate cert = JSmartCardManager.getInstance().getSignatureCertificate(true, false);

        // add certificate to show who signed the document
        counterSignature.addKeyInfo(new ECertificate(cert.getEncoded()));

        // now sign it by using smart card
        counterSignature.sign(JSmartCardManager.getInstance().getSigner(PIN, cert));

        // signature contains itself and counter signature
        signature.write(new FileOutputStream(BASE_DIR + SIGNATURE_FILENAME));
    }

    @Test
    public void validate() throws Exception {
        Validation.validate(SIGNATURE_FILENAME);
    }
}
