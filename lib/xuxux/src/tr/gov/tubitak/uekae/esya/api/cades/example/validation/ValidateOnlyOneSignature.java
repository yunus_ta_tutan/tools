package tr.gov.tubitak.uekae.esya.api.cades.example.validation;

import junit.framework.TestCase;
import tr.gov.tubitak.uekae.esya.api.cades.example.testconstans.TestConstants;
import tr.gov.tubitak.uekae.esya.api.cmssignature.attribute.AllEParameters;
import tr.gov.tubitak.uekae.esya.api.cmssignature.attribute.EParameters;
import tr.gov.tubitak.uekae.esya.api.cmssignature.signature.BaseSignedData;
import tr.gov.tubitak.uekae.esya.api.cmssignature.signature.Signer;
import tr.gov.tubitak.uekae.esya.api.cmssignature.validation.CertificateRevocationInfoCollector;
import tr.gov.tubitak.uekae.esya.api.cmssignature.validation.SignatureValidationResult;
import tr.gov.tubitak.uekae.esya.api.cmssignature.validation.SignatureValidator;
import tr.gov.tubitak.uekae.esya.asn.util.AsnIO;

import java.util.Hashtable;
import java.util.List;

public class ValidateOnlyOneSignature extends TestCase
{
	public void testValidateOnlyASignature() throws Exception
	{
		byte [] sign = AsnIO.dosyadanOKU(TestConstants.getDirectory() + "testdata/counterSignatures.p7s");
		
		Hashtable<String, Object> params = new Hashtable<String, Object>();
		params.put(EParameters.P_CERT_VALIDATION_POLICY, TestConstants.getPolicy());
		
		BaseSignedData bs = new BaseSignedData(sign);
		
		CertificateRevocationInfoCollector collector = new CertificateRevocationInfoCollector();
		collector._extractAll(bs.getSignedData());
		
		List<Signer> sis  = bs.getSignerList();
		
		
		for(Signer si:sis)
		{
			SignatureValidator sv = new SignatureValidator(sign);
			sv.setCertificates(collector.getAllCertificates());
			sv.setCRLs(collector.getAllCRLs());
			sv.setOCSPs(collector.getAllBasicOCSPResponses());
			SignatureValidationResult svr = new SignatureValidationResult();
			params.put(AllEParameters.P_PARENT_SIGNER_INFO, si.getSignerInfo());
			sv.verify(svr,si.getCounterSigners().get(0),true,params);
			
			System.out.println(svr);
		}
		
		
	}
}
