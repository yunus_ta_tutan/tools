package tr.gov.tubitak.uekae.esya.api.cades.example.validation;

import junit.framework.TestCase;
import tr.gov.tubitak.uekae.esya.api.asn.cms.EAttribute;
import tr.gov.tubitak.uekae.esya.api.asn.cms.EContentInfo;
import tr.gov.tubitak.uekae.esya.api.asn.cms.ESignedData;
import tr.gov.tubitak.uekae.esya.api.asn.pkixtsp.ETSTInfo;
import tr.gov.tubitak.uekae.esya.api.asn.x509.ETime;
import tr.gov.tubitak.uekae.esya.api.cades.example.testconstans.TestConstants;
import tr.gov.tubitak.uekae.esya.api.cmssignature.attribute.AttributeOIDs;
import tr.gov.tubitak.uekae.esya.api.cmssignature.signature.BaseSignedData;
import tr.gov.tubitak.uekae.esya.api.cmssignature.signature.EST;
import tr.gov.tubitak.uekae.esya.asn.util.AsnIO;

import java.util.Calendar;
import java.util.List;

/**
 * Get times from signature.
 * @author orcun.ertugrul
 *
 */
public class GetTime extends TestCase
{
	/**
	 * Gets signature time stamp. It indicates when the sign was created.
	 * @throws Exception
	 */
	public void testSignatureTS() throws Exception 
	{
		byte[] input = AsnIO.dosyadanOKU(TestConstants.getDirectory() +  "testdata/ESA-Converted-1.p7s");
		BaseSignedData bs = new BaseSignedData(input);
		EST estSign = (EST)bs.getSignerList().get(0);
		Calendar time = estSign.getTime();
		System.out.println(time.getTime().toString());
	}
	
	/**
	 * Gets signing time attribute time. It indicates the declared time when the signature is created. 
	 * @throws Exception
	 */
	public void testSigningTme()throws Exception 
	{
		byte[] input = AsnIO.dosyadanOKU("testdata/BES-2.p7s");
		BaseSignedData bs = new BaseSignedData(input);
		List<EAttribute>  attrs = bs.getSignerList().get(0).getSignedAttribute(AttributeOIDs.id_signingTime);
		ETime time = new ETime(attrs.get(0).getValue(0));
		System.out.println(time.getTime().getTime().toString());
	}
	
	/**
	 * Gets archive time stamp. It indicated then signature is converted to ESA.
	 * @throws Exception
	 */
	public void testarchiveTimestamp() throws Exception 
	{
		byte[] input = AsnIO.dosyadanOKU("testdata/ESA-Converted-1.p7s");
		BaseSignedData bs = new BaseSignedData(input);
		List<EAttribute>  attrs = bs.getSignerList().get(0).getUnsignedAttribute(AttributeOIDs.id_aa_ets_archiveTimestamp);
		List<EAttribute>  attrsV2 = bs.getSignerList().get(0).getUnsignedAttribute(AttributeOIDs.id_aa_ets_archiveTimestampV2);
		attrs.addAll(attrsV2);
		for (EAttribute attribute : attrs) 
		{
			EContentInfo contentInfo = new EContentInfo(attribute.getValue(0));
			ESignedData sd = new ESignedData(contentInfo.getContent());
			ETSTInfo tstInfo = new ETSTInfo(sd.getEncapsulatedContentInfo().getContent());
			System.out.println(tstInfo.getTime().getTime().toString());
		}
		
	}
	
	
}
