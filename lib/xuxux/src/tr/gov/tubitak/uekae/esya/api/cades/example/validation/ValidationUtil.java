package tr.gov.tubitak.uekae.esya.api.cades.example.validation;

import junit.framework.TestCase;
import tr.gov.tubitak.uekae.esya.api.cades.example.testconstans.TestConstants;
import tr.gov.tubitak.uekae.esya.api.certificate.validation.check.certificate.CertificateStatusInfo;
import tr.gov.tubitak.uekae.esya.api.certificate.validation.policy.PolicyReader;
import tr.gov.tubitak.uekae.esya.api.certificate.validation.policy.ValidationPolicy;
import tr.gov.tubitak.uekae.esya.api.cmssignature.ISignable;
import tr.gov.tubitak.uekae.esya.api.cmssignature.attribute.EParameters;
import tr.gov.tubitak.uekae.esya.api.cmssignature.validation.SignedDataValidation;
import tr.gov.tubitak.uekae.esya.api.cmssignature.validation.SignedDataValidationResult;
import tr.gov.tubitak.uekae.esya.api.cmssignature.validation.SignedData_Status;
import tr.gov.tubitak.uekae.esya.api.signature.certval.CertValidationPolicies;
import tr.gov.tubitak.uekae.esya.api.signature.certval.CertValidationPolicies.CertificateType;
import tr.gov.tubitak.uekae.esya.asn.util.AsnIO;

import java.io.FileInputStream;
import java.util.Hashtable;

public class ValidationUtil extends TestCase
{
	public void testValidation() throws Exception
	{ 
		byte [] input = AsnIO.dosyadanOKU(TestConstants.getDirectory() +  "testdata/BES-1.p7s");
		
		SignedDataValidationResult sdvr = validate(input, null);

		CertificateStatusInfo csi = sdvr.getSDValidationResults().get(0).getCertStatusInfo();

		assertEquals(SignedData_Status.ALL_VALID, sdvr.getSDStatus());
	}	
	
	public static SignedDataValidationResult validate(byte [] signature, ISignable externalContent) throws Exception
	{
		return validate(signature, externalContent, TestConstants.getPolicy());
	}
	
	
	public static SignedDataValidationResult validate(byte [] signature, ISignable externalContent, ValidationPolicy policy) throws Exception
	{
		Hashtable<String, Object> params = new Hashtable<String, Object>();
		params.put(EParameters.P_CERT_VALIDATION_POLICY, policy);
		if(externalContent != null)
			params.put(EParameters.P_EXTERNAL_CONTENT, externalContent);
		
		//Use only reference and their corresponding value to validate signature
		params.put(EParameters.P_FORCE_STRICT_REFERENCE_USE, true);
		
		//Ignore grace period which means allow usage of CRL published before signature time 
		//params.put(EParameters.P_IGNORE_GRACE, true);

		//Use multiple policies if you want to use different policies to validate different types of certificate
		//CertValidationPolicies certificateValidationPolicies = new CertValidationPolicies();
		//certificateValidationPolicies.register(CertificateType.DEFAULT.toString(), policy);
		//ValidationPolicy maliMuhurPolicy=PolicyReader.readValidationPolicy(new FileInputStream("./config/certval-policy-malimuhur.xml"));
		//certificateValidationPolicies.register(CertificateType.MaliMuhurCertificate.toString(), maliMuhurPolicy);
		//params.put(EParameters.P_CERT_VALIDATION_POLICIES, certificateValidationPolicies);
		
		SignedDataValidation sdv = new SignedDataValidation();
		SignedDataValidationResult sdvr = sdv.verify(signature, params);
		
		return sdvr;
	}
		
}
