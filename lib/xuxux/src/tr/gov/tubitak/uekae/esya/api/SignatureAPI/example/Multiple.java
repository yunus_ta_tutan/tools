package tr.gov.tubitak.uekae.esya.api.SignatureAPI.example;

import org.junit.Assert;
import org.junit.Test;
import tr.gov.tubitak.uekae.esya.api.asn.x509.ECertificate;
import tr.gov.tubitak.uekae.esya.api.common.crypto.BaseSigner;
import tr.gov.tubitak.uekae.esya.api.signature.*;
import tr.gov.tubitak.uekae.esya.api.smartcard.example.SmartCardManager;

public class Multiple {
	   @Test
	    public void createParallelToExisting() throws Exception {
	        Context c = Constants.createContext();
	        SignatureContainer sc = Constants.readSignatureContainer("bes_enveloping", c);
	        
	        boolean checkQCStatement = Constants.getCheckQCStatement();		
			//Get qualified or non-qualified certificate.
			ECertificate cert = SmartCardManager.getInstance().getSignatureCertificate(checkQCStatement, !checkQCStatement);
			BaseSigner signer = SmartCardManager.getInstance().getSigner(Constants.getPIN(), cert);        
	        
	        Signature s = sc.createSignature(cert);
	        s.addContent(Constants.getContent(), true);
	        s.sign(signer);
	        SmartCardManager.getInstance().logout();	        
	        Constants.dosyaYaz(sc, "parallel_bes");
	    }

	   @Test
	    public void createParallelToExistingDetached() throws Exception {
	        Context c = Constants.createContext();
	        c.setData(null);
	        SignatureContainer sc = Constants.readSignatureContainer("bes_detached", c);
	        
	        boolean checkQCStatement = Constants.getCheckQCStatement();		
			//Get qualified or non-qualified certificate.
			ECertificate cert = SmartCardManager.getInstance().getSignatureCertificate(checkQCStatement, !checkQCStatement);
			BaseSigner signer = SmartCardManager.getInstance().getSigner(Constants.getPIN(), cert);        
	        
	        Signature s = sc.createSignature(cert);
	        s.addContent(Constants.getContent(), false);
	        s.sign(signer);
	        SmartCardManager.getInstance().logout();	        
	        Constants.dosyaYaz(sc, "parallel_bes_detached");
	    }
	   
	    @Test
	    public void createSerialToExisting() throws Exception {
	        Context c = Constants.createContext();
	        SignatureContainer sc = Constants.readSignatureContainer("bes_enveloping", c);
	        
	        boolean checkQCStatement = Constants.getCheckQCStatement();		
			//Get qualified or non-qualified certificate.
			ECertificate cert = SmartCardManager.getInstance().getSignatureCertificate(checkQCStatement, !checkQCStatement);
			BaseSigner signer = SmartCardManager.getInstance().getSigner(Constants.getPIN(), cert); 
			
	        Signature cs = sc.getSignatures().get(0).createCounterSignature(cert);
	        cs.sign(signer);
	        SmartCardManager.getInstance().logout();	        
	        Constants.dosyaYaz(sc, "serial_bes");
	    }

	    @Test
	    public void createSerialToSerial() throws Exception {
	        Context c = Constants.createContext();
	        SignatureContainer sc = Constants.readSignatureContainer("serial_bes", c);
	        Signature s = sc.getSignatures().get(0);
	        Signature counter = s.getCounterSignatures().get(0);
	        
	        boolean checkQCStatement = Constants.getCheckQCStatement();		
			//Get qualified or non-qualified certificate.
			ECertificate cert = SmartCardManager.getInstance().getSignatureCertificate(checkQCStatement, !checkQCStatement);
			BaseSigner signer = SmartCardManager.getInstance().getSigner(Constants.getPIN(), cert); 
			
	        Signature counterOfCounter = counter.createCounterSignature(cert);
	        counterOfCounter.sign(signer);
	        SmartCardManager.getInstance().logout();	        
	        Constants.dosyaYaz(sc, "serial_to_serial_bes");

	    }

	    @Test
	    public void validateSerial() throws Exception {
	        ContainerValidationResult cvr = Validation.validateSignature("serial_bes");
	        Assert.assertEquals(ContainerValidationResultType.ALL_VALID, cvr.getResultType());
	    }

	    @Test
	    public void validateParallel() throws Exception {
	        ContainerValidationResult cvr = Validation.validateSignature("parallel_bes");
	        Assert.assertEquals(ContainerValidationResultType.ALL_VALID, cvr.getResultType());
	    }

	    @Test
	    public void validateParallelDetached() throws Exception {
	        ContainerValidationResult cvr = Validation.validateSignature("parallel_bes_detached");
	        Assert.assertEquals(ContainerValidationResultType.ALL_VALID, cvr.getResultType());
	    }
	    
	    @Test
	    public void validateSerialToSerial() throws Exception {
	        ContainerValidationResult cvr = Validation.validateSignature("serial_to_serial_bes");
	        Assert.assertEquals(ContainerValidationResultType.ALL_VALID, cvr.getResultType());
	    }

}
